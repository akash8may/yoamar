module.exports = {
	// other configurations
	module: {
		loaders: [{
			test: /\.css$/,
			loader: 'ignore-loader'
		}]
	},
	entry: './js/yo-es.js',
	output: {
		filename: 'yo-es.js',
	}
};
