const gulp = require('gulp'),
	babel = require('gulp-babel'),
	named = require('vinyl-named'),
	webpack = require('webpack-stream'),
	ignore_loader = require('ignore-loader'),
	webpack_config = {
		// watch: true,
		mode: 'development',
		// config: './webpack.config.js',
		entry: './js/admin/yo-es.js',
		output: {
			filename: 'yo-es.js',
		},
		module: {
			rules: [{
					test: /\.css$/,
					loader: 'ignore-loader'
				},
				{
					test: /\.m?js$/,
					exclude: /(node_modules|bower_components)/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env'],
							plugins: ['transform-es2015-template-literals', '@babel/plugin-transform-runtime']
						}
					}
				}]
		},
	};

// gulp.task('es6', () => {
// 	webpack_config.entry = './js/yo-es.js';
// 	return gulp.src('js/yo-es.js')
// 		// .pipe(named())
// 		.pipe(webpack(webpack_config))
// 		// .pipe(minify())
// 		// .pipe(babel({
// 		// 	presets: ['@babel/preset-env'],
// 		// 	plugins: ['transform-es2015-template-literals', '@babel/plugin-transform-runtime']
// 		// }))
// 		.pipe(gulp.dest('../assets/site/js'));
// });

gulp.task('admin_es6', () => {
	webpack_config.entry = './js/admin/yo-es.js';
	return gulp.src('js/admin/yo-es.js')
		.pipe(webpack({
			// watch: true,
			mode: 'development',
			// config: './webpack.config.js',
			entry: './js/admin/yo-es.js',
			output: {
				filename: 'yo-es.js',
			},
			module: {
				rules: [{
						test: /\.css$/,
						loader: 'ignore-loader'
					},
					{
						test: /\.m?js$/,
						exclude: /(node_modules|bower_components)/,
						use: {
							loader: 'babel-loader',
							options: {
								presets: ['@babel/preset-env'],
								plugins: ['transform-es2015-template-literals', '@babel/plugin-transform-runtime']
							}
						}
					}]
			},
		}))
		.pipe(gulp.dest('../assets/admin/js'));
});

gulp.task('merchant_es6', () => {
	webpack_config.entry = './js/merchant/yo-es.js';
	return gulp.src('js/merchant/yo-es.js')
		.pipe(webpack({
			// watch: true,
			mode: 'development',
			// config: './webpack.config.js',
			entry: './js/merchant/yo-es.js',
			output: {
				filename: 'yo-es.js',
			},
			module: {
				rules: [{
						test: /\.css$/,
						loader: 'ignore-loader'
					},
					{
						test: /\.m?js$/,
						exclude: /(node_modules|bower_components)/,
						use: {
							loader: 'babel-loader',
							options: {
								presets: ['@babel/preset-env'],
								plugins: ['transform-es2015-template-literals', '@babel/plugin-transform-runtime']
							}
						}
					}]
			},
		}))
		.pipe(gulp.dest('../assets/merchant/js'));
});

gulp.task('logistics_es6', () => {
	// webpack_config.entry = './js/logistics/yo-es.js';
	return gulp.src('js/logistics/yo-es.js')
		.pipe(webpack({
			// watch: true,
			mode: 'development',
			// config: './webpack.config.js',
			entry: './js/logistics/yo-es.js',
			output: {
				filename: 'yo-es.js',
			},
			module: {
				rules: [{
						test: /\.css$/,
						loader: 'ignore-loader'
					},
					{
						test: /\.m?js$/,
						exclude: /(node_modules|bower_components)/,
						use: {
							loader: 'babel-loader',
							options: {
								presets: ['@babel/preset-env'],
								plugins: ['transform-es2015-template-literals', '@babel/plugin-transform-runtime']
							}
						}
					}]
			},
		}))
		.pipe(gulp.dest('../assets/logistics/js'));
});

gulp.task('buyer_es6', () => {
	// webpack_config.entry = './js/buyer/yo-es.js';
	return gulp.src('js/buyer/yo-es.js')
		.pipe(webpack({
			// watch: true,
			mode: 'development',
			// config: './webpack.config.js',
			entry: './js/buyer/yo-es.js',
			output: {
				filename: 'yo-es.js',
			},
			module: {
				rules: [{
						test: /\.css$/,
						loader: 'ignore-loader'
					},
					{
						test: /\.m?js$/,
						exclude: /(node_modules|bower_components)/,
						use: {
							loader: 'babel-loader',
							options: {
								presets: ['@babel/preset-env'],
								plugins: ['transform-es2015-template-literals', '@babel/plugin-transform-runtime']
							}
						}
					}]
			},
		}))
		.pipe(gulp.dest('../assets/site/js'));
});

gulp.task('forms', () => {
	// webpack_config.entry = './js/buyer/yo-es.js';
	return gulp.src('js/modules/components/forms.js')
		.pipe(webpack({
			// watch: true,
			mode: 'development',
			// config: './webpack.config.js',
			entry: './js/modules/components/forms.js',
			output: {
				filename: 'forms.js',
			},
			module: {
				rules: [{
						test: /\.css$/,
						loader: 'ignore-loader'
					},
					{
						test: /\.m?js$/,
						exclude: /(node_modules|bower_components)/,
						use: {
							loader: 'babel-loader',
							options: {
								presets: ['@babel/preset-env'],
								plugins: ['transform-es2015-template-literals', '@babel/plugin-transform-runtime']
							}
						}
					}]
			},
		}))
		.pipe(gulp.dest('../assets/site/js'));
});

gulp.task('default', ['admin_es6', 'merchant_es6', 'logistics_es6',  'buyer_es6', 'forms'], () => {
	gulp.watch(['js/modules/*.js', 'js/modules/components/*.js, js/dom_variables.js'], ['admin_es6', 'merchant_es6', 'logistics_es6',  'buyer_es6', 'forms']);

	gulp.watch(['js/modules/components/forms.js'],																	['forms']);
	gulp.watch(['js/admin/yo-es.js',		'js/admin/modules/*.js', 		'js/admin/modules/components/*'], 		['admin_es6']);
	gulp.watch(['js/merchant/yo-es.js', 	'js/merchant/modules/*.js', 	'js/merchant/modules/components/*'], 	['merchant_es6']);
	gulp.watch(['js/logistics/yo-es.js', 	'js/logistics/modules/*.js', 	'js/logistics/modules/components/*'], 	['logistics_es6']);
	gulp.watch(['js/buyer/yo-es.js', 		'js/buyer/modules/*.js', 		'js/buyer/modules/components/*'], 		['buyer_es6']);
});
