
# Prioritize
- User flow first
- Admin/Backend

##TODO
- [BACKEND] Private & public wishlist - each list is independent of each other
- [BACKEND/AUTH] Accept terms of use when signing up

##DONE
Saved items - When a user decides to save an item for later:

	- Move saved items to cart, move to wishlist, delete from saved items
	- Display saved items below cart items
  
- Change landing page text to "Increasing Africa's exports"
- Move unit price next to quantity in the cart page
- Add option to add quantity of items to add to cart
- Show number input arrows by default in cart so that user knows it is an editable field
- Change quantity selection to dropdown (1-10, 10+). If user requires more than 10 items then allow them to enter the number of items they want.
- Change name to "YoAMar"

Warehouse needs:
	- Items are sent individually
	- Shipping tracking will be done by the courier
	- Shipping rates will be based on courier pricing (updated once a year)
- Prepare information needed from the couriers (posta)
- Send excel file containing the list of tables
- Send an excel file containing the tables + fields

### Footer Content
- FAQ
- Contact
- Privacy Policy
- Terms and conditions
- Customer service number

