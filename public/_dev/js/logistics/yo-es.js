import "./modules/warehouse.js"; 
import "./modules/shipping.js"; 
import notifications from "../modules/notifications.js";
import api_search from "../modules/components/api_search.js";

import {BASE_URL} from "../config.js";

var Ui = class Uistyles {
	constructor(base_url) {
        var notif = new notifications(base_url),
            search = new api_search(document.getElementById("jsWarehouseSearch"), base_url);
        window.notify = notif;
    }
}

var yo = new Ui(BASE_URL);
window.yo = yo;
