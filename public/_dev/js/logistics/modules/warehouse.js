import toast from './../../modules/toast.js';
import 'easy-autocomplete';

// $(document).ready(function(){
	// Get warehouse items
	function getWarehouseItems()
	{
		console.log("Get warehouse items");
	}

	// Add item to warehouse
	function addWarehouseItem()
	{
		console.log("Adding warehouse items");
	}

	// Return item from warehouse
	function returnWarehouseItem()
	{
		console.log("Return warehouse item");
	}
	// Autocomplete
	var options = {
		data: ["blue", "green", "pink", "red", "yellow"]
	};
	$("#search-input").easyAutocomplete(options);

// });

export default {getWarehouseItems,addWarehouseItem,returnWarehouseItem};
