import colorInput from "./components/color-input.js";
import toast from "./../../modules/toast";
import uid from "./id_generator.js";
import {MAX_IMAGE_SIZE} from '../../config.js';
var is_img_resizing_complete = false;

function change_child_select(el, child) {
    const target_el = document.forms['product-form'].elements[child],
        value = el.value,
        hidden_options = target_el.querySelectorAll(`:not([data-category-id="${value}"])`),
        visible_options = target_el.querySelectorAll(`[data-category-id="${value}"]`),
        empty_option = `<option class="js-emptyState" selected disabled value="-1">no ${(child.split('_').join(' '))} found</option>`;
    
    $(target_el).find('option.js-emptyState').remove();
    $(target_el).find('option.d-none').removeClass('d-none');
    $(hidden_options).addClass('d-none').removeAttr('selected');
    $(visible_options[0]).attr('selected', 'selected');
    
    if(visible_options.length == 0) {
        console.warn('visibles are 0');
        $(target_el).append(empty_option);

    } else if($(`form#product-form select[name="${child}"][onchange]`).length > 0) {
        //trigger onchange event on the child of the changed target_el select 
        $(`form#product-form select[name="${child}"][onchange]`).triggerHandler('change');

    }
}
function product_error(error, inputs, clear = false) {
    const product_tabs = document.getElementById("product-form").querySelectorAll(".nav-tabs .nav-item > .nav-link");
    
    if(clear) {
        // $("product-form").querySelectorAll(".nav-tabs .nav-item > .nav-link.is-invalid")
        $('#product-form').find('.is-invalid').removeClass('is-invalid');
        return;
    }

    var id;
    // console.log(product_tabs);
    
    Object.entries(inputs).forEach(([key, el]) => {
        $(el).addClass("is-invalid")
            .parents('.form-group').find('label').addClass("is-invalid");
        
        id = $(el).parents("[role='tabpanel']")[0].id;
        var tab_link = $(product_tabs).filter(function(i, e) {
            return $(e).attr('href') === '#'+id;
        });
        tab_link.addClass('is-invalid');
        console.log(tab_link);
    });
    toast("error - " + error, "error red", 6000);
    $('body').one('onchange', 'input.is-invalid[type=file]', function(f) {
        f.preventDefault();
        $(this).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
        console.log('invalid file input sorted');
    });
    $('body').one('focusout', 'input.is-invalid', function(f) {
        f.preventDefault();
        $(this).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
        console.log('invalid input sorted');
    });
}
async function process_form(form = document.forms['product-form'], is_update = false) {
    let data = new Object, fData = new FormData(), required_inputs = new Object, errors = false;
    for (let index = 0; index < form.length; index++) {
        if(form.elements[index].name !== '' || form.elements[index].name !== 'is_active') {
            switch (form.elements[index].type) {
                case 'file':
                    for (let _i = 0; _i < form.elements[index].files.length; _i++) {
                        // Check the file type.
                        if (!form.elements[index].files[_i].type.match('image.*')) {
                            continue;
                        }
                        //if the size is maximum the required size, resize...
                        if (form.elements[index].files[_i].size > MAX_IMAGE_SIZE * 1000) {
                            var file_result = await resize_img_file(form.elements[index].files[_i], (_f) => {
                                fData.append(form.elements[index].name+'-'+_i, form.elements[index].files[_i], form.elements[index].files[_i].name);
                                console.log('file', _f);

                            });
                            console.log('file result', file_result);
                        } else {
                            fData.append(form.elements[index].name+'-'+_i, form.elements[index].files[_i], form.elements[index].files[_i].name);
                        }
                    }
                    break;
            
                default:
                    data[form.elements[index].name] = form.elements[index].value;
                    await fData.append(form.elements[index].name, form.elements[index].value);
                    console.log(".......................data");
                    break;
            }

            //if it's a required input and is empty, append to required_object_input
            if(form.elements[index].required === true && form.elements[index].value === "") {
                if(form.elements[index].type !== 'file' && is_update !== true)
                {
                    required_inputs[form.elements[index].name] = await form.elements[index];
                }
            }
        }
    }

    is_img_resizing_complete = true;
    if(Object.keys(required_inputs).length > 0) {
        console.log(required_inputs);
        errors = true;
    }

    return {fData, errors, data};
}
function preview_product_image(input) {
    let _input = input || document.forms['product-form']['product_images'],
        file_reader = {},
        hook = document.getElementById('product-image-list'),
        template = product_image_template();
    $(hook).find('.image-list').remove();
    Object.entries(_input.files).forEach(([id, image]) => {
        template = product_image_template(id);
        $(hook).append(template);
        $('#product_image_'+ id).fadeOut(0);

        file_reader[id] = new FileReader();
        file_reader[id].readAsDataURL(image);
        file_reader[id].onloadend = function (i) {
            console.log(i);
            console.log(image);
            $('#product_image_'+ id + ' .image-title')[0].innerText = image.name;
            var _image = new Image();
            _image.title = image.name;
            _image.src = file_reader[id].result;
            $('#product_image_'+ id).prepend(_image);
            $('#product_image_'+ id + ' img').attr('class', 'rounded shadow-sm');

            setTimeout(() => {
                $('#product_image_'+ id).fadeIn(400);
            }, 1000);
        };
    });
}
/**
 * 
 * @param {dom element} input the input element
 * @param {string} parent_selector the parent hook that contain the rates elements
 * @param  {...any} args 0 = unit_price input, 1 = yoamar_rate, 2 = yoamar_rate_type
 */
function calculate(input, parent_selector, ...args) {
    const yoamar_rate = input.dataset.rate || args[1],
        yoamar_rate_type = input.dataset.rateType || args[2],
        unit_price = $('form '+args[0]).val()  || input.value,
        rates_hooks = {
            'yoamar': $(parent_selector).find('.js-yoamarPrice')[0],
            'seller': $(parent_selector).find('.js-sellerPrice')[0]
        };
    console.log(unit_price);
    if(yoamar_rate_type === '%') {
        //use percentage
        let rate = unit_price * (yoamar_rate / 100);
        rate = Math.round((rate + 0.00001) * 100) / 100;
        
        rates_hooks.yoamar.innerText = rate;
        rates_hooks.seller.innerText =  Math.round(((unit_price - rate) + 0.00001) * 100) / 100;
    }
}

function upload_new_product() {
    product_error('', '', true);
    
    const form = document.forms['product-form'],
        url = yo.base_url;
    let {data, fData, errors} = process_form();
    console.log('processed', fData);
    if(errors === true) {
        toast("Some required inputs are empty", "bg-red");
        return;
    }

    let _post =  new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: url + '/' + $(form).attr('action'),
            data: fData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                resolve(response);
            },
            error: function (response) { 
                reject(response);
            }
        });
    });
    
    _post.then((resp) => {
        console.log(resp);
        if(resp.success) {
            toast(data.product_name +' has been added','success green',3000);

            setTimeout(() => {
                //go to the products page
                // TODO: Find a way to pass this url instead of having to change manually here
                location.assign(yo.base_url+"/merchant/products");
            }, 3700);
        }

    }, (rej) => {
        toast(data.product_name +' has an issue','error red',3000);
        console.log(rej);
    });
}

function update_product() {
    console.log("updating product");
    product_error('', '', true);
    
    const form = document.forms['product-form'],
        url = yo.base_url;
    let {data, fData, errors} = process_form(document.forms['product-form'], true);
    if(errors === true) {
        toast("Some required inputs are empty");
        return;
    }

    let _post =  new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: url + '/' + $(form).attr('action'),
            data: fData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                resolve(response);
            },
            error: function (response) { 
                reject(response);
            }
        });
    });
    
    _post.then((resp) => {
        console.log(resp);
        if(resp.success) {
            toast(data.product_name +' has been updated','success green',3000);

            setTimeout(() => {
                //go to the products page
                // TODO: Find a way to pass this url instead of having to change manually here
                // location.assign(yo.base_url+"/merchant/products");
            }, 3700);
        }

    }, (rej) => {
        toast('Update error\n'+ data.product_name +' has an issue','error red',3000);
        console.log(rej);
    });
    // document.forms["product-form"].submit();
}

function delete_product(_e) {
    // find the selected product(s) id
    var selected = document.querySelectorAll('input[type=checkbox][name=product_list_item]:checked'),
        selected_products = [],
        delete_result = [];
    if(selected.length === 0 && _e === undefined) {
        toast("Select the product to delete", "bg-error error red");
        return;
    } else if(_e !== undefined) {
        var product_id = _e.id || _e.dataset.id || _e.dataset.productId;
        selected_products = [product_id];
    } else if(selected.length > 0) {
        selected.forEach(el => selected_products.push(el.id || el.dataset.id));
    }
    console.log(selected_products);
    selected_products.forEach((e, i) => {
        delete_result[i] = new Promise((resolve, reject) => {
            $.ajax({
                type: "POST",
                url: yo.base_url + "/merchant/product_delete/" + e,
                data: {id: e},
                dataType: "json",
                success: function (response) {
                    resolve(response);
                    console.log(response);
                },
                error: function (response) {
                    reject(response);
                }
            });
        });
    });
    Promise.all(delete_result).then((res, rej) => {
        console.log(res);
        res.forEach(r => {
            toast(r.message, (r.ok === true) ? "green bg-green" : "red bg-red");
            setTimeout(() => {
                location.reload();
                return;
            }, 3500);
        });
        toast("The product list will refresh after a few seconds");
    });
}

function product_image_template(id='') {
    if(id === '') {
        return `<div class="col-3 image-list mb-4 rounded-top position-relative" id="${id}">
            <img src="" height="100%" width="100%" class="rounded shadow-sm"/>
            <p class="image-title"><i>title</i></p>
        </div>`;
    } else {
        return `<div class="col-lg-3 col-sm-6 col-12 position-relative image-list mb-4 rounded-top position-relative" id="product_image_${id}">
        <p class="image-title"><i>title</i></p>
        </div>`;    
    }
}

async function resize_img_file(_file, callbackFn) {
    var file = _file;
    await new Promise((resolve, reject) => {
        const fileName = _file.name;
        const reader = new FileReader();
    
        reader.readAsDataURL(_file);
        reader.onload = event => {
            const img = new Image();
            const ration = 0.9;
            
            img.src = event.target.result;
            img.onload = () => {
                // ration = ration - 0.1;

                const elem = document.createElement('canvas');
                // img.width and img.height will give the original dimensions
                const _w = img.width * ration;
                const _h = img.height * ration;

                // console.info(_w, _h);
                elem.width = img.width;
                elem.height = img.height;
                const ctx = elem.getContext('2d');
                ctx.drawImage(img, 0, 0, _w, _h);

                
                new Promise(resolve => {
                    ctx.canvas.toBlob((blob) => {
                        resolve(blob)
                    }, _file.type, 1);

                }).then(blob => {
                    file = new File([blob], fileName, {
                        type: _file.type,
                        lastModified: Date.now()
                    });

                    console.log(file.size > (MAX_IMAGE_SIZE * 1000), ':-', _file.size, '>', file.size);
                    if (file.size > (MAX_IMAGE_SIZE * 1000)) {
                        resize_img_file(file, callbackFn);
                    } else {

                        resolve(file);
                        console.log('after resize');
                    }
                });
            },
            reader.onerror = error => rej(error);
        };
    }).then(res => {
        callbackFn(res)
    });
}

$("body").on("change", "select[data-child-select]", (_e) => {
    change_child_select(_e.currentTarget, _e.currentTarget.dataset.childSelect);
});
$("body").on("change", "input[type=file][name=product_images]", (_e) => {
    preview_product_image(_e.currentTarget);
});
$("body").on("change", "input[type=number][name=sale_price]", (_e) => {
    calculate(_e.currentTarget, '.js-saleCalculator');
});
$("body").on("change", "input[type=number][name=unit_price]", (_e) => {
    calculate(_e.currentTarget, '.js-priceCalculator')
});
$("body").on("click", ".js-deleteProduct", (_e) => {
    if(confirm("Are you sure you'd like to delete the product?\n All data and images will be deleted")) {
        delete_product();
    }
});
$("body").on("click", ".js-addProduct", (_e) => {
    upload_new_product();
});
$("body").on("click", ".js-editProduct", (_e) => {
    // document.forms["product-form"].submit();
    update_product();
});
$("body").on("keyup", "[type='number'].js-roundOf", function(_e) {
    // document.forms["product-form"].submit();
    var number_value = this.value;
    var array_value = number_value.toString().split('.');
    var new_value;
    if(/\d/.test(number_value) && array_value.length > 1) {
        console.info('rounding off', number_value);
        //get the decimal value
        var decimal = Number(array_value[1].split('')[0]);

        if(decimal < 5) {
            new_value = Number(array_value[0]);
        } else if(decimal > 5) {
            new_value = Number(array_value[0]) + 1;
        } else {
            new_value = Number(number_value);
        }

        setTimeout(() => {
            this.value = new_value;
        }, 220);
    }

});

if(document.forms['product-form'] !== undefined) {
    new colorInput({
        form: 'product-form',
        input: 'color'
    });
}

export default {preview_product_image, product_error, change_child_select, preview_product_image, calculate};