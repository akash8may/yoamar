import toast from './../../modules/toast.js';

$('.document').ready(function(){
	
	// #region HELPERS
	function _get_review_data(btn)
	{
		return {
			'review_id':btn.dataset.reviewId,
			'reply_text':document.forms['formReviewReply'].elements['reply_text'].value,
		}
	}
	function _get_review_reply_data(btn)
	{
		return {
			'review_id':btn.dataset.reviewId,
			'review_reply_id':btn.dataset.id,
			'reply_text':document.forms['formReviewReplyUpdate'].elements['reply_text'].value,
		}
	}

	// Reload page
	function _reload_page()
	{
		setTimeout(function() {
			location.reload();
		}, 1500);
	}
	// #endregion HELPERS
	
	// #region HANDLERS
	// Create review reply handler
	$('.js-createReviewReply').click(function(e) {
        e.preventDefault();
		var data = _get_review_data(this);
		// If no text reply was provided
		if(/\w\S/.test(data.reply_text) == false)
		{
			toast('You have not written a reply', "bg-warning");
			return;
		}
		
		create_review_reply(data.review_id,data.reply_text);
	});
	
	// Update review reply handler
	$('#btnUpdateReviewReply').on('click', function(e) {
		e.preventDefault();
		var data = _get_review_reply_data(this);
		update_review_reply(data.review_reply_id,data.reply_text);
	});

	// Delete review reply handler
	$('.js-deleteReviewReply').on('click', function(e) {
		e.preventDefault();
        var review_reply_id = this.dataset.id;
        if(confirm("Confirm Delete reply.\nAre you sure you'd want to delete your reply?"))
        {
            delete_review_reply(review_reply_id);
        }
	});
	
	$('.modal#modalReviewReply').on('show.bs.modal', function (e) {
		console.log(e.relatedTarget.dataset.reviewId);
		var review_id = e.relatedTarget.dataset.reviewId;
		if($(this).find('form .btn.js-createReviewReply').attr('data-review-id') !== review_id) {
			$(this).find('form .btn.js-createReviewReply').attr('data-review-id', review_id);
		}
	});
	
	$('.modal#modalReviewReplyUpdate').on('show.bs.modal', function (e) {
        var review_id = e.relatedTarget.dataset.id,
            review_text = $(e.relatedTarget).parents('.review-item').find('.review')[0].innerText;
        
		if($(this).find('form .btn.js-updateReviewReply').attr('data-id') !== review_id) {
            $(this).find('form .btn.js-updateReviewReply').attr('data-id', review_id);
            document.forms['formReviewReplyUpdate'].elements['reply_text'].value = review_text;
		}
	});
	
	// #endregion HANDLERS

	// Create review reply
	function create_review_reply(review_id,reply_text)
	{
        var url = yo.base_url + "/api/reply_to_review",
            data = {
                'review_id':review_id,
                'reply_text':reply_text,
            };

		$.ajax({
			type: "POST",
			url: url,
			data: data,
			dataType: "json",
			success: function (response) {
				toast(response.message, (response.ok) ? 'error red': 'success green');
				if(response.ok == true) {
					_reload_page();
				}
			},
			error: function (response) { 
				toast(response.message, (response.ok) ? 'error red': 'success green');
			}
		});
	}

	// Update review reply
	function update_review_reply(review_reply_id,reply_text)
	{
		//update_review reply - reply_id, reply_text
        var url = yo.base_url + "/api/update_review_reply",
            data = {
                'review_id':review_reply_id,
                'reply_text':reply_text,
            };

		$.ajax({
			type: "POST",
			url: url,
			data: data,
			dataType: "json",
			success: function (response) {
                toast(response.message, (response.ok) ? 'error red': 'success green');
                if(response.ok == true) {
					_reload_page();
				}
			},
			error: function (response) { 
				toast(response.message, (response.ok) ? 'error red': 'success green');
			}
		});
	}

	// Delete review reply
	function delete_review_reply(review_reply_id)
	{
        var url = yo.base_url + "/api/delete_review_reply",
            data = {
                'reply_id':review_reply_id
            };

		$.ajax({
			type: "POST",
			url: url,
			data: data,
			dataType: "json",
			success: function (response) {
				toast(response.message, (response.ok) ? 'error red': 'success green');
				if(response.ok == true) {
					_reload_page();
				}
			},
			error: function (response) { 
				toast(response.message, (response.ok) ? 'error red': 'success green');
			}
		});
	}
});

export {create_review_reply, update_review_reply, delete_review_reply};