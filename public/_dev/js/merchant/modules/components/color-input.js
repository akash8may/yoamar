// import colorpicker from "bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js";
import uid from "./../id_generator.js";
import toast from "./../../../modules/toast.js";
class colorInput {
	constructor(input, activePicker) {
		//bind plugin definition to jQuery
		// $.fn.colorpicker = colorpicker;
	
		this._picker = {};
		this.activePicker = activePicker || null;
        this.formInput = document.forms[`${input.form}`][`${input.input}`];

		$("body").on('click', '.colorpicker .colorpicker-bar a', (_e) => {
			this.remove_color_picker(_e.currentTarget.dataset.id);
            console.log('remove_color_picker');
        });

        $("body").on('click', '.js-removeProductColor', (_e) => {
            this.remove_color(_e.currentTarget.dataset.id);
            console.log('remove_color');
        });

        $("body").on('click', '.js-addProductColor', (_e) => {
            this.add_new_color(_e.currentTarget);
            console.log('add_new_color');
        });

        $("body").on('change', '.custom-control.color input[id^=picker-]', (_e) => {
			var input = _e.currentTarget;
            this.update_picker_color(input.value, input.id.split("picker-").pop());
            console.log('update_picker_color');
        });
    }
    
	add_new_color(clicked) {
		const new_id = uid(),
			color_template = this.color_component_template(new_id);

		$(clicked).before(color_template);
		this._picker[new_id] = new $.colorpicker('#picker-' + new_id, {
			color: '#55C6C6',
			format: 'auto',
			// popover : {
			//     placement : "top"
			// },
			on: ('colorpickerHide', (e) => {
			    console.log('hiding\n', e);
			}),
			template: this.color_picker_template(new_id),
		});

		//debounce it kiasi
		setTimeout(() => {
			this._picker[new_id].show();
		}, 450);
	}

	color_picker_template(new_id) {
		return `<div class="colorpicker">
        <div class="colorpicker-saturation"><i class="colorpicker-guide"></i></div>
        <div class="colorpicker-hue"><i class="colorpicker-guide"></i></div>
        <div class="colorpicker-bar">
            <a class="btn btn-primary d-block" href="#!" data-id="${new_id}">Add Color</a>
        </div></div>`;
    }
    color_component_template(new_id) {
        return `<div class="float-left custom-control color d-inline-block disabled" data-color id='${new_id}'>
        <input id="picker-${new_id}" type="text"
            class="position-absolute border-0" style="width:36px;height:36px; top: 0; left: 0; opacity: 0;" 
            autofocus="true"/>
        <a data-id="${new_id}" class="js-removeProductColor"><i class="fa fa-close text-primary87"></i>
        </div>`;
    }

	update_picker_color(value, color_id) {
		$('#' + color_id).css('background-color', value);
		$('#' + color_id).attr('data-color', value);
	}
	/**
	 * updates a hidden input[name=color] 
	 * takes the values of all the colors added
	 */
	update_color() {
		let input = this.formInput,
			input_val = '',
			colors = document.querySelectorAll('.yo-product-variable .color > input');
		// console.log(colors);

		Object.entries(colors).forEach(([id, element]) => {
			input_val += element.value + ",";
		});
		input.value = input_val;
	}
	remove_color_picker(id) {
		id = id || id.dataset.id;
		// console.log(id);
		document.getElementById('picker-' + id).setAttribute('disabled', true);
		setTimeout(() => {
			this._picker[id].destroy();
			$('#' + id).removeClass('disabled');
			$('#' + id).removeAttr('disabled');
		}, 500);
		setTimeout(() => {
			toast("Success\nColor property created", "bg-success green bg-success", 2600);
		}, 550);

		this.update_color();
	}
	remove_color(id) {
		$('#' + id).remove();
		this.update_color();
	}

}

export default colorInput;