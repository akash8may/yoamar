import {API_URL} from '../../../config.js';
import {addProductQuantity_class, minusProductQuantity_class} from '../../../dom_variables.js';
import toast from '../../../modules/toast.js';

const component_root_class = '.yo-quantity-changer';

class quantity_changer {
    constructor() {
        const this_ = this;

        $(component_root_class).on('click', addProductQuantity_class, function () {
            const component_root = $(this).parents('.yo-quantity-changer')[0];
            const input_ = component_root.querySelector('input[type="number"]');
            
            this_.addProductQuantity = input_;
        });

        $(component_root_class).on('click', minusProductQuantity_class, function () {
            const component_root = $(this).parents('.yo-quantity-changer')[0];
            const input_ = component_root.querySelector('input[type="number"]');
            
            this_.minusProductQuantity = input_;
        });

        $(component_root_class).on('change', 'input[type="number"]', function () {
            const component_root = $(this).parents('.yo-quantity-changer')[0];
            let quantity = component_root.querySelector('input[type="number"]').value;
            let product_id = component_root.dataset.id;
            console.log(product_id);
            if(product_id !== null) {
                setTimeout(() => {
                    this_.changeProductQuantity(product_id, quantity);
                }, 200);
            }
        });
    }

    /**
     * @param {DOMelement} component_root 
     */
    set addProductQuantity(input_) {
        input_.value = Number(input_.value) + 1;
        console.log(input_.value, $(input_).val());
        $(input_).trigger('change');
    }

    /**
     * @param {DOMelement} component_root 
     */
    set minusProductQuantity(input_) {
        if(Number(input_.value != 0)) {
            input_.value = Number(input_.value) - 1;
            $(input_).trigger('change');
        }
    }

    changeProductQuantity(product_id, quantity) {
        $.ajax({
        	type: "POST",
        	url: API_URL + "change_product_quantity",
            dataType: "json",
            data: {
                "product_id" : product_id,
                "quantity" : Math.abs(quantity)
            },
        	success: function (response) {
                toast(response.message, response.ok == true ? "bg-success" : "bg-danger", 700);
        	},
        	error: function (response) {
                toast(response.message, "bg-danger");
        	}
        });
    }
}

export default quantity_changer;