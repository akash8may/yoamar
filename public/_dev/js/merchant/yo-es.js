import * as product_manage from "./modules/product-manage.js";
import * as reviews from "./modules/reviews.js";
import quantity_changer from "./modules/components/quantity-changer-input.js";
import notifications from "./../modules/notifications.js";
import buyer from "../modules/buyer.js";
import {BASE_URL, API_URL} from "../config.js";
import {productEditor_id} from '../dom_variables.js';

class adminUi extends buyer {
    constructor(base_url) {
        super(base_url)
        window.notify = new notifications(API_URL);

        this._editor = {};
        this.url = base_url;
        this.init_plugins = window.location.href.split("merchant/").pop();

        this.listeners();
    }

    //#region quill editor
    activate_quill(editor_id, placeholder_text) {
        $(document).ready(() => {
            this._editor = new Quill(editor_id, {
                theme: 'snow',
                placeholder: placeholder_text || 'Product description',
                modules: {
                    toolbar: [
                        ['bold', 'italic', 'underline', 'strike'],
                        [{ 'list': 'ordered'}, { 'list': 'bullet' }]
                    ]
                }
            });
            this.update_product_description(this._editor.root.innerHTML);
            console.log('quilled');
        });
    }
    update_product_description(text = '') {
        let html_content = text,
            textarea = document.forms['product-form']['description'];
        textarea.value = html_content;

        this._editor.on('text-change', () => {
            html_content = this._editor.root.innerHTML;
            textarea.value = html_content;
            //if it's value is only an enter, clean it
            if(document.querySelector('#description-editor .ql-editor').innerText === "\n") textarea.value = "";
        });
    }
    //#endregion

    toggle_select_all_items(_this, ...args) {
        let parent = args[0] || $(_this).parents('table'),
            inputs = parent.find(`.checkbox-select-item[type=checkbox]`);
            
        console.log(_this.checked);
        if(_this.checked) {
            //check all
            inputs.each((index, input) => { input.checked = true });
        } else {
            //uncheck all
            inputs.each((index, input) => { input.checked = false });
        }

    }
    
    listeners() {
        $('body').on('click', '[class*="js-"][href="#!"]', (f) => {
			f.preventDefault();
			console.log('js-link');
        });
        let _this = this;
        $('body').on('change', '.checbox-select-all', function (f) {
            f.preventDefault();
            console.info('selecting all');
            _this.toggle_select_all_items(this);
        });
    }

    set init_plugins(page) {
        page = page.split('#')[0];
        page = page.split('?')[0];
        var _page = page.split('/'),
            method = _page[1];
        page = _page[0];
        console.info("console.info");
        if(page === 'product') {
            switch (method) {
                case 'add':
                case 'edit':
                    this.activate_quill(productEditor_id);
                    break;
            
                default:
                    break;
            }

        } else if(page === 'products') {
            new quantity_changer();
        }
    }
}

var yo = new adminUi(BASE_URL);
window.yo_ = yo;

export default adminUi;