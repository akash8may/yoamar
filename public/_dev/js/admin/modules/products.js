import toast from "./../../modules/toast";
import {API_URL} from "./../../config.js";

function toggleVerifyProduct($button, productId) {
	const isVerified = Boolean($button.attr("data-is_verified"));
	let newState = !isVerified;
	let url = API_URL + "verify_product/" + productId;
	let data = {
		"verify": newState
	};

	// AJAX request
	$.post(url, data, function (response) {
		var toastClass;

		if (response.ok) {
			toastClass = "bg-success green";
            
            setTimeout(() => {
                location.reload(true);
            }, 3000);
		} else {
			toastClass = "bg-error red";
			
		}

		// Toast message
		toast(response.message, toastClass, 2500);
	});
}

// Toggle enable account button clicked
$(".btnVerifyProduct").on("click", function () {
	let $currentButton = $(this);
	let productId = $currentButton.attr("data-product_id");

	toggleVerifyProduct($currentButton, productId);
});