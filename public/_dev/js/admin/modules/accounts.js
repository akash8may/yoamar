import toast from "./../../modules/toast";
import {API_URL} from "./../../config.js";

// Toggles the active state in the various sections of the DOM
function toggleDomActiveState($button,isActive)
{
	// Update the button's active state attribute
	$button.attr("data-is_active",isActive);

	let $accountData = $button.parents(".accountData");

	let $accountStatus = $accountData.children(".accountStatus");

	// Button classes
	let enableButtonClass = "btn-outline-success";
	let disableButtonClass = "btn-outline-warning";

	// Account status related attributes to be updated based on the active state
	let newButtonClass,newButtonText,newStatusText;
	
	// Set the various attributes based on whether or not the  button is active
	if(isActive){
		// Show disable button
		$button.removeClass(enableButtonClass);
		$button.addClass(disableButtonClass);

		newButtonText = "Disable";
		newStatusText = "Enabled";
	}else{ 
		// Show enable button
		$button.removeClass(disableButtonClass);
		$button.addClass(enableButtonClass);

		newButtonText = "Enable";
		newStatusText = "Disabled";
	}

	// Update the button text as well as account status text
	$button.html(newButtonText);
	$accountStatus.html(newStatusText);
}

function toggleEnableAccount($button,user_id)
{
	let isActive = $button.attr("data-is_active");
	isActive = (isActive === "true");
	let newState = !isActive;

	let url = API_URL+"update_account_active/"+user_id;
	let data = {
		"is_active": newState
	};
	
	// AJAX request
	$.post(url,data,function(response){
		let message;
		let toastClass;
		let action = newState ? "enable" : "disable";

		if(response.ok)
		{	
			toastClass = "bg-success green";
			message = `Successfully ${action}d account`;

			// Toggle button state
			toggleDomActiveState($button,newState);
		}
		else
		{
			toastClass = "error red";
			message = `Failed to ${action} account`;
		}

		// Toast message
		toast(message, toastClass, 2500);
	});
}

// Toggle enable account button clicked
$(".btnToggleEnableAccount").on("click",function(){
	let $currentButton = $(this);
	let userId = $currentButton.attr("data-user_id");

	toggleEnableAccount($currentButton,userId);
});

// Delete account
function deleteAccount($button,user_id,$modalConfirmDelete){

	let url = API_URL+"delete_account/"+user_id;

	let $tableRow = $button.parents(".accountData");

	$.post(url,function(response){
		let message;
		let toastClass;

		// Deleted account successfully
		if(response.ok)
		{
			toastClass = "bg-success green";
			message = `Successfully deleted account`;
				
			// Hide the modal and reset the user id value
			$modalConfirmDelete.modal('hide');

			// Remove the table row from the DOM
			$tableRow.fadeOut(250,function(){
				$(this).remove();
			});
		}
		else
		{
			toastClass = "error red";
			message = `Failed to delete account`;
		}

		toast(message, toastClass, 2500);
	});
}

// Declaring this to allow us to use this in modal
let $clickedButton;

// Delete account
$(".btnDeleteAccount").on("click",function(){
	// Set the clicked button to the current button. To be used when confirming the delete
	$clickedButton = $(this);	
	let $modalConfirmDelete = $("#confirmDelete");
	
	$modalConfirmDelete.modal('show');
});

// Confirm delete clicked
$(".btnConfirmDelete").on("click",function(){
	let userId = $clickedButton.attr("data-user_id");

	let $modalConfirmDelete = $("#confirmDelete");
	deleteAccount($clickedButton,userId,$modalConfirmDelete);
});
