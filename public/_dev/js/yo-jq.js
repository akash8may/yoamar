/*global $, jQuery, alert, console*/
var Ui = class Uistyles {
	construct() {
		console.log('ui init');
		
		$('body').on('click', '[class*="js-"][href="#!"]', function(f) {
			f.preventDefault();
			console.log('js-link');
		});
        // this.imageShowIn();
	}

    //  TOGGLE FUNCTIONS---------------------------------------------------------------------
	toggleProductCard(product_id, link_el) {
		Uistyles.resetToggleMoreLess($('.product-card .pr-info.info-open').parents('.product-card'));
		$('.product-card .pr-info.info-open').find('.pr-about').addClass('text-truncate');
		$('.product-card .pr-info.info-open').removeClass('info-open');
		
		try {
			if ($(link_el).hasClass('js-infoOpen')) {
				$('#'+product_id + ' .pr-info').addClass('info-open').find('.pr-about').removeClass('text-truncate').promise().done(function() {
					$('#'+product_id).parent('.pr-about').removeClass('text-truncate');
					$('#'+product_id).find('.js-infoClose').removeClass('d-none');
					$(link_el).addClass('d-none');
				});
			} else if ($(link_el).hasClass('js-infoClose')) {
				$('#'+product_id + ' .pr-info').removeClass('info-open').find('.pr-about').addClass('text-truncate').promise().done(function() {
					$('#'+product_id).find('.pr-about').addClass('text-truncate');
					$('#'+product_id).find('.js-infoOpen').removeClass('d-none');
					$(link_el).addClass('d-none');
				});
			}
		} catch (error) {
			console.log(error);
		}
	}

	toggleReadMore(this_el, toggle_scope) {

		Uistyles.resetToggleMoreLess($(this_el).parent());
		var is_truncated = Uistyles.isTruncated($(this_el).parent()),
			target_el, target_el_style;
		
		if(toggle_scope === 'sibling') {
			target_el = $(this_el).siblings('[data-toggle-read-more]');
		} else {
			target_el = $(this_el).parent().find('[data-toggle-read-more]');
		}
		
		if($(this_el).hasClass('js-infoOpen')) {
			target_el_style = target_el.attr('data-toggle-read-more');
			(target_el_style === '' ? target_el_style = "100vh" : target_el_style = target_el_style);
			
			target_el.removeClass('text-truncate').css({
				'max-height' : target_el_style,
				'overflow-y' : 'auto'
			});
			$(this_el).addClass('d-none').parent().find('.js-infoClose').removeClass('d-none');

		} else if($(this_el).hasClass('js-infoClose')) {
			target_el.removeAttr('style');
			if(is_truncated) {
				target_el.addClass('text-truncate');
			}

		}

	}
	
	toggleSearchbar(this_el, action) {
		let form_el = document.forms['navSearchbar'];
		var fixed_form = $(form_el).clone(true, true),
			hook = $('header .js-fixedNavbar');

		if(action === 'open') {
			fixed_form.addClass('shadow').removeClass('d-none');
			hook.css('display', 'none').removeClass('d-none').prepend(fixed_form).fadeIn();
			
		} else {
			hook.fadeOut().promise().done(function() {
				hook.find('form').remove();
				hook.removeAttr('style').addClass('d-none');
			});
		}
	}

	static isTruncated(_el) {
		if(_el.hasClass('text-truncate')) {
			return true;
		} else { return false;}
	};
	// reset the states of the read more/less buttons
	static resetToggleMoreLess(parent_el) {
		parent_el.find('.js-infoClose').addClass('d-none');
		parent_el.find('.js-infoOpen').removeClass('d-none');
	}
	// --------------------------------------------------------------------------------------

	// CART FUNCTIONS------------------------------------------------------------------------
	addToCart(product_id, data) {

	}
	
	removeFromCart(product_id, el) {
		console.log(el);
		console.log(product_id);
	}

	updateItemNumber(this_el, ...args) {
		var form_el, input_val,
			content_template = "<div class='form-group' style='min-width:200px;'><label for='formLargeQuantityOrder'>Quantity</label><input type='number' required max='200' class='form-control' id='formLargeQuantityOrder' value'10' placeholder='11, 12... 20'></div><a href='#!' onclick='yo.ui.updateItemNumber(this, \"popper\")' class='btn btn-sm btn-primary text-center d-block'>okay</a>";
		
		if(args[0] === 'popper') {
			form_el = $(this_el).parents('.popover');
			input_val = form_el.find('#formLargeQuantityOrder').val();

			form_el = form_el.parents('form');

			if(input_val === '') {
				input_val = 1;
				form_el.find("select > option[value='"+ input_val +"']").prop('selected', true);
				form_el.find("select optgroup:not(.d-none)").addClass('d-none');
			} else if(Number(input_val) < 10) {
				form_el.find("select > option[value='"+ input_val +"']").prop('selected', true);
				form_el.find("select optgroup:not(.d-none)").addClass('d-none');
			} else {
				console.log(form_el.find("select option.user-defined"));
				form_el.find("select optgroup").removeClass('d-none');
				form_el.find("select option.user-defined").html(input_val).attr('value', input_val).prop('selected', true);
			}

			$(this_el).parents('.popover').popover('dispose');
		} else {
			form_el = $(this_el).parents('form');
			input_val = $(this_el).val();

			form_el.find('.popover').popover('dispose');
			
			if(Number($(this_el).val()) === 10) {
				$(this_el).popover({
					container : form_el,
					content : content_template,
					html : true,
					placement : 'left',
					title : "Please write the quantity you'd like to purchase"
				});
				$(this_el).popover('show');
			} else {
				form_el.find("select optgroup").addClass('d-none');
			}
		}
		
		form_el.find("[name='item-number']").val(input_val);

		console.log(form_el.find("[name='item-number']").val());
	}
	
	openCart() {

    }
    closeCart() {

	}
	// -------------------------------------------------------------------------------------

	// SEARCH FUNCTIONS---------------------------------------------------------------------
	updateSearchbar(...args) {
		let value = args[0];
		var $this = this;

		//the second argument is the input name to update
		switch(args[1]) {
			case 'cat':
				$this.searchCategory = Number(value);

				break;
			case 'q':
			default:
			// update the search input
			$(document.forms['navSearchbar']).find("[name='q']").val(value);

			break;
		}
	}
	
	set searchCategory(category_id) {
		category_id = category_id || '';

		var form_el = document.forms['navSearchbar'],
			category_input = $(form_el).find("[name='cat']");
			category_input.val(category_id);

			console.log(category_input.val());
	}

	// ------------------------------------------------------------------------------------
	
    changeimgList(to, list_type) {

    }

	yoZoom(img_el) {
		
	}
	
	updateProductCard(el, data) {

    }
    showInBundleCardImages() {

    }
    shuffleBundleCardImageGrid() {

    }
    

};

var Async = class {
    //--------------
    //  Async related functions
    //--------------

    construct() {
        console.log('Main async_s created');
    }
    
    searchThis(search_string) {

    }

    filterList(args, list_type) {

	}
	
	getReviews(product_id, el) {

	}

	addToCart(product_id, btn_el) {

	}
};