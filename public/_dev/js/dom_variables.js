export const checkoutForm = "checkout-form";
export const productEditor = 'description-editor';

export const productEditor_id = '#'+productEditor;

export const productCard_class = '.product-card';
export const cartItem_class = '.cart-item';
export const cartIcon_class = '.yo-cart-icon';

export const addToCart_class = '.js-addToCart';
export const addToWishlist_class = '.js-addToWishlist';
export const addToSavedItem_class = '.js-addTosavedItem';

export const removeFromCart_class = '.js-removeFromCart';
export const removeFromWishlist_class = '.js-removeFromWishlist';
export const removeSavedItem_class = '.js-removeSavedItem';

export const moveFromCartToWishlist_class = '.js-moveTo.-f-c-t-w';
export const moveFromCartToSavedItem_class = '.js-moveTo.-f-c-t-si';

export const moveFromWishlistToCart_class = '.js-moveTo.-f-w-t-c';
export const moveFromWishlistToSavedItem_class = '.js-moveTo.-f-w-t-si';

export const moveFromSavedItemToCart_class = '.js-moveTo.-f-si-t-c';
export const moveFromSavedItemToWishlist_class = '.js-moveTo.-f-si-t-w';

export const makeWishlistPrivate_class = '.js-makeWishlistPrivate';

export const toggleSearchbar_class = '.js-toggleSearchbar';

export const addProductQuantity_class = '.js-addProductQuantity';
export const minusProductQuantity_class = '.js-minusProductQuantity';

export const addProductColor_class = '.js-addProductColor';
export const removeProductColor_class = '.js-removeProductColor';

export const checkboxSelectAll_class = '.checkbox-select-all';
export const checkboxSelectItem_class = '.checkbox-select-item';

export const quantityChangerComponent_class = '.yo-quantity-changer';
export const productVariableComponent_class = '.yo-product-variable';