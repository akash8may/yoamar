import user_profile from "./user_profile.js";
import yo_async from "./yo_async.js";
import { API_URL } from "../config.js";

class buyer extends yo_async {
    constructor(base_url) {
		console.log('base_route buyer');
		super(API_URL);
		
		this.is_logged_in = false;
		var location_path = window.location.pathname.split('/');
		/**
		 * Gets user's data if logged in
		 * Useful for future if|when async buyer functions are added that need the user's data
		 */
		var caller = async () => {
			this.is_logged_in = await this.userIsLoggedIn;
			
			// if logged in, get currentUser and add events
			if(this.is_logged_in.ok == true && this.is_logged_in.data[0] == true) {
				this.is_logged_in = this.is_logged_in.data[0];
				this.user = await this.currentUser;

				if(this.user.ok == true) {
					this.user = this.user.data;
				}
			}
		}
		// caller();
		
		if (location_path.find(x => x == 'profile') && location_path.find(x => x == 'edit')) {
			new user_profile();
		}

		buyer.listeners(location_path);
	}
	
    static listeners(location_path) {
		//this should work but for some reason it does not
		
		$('body').on('show.bs.modal', '#modalAddPaymentMethod', function (event) {
			let button = $(event.relatedTarget), // Button that triggered the modal
				recipient = button.data('method'); // Extract info from data-* attributes

			$(`#modalAddPaymentMethod a[href='#${recipient}']`).tab('show');
			$(`#modalAddPaymentMethod input[name=payment_method]`).val(recipient);

		});
		$('body').on('hide.bs.modal', '#modalAddPaymentMethod', (event) => {
			this.clearFormInputs('#modalAddPaymentMethod');
		});

		$('body').on('show.bs.modal', '#modalEditPaymentMethod', (event) => {
			let button = $(event.relatedTarget), // Button that triggered the modal
				info = super.getPaymentInfo(event.relatedTarget.parentNode.id),
				recipient = button.data('method'); // Extract info from data-* attributes

			$(`#modalEditPaymentMethod a[href='#${recipient}']`).tab('show');
			$(`#modalEditPaymentMethod a[href='#${recipient}']`).find('input')[0].checked = true;
			$(`#modalEditPaymentMethod input[name=payment_method]`).val(recipient.split('2')[0]);
			try {
				info.then((res) => {
					// console.log(res);
					toast(res.message, (res.ok ? "bg-success green" : "error red"));

					if (!res.ok) {
						setTimeout(() => {
							location.reload();
							return;
						}, 600);
						throw "error in results", res;
					}

					var data = Object.entries(res.data).find(([i, ob]) => ob.length > 0);

					switch (data[0]) {
						case 'mpesa':
							$(`#modalEditPaymentMethod input[name=mpesa_phone]`).val(data[1][0].number);
							break;
						case 'paypal':
							$(`#modalEditPaymentMethod input[name=paypal_id]`).val(data[1][0].email);
							break;
						case 'card':
							$(`#modalEditPaymentMethod input[name=card_number]`).val(data[1][0].number);
							$(`#modalEditPaymentMethod input[name=card_expiry]`).val(data[1][0].expiry_date);
							$(`#modalEditPaymentMethod input[name=card_security_code]`).val(data[1][0].cvc || data[1][0].card_security_code);
							break;
					}
					$(`#modalEditPaymentMethod form`).attr('action', $(`#modalEditPaymentMethod form`).attr('data-action') + '/' + data[1][0].id);
					// console.log(data);

				}, (rej) => {
					// console.log(rej);
					toast(rej.message, (rej.ok ? "bg-success green" : "error red"));

					throw "error in get", res;
				});
			} catch (error) {

			}

		});
		$('body').on('hide.bs.modal', '#modalEditPaymentMethod', (event) => {
			this.clearFormInputs('#modalEditPaymentMethod');
			$(`#modalEditPaymentMethod form`).attr('action', $(`#modalEditPaymentMethod form`).attr('data-action'));
		});
		
		$('body').on('click', '.btn.js-deletePayment', function (f) {
			f.preventDefault();
			console.log(this);
			//prompt form submitting if it's a submit button
			if (this.type === 'submit') {
				var c = confirm('You are about to delete this payment method');
				if (c) {
					this.form.elements['delete_payment_info'].checked = true;
					this.form.submit();
				} else {
					this.form.elements['delete_payment_info'].checked = false;
				}
			}
		});
	}

	get currentUser() {
		return super.currentUser;
	}

	get userIsLoggedIn() {
		return super.userIsLoggedIn;
	}

}

export default buyer;