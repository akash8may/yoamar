import toast from './toast.js';
import Push from "push.js";
import interval from 'interval-promise';
import yo_async from './yo_async.js';

class notification extends yo_async {
	constructor(api_route) {
        super(api_route);
        this.running = false;
        this.api_route = api_route;
        this.notification_badge = $('.yo-notification-icon');
        this.notification_count = 0;
        this.notifications = [];
        this.new_notifications_log = [];
        this.last_notified;

        var _init = async () => {
            var user = await super.userIsLoggedIn;
            if(user.data[0] == true) {
                this.running = true;
                this.show_notification();
            }
        };
        _init();
        
        $('.js-markNotificationRead').click((e) => { 
            e.preventDefault();
            var notification_id = e.currentTarget.dataset.id || $(e.currentTarget).parents('.card-body').attr('id');
            if(/\w\\/.test(e.currentTarget.href) == false) {
                this.markAsRead(notification_id);
            } else {
                // Some notification are marked seen when the action needed is performed
            }
        });
	}

	get newNotifications() {
        try {
            return new Promise((resolve, reject) => {
                $.ajax({
                    type: "GET",
                    url: this.api_route + "get_new_user_notifications",
                    dataType: "json",
                    timeout: 10000,
                    success: function (response) {
                        if (response.ok == true) {
                            resolve(response);
                        } else {
                            // resolve(response);
                            console.log(response);
                        }
                        // console.log(response);
                    },
                    error: function (reject) {
                        console.log('reject error');
                        resolve(reject);
                    }
                });

            });
        } catch (error) {
            console.error(error);
        }
    }
    
    /**
     * @param {boolean} to
     */
    stop_notification() {
        this.running = false;
    }

    show_notification() {
        notification.run(this);
    }

    static run(_this) {
    	interval(async (iteration, stop) => {
    		if (_this.running == false) {
                stop();
                return;
    		} else {
                _this.running = true;
            }
    		// console.info(iteration);
    		var notifications = await _this.newNotifications;
    		await _this.processNewNotifications(notifications);
    	}, 12000, {
    		stopOnError: false
    	});
    }

    markAsRead(notification_id, reload = true) {
        $.ajax({
            type: "POST",
            url: this.api_route + "api/mark_notification_read",
            data: {id: notification_id},
            dataType: "json",
            success: function (response) {
                // console.log(response);
                if(response.ok == true) {
                    toast(response.message, response.ok == true ? "bg-success green": "bg-error red");
                    if(reload)
                    {
                        setTimeout(() => {
                            location.reload();
                        }, 780);
                    }
                }
            },
            error: function (response) {
                console.error("error in marking notification as read");
                toast(response.message, "bg-error red");
            }
        });
    }

    // /**
    //  * 
    //  * @param {Sting} from SOCKET||MYSQL
    //  */
    // new_notifications(from = 'local') {
    //     let notifs;
    //     if(from === 'mysql' || from === 'local') {
    //         this.local_db_notifications();
    //     } else {
    //         this.socket_notifications();
    //     }
    // }
    
	/**
	 * 
	 * @param {String} type toast||icon-badge||icon-animate
	 * @param {String} message notification text message
	 */
	notify(by = 'toast', {title, description, link, type}) {
        // console.log(`showing notification\n Title: ${title}`);
        this.last_notified = new Date().getTime();
        // console.info(this.last_notified);

        switch (by) {
            case 'icon':
            case 'icon-badge':
            case 'icon-animate':
                // should only animate the icon badge
                this.notification_badge.addClass('new').promise().done(() => {
                    console.log("icon should animate");
                    this.notification_badge.find('.notification-button').removeClass('d-none');
                    setTimeout(() => {
                        this.notification_badge.removeClass('new');
                    }, 15000);
                });
            case 'toast':
                toast(`${title}\n<br>${description}`, "blue bg-info", 10000, link);
                break;
            case 'push':
                Push.create(title, {
                    body: description,
                    link: link,
                    // icon: '/icon.png',
                    timeout: 4000,
                    onClick: function () {
                        window.focus();
                        this.close();
                    }
                });
                break;
        }

        if(type === "warning" || type === "bg-success") {
            Push.create(title, {
                body: description,
                link: link,
                // icon: '/icon.png',
                timeout: 4000,
                onClick: function () {
                    window.focus();
                    this.close();
                }
            });
        }

    }

    processNewNotifications(notifications) {
        console.log('notifications process');
        try {
            if(notifications.ok === true && typeof notifications.data === "object") {
                if(notifications.data.length > this.notification_count) {
                    var new_notifications = [],
                        notified,
                        is_notification_page = /notifications/.test(location.pathname) && /merchant/.test(location.pathname);
                    //if it's a merchant notifications page, reload
                    if(is_notification_page == true) {
                        // location.reload();
                        // return;
                    }
                    // this.notification_count  = notifications.data.length;
                    notifications.data.forEach((i, o) => {
                        // console.log(i);
                        // TODO: if the notification id is found in this.notifications, don't show the notification
                        notified = this.new_notifications_log.filter(x => x === i.id);
                        console.log(notified);
                        var _now = new Date().getTime();
                        if(notified.length > 0) {
                            if (_now > (this.last_notified + 3000)) {
                                this.notify('icon-badge', i);
                                // this.notify('toast', i);
                                // this.notify('push', i);
                            }
                        } else {
                            this.new_notifications_log.push(i.id);
                            this.notify('icon-badge', i);
                            // this.notify('toast', i);
                            // this.notify('push', i);
                        }
                    });
                    // console.log(new_notifications);
                }
            }
        } catch(er) {
            console.error(er);
        }
    }
}

export default notification;
