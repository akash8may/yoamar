import toast from "./toast.js";
import {get_productId, disableBtn, enableBtn} from './product_helpers.js';
import {
	yo_modal,
	modal_wishlist_buttons,
	suggest_button
} from "./yo_templates.js";
import yo_async from '../modules/yo_async.js';
import {
	API_URL
} from '../config.js';
import {
	cartItem_class,
	cartIcon_class,
	productCard_class,

	addToCart_class,
	addToWishlist_class,
	addToSavedItem_class,

	removeFromCart_class,
	removeFromWishlist_class,
	removeSavedItem_class,

	moveFromCartToWishlist_class,
	moveFromCartToSavedItem_class,
	moveFromWishlistToCart_class,
	moveFromWishlistToSavedItem_class,
	moveFromSavedItemToCart_class,
	moveFromSavedItemToWishlist_class,
	makeWishlistPrivate_class
} from '../dom_variables.js';
import {
	stringToBoolean
} from "./components/stringToBoolean.js";

class yo_cart extends yo_async {
	constructor(is_cart_page = false) {
		super(API_URL);
		//should update the nav cart icon
		this.setCartAmount = false;
		this.cartLog = [];
		this.cart_icon = document.querySelectorAll(cartIcon_class);

		if (is_cart_page == true) {
			this.cartCalculator();
		}

		this.listeners(is_cart_page);
	}

	listeners(is_cart_page) {
		const saveLaterAdd = async (evt) => {
			var btn = evt.currentTarget;
			var product_id = btn.dataset.id || $(btn).parents(cartItem_class).attr('data-id');
			console.log('listenerHandler');
			disableBtn(btn);
			await this.saveForLater(product_id, btn);
			enableBtn(btn);
		};
		const saveLaterRemove = async (evt) => {
			var btn = evt.currentTarget;
			var product_id = btn.dataset.id || $(btn).parents(cartItem_class).attr('data-id');
			disableBtn(btn);
			await this.removeSavedItem(product_id, btn);
			enableBtn(btn);
		};
		const moveTo = async (evt) => {
			var btn = evt.currentTarget,
				from = btn.dataset.from || null,
				to = btn.dataset.to || null,
				product_id = btn.dataset.id || $(btn).parents(cartItem_class).attr('data-id') || null;

			disableBtn(btn);
			await this.moveItem(product_id, btn, from, to, is_cart_page);
			enableBtn(btn);
		};
		const cartRemove = async (evt) => {
			var btn = evt.currentTarget;
			var product_id = btn.dataset.id || $(btn).parents(cartItem_class).attr('data-id');

			disableBtn(btn);
			await this.removeFromCart(product_id, evt.currentTarget);
			enableBtn(btn);
		};
		const updateCartProductNumber = (evt) => {
			this.updateItemNumber(evt.currentTarget);
		};
		const cartAdd = async (evt) => {
			evt.preventDefault();
			var btn = evt.currentTarget;
			if ($(btn).hasClass('disabled')) return;

			disableBtn(btn);
			await this.addToCart(null, evt.currentTarget);
			enableBtn(btn);
		};
		const wishlistAdd = async (evt) => {
			evt.preventDefault();
			var btn = evt.currentTarget;
			if ($(btn).hasClass('disabled')) return;

			disableBtn(btn);
			await this.addToWishlist(null, evt.currentTarget, null, stringToBoolean(evt.currentTarget.dataset.reload));
			enableBtn(btn);
		};
		const wishlistRemove = async (evt) => {
			evt.preventDefault();
			// if ($(evt.currentTarget).hasClass('disabled')) return;
			var btn = evt.currentTarget;
			disableBtn(btn);
			await this.removeFromWishlist(null, btn, true, stringToBoolean(btn.dataset.reload));
			enableBtn(btn);
		};
		const makeWishlistPrivate = async (evt) => {
			evt.preventDefault();
			if ($(evt.currentTarget).hasClass('disabled')) return;
			var btn = evt.currentTarget,
				from = btn.dataset.from || null,
				to = btn.dataset.to || null,
				product_id = btn.dataset.id || $(btn).parents(cartItem_class).attr('data-id') || null;

			disableBtn(btn);
			await this.moveItem(product_id, btn, from, to, is_cart_page);
			enableBtn(btn);
		};
		const moveTo_class = `${moveFromCartToWishlist_class}, ${moveFromCartToSavedItem_class}, 
			${moveFromWishlistToCart_class}, ${moveFromWishlistToSavedItem_class}, 
			${moveFromSavedItemToCart_class}, ${moveFromSavedItemToWishlist_class}`;


		$('body').on('click', addToCart_class, cartAdd);
		$('body').on('click', addToWishlist_class, wishlistAdd);
		$('body').on('click', addToSavedItem_class, saveLaterAdd);

		$('body').on('click', removeFromCart_class, cartRemove);
		$('body').on('click', removeSavedItem_class, saveLaterRemove);
		$('body').on('click', removeFromWishlist_class, wishlistRemove);

		$('body').on('click', moveTo_class, moveTo);

		$('body').on('click', makeWishlistPrivate_class, makeWishlistPrivate);

		$('body').on('click', ".js-updateItemNumber:not(select)", updateCartProductNumber);
		$('body').on('change', "select.js-updateItemNumber", updateCartProductNumber);
	}

	/**
	 * Change this to moveItem()
	 * @param {String} product_id id
	 * @param {Button} el clicked button element
	 * @param {String} to Cart|Wishlist|SavedItem
	 * @param {String} from Cart|Wishlist|SavedItem
	 * @param {Boolean} is_cart_page Used to run the cart calculator if the user is in the cart page
	 */
	async moveItem(product_id = null, el, from = null, to, is_cart_page = false) {
		if (to == null || from == null || product_id == null) return;
                if(to === 'wishlist' && from === 'wishlist') {
			await this.removeFromWishlist(product_id, el, false, false);
			await this.addToWishlist(product_id, el, null, true);

			return;
		}

		
		if (to === 'wishlist') {
			await this.addToWishlist(product_id, el, null, true);
		} else if (to === 'cart') {
			await this.addToCart(product_id, el, null, true);
		} else if (to === 'savedItem') {
			await this.saveForLater(product_id, el, true);
		}

		if (from === 'savedItem') {
			await this.removeSavedItem(product_id, el, false, false);
		} else if (from === 'wishlist') {
			await this.removeFromWishlist(product_id, el, false, false);
		} else if (from === 'cart') {
			await this.removeFromCart(product_id, el, false, false);
		}

		//this is not needed since by default the page reloads
		if (is_cart_page == true)
			this.cartCalculator();
	}

	//#region saved item functions
	/**
	 * @param {String} product_id 
	 * @param {Button} el clicked button element
	 */
	saveForLater(product_id, el, reload = false) {
		return new Promise((resolve, reject) => {
			console.log('save for later');
			this.removeFromCart(product_id, el, false).then((res) => {
				console.log('save for later done');
				console.log(res);
				if (!res.ok) return;
				
				productAddTo(product_id, {
					product_id: product_id
				}, 'save_item_for_later').then((res) => {
					var show_toast_time = 1000;
					var reload_wait_time = show_toast_time + 200;
					resolve(res);
	
					toast(res.message, (res.ok ? "bg-success green" : "error red"), show_toast_time);

					if (reload == true) {
						setTimeout(() => {
							location.reload();
						}, reload_wait_time);
					}
				}, (rej) => {
					reject(rej);
					toast(rej.message, "error red");
				});
	
			}, (rej) => {
				reject(rej);
				toast('sorry. Couldn\'t add to saved items', "error red");
			});

		}, (rej) => {
			toast('sorry. Couldn\'t add to saved items', "error red");
		});

	}

	/**
	 * Removes a saved item
	 * @param {String} product_id 
	 * @param {Button} el clicked button element
	 * @param {Boolean} show_toast whether to show toast message
	 * @param {Boolean} reload reload page onComplete. Default is false
	 */
	removeSavedItem(product_id, el, show_toast = true, reload = false) {
		return new Promise((resolve, reject) => {
			productRemove(product_id, 'saved').then((res) => {
				if (res.ok) {
					var show_toast_time = 1000;
					var reload_wait_time = show_toast_time + 200;
					resolve(res);

					if (show_toast) {
						toast(res.message, "bg-success green");
					}
					if (reload == true) {
						setTimeout(() => {
							location.reload();
						}, reload_wait_time);
					}
				}
			}, (rej) => {
				reject(rej);
				toast(rej.message, "error red");
			});

		});
	}
	//#endregion saved item functions

	//#region cart functions
	/**
	 * 
	 * @param {String} product_id 
	 * @param {button} el clicked button that has triggered the event
	 * @param {int} quantity of the product
	 * @param {boolean} reload reload page onComplete. Default is false
	 * @param  {...any} args 
	 */
	async addToCart(product_id, el, quantity, reload = false, ...args) {

		if (product_id == null) {
			product_id = get_productId(product_id, el);
		}
		//get popper if quantity is undefined
		if (quantity === undefined || quantity === null) {
			var popper_input_id, product_quantity, max_order_quantity, content_template;
			var product_details = await super.getSingleProduct(product_id);
			
			if (productPassesTest(product_details.data) == false) {
				var btn = suggest_button('Add to wishlist', "#!", addToWishlist_class.split('.')[1], `data-id='${product_id}'`);
				toast(
					`Sorry this item cannot be bought yet \n ${btn}`,
					"bg-info",
					8000,
					undefined,
					// function () {
					// 	$(el).parents(`[data-id="${product_id}"]`).find(addToWishlist_class).trigger("click");
					// }
				);

				return;
			}
			var alreadySelectedQty = product_details.data.alreadySelectedQty;
			max_order_quantity = product_details.data.quantity;
			//product_quantity = $(el).attr('data-quantity-added') || 1;
			product_quantity = (product_details.data.alreadySelectedQty)?product_details.data.alreadySelectedQty:1;
			
			popper_input_id = await 'productQuantity_' + product_id;
			content_template = await yo_cart.cartItemQuantityPopper(popper_input_id, '', product_quantity, max_order_quantity);

			const $popOver = $('.popover');
			const $productCard = $popOver.parents(productCard_class);
			$productCard.removeAttr('style');
			$popOver.parent().children(addToCart_class).removeClass('disabled');
			$popOver.popover('dispose');

			$(el).popover({
				container: $(el).parent(),
				content: content_template,
				html: true,
				placement: 'top',
				title: "Please write the quantity you'd like to purchase <a href='#!' class='popover-close float-right text-primary-dark87'><i class='fa fa-close'></i></a>",
			});
			$(el).parents(`${productCard_class}`).css({
				'overflow': 'visible',
				'z-index': 555
			});
			$(el).addClass('disabled').popover('show');
			$(el).on('shown.bs.popover', () => {
				let trigger = $(el).parent().find('.popover .popover-body a'),
					input = $('#' + popper_input_id),
					input_val;
				// input[0].value = 1;
				trigger.click(() => {
					input_val = Number(input.val());
					if (input_val > 0) {
						console.log(product_id);
						this.addToCart(product_id, el, input_val, reload);
						$(el).popover('dispose');
						$(el).removeClass('disabled').parents(`${productCard_class}`).removeAttr('style');
					} else return;
				});
			});

			return;
		}
		// console.log(quantity, product_id, reload);

		let data = {
				'product_id': product_id,
				'quantity': quantity
			},
			serve = productAddTo(product_id, data, 'add_to_cart'),
			success_text = "Added " + quantity + " item" + (quantity === 1 ? '' : 's');

		serve.then((res) => {
			//success toast
			toast(res.message, (res.ok ? "bg-success green" : "error red"));
			$(el).html(success_text).addClass('js-added').attr('data-quantity-added', quantity);
			
			//remove item from saved item
			if ($(el).hasClass('js-removeSavedItem')) {
				this.removeSavedItem(product_id, el);
				return;
			}
			if (reload) location.reload(true);

		}, (rej) => {
			//error toast
			toast("An error occurred while trying to add item to cart", "error red");
			console.log(rej);
		});
	}

	/**
	 * remove a cart-item from cartList
	 * @param {String} product_id 
	 * @param {Button} el clicked button element
	 * @param {Boolean} show_toast whether to show a toast message
	 * @param {Boolean} reload reload page onComplete. Default is false
	 */
	removeFromCart(product_id, el, show_toast = false, reload = false) {
		return new Promise((resolve, reject) => {
			if (product_id == null) {
				product_id = get_productId(product_id, el);
			}
			var $item = $(`${cartItem_class}[data-id="${product_id}"]`),
				pad = document.createElement("SPAN"),
				pad_id = `spacer${Math.ceil(Math.random()*5)}`;
	
			pad.style.height = $item.height() + 'px';
			pad.setAttribute('id', pad_id);
			pad_id = '#' + pad_id;
	
			$item.addClass('slide-out active');
	
			var serve = productRemove(product_id, 'cart');
			serve.then((res) => {
				resolve(res);
				$item.after(pad.outerHTML).remove();
	
				$(pad_id).animate({
					height: 0
				}, 800, () => {
					$(pad_id).remove();
				});
				
				this.cartCalculator();
				var show_toast_time = 1000;
				var reload_wait_time = show_toast_time + 200;
	
				if (show_toast == true) {
					toast(res.message, (res.ok ? "bg-success green" : "error red"), (res.ok ? show_toast_time : null));
					if ($('.js-cartList .cart-item').length === 0)
						location.reload();
					
				}
				if (reload == true) {
					setTimeout(() => {
						location.reload();
					}, reload_wait_time);
				}
			}, (rej) => {
				reject(rej);
				toast(rej.message, "error red");
	
			});

			if (show_toast == true) {
				toast(res.message, (res.ok ? "bg-success green" : "error red"), (res.ok ? 700 : null));
				if ($('.js-cartList .cart-item').length === 0)
					location.reload();
			}
		}, (rej) => {
			toast(rej.message, "error red");

		});

		return serve;
	}
	//#endregion cart functions

	//#region wishlist functions
	/**
	 * 
	 * @param {String} product_id 
	 * @param {Button} el clicked button element
	 * @param {Boolean} is_private whether to add to private or public wishlist
	 * @param {Boolean} reload whether to reload page onComplete
	 * @param  {...any} args 
	 */
	addToWishlist(product_id, el, is_private = null, reload = false, ...args) {
		return new Promise((resolve, reject) => {
			var is_logged_in = super.is_logged_in || false;
			console.info('------------', is_logged_in);

			if (product_id == null) {
				product_id = get_productId(product_id, el);
			}
			is_private = el.dataset.isPrivate || is_private;
			reload = stringToBoolean(el.dataset.reload) || reload;
			//open modal
			if (is_private === null) {
				if ($('#modalAddTo_' + product_id).length > 0) {
					$('#modalAddTo_' + product_id).modal('show');

				} else {
					let body_template = modal_wishlist_buttons(el, product_id, reload),
						modal_id = 'modalAddTo_' + product_id,
						modal_title = 'Add to public or private wishlist?',
						modal_template = yo_modal(modal_id, modal_title, body_template, '', false, reload);
		
					$('body').append(modal_template);
					$('#' + modal_id).modal('show');
					
				}

				is_logged_in = super.userIsLoggedIn;
				resolve(true);
				return;
			}
	
			let data = {
				'product_id': product_id,
				'is_private': stringToBoolean(is_private)
			};
			
			//if not logged in and the user wants to save wishlist as a private item, cancel/toast needs to be logged in
			if(data.is_private == true && is_logged_in == false) {
				toast(`You need to create an account in order to save to make your wishlist private <a href="${BASE_URL}auth/login" class="ml-1 btn btn-primary">Log in</a>`, "bg-info", 7000);
				return;
			}

			productAddTo(product_id, data, 'add_to_wishlist').then((res) => {
				//success toast
				resolve(res);
				toast(res.message, (res.ok ? "bg-success green" : "error red"));
				$('.yo-product').find(addToWishlist_class).addClass('disabled');
				if (res.ok && $('.yo-product').find(`${addToWishlist_class} .fa-heart-o`).length > 0) {
					$('.yo-product').find(`${addToWishlist_class} .fa-heart-o`)
						.addClass('fa-heart')
						.removeClass('fa-heart-o');
				}
				//remove item from saved item
				if ($(el).hasClass('js-removeSavedItem') && res.ok) {
					yo_cart.yo_cart.prototype.removeSavedItem(product_id, el, reload);
					return;
				}
	
				if (reload == true) {
					setTimeout(() => {
						location.reload(true);
					}, 500);
				}
				console.log(res);
			}, (rej) => {
				//error toast
				reject(rej);
				toast(rej.message, (rej.ok ? "bg-success green" : "error red"));
				console.error(rej);
			});
		});
	}

	/**
	 * 
	 * @param {String} product_id 
	 * @param {Button} el clicked button element
	 * @param {Boolean} show_toast show toast onComplete
	 * @param {Boolean} reload reload page onComplete
	 */
	removeFromWishlist(product_id, el, show_toast = true, reload = false) {
		return new Promise((resolve, reject) => {
			if (product_id == null) {
				product_id = get_productId(product_id, el);
			}
	
			const serve = productRemove(product_id, 'wishlist');
			var show_toast_time = 2000;
			var reload_wait_time = show_toast_time + 240;
	
			serve.then(res => {
				if (res.ok == true) {
					resolve(res);
					if (show_toast == true) {
						toast(res.message, 'bg-success', show_toast_time);
					}
					if (reload == true) {
						setTimeout(() => {
							location.reload();
						}, reload_wait_time);
					}
				} else {
					resolve('Failed to remove product from wishlist');
					toast('Failed to remove product from wishlist', 'bg-warning', 1500);
				}
			}, onrejected => {
				reject(onrejected);
				toast('Failed to remove product from wishlist', 'bg-warning', 1500);
			});

		});
	}
	//#endregion wishlist functions

	//#region cart calculator functions
	/** update a cart-item's quantity */
	async updateItemNumber(this_el, ...args) {
		console.log(this);
		var form_el, input_val, content_template;
		var item_id = get_productId(null, this_el);
		var item_value = Number($(this_el).val()) || 1;
		var product = await super.getSingleProduct(item_id);

		if (productPassesTest(product.data) == false) {
			var btn = suggest_button('Add to wishlist', "#!", addToWishlist_class.split('.')[1], `data-id='${product_id}'`);
			toast(
				`Sorry this item cannot be bought <br> \n ${btn}`,
				"bg-info",
				8000,
				undefined,
				// function () {
				// 	$(el).parents(`[data-id="${product_id}"]`).find(addToWishlist_class).trigger("click");
				// }
			);

			return;
		}
		content_template = await yo_cart.cartItemQuantityPopper(null, null, item_value, product.data.quantity);
		//if event is fired from the popper cartItemAmount
		if ($(this_el).hasClass('yo-popperAction') || args[0] === 'popper') {
			form_el = $(this_el).parents('.popover');
			input_val = form_el.find('#formLargeQuantityOrder').val();
			// reset form element from the popover to the actual form element
			form_el = form_el.parents('form');

			if (input_val === '') {
				input_val = 1;
				form_el.find("select > option[value='" + input_val + "']").prop('selected', true);
				form_el.find("select optgroup:not(.d-none)").addClass('d-none');
			} else if (Number(input_val) < 10) {
				form_el.find("select > option[value='" + input_val + "']").prop('selected', true);
				form_el.find("select optgroup:not(.d-none)").addClass('d-none');
			} else {
				console.log(form_el.find("select option.user-defined"));
				form_el.find("select optgroup").removeClass('d-none');
				form_el.find("select option.user-defined").html(input_val).attr('value', input_val).prop('selected', true);
			}

			form_el.find("select").removeAttr('disabled');
			$(this_el).parents('.cart-item').removeClass('active');
			$(this_el).parents('.popover').popover('dispose');
		} else {
			form_el = $(this_el).parents('form');
			input_val = $(this_el).val();

			form_el.find('.popover').popover('dispose');

			//if the selected <option> is 10, show popper for the user to write a custom number, default value being 10
			if (item_value === 10) {
				$(this_el).popover({
					container: form_el,
					content: content_template,
					html: true,
					placement: 'left',
					title: "Please write the quantity you'd like to purchase"
				});
				$(this_el).popover('show');
				$(this_el).parents('.cart-item').addClass('active');
				//now only enabled when the popper okay button is used
				form_el.find("select")[0].setAttribute('disabled', true);
			} else {
				form_el.find("select optgroup").addClass('d-none');
			}
		}

		form_el.find("[name='item-number']").val(input_val);

		// async function to update the database
		// on promise.done()...
		const serve = await this.updateCartItem(item_id, {
			quantity: input_val
		});

		if (serve.ok == true) {
			this.updateCartItemAmount(item_id, input_val);
		} else {
			toast(serve.message, "bg-danger red");
		}

		console.log(form_el.find("[name='item-number']").val());
	}

	/** Handles calculating the price of the cart item
	 * calls cartCalculator() method
	 */
	updateCartItemAmount(item_id, new_item_number) {
		var item_hook = document.querySelector(`[data-id="${item_id}"]`),
			item_base_amount = item_hook.dataset.baseAmount;

		new_item_number = Number(item_base_amount) * Number(new_item_number);
		parseFloat(new_item_number);
		if (isNaN(new_item_number)) {
			// error handler?
		}

		this.cartCalculator();
	}

	/**
	 * 
	 * @param {String} product_id 
	 * @param {Object} quantity to update
	 */
	updateCartItem(product_id, { quantity }) {
		let url = window.yo.base_url + "/api/update_cart_item",
			data = {
				product_id: product_id,
				quantity: quantity
			};

		return new Promise((resolve, reject) => {
			$.ajax({
				type: "POST",
				url: url,
				data: data,
				dataType: "json",
				success: function (response) {
					resolve(response);
				},
				error: function (response) {
					reject(response);
				}
			});
		});
	}

	/** Handles calculating the price of the whole cart list
	 * calls two methods
	 * 1. for updating the amount,
	 * 2. and for updating the item counts
	 */
	 static chartNull()
	 {
 		$('.js-cartBasePrice, .js-cartDeliveryPrice, .js-cartTotalPrice').text(0);
		this.setCartAmount=false;
		yo_cart.cartTotalAmount(0);
		yo_cart.cartTotalItems(0);
		$(".btn-checkout").addClass('disabled');
		$('.total_count_bracket,.cart-itom-count').hide();
		if($('.js-cartList .cart-item').length ==0 ){
			$('.emptycart').show();
			$('.notemptycart').hide();
			//$('.cartcheckout ').hide();	
		}
			
	 }
	static cartCalculator($this = '') {
		const reducer = (accumulator, currentValue) => accumulator + currentValue.amount;
		const base_reducer = (accumulator, currentValue) => accumulator + currentValue.base_amount;
		const number_reducer = (accumulator, currentValue) => accumulator + currentValue.number;
		var cart_items_data = [],
			total_base_amount = 0,
			total_amount = 0,
			total_items = 0;
		console.log('starting cart calculator', $('.js-cartList .cart-item').length);
		if($('.js-cartList .cart-item').length ==0 ){
			yo_cart.chartNull();
		}
				
		if ($this != '')
			cart_items_data = $this;
		else if ($('.js-cartList .cart-item[data-id]').length === 0) {

			console.log($('.js-cartList .cart-item'));
			yo_cart.cartTotalItems(0);

			return 0;
		} else
			$('.js-cartList .cart-item').map((key, el) => {
				var base_amount = Number(el.dataset.baseAmount),
					number = Number($(el).find("input[name='item-number']").val()) || 1,
					amount = base_amount * number;

				cart_items_data.push({
					id: el.getAttribute('id'),
					baseAmount: base_amount,
					number: number,
					amount: amount < base_amount ? base_amount : amount
				});

				total_base_amount = total_base_amount + base_amount;
			});

		total_base_amount = cart_items_data.reduce(base_reducer, 0);
		total_amount = cart_items_data.reduce(reducer, 0);
		total_items = $('.js-cartList .cart-item').length;

	 	total_items=0;
		$('.js-updateItemNumber').each(function(){
			total_items += parseInt($(this).children("option:selected").val());
		})

		total_items=0;
        	total_amount=0;
		$('.item-amount-details').each(function(){
		
			var number=  parseInt($(this).find("option:selected").val());
			total_items += number;
			total_amount += Number($(this).find(".product-amount").text()) * number; 
		});





		if(total_items ==0 || $('.js-cartList .cart-item').length ==0 ){
			yo_cart.chartNull();
		}else{
			this.setCartAmount = true;
			var baseamount = Math.round((total_amount + 0.00001) * 100) / 100;
			yo_cart.cartBaseAmount(baseamount);
			yo_cart.cartTotalAmount(baseamount);		
			yo_cart.cartTotalItems(total_items);
			this.setCartAmount = false;
		}
		// update the cart list
		// async on offers and such
	}
	/** should set the total amount of the cart
	 * usually will be called in the cartCalculator method
	 * will set this.setCartAmount.permission back to false
	 * then log in the old_total_val
	 */
	// TO-DO: Make this a setter. For some reason setters can't be called inside methods
	static cartBaseAmount(new_val){
		$('.js-cartBasePrice').text(new_val);	
	}


	static cartTotalAmount(new_val) {
		if (!this.setCartAmount) return;
		var hook = $('.js-cartTotalPrice span')[0],
			old_val = parseFloat(hook.innerText);

		if (hook.textContent !== undefined)
			hook.textContent = new_val;
		else
			hook.innerText = new_val;
		this.setCartAmount = true;

	}
	static cartTotalItems(new_val) {
		var hook = $('.js-cartTotalItems'),
			old_val = parseFloat(hook.text());
		if(new_val==0)
			$(hook).hide();
		else
			$(hook).show();
		hook.text(new_val);

	}
	//#endregion cart calculator

	static get_productId(product_id, el) {
		if (product_id === null && el !== null) {
			return el.dataset.id || $(el).parents(`${productCard_class}`).attr('data-id') || $(el).parents('.cart-item').attr('data-id') || $(el).parents('.yo-product').attr('id').split('pr_')[1];
		} else if (product_id === null && el === null) {
			toast("sorry, there's an error in adding to cart", "error red");
			return;
		} else {
			return product_id;
		}

	}

	/**
	 * 
	 * @param {String} id DOMElement.id
	 * @param {String} action a js class to bind to a cart function ('.js-*')
	 * @param {String} value default value
	 */
	/**
	 * By default the popper template is used for inputing bulk item orders (>10)
	 */
	static cartItemQuantityPopper(id = 'formLargeQuantityOrder', action = "js-updateItemNumber", value = '10', max_value = '50') {
		var placeholder = 'Enter value',
			template = `<div class='form-group' style='min-width:200px; z-index:555;'>
       	<label for='${id}'>Quantity</label>
       	<input type='number' 
			required max='${max_value}' min='1'
			class='form-control' 
			id='${id}' 
			value='${value}' 
			placeholder='${placeholder}'></div>
		<a href='#!' 
			class='btn btn-sm btn-primary text-center d-block ${action} yo-popperAction'>okay</a>`;

		return template;
	}
}

/**
 * Async function that adds a product to the cart (in the server)
 * @param {String} product_id
 * @param {Object} data /Array/Obj
 * @param {String} url Api/{url}
 * 
 */
function productAddTo(product_id, data, url) {
	console.log(product_id, 'adding to ' + API_URL + url);
	url = API_URL + url;
	var $qty=data.quantity;
	return new Promise((resolve, reject) => {
		$.ajax({
			type: "POST",
			url: url,
			data: data,
			dataType: "json",
			success: function (response) {
				
				updateCartvalue();
				resolve(response);

			},
			error: function (response) {
				reject(response);
			}
		});
	});
}

function updateCartvalue(){

	var url = API_URL + "get_cart";
	$.ajax({
		type: "POST",
		url: url,
		dataType: "json",
		success: function (response) {
			 $('.js-cartTotalItems').text(response);	
			  $('.js-cartTotalItems').show();		
		},
		error: function (response) {
		}
	});
}


/**
 * 
 * @param {String} product_id 
 * @param {String} from cart||wishlist||saved
 */
function productRemove(product_id, from) {
	var url = API_URL + "remove_" + from + "_item",
		data = {
			product_id: product_id
		};

	return new Promise((resolve, reject) => {
		$.ajax({
			type: "POST",
			url: url,
			data: data,
			dataType: "json",
			success: function (response) {
				resolve(response);
			},
			error: function (response) {
				reject(response);
			}
		});
	});
}



function productPassesTest(data) {
	return (Number(data.is_verified) == 1 || Boolean(data.is_verified) == true ||
		Number(data.is_available) == 1 || Boolean(data.is_available) == true ||
		Number(data.quantity) == 1);
}

export {
	yo_cart,
	productAddTo,
	productRemove
};
