import toast from "./toast";

class paypal_payment {

    constructor(form_) {
        this.form = document.forms[form_];
        this.payerID;

        paypal.Button.render({
            // Set your environment
            env: 'sandbox', // sandbox | production
    
            // Specify the style of the button
            style: {
                layout: 'vertical',  // horizontal | vertical
                size:   'medium',    // medium | large | responsive
                shape:  'rect',      // pill | rect
                color:  'gold'       // gold | blue | silver | black
            },
    
            // Specify allowed and disallowed funding sources
            //
            // Options:
            // - paypal.FUNDING.CARD
            // - paypal.FUNDING.CREDIT
            // - paypal.FUNDING.ELV
            funding: {
                allowed: [ paypal.FUNDING.CARD, paypal.FUNDING.CREDIT ],
                disallowed: [ ]
            },
    
            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
    
            // client: {
            // 	sandbox:    'AcwPXdBqwl4TP72jUANy27tZ-8Y2Jgt4dCN8dQvSYHed54lF-x-UaYIDDSSaPvuVTf94to_xPXdogsJU',
            // },
    
            // Set up the payment:
            // 1. Add a payment callback
            payment: (data, actions) => {
                // 2. Make a request to your server
                return actions.request.post(yo.base_url+'/api/authorize_paypal_payment/')
                .then((res) => {
                    console.log(res);
                    // 3. Return res.id from the response
                    return res.data.id;
                });
            },
            // Execute the payment:
            // 1. Add an onAuthorize callback
            onAuthorize: (data, actions) => {
                // 2. Make a request to your server
                this.payerID = data.payerID;
                
                return actions.request.post(yo.base_url+'/api/complete_paypal_payment/', {
                    paymentID: data.paymentID,
                    payerID:   data.payerID
                })
                .then((res) => {
                    // 3. Show the buyer a confirmation message.
                    if(res.ok == true) {
                        toast('You have purchased products', "bg-success green", 500,);
                        
                        var _input = document.createElement("input");
                        _input.name = "paypal_id";
                        _input.type = "hidden";
                        _input.value = this.payerID;
                        this.form.append(_input);
                        
                        setTimeout(() => {
                            this.form.submit();
                        }, 550);
                        
                    } else {
                        toast('Error in processing your payPal payment', "error red", 5000);
                    }
                });
            }
        }, '#paypal-button-container');
        
    }

}

export default paypal_payment;