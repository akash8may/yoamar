import yo_productvariables from './components/yo_productvariables';

class product_detail {
	constructor(product_id) {
		this.product_id = product_id || null;
        console.log(this.product_id);
        this.listeners();
        this.zoompleInit();
	}


	listeners() {
		let change_product_image = (evt) => {
			this.changeImgList(evt.currentTarget);
		}

		$('body').on('click', '.product-img-list input[type="radio"]', change_product_image);

	}

	changeImgList(selected_el, list_type = 'radio') {
		const img_thumbnail_link = selected_el.value,
			img_link = selected_el.dataset.value,
			target_img = $('.yo-product .img-holder img:not(.d-none)');
		let selected_img = new Image(),
			target = new Promise((resolve, reject) => {
				//add loader
				selected_img.onload = function (res) {
					resolve(res);
				};
			});

		selected_img.src = img_link;
		target_img.attr('src', img_thumbnail_link).parent('.zoomple').attr('href', img_link);
		target_img.attr('data-im-orientation', this.setImageOrientation($('.yo-product .img-holder img:not(.d-none)')[0], true));

		target.then((res) => {
			//imecomethruu
			setTimeout(() => {
				target_img.attr('src', img_link);
			}, 800);
			console.log(res, 'imecomethruu');
		}, (rej) => {
			//imebeat
			console.log(rej, 'imebeat');
		});

	}

	/**
	 * looks at the image passed to it and determines its orientation
	 * @param {html} img_el image element
	 * @param {boolean} _return whether to return the result;
	 */
	setImageOrientation(img_el, _return = false) {
		console.log($(img_el));
		if (img_el.naturalHeight < img_el.naturalWidth) {
			if (_return)
				return "horizontal";
			img_el.dataset.imOrientation = "horizontal";
		} else if (img_el.naturalHeight > img_el.naturalWidth) {
			if (_return)
				return "vertical";
			img_el.dataset.imOrientation = "vertical";
		} else {
			if (_return)
				return "square";
			img_el.dataset.imOrientation = "square";
		}
    }
    
    zoompleInit() {
		var screenSize = $(window).width(),
			is_mobile = false,
			images;
		if (screenSize < 700) {
			is_mobile = true;
		}
		$(document).ready((ev) => {
			console.log('zoomple init');
			images = $('[data-im-orientation]');
			$.each(images, (i, el) => {
				this.setImageOrientation(el);
			});

			$('.zoomple').zoomple({
				zoomWidth: $('.product-details-container').outerWidth(false) + 24,
				bgColor: 'rgba(223, 240, 243, 0.54)',
				zoomHeight: is_mobile ? $('.product-details-container').outerWidth(false) + 24 : $('.product-img-container').outerHeight(true),
				roundedCorners: is_mobile,
				attachWindowToMouse: is_mobile,
				showCursor: is_mobile,
				windowPosition: is_mobile ? {
					x: 'right',
					y: 'bottom'
				} : {
					x: 'right',
					y: 'top'
				},
				offset: is_mobile ? {
					x: -1 * ($('.product-details-container').outerWidth(false) / 3),
					y: 24
				} : {
					x: 24,
					y: 54
				}
			});
		});
	}
}

export default product_detail;