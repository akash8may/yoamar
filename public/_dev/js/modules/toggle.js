import {productCard_class} from '../dom_variables.js';

/** toggles read more on a target element 
 * the target element must either be a sibling 
 * or it shares the same parent with the trigger element */
function toggleReadMore(this_el, toggle_scope) {
	resetToggleMoreLess($(this_el).parent());
	var is_truncated = isTruncated($(this_el).parent()),
		target_el, target_el_style;

	if (toggle_scope === 'sibling') {
		target_el = $(this_el).siblings('[data-toggle-read-more]');
	} else {
		target_el = $(this_el).parent().find('[data-toggle-read-more]');
	}

	if ($(this_el).hasClass('js-infoOpen')) {
		target_el_style = target_el.attr('data-toggle-read-more');
		target_el_style = (target_el_style === '') ?  "100vh": target_el_style;

		target_el.removeClass('text-truncate').css({
			'max-height': target_el_style,
			'overflow-y': 'auto'
		});
		$(this_el).addClass('d-none').parent().find('.js-infoClose').removeClass('d-none');

	} else if ($(this_el).hasClass('js-infoClose')) {
		var default_style = target_el.attr('data-default') || '';
		target_el.removeAttr('style');

		if(default_style !== '') {
			target_el.css('max-height', default_style);
		}

		if (is_truncated) {
			target_el.addClass('text-truncate');
		}
	}
}

function toggleFileInput(input_name) {
	$(`[name=${input_name}][type=file]`).click();
	// console.info($(`[name=${input_name}][type=file]`));
	console.info('triggering file input\n', input_name);
}

/** if text has .truncate class */
function isTruncated(_el) {
	if (_el.hasClass('text-truncate')) {
		return true;
	} else {
		return false;
	}
};

/** reset the states of the read more/less buttons */
function resetToggleMoreLess(parent_el) {
	parent_el.find('.js-infoClose').addClass('d-none');
	parent_el.find('.js-infoOpen').removeClass('d-none');
}

function toggleInfoEvent() {
	$('body').on('click', `:not(${productCard_class}) .js-infoOpen, :not(${productCard_class}) .js-infoClose`, (f) => {
		f.preventDefault();
		toggleReadMore(f.currentTarget, 'sibling');
	});
}

export {toggleReadMore, toggleFileInput, isTruncated, resetToggleMoreLess, toggleInfoEvent};