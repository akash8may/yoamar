import toastify from "toastify-js/src/toastify.js";

/**
 * 
 * @param {String} text message for the toast
 * @param  {...any} args className/String, duration/Int, destination_link/String, callback/Function
 */
export default function(text, ...args) {
    toastify({
        text: text,
        duration: args[1] || 5000,
        destination: args[2] || undefined,
        // newWindow: true,
        close: false,
        gravity: "top", // `top` or `bottom`
        positionLeft: false, // `true` or `false`
        className: `zindex-tooltip ${args[0] || "bg-success green"}`,
        callback: args[3] || undefined
        // backgroundColor: "linear-gradient(to right, #00b09b, #96c93d)",
    }).showToast();
}