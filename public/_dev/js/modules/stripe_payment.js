import toast from "./toast.js";
class stripe_payment {
	/**
	 * 
	 * @param {String} form_el string of the form id
	 */
    constructor(form_el) {
        this.form = document.getElementById(form_el);
    }
    
    init() {
		const style = {
			base: {
				padding: '20px 0',
				height: '24px',
				// Add your base input styles here. For example:
				fontFamily: 'Raleway, Roboto, sans-serif',
				lineHeight:'24px'
				// color: "#32325d",
			},
		};
		var stripe = Stripe('pk_test_UKZF7dxO5cC2AhALWGzSbInq');
		var elements = stripe.elements();
		// Create an instance of the card Element.
		const card = elements.create('card', {style: style});
		// Add an instance of the card Element into the `card-element` <div>.
		card.mount('#card-element');

		card.addEventListener('change', ({error}) => {
			const displayError = document.getElementById('card-errors');
			if (error) {
			  	displayError.textContent = error.message;
			} else {
			  	displayError.textContent = '';
			}
		});
		// Create a token or display an error when the form is submitted.
		this.form.addEventListener('submit', (event) => {
			event.preventDefault();

			switch (this.form.elements['payment_method'].value) {
				case 'card':
				case 'visa':
				case 'mastercard':
					// RODO: Disable the place order button
					event.currentTarget.disabled = true;
					console.log('sending stripe...');
					stripe.createToken(card).then(({token, error}) => {
						console.log(card);
						if (error) {
							// Inform the customer that there was an error.
							var errorElement = document.getElementById('card-errors');
							errorElement.textContent = error.message;
						} else {
							console.log(token)
							toast("Card verified, proceeding to making your order", "bg-success green", 1000);
							// Send the token to your server.
							setTimeout(() => {
								this.stripeTokenHandler(token, true);
							}, 880);
						}
					});
					
					break;
				case 'paypal':
					console.log('Sending paypal...');
					
					break;
				default:
					this.form.submit();
					break;
			}
		});

    }
    
    stripeTokenHandler(token, save_card=false) {
		// Insert the token ID into the form so it gets submitted to the server
		const hiddenInput = document.createElement('input');
		hiddenInput.setAttribute('type', 'hidden');
		hiddenInput.setAttribute('name', 'stripeToken');
		hiddenInput.setAttribute('value', token.id);
		this.form.appendChild(hiddenInput);
		//if the user would like to save the credit card data
		if(save_card == true) {
			this.form.elements['card_number'].value = token.card.last4
			this.form.elements['card_expiry_month'].value = token.card.exp_month;
			this.form.elements['card_expiry_year'].value = token.card.exp_year;
			this.form.elements['cvc'].value = token.card.cvc;
		}
		console.log(this.form.elements['card_number'].value);
		// Submit the form
		this.form.submit();
    }
}

export default stripe_payment;
