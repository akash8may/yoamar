/**
 * get a modal string template
 * @param {string} id modal_id
 * @param {string} title modal title
 * @param {html_string} body html of the modal body
 * @param {html string} footer html of the modal footer
 * @param {boolean} single_button whether th modal should hsve a close button at the footer
 */
function yo_modal(id, title, body, buttons, footer = false, reload = false) {
	if (footer) {
		footer = `<div class="modal-footer">
				${buttons}
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" data-reload="${reload}" class="btn btn-primary">Save changes</button>
			</div>`;
	} else {
		footer = '';
	}

	return `<div class="modal fade" id="${id}" tabindex="-1" role="dialog" aria-labelledby="${id + title.split(' ')[0]}" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered _modal-sm _modal-lg" role="document">
		  <div class="modal-content">
			<div class="modal-header">
			  <h5 class="modal-title" id="${id + title.split(' ')[0]}">${title}</h5>
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>
			<div class="modal-body">
			  ${body}
			</div>
			${footer}
		  </div>
		</div>
	  </div>`;
}

/**
 * 
 * @param {Element} el button element that had triggered the wishlist event
 * @param {String} product_id id of the product
 */
function modal_wishlist_buttons(el, product_id, reload = false) {
	return `<div class="btn-group d-block">
				<a class="btn btn-primary my-btn-primary d-inline-block w-100 mb-2 js-addToWishlist ${($(el).hasClass('js-removeSavedItem')) ? 'js-removeSavedItem': ''}" href="#!" data-id="${product_id}" data-is-private="false" data-reload="${reload}" data-dismiss="modal" aria-label="Close">Public Wishlist</a>
				<a class="btn btn-primary my-btn-primary d-inline-block w-100 js-addToWishlist ${($(el).hasClass('js-removeSavedItem')) ? 'js-removeSavedItem': ''}" href="#!" data-id="${product_id}" data-is-private="true" data-reload="${reload}" data-dismiss="modal" aria-label="Close"><i class="fa fa-lock mr-3"></i>private Wishlist</a>
			</div>`
}

/**
 * 
 * @param {String} body either html or a string to be inserted in the dropdown
 * @param {Boolean} empty_state whether the dropdown has any data. If true, the body is assumed to be a message
 */
function searchbar_dropdown(body = 'results', empty_state = false) {
	return `<div class="yo-dropdown shadow position-absolute show">
		${empty_state == true ? '<h3 class="">'+body+'</h3>': body}
	</div>`;
}

/**
 * 
 * @param {Object} param0 name and image(default = false). To be added as innerHTML
 * @param {JSON} data either a json object or when stingified
 */
function searchbar_dropdown_list_item({product_name, merchant_company, id}, data, image = false) {
	if(typeof data === "object") {
		data = JSON.stringify(data);
	}

	return `<div class="dropdown-item js-dataInputs border-bottom" data-item-data='${data}' data-id="${id}"> 
		<p>${product_name} - <span>${merchant_company}</span></p>
	</div>`;
}

/**
 * 
 * @param {String} text innerText
 * @param {String} url default="#!"
 * @param {String} classes default="btn-primary"
 * @param {String} attributes html attributes
 */
function suggest_button(text, url = "#!", classes, attributes) {
	return `<a href="${url}" class="btn btn-primary ml-1 ${classes || ''}" ${attributes ||''}> ${text}</a>`;
}

export {yo_modal, modal_wishlist_buttons, searchbar_dropdown, searchbar_dropdown_list_item, suggest_button};
