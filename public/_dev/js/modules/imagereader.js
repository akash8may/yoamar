//Read file path
/**
 * 
 * @param {DOMElement} input Fie input
 * @param {String||DOMElement} hook could be an image alement or a parent of an image element
 */
export default function(input, hook) {
	let file = input.files[0],
		hook_image = $(hook),
		file_reader = new FileReader();

	// it could be a parent element (like `a > img`)
	if (hook.nodeName !== 'IMG') {
		//select the first image element
		hook_image = hook.querySelector('img');
		//make it jQuery
		hook_image = $(hook_image);
	}

	if (input.files.length === 0) {
		hook_image.attr('src', hook_image.attr('data-default-img'));
		return;
	};
	// hook_image.attr('data-default-img', hook_image.attr('src'));

	file_reader.readAsDataURL(file);
	file_reader.onloadend = function (i) {
		setTimeout(() => {
			hook_image.attr('src', file_reader.result);
		}, 880);
	};
}