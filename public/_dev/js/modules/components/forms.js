import PasswordRevealer from 'password-revealer';

if(document.querySelectorAll('input[type=password]').length > 0)
{
    // console.log(document.querySelectorAll('input[type=password]'));
    var inputs = document.querySelectorAll('input[type=password]');
    // Init password Revealer
    const toggle_btns = document.querySelectorAll('.password-field');
    toggle_btns.forEach((btn, i) => {
        btn = btn.form.querySelectorAll('.password-revealer')[i];
        var password_ = PasswordRevealer(inputs[i], {
            trigger: {
                selector : btn
            }
        });

        btn.addEventListener('click', (el) => {
            password_.toggle();

            if($(el.target).hasClass('fa-eye'))
                $(el.target).removeClass('fa-eye').addClass('fa-eye-slash');
            else
                $(el.target).removeClass('fa-eye-slash').addClass('fa-eye');
        });
    });
}