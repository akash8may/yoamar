import {toggleSearchbar_class} from '../../dom_variables.js';

class yo_search {
	constructor({
		small_searchbar,
		large_searchbar
	}) {
		this.nav_searchbar = small_searchbar;
		this.landing_searchbar = large_searchbar;
		this.searchbar = this.landing_searchbar === undefined ? this.nav_searchbar : this.landing_searchbar;

		this.listeners(this.searchbar);

		//prepopulate the searchbar if the url has search queries
		if (window.location.search !== "" && this.searchbar.elements['q'].value === '') {
			var category = window.location.search.match(/cat=\d+$/g),
				search_name = window.location.search.match(/q=\w+/g);

			console.info(search_name);
			if(category !== null) {
				category = category[0].split('=')[1];
				this.updateSearchbar(category, 'cat');
			}
			if(search_name !== null) {
				search_name = search_name[0].split('=')[1];
				this.updateSearchbar(search_name, 'q');
			}
		}

	}

	listeners(searchbar) {
		var dropdown_items = searchbar.querySelectorAll('.yo-dropdown-select .dropdown-item');
		$(dropdown_items).on('click', (e) => {
			this.updateSearchbar(e.currentTarget.dataset.value, e.currentTarget.dataset.inputName, e.currentTarget.dataset.valueName);
		});

		//Add eventlistener for toggle searchbar on mobile screens only
		if ($(toggleSearchbar_class).parent().css('display') !== 'none') {
			
			$('body').on('click', toggleSearchbar_class, (evt) => {
				this.toggleSearchbar(evt.currentTarget.dataset.action);
			});

		}
	}

	/**
	 * update the names of the search form inputs
	 * both the category input and search input
	 * @param  {...any} args value, input_name, value_name
	 */
	updateSearchbar(...args) {
		let value = args[0],
			string_value = args[2] || false,
			input_name = args[1],
			searchbar_dropdown_btn = this.searchbar.querySelector('button.dropdown-toggle');
		console.log(string_value);
		//the second argument is the input name to update
		switch (input_name) {
			case 'cat':
				if (Number(value) <= 0) {
					this.resetSearchbar();
				} else {
					this.searchCategory = Number(value);
				}
				break;
			case 'q':
			default:
				// update the search input
				this.searchbar.querySelector("input[name='q']").value = value;
				break;
		}

		if (string_value !== false) {
			searchbar_dropdown_btn.innerText = string_value;
		}
	}

	resetSearchbar() {
		let searchbar = this.landing_searchbar || this.nav_searchbar,
			searchbar_dropdown_btn = searchbar.querySelector('button.dropdown-toggle');

		searchbar.querySelectorAll("input[type=text]").value = "";
		searchbar_dropdown_btn.innerText = 'All';
	}

	/** toggles mobile searchbar */
	toggleSearchbar(action) {
		let hook = $('header .js-fixedNavbar'),
			fixed_form = $(this.nav_searchbar).clone(true, true);

		if (action === 'open') {
			fixed_form.addClass('shadow').removeClass('d-none').children('.js-searchbar').removeClass('d-none');
			hook.css('display', 'none').removeClass('d-none').prepend(fixed_form).fadeIn();
		} else if(action === 'close') {
			hook.fadeOut().promise().done(() => {
				hook.find('form').remove();
				hook.removeAttr('style').addClass('d-none');
			});
		} else {
			console.info('unkown action :', action);
		}
	}

	searchHint(string) {

	}

	/** set the category input of the searchbar */
	set searchCategory(category_id) {
		category_id = category_id || '';

		var form_el = this.searchbar,
			category_input = form_el.querySelector("input[name='cat']");
		category_input.value = category_id;
	}

}

export default yo_search;
