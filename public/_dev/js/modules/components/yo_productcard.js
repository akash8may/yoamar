import {resetToggleMoreLess} from '../toggle.js';
import toast from "../toast.js";
import {productCard_class} from '../../dom_variables.js';

class productcard {
    constructor() {
        let toggleProductCardInfo = (evt) => {
            evt.preventDefault();
            if ($(evt.currentTarget).hasClass('disabled')) return;
            this.toggleProductCard(null, evt.currentTarget);
        };

        $('body').on('click', `${productCard_class} .js-infoOpen`, toggleProductCardInfo);
		$('body').on('click', `${productCard_class} .js-infoClose`, toggleProductCardInfo);
	}

    /** toggles read more on the product cards */
	toggleProductCard(product_id, link_el) {
		const openned_card = $(`${productCard_class} .pr-info.info-open`);
		product_id = productcard.get_productId(product_id, link_el);
		
		resetToggleMoreLess(openned_card.parents(`${productCard_class}`));
		openned_card.removeClass('info-open').find('.pr-about').addClass('text-truncate');

		try {
			if ($(link_el).hasClass('js-infoOpen')) {
				$('#' + product_id + ' .pr-info').addClass('info-open').find('.pr-about').removeClass('text-truncate').promise().done(function () {
					$('#' + product_id).find('.pr-about').removeClass('text-truncate');
					$('#' + product_id).find('.js-infoClose').removeClass('d-none');
					$(link_el).addClass('d-none');
				});
			} else if ($(link_el).hasClass('js-infoClose')) {
				$('#' + product_id + ' .pr-info').removeClass('info-open').find('.pr-about').addClass('text-truncate').promise().done(function () {
					$('#' + product_id).find('.pr-about').addClass('text-truncate');
					$('#' + product_id).find('.js-infoOpen').removeClass('d-none');
					$(link_el).addClass('d-none');
				});
			}
		} catch (error) {
			console.log(error);
		}
    }

	static get_productId(product_id, el) {
		if (product_id === null && el !== null) {
			return el.dataset.id || $(el).parents(`${productCard_class}`).attr('id') || $(el).parents('.cart-item').attr('id') || $(el).parents('.yo-product').attr('id').split('pr_')[1];
		} else if (product_id === null && el === null) {
			toast("sorry, there's an error in adding to cart", "error red");
			return;
		} else {
			return product_id;
		}
	}
	//#endregion
}

export default productcard;