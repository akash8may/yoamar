import yo_async from "../yo_async.js";
import toast from "../toast.js";
import {
	searchbar_dropdown,
	searchbar_dropdown_list_item
} from "../yo_templates.js";

class api_search extends yo_async {
	constructor(searchbar, base_url) {
		super(base_url);
		if (searchbar === undefined || base_url === undefined) {
			console.error("undefned searchbar or base_url");
		}
		console.log(base_url);
		console.log(searchbar);
		this.route = base_url + "api/get_products?q=";
		this.searchbar = searchbar;
		this.search_str = {
			counts: 0,
			search_string: ''
		};
		if (this.searchbar)
			this.listeners();
	}

	listeners() {
		// searchbar listener
		this.searchbar.addEventListener("keyup", this.new_search.bind(this));

		$("body").on('click', '.yo-dropdown > .dropdown-item.js-dataInputs', (evt) => {
			this.process_dropdown_item(evt.currentTarget);
			this.hide_dropdown();
		});
	}

	new_search(evt) {
		let filter = this.searchbar.value,
			results,
			as_ = async () => {
				results = await super.getWarehouseItems(filter);
				if (results.ok == false)
					this.search_str.counts++;
				else
					this.search_str.counts = 0;
				console.info(results);

				await this.process_search_results(results);
			};
		console.info(filter);
		if (filter.match(/\w/) === null) {
			this.hide_dropdown();
			this.search_str.search_string = '';
			return;
		} else {
			console.table(this.search_str);
			//search to heppen after every two events
			// if(filter.length >= this.search_str.search_string.length + 2) {
			as_();
			this.search_str.search_string = filter;
			// } else {
			//     this.search_str.search_string = '';
			//     this.process_search_results({
			//         ok : true,
			//         data : []
			//     });
			// }
		}

		function searchFail(message) {
			console.error("error in getting warehouse items");
			console.error(message);
		}
	}

	process_search_results(results) {
		if (results.ok == false && this.search_str.counts > 15) {
			toast("Still can't find any results", "bg-error red");
			// return;
		} else if (results.ok == false) {
			// hide the dropdown
			this.hide_dropdown();
			console.info("hidding dropdown");
			return;
		} else if (results.ok == true && results.data.length == 0) {
			if (this.dropdown_is_appended) {
				//clear the results with
				this.dropdown_data = "<p>No results</p>";
				setTimeout(() => {
					this.hide_dropdown();
				}, 440);
			}
		}
		var template = searchbar_dropdown('No Results found', true),
			item_template = '';

		results.data.forEach(search_item => {
			item_template = item_template + searchbar_dropdown_list_item(search_item, search_item);
		});
		// console.log(this.dropdown_is_appended, item_template);
		//show dropdown if it's not appended yet
		if (!this.dropdown_is_appended) {
			console.info("need to append the dropdown");
			template = searchbar_dropdown(item_template, false);
			//append
			this.searchbar.insertAdjacentHTML('afterend', template);
		}

		this.dropdown_data = item_template;
		//replace the dropdown's data
	}

	process_dropdown_item(item) {
		let item_data = item.dataset.itemData,
			input_data;

		item_data = JSON.parse(item_data);
		// console.log();
		var process = async () => {
			var product = await super.getSingleProduct(item_data.product_id);
			// console.log(product);
			if (product.ok == false) {
				toast(product.message, "bg-red error");
				return;
			}
			product = product.data;
			this.searchbar.value = product.product_name;
			input_data = {
				product_id: item_data.product_id,
				product_name: item_data.product_name || product.product_name,
				unit_price: product.unit_price,
				merchant_company: item_data.merchant_company,
				description: product.description,
			};
			Object.entries(input_data).forEach(([name, value]) => {
				document.querySelector(`[data-form-group=${name}]`).innerHTML = value;
			});
		}
		process();
	}

	hide_dropdown() {
		$(".yo-dropdown.show").fadeOut().promise().done(function () {
			$(this).removeClass("show").html("").fadeIn(110);
		});
	}
	set dropdown_data(data) {
		var dropdown = $(this.searchbar).siblings(".yo-dropdown");

		dropdown.addClass("show").html(data).fadeIn(200);
	}

	get dropdown_is_appended() {
		console.log(this);
		var dropdown = $(this.searchbar).siblings(".yo-dropdown");
		console.log(dropdown);
		if (dropdown === undefined || dropdown.length === 0)
			return false;
		else
			return true;
	}

	get dropdown_is_hidden() {
		var dropdown = $(this.searchbar).siblings(".yo-dropdown");
		console.log(dropdown);
		if (dropdown === undefined)
			return false;

		if (dropdown.hasClass("show"))
			return false;
		else
			return true;
	}
}

export default api_search;
