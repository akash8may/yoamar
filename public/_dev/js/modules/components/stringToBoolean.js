export function stringToBoolean(string = 'false') {
	switch (string.toLowerCase().trim()) {
		case "true":
		case "yes":
		case "1":
			return true;
		default:
			return false;
	}
}
