import {
	productCard_class,
	cartItem_class
} from '../dom_variables.js';

export function get_productId(product_id, el) {
	if (product_id === null && el !== null) {
		var id = el.dataset.id || $(el).parents(`${productCard_class}`).attr('data-id') || $(el).parents(cartItem_class).attr('data-id') || $(el).parents('.yo-product').attr('data-id');
	
		if(id === null || id === undefined) {
			toast("sorry, there's an error", "error red");
			return null;
		} else {
			return id;
		}
	} else if (product_id === null && el === null) {
		toast("sorry, there's an error", "error red");
		return null;
	} else {
		return product_id;
	}
}

export function enableBtn(btn) {
	$(btn).removeClass("disabled").removeAttr("disabled");
}

export function disableBtn(btn) {
	$(btn).addClass("disabled").attr("disabled", "true");
}