import toast from './toast.js';

$('.document').ready(function(){
	
	// Constants
    var MAX_RATING = 5;
	var full_star_icon = 'fa-star';
	var empty_star_icon = 'fa-star-o';

	// #region HELPERS
	function _get_review_data(btn)
	{
		return {
			'product_id':btn.dataset.productId,
			'rating':$('.product-ratings').attr('data-rating'),
			'comment':document.forms['formReview'].elements['review'].value,
		}
	}

	// Reload page
	function _reload_page()
	{
		setTimeout(function() {
			location.reload();
		}, 1500);
	}
	// #endregion HELPERS
	
	// #region HANDLERS
	// Create review handler
	$('#btnAddReview').click(function(e) {
		e.preventDefault();
		var data = _get_review_data(this);
		// If no rating was provided
		if(data.rating <= 0)
		{
			alert('Please provide a rating before attempting to create a review');
			return;
		}
		
		create_review(data.product_id,data.rating,data.comment);
	});
	
	// Update review handler
	$('#btnUpdateReview').on('click', function(e) {
		e.preventDefault();
		var data = _get_review_data(this);
		update_review(data.product_id,data.rating,data.comment);
	});

	// Delete review handler
	$('#btnDeleteReview').on('click', function(e) {
		e.preventDefault();
		var data = _get_review_data(this);
		delete_review(data.product_id);
	});
	
	$('.product-ratings.js-review').children('li').on('click',function(){  
        updateStarRating($(this));
	});
	
	$('.modal#modalReview').on('show.bs.modal', function (e) {
		console.log(e.relatedTarget.dataset.productId);
		var product_id = e.relatedTarget.dataset.productId;
		if($(this).find('form .btn#btnAddReview').attr('data-product-id') !== product_id) {
			$(this).find('form .btn#btnAddReview').attr('data-product-id', product_id);
			resetStarRating();
		}
	});
	
	// #endregion HANDLERS

    function updateStarRating($current_rating){
		var $next_stars = $current_rating.nextAll('li');
		var $prev_stars = $current_rating.prevAll('li');

		// Clear all stars after the clicked child
		// $next_stars.children('.fa').removeClass(full_star_icon);
		$next_stars.children('.fa').addClass(empty_star_icon);

		// Add full stars to all siblings up until the star we are at
		$prev_stars.children('.fa').removeClass(empty_star_icon);
		$prev_stars.children('.fa').addClass(full_star_icon);
		
		$current_rating.children('.fa').removeClass(empty_star_icon);
		$current_rating.children('.fa').addClass(full_star_icon);

		// Update computed value & return it
		var computed_rating = $current_rating.attr('data-index');

		// Update the data-rating attribute
		$current_rating.parents('.product-ratings').attr('data-rating',computed_rating);
	
		return computed_rating;
    };

	function resetStarRating() {
		$('.yo-rating-link').find('ul').attr('data-rating', 0)
		$('.yo-rating-link').find('.fa').removeClass(full_star_icon).addClass(empty_star_icon);
	}
	// Create review
	function create_review(product_id,rating,comment)
	{
        var url = yo.base_url + "/api/review_product",
            data = {
                'product_id':product_id,
                'rating':rating,
                'comment':comment,
            };

		$.ajax({
			type: "POST",
			url: url,
			data: data,
			dataType: "json",
			success: function (response) {
				toast(response.message, (response.ok) ? 'error red': 'success green');
				if(response.ok == true) {
					_reload_page();
				}
			},
			error: function (response) { 
				toast(response.message, (response.ok) ? 'error red': 'success green');
			}
		});
	}

	// Update review
	function update_review(product_id,rating,comment)
	{
		//update_review - product_id, rating, comment
        var url = yo.base_url + "/api/update_review",
            data = {
                'product_id':product_id,
                'rating':rating,
                'comment':comment,
            };

		$.ajax({
			type: "POST",
			url: url,
			data: data,
			dataType: "json",
			success: function (response) {
				toast(response.message, (response.ok) ? 'error red': 'success green');
			},
			error: function (response) { 
				toast(response.message, (response.ok) ? 'error red': 'success green');
			}
		});
	}

	// Delete rating
	function delete_review(product_id)
	{
        var url = yo.base_url + "/api/remove_review",
            data = {
                'product_id':product_id
            };

		$.ajax({
			type: "POST",
			url: url,
			data: data,
			dataType: "json",
			success: function (response) {
				toast(response.message, (response.ok) ? 'error red': 'success green');
				if(response.ok == true) {
					_reload_page();
				}
			},
			error: function (response) { 
				toast(response.message, (response.ok) ? 'error red': 'success green');
			}
		});
	}
});

export {create_review, update_review, delete_review};