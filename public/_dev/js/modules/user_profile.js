import {toggleFileInput} from "./toggle.js";
import toast from '../modules/toast.js';
import imageReader from "./imagereader.js";
class user_profile {
    constructor() {
		console.log('user profile');
	}

	eventListeners() {
		$('.js-toggleUserAvatar.yo-edit').unbind('click').bind('click', (_e) => {
            _e.preventDefault();
            console.log(".........bbbbb.....");
            toggleFileInput(_e.currentTarget.dataset.inputName);
        });
        $('body').on('change', 'input[hidden][type=file][data-change-link]', (_e) => {
            var input = _e.currentTarget;
            imageReader(input, document.querySelector(input.dataset.changeLink));
		});
		
	}

	clearFormInputs(parent_el) {
		$(parent_el).find('input').val('');
	}

}

export default user_profile;