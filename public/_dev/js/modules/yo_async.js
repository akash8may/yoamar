import {API_URL} from '../config.js';

/** async functions.
 * here we put every server interaction code
 */
var yo_async = class {
	constructor(api_url) {
		console.log('Main async_s created');

	}

	getPaymentInfo(id) {
		let url = API_URL + "get_payment_info"
		return new Promise((resolve, reject) => {
			$.ajax({
				type: "GET",
				url: url,
				data: {
					'id': id
				},
				dataType: "json",
				success: function (response) {
					resolve(response);
				},
				error: function (response) {
					reject(response);
				}
			});
		});
	}
	/**
	 * NOTE THAT 'this' references the module class calling this function
	 * @param {String} filter_str searchbar value
	 */
	getWarehouseItems(filter_str) {
		let url = API_URL + "get_warehouse_items?q=";
		url = url+filter_str;

		console.log(url);
		return new Promise((resolve, reject) => {
			$.ajax({
				type: "GET",
				url: url,
				dataType: "json",
				success: function (response) {
					resolve(response);
				},
				error: function (response) {
					reject(response);
				}
			});
		});
	}

	getSingleProduct(product_id) {
		let url = API_URL + "get_single_product/";
		url = url+product_id;

		return new Promise((resolve, reject) => {
			$.ajax({
				type: "GET",
				url: url,
				dataType: "json",
				success: function (response) {
					// console.log(response.responseText);
					resolve(response);
				},
				error: function (response) {
					// console.error(response.responseText);
					reject(response);
				}
			});
		});
	}

	getProducts(filter_str) {
		let url = API_URL + "get_products?q=";
		url = url+filter_str;
		console.info(this);

		return new Promise((resolve, reject) => {
			$.ajax({
				type: "GET",
				url: url,
				dataType: "json",
				success: function (response) {
					resolve(response);
				},
				error: function (response) {
					reject(response);
				}
			});
		});
	}

	get currentUser() {
		return new Promise((resolve, reject) => {
			$.ajax({
				type: "GET",
				url: API_URL + "get_current_user",
				dataType: "json",
				success: function (response) {
					resolve(response);
					console.log(response);
					return response;
				}
			});

		});
	}

	get userIsLoggedIn() {
		return new Promise((resolve, reject) => {
			$.ajax({
				type: "GET",
				url: API_URL + "user_logged_in",
				dataType: "json",
				success: function (response) {
					resolve(response);
					// console.log('userIsLoggedIn');
				}
			});
		});
	}
}

export default yo_async;
