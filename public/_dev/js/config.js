import {baseUrl as localBaseUrl} from "./config/development/config_variables.js";
import {baseUrl as productionUrl} from "./config/production/config_variables.js";
// Base URL
const BASE_URL = (window.location.hostname == "yomar.com") ? localBaseUrl : productionUrl;

// API URL 
//* May change to be different in local/dev eg. if we use subdomain in production
const API_URL = BASE_URL+"api/";

const MAX_IMAGE_SIZE = 2400 //kilobytes

export { API_URL, BASE_URL, MAX_IMAGE_SIZE };
