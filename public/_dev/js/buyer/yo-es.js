/** console, jQuery, $ */
// components
import product_card from '../modules/components/yo_productcard.js';
import cart_card from '../modules/components/yo_cartcard.js';
import yo_search from '../modules/components/yo_searchbar.js';
import buyer from '../modules/buyer.js';
import slideout from 'slideout/dist/slideout.min.js';
import notification from '../modules/notifications.js';
//helpers
import toast from '../modules/toast.js';
import {toggleInfoEvent} from '../modules/toggle.js';
import {BASE_URL, API_URL} from '../config.js';
import {checkoutForm} from '../dom_variables.js';
// pages
import {yo_cart} from '../modules/yo_cart.js';
import product_detail from '../modules/product_detail.js';
import checkout from './modules/checkout.js';

/** 
 * All DOM/User interaction functions 
 */
var Ui = class Uistyles extends buyer {
	constructor(base_url) {
		super(base_url);
		new notification(API_URL);
		new product_card();
		new cart_card();
		new yo_search({
			small_searchbar: document.forms['navSearchbar'],
			large_searchbar: document.forms['navSearchbarLanding'],
		});
		// Load methods that only will work in certain pages
		if (location_path.find(x => x === 'cart')) {
			// for yo_cart.cartCalculator() to be called;
			new yo_cart(true);
		} else if (location_path.find(x => x === 'product')) {
			new product_detail(location_path.pop());
			new yo_cart(false);
		} else if (location_path.find(x => x === 'checkout')) {
			new checkout(API_URL, checkoutForm);
		} else {
			new yo_cart(false);
		}
		
		this.slideout = slideout;

		this.listeners("init");
	}
	listeners(to = "init") {
		const _this = this;
		
		if ($('#slide-out-content').length > 0) {
			this.slideout = new Slideout({
				'panel': document.getElementById('slideout-body'),
				'menu': document.getElementById('slide-out-content'),
				'padding': 320,
				'tolerance': 70
			});
		}

		if(to == 'init') {
			$('body').on('click', '[class*="js-"][href="#!"]', (f) => {
				f.preventDefault();
				//could help in collecting data, like heapish
				console.log('js-link');
			});
			
			$('body').on('click', 'a.popover-close', function (f) {
				f.preventDefault();
				var popover_id = $(this).parents('.popover[id]').attr('id');
				$('#' + popover_id).siblings('.disabled').removeClass('disabled');
				$('#' + popover_id).popover('dispose');

			});

			toggleInfoEvent();
		}
	}

	clearFormInputs(parent_el) {
		$(parent_el).find('input').val('');
	}

	toggleSlideOut() {
		console.log('sliding ' + (this.slideout.isOpen() ? 'out' : 'in'))
		this.slideout.toggle();
	}

	static checkPopperState(el) {
		console.log(el);
	}
	
	//#region  SINGLE PRODUCT_DETAILS FUNCTIONS---------------------------------------------------------------------
	
	/** handler for the single product list of images
	 * switches the image shown with the selected one
	 * list_type could also be table/li. Default is radio
	 * first, add a loader on the target_hook
	 * when the high res image loads, replace the target_img
	 */
	changeImgList(to, list_type = 'radio') {
		const img_thumbnail_link = to.value,
			img_link = to.dataset.value,
			target_img = $('.yo-product .img-holder img:not(.d-none)');
		let selected_img = new Image(),
			target = new Promise((resolve, reject) => {
				//add loader
				selected_img.onload = function (res) {
					resolve(res);
				};
			});

		selected_img.src = img_link;
		target_img.attr('src', img_thumbnail_link).parent('.zoomple').attr('href', img_link);
		target_img.attr('data-im-orientation', this.setImageOrientation($('.yo-product .img-holder img:not(.d-none)')[0], true));

		target.then((res) => {
			//imecomethruu
			setTimeout(() => {
				target_img.attr('src', img_link);
			}, 800);
			console.log(res, 'imecomethruu');
		}, (rej) => {
			//imebeat
			console.log(rej, 'imebeat');
		});

	}
	/** zoom function for the selected product image of  single product */
	yoZoom(img_el) {
		const hook = $('js-zoomer');
		console.log(img_el, hook);

		hook.removeClass('d-none').prepend(img_el);
	}

	//#endregion ---------------------------------------------------------------------

}

const location_path = location.pathname.split('/');
const yo = new Ui(BASE_URL);