import stripe_payment from '../../modules/stripe_payment.js';
import paypal_pay from '../../modules/paypal_payment.js';
import toast from '../../modules/toast.js';

class Checkout {
    constructor(route,checkout_form) {
        if (document.forms[checkout_form] === undefined) {
        	location.reload();
        	return;
        }
        var _form = document.forms[checkout_form];
        var _this = this;
        this.route = route;
        this.mpesa_btn = _form.querySelector('.btn.mpesa-btn');
        this.mpesa_input = _form.elements['mpesa_number'];
        this.order_total_hook = _form.querySelector('.js-order-total');
        this.order_total = _form.querySelector('.js-order-total').dataset.baseTotal;

        if (Stripe instanceof Function) {
            const stripe_pay = new stripe_payment(checkout_form);
            stripe_pay.init();
        }
        if (paypal instanceof Object) {
            new paypal_pay(checkout_form);
        }

        this.mpesa_btn.addEventListener('click', (r) => {
            console.log(r);
            console.log( /^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/gm.test(this.mpesa_input.value));

            if (/^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/gm.test(this.mpesa_input.value) == false) return;

            this.mpesa_payment(this.mpesa_input.value);
        });

        if ($('input[name=_payment_method]') !== undefined) {
        	$('.nav-link input[name=_payment_method]').click(function (f) {
        		f.preventDefault();
        		$('input[name="payment_method"]').val(this.value);
        		console.log($('input[name="payment_method"]').val());
            });
        }

        if ($('input[name=_shipping_method]') !== undefined) {
        	$('.nav-link input[name=_shipping_method]').click(function (f) {
                f.preventDefault();

                $('input[name="shipping_method"]').val(this.value);
                var order_total = Number(this.dataset.shippingCost) + Number(_this.order_total);

                _this.order_total_hook.innerHTML = (Math.round((order_total + 0.00001) * 100) / 100);
            });
        }
    }

    mpesa_payment(phone) {
        /**
         * format number
         */
    
        var phone_number = format_number(phone);
        if(phone_number == false) return;
        
        this.mpesa_input.value = phone_number;
        // I should toast here
        
        // phone_number = "+" + phone_number;
        console.log(phone_number);
         
        return new Promise((resolve, reject) => {
            $.ajax({
                type: "POST",
                url: this.route + "lipa_na_mpesa",
                data: {
                    "phone_number" : phone_number
                },
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    resolve(response);
    
                    if(response.ok == true) {
                        toast(response.message, "bg-success green", 500);
                        setTimeout(() => {
                            document.forms[checkout_form].submit();
                        }, 550);
                    }
                }
            });
        });
    }
}

function format_number(phone) {
    /**
     * format number
     * 
     * final result should be +254 7xx xxx xxx
     */

    var _p = ("" + phone).split('');

    if(_p[0] == "0") {
        _p[0] = "+254";
    } else if(_p[0] == "7") {
        _p.unshift("+254");
    }
    console.log(_p);

    if(_p.length < 10) {
        toast("Error in your phone number's format,\n please write it in this format: +254 7xx xxx xxx", "Error red");
        return false;
    }

    phone = _p.join('');
    
    return Number(phone);
}


export default Checkout;