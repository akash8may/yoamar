/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js/modules/components/forms.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/modules/components/forms.js":
/*!****************************************!*\
  !*** ./js/modules/components/forms.js ***!
  \****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var password_revealer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! password-revealer */ \"./node_modules/password-revealer/dist/password-revealer.js\");\n/* harmony import */ var password_revealer__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(password_revealer__WEBPACK_IMPORTED_MODULE_0__);\n\n\nif (document.querySelectorAll('input[type=password]').length > 0) {\n  // console.log(document.querySelectorAll('input[type=password]'));\n  var inputs = document.querySelectorAll('input[type=password]'); // Init password Revealer\n\n  var toggle_btns = document.querySelectorAll('.password-field');\n  toggle_btns.forEach(function (btn, i) {\n    btn = btn.form.querySelectorAll('.password-revealer')[i];\n    var password_ = password_revealer__WEBPACK_IMPORTED_MODULE_0___default()(inputs[i], {\n      trigger: {\n        selector: btn\n      }\n    });\n    btn.addEventListener('click', function (el) {\n      password_.toggle();\n      if ($(el.target).hasClass('fa-eye')) $(el.target).removeClass('fa-eye').addClass('fa-eye-slash');else $(el.target).removeClass('fa-eye-slash').addClass('fa-eye');\n    });\n  });\n}\n\n//# sourceURL=webpack:///./js/modules/components/forms.js?");

/***/ }),

/***/ "./node_modules/password-revealer/dist/password-revealer.js":
/*!******************************************************************!*\
  !*** ./node_modules/password-revealer/dist/password-revealer.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("!function(e,t){ true?module.exports=t():undefined}(this,function(){return function(e){function t(o){if(r[o])return r[o].exports;var n=r[o]={exports:{},id:o,loaded:!1};return e[o].call(n.exports,n,n.exports,t),n.loaded=!0,n.exports}var r={};return t.m=e,t.c=r,t.p=\"\",t(0)}([function(e,t){\"use strict\";Object.defineProperty(t,\"__esModule\",{value:!0});var r=\"function\"==typeof Symbol&&\"symbol\"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&\"function\"==typeof Symbol&&e.constructor===Symbol?\"symbol\":typeof e},o=function(e,t){var o={isRevealed:!1,trigger:{selector:\".PasswordRevealer-trigger\",eventListener:\"click\"}};if(!e)throw new Error(\"Missing input argument\");if(\"string\"==typeof e&&(e=document.querySelector(e)),\"INPUT\"!==e.nodeName)throw new Error(\"First argument (input) must be an input element\");\"object\"===(\"undefined\"==typeof t?\"undefined\":r(t))?(t=Object.assign({},o,t),t.trigger=Object.assign(o.trigger,t.trigger)):t=o;var n=t,i=n.isRevealed,s=function(){e.type=\"text\",i=!0},u=function(){e.type=\"password\",i=!1},f=function(){i?u():s()};i&&s();var c=function(){var e=arguments.length<=0||void 0===arguments[0]?f:arguments[0],r=t.trigger,o=r.selector,n=r.eventListener,i=o;if(\"string\"==typeof o&&(i=document.querySelector(o)),!i||!i.nodeType)throw new Error(\"Trigger must be a HTML element\");i.addEventListener(n,e)};return{show:s,hide:u,toggle:f,init:c}};t[\"default\"]=o,e.exports=t[\"default\"]}])});\n\n//# sourceURL=webpack:///./node_modules/password-revealer/dist/password-revealer.js?");

/***/ })

/******/ });