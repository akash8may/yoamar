<?php
$responsive = $responsive ?? FALSE;
// $compArgs = array(
//     'class' => '',
//     'attributes' => '',
//     'responsive' => FALSE #Always use caps in Codeigniter for boolean/NULL
// );
?>
<a href="#" class="btn btn-primary my-btn-primary js-addToWishlist <?= get_value_or_default(@$extra_classes); ?>" 
    <?= get_value_or_default(@$attributes); ?>>
    <i class="fa fa-heart-o <?= $responsive ?? 'd-inline-block'; ?> "></i>
    <?= $responsive ? '' : 'Add to wishlist'; ?>
</a>