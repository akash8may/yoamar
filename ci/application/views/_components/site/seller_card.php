<div class="card">
    <div class="card-body">
        <!-- TEMPLATEstart user-info-->
        <?php 
        $this->load->view('_components/site/user_info', array(
            'size' => 'small',
            'extra_classes' => 'p-0',
            'subtitle' => $this->load->view('_components/ratings', array(
                'class' => 'seller-ratings',
                'rating' => 4.5,
            ), TRUE)
        ));
        ?>
        <!-- TEMPLATEend user-info-->

        <h4 class="d-none card-title">Title</h4>
        <h6 class="text-muted card-subtitle mb-2 d-none">Subtitle</h6>
        <p class="card-text">Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
        <a href="#" class="card-link text-primary54">
            <i class="fa fa-heart"></i>
        </a>
        <a href="#" class="card-link text-primary54">
            <i class="fa fa-heart pr-2"></i>2000
        </a>
    </div>
    <!-- TEMPLATEstart bundle_card-->
    <?php 
        $this->load->view('_components/site/bundle_card', array(
            'id' => 'seller6565d',
            'size' => 'horizontal',
            'grid' => '_even',
            'extra_classes' => 'w-100 rounded-0'
        ));
    ?>
    <!-- TEMPLATEend bundle_card-->
</div>
