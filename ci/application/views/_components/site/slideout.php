
<div id="slide-out-content" class="d-block bg-secondary26 <?= @$class; ?>" >
    <header class="py-2 px-3 mb-2 d-block bg-secondary26 border-bottom">
        <h4 class="d-inline-block my-1"><b>Yo-Amar</b></h4>
        <ul class="nav navbar-nav ml-auto d-inline-block float-right">
            <li class="nav-item" role="presentation">
                <a class="nav-link text-primary" href="#!" onclick="yo.ui.toggleSlideOut()">
                    <i class="fa fa-close"></i>
                </a>
            </li>
        </ul>
    </header>
	<div class="category-list p-2">
		<div class="pb-1 mb-1 category-list-header position-relative">
			<h6 class="d-inline-block yo-title-sm">CATEGORIES</h6>
		</div>
		<ul class="list-group">
			<?php 
			$count = 0;
			foreach($categories as $category):
				$subcategories = $this->utility->get_subcategories(array(
					'category_id'=>$category->id,
					'level'=>PRODUCT_FAMILY
				));	
				$accordion_class = 'item-'.$category->id;
				$should_collapse =  ($count === 0);
				if(!empty($subcategories)):
			?>
			<li class="list-group-item p-0 border-right-0 border-left-0 border-top-0">
				<div role="tablist" id="slideout-category-list-<?= $category->id;?>">
					<div class="card border-0">
						<div class="card-header border-0 p-0" role="tab">
							<h5 class="mb-0 p-0">
								<a data-toggle="collapse" 
									aria-expanded="<?= $should_collapse ? 'true':'false'; ?>" 
									aria-controls="slideout-category-list-<?= $category->id;?> <?= '.'.$accordion_class?>" 
									href="#slideout-category-list-<?= $category->id;?> <?= '.'.$accordion_class?>"
									class="d-block"><b><?= $category->title; ?></b></a>
							</h5>
						</div>
						<div class="collapse <?= $accordion_class?> <?= get_if_not_empty($should_collapse,'show'); ?> bg-secondary26" role="tabpanel" data-parent="#slideout-category-list-<?= $category->id;?>">
							<div class="card-body pr-0 py-0">
								<ul class="list-group list-group-flush">
									<!--ts  -->
									<?php foreach($subcategories as $subcategory):?>
									<li class="list-group-item p-0 ">
										<a href="<?= site_url('categories/'.$subcategory->id); ?>" class="d-block"><?= $subcategory->title; ?></a>
									</li>
									<?php endforeach; ?>
									<!--te  -->
								</ul>
							</div>
						</div>
						
					</div>
				</div>
			</li>
			<?php 
				endif;
				$count++;	
			endforeach; 
			?>
		</ul>
	</div>
</div>