<?php 
//if the ui uses the timeline ui version or list ui verion
$in_timeline = $in_timeline ?? FALSE;
//whether to higghlight the notification item
$highlight = $highlight ?? FALSE;
?>
<!-- ----------------------------------------------------------------- -->
<div class="notification-list">

<?php if($in_timeline): ?>
    <!-- LOOP NOTIFICATION TIME_OBJECT start-->
    <div class="row notification-item">
        <div class="col-1 offset-md-2 p-0 border-right">
            <div class="timeline-spot float-right my-1 bg-light"></div>
        </div>
        <div class="col-11 col-md-8 pb-3">
            <span class="py-2 text-info54 pl-2 font-italic font-weight-bold d-none">12 hrs ago</span>

            <?php if(!empty($notifications) && is_array($notifications)): ?>
            <div class="card shadow w-75 mt-2 border-0">
                <?php
                // <!-- LOOP NOTIFICATIONS -->
                foreach($notifications as $key => $notification): #var_dump($notification); ?>
                <div class="card-body border-bottom <?= ((int)$notification->is_read === 0) ? 'bg-info12' : '' ?>" id="<?= $notification->id ?>" data-link="<?= $notification->link ?>" data-date="<?= $notification->date_sent ?>">
                    <h5 class="card-title"><?= $notification->title ?></h5>
                    <p class="card-text"><?= $notification->description ?></p>
                    <?php if ((int)$notification->is_read === 0) : 
                    if(isset($notification->link)): ?>
                    <a href="<?= $notification->link ?>" class="card-link text-primary">GO</a>
                    <?php endif; ?>
                    <a href="#!" data-id="<?= $notification->id ?>" class="card-link js-markNotificationRead text-info">Mark as read</a>
                    <?php else: ?>
                    <p class="card-link d-inline-block text-success12 mb-0"><i class="fa fa-check mr-1"></i>Read</p>
                    <?php endif; ?>
                </div>
                <?php endforeach; 
                // <!-- LOOP NOTIFICATIONS end--> 
                ?>
            </div>
            <?php endif;?>
        </div>
    </div>
    <!-- LOOP NOTIFICATION TIME_OBJECT end-->
<?php else: ?>
    <div class="row user-list notification-item">
        <div class="col-12 user-item py-0">
        <?php 
        // <!--LOOP -->
        foreach($notifications as $key => $notification): ?>
            <!-- TEMPLATEstart user notification-item -->
            <div class="user-container px-2 <?= $extra_classes ?> pb-2 border-bottom" id="<?= $notification->id ?>" data-link="<?= $notification->link ?>" data-date="<?= $notification->date_sent ?>">
                <p class="user-name m-0">
                    <a href="<?= $notification->link ?>" class="h5"><?= $notification->title ?></a></br>
                    <a style="white-space: normal" class="font-weight-light" href="<?= $notification->link ?>"><?= $notification->description ?></a></br>
                    <span class=""><?= humanize_date($notification->date_sent) ?></span></br>
                </p>
                <a href="#!" data-id="<?= $notification->id ?>" class="user-delete js-markNotificationRead text-primary87 bg-light rounded-0 px-2 py-1">
                    <i class="fa fa-eye mr-1"></i> Mark as read
                </a>
            </div>
            <!-- TEMPLATEend user notification-item -->
        <?php endforeach; 
        // <!--LOOPend   -->
        ?>
        </div>
    </div>
<?php endif; ?>

</div>
<!-- ------------------------------------------------------------------- -->