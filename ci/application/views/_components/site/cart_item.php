<?php
$product_thumb_link = @$image->thumbnail_link ?? base_url(PRODUCT_PLACEHOLDER_IMG);
?>
<!--TEMPLATE CART -ITEM-->







<?php /* ?>
<div class="cart-item" id="ci_<?= $id ?>" data-id="<?= $id ?>" data-base-amount="<?= $product->unit_price ?>">
	<div class="card d-flex flex-sm-row flex-column flex-nowrap border-0">
		<div class="flex-grow-0 flex-shrink-0 yo-cart-item-image">
			<img class="text-center align-middle float-right h-100" src="<?= $product_thumb_link ?>" />
		</div>
		<div class="card-body flex-grow-0 yo-cart-item-body pl-3 py-2 pr-0">
			<div class="row">
				<div class="col-12">
					<h4 class="card-title pt-1 pt-md-3"><?= $product->product_name ?></h4>
					<div class="card-text cart-item-details mb-1">
						<?= $product->description ?>
						<!-- <span class="ml-1 d-none">
							<i class="fa fa-circle" style="color:#6267dc;"></i>
						</span> -->
					</div>
				</div>
				<div class="col-12 col-lg-8 mt-lg-3 pt-1">
					<a class="card-link btn btn-sm btn-outline-primary px-1 js-moveTo -f-c-t-si" data-to="savedItem" data-from="cart" href="#!" title="Move to cart">
						Save for later
					</a>
					<a class="card-link btn btn-sm btn-outline-primary px-1 ml-1 js-moveTo -f-c-t-w" data-to="wishlist" data-from="cart" href="#!" title="Move to wishlist">
						<i class="fa fa-heart-o pr-1 d-none d-lg-inline"></i>Move to Wishlist
					</a>
					<a class="card-link btn btn-sm btn-primary px-1 ml-1 js-removeFromCart" href="#!" data-id="<?= $id ?>" title="Remove product(s) from cart">
						<i class="fa fa-trash pr-1 d-none d-lg-inline"></i> Remove
					</a>
				</div>
				<div class="col-12 col-lg-4 mt-3 mt-lg-0">
					<div class="row">
						<div class="col-6">
							<h6 class="text-muted card-subtitle mb-2 text-primary-dark word-wrap">Price</h6>
							<p class="lead word-wrap mb-1">$<span class="d-inline js-baseAmount"><?= $product->unit_price ?></span></p>
						</div>
						<div class="col-6">
							<form  action="cart/product/update/<?= $id ?>/" method="post">
								<h6 class="text-muted card-subtitle mb-2 text-primary-dark word-wrap">Quantity</h6>
								<select class="form-control-sm w-auto text-primary-dark js-updateItemNumber" 
									name="number of items">
									<?php 
									$option_numbers = ($product->quantity > 10) ? 10 : $product->quantity;
									for($i = 1; $i <= $option_numbers; $i++):  ?>
									
									<option value="<?= $i ?>" <?= ($i === (int)$quantity) ? 'selected' : '' ?>>
										<?= (($i == 10) ? '10+': $i); ?>
									</option>
									
									<?php endfor; 
									if($quantity > 10) : ?>
									
									<option value="<?= $quantity ?>" selected disabled class="disabled">
										<?= $quantity ?>
									</option>
									
									<?php endif; ?>
								</select>
								<input type="number" max="<?= @$max; ?>" class="d-none" name="item-number" value="<?= $quantity ?>">
							</form>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<?php */ ?>