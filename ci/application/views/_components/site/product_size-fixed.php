<div class="form-group yo-product-variable yo-size /*d-inline-block mr-3*/ my-1">
    <label class="text-primary-dark mb-1 d-block">Sizes available</label>
    <div class="yo-size-list js-ui-overflow-x py-1">
        <?php 
        $staticData = array(
            array(
               'value' => 'l',
                'id' => 'sizel',
                'label' => 'L'
            ),
            array(
               'value' => 'xxl',
                'id' => 'sizexxl',
                'label' => 'XXL'
            ),
            array(
               'value' => 's',
                'id' => 'sizes',
                'label' => 'S'
            )
        );
        
        foreach($staticData as $d): ?>

        <div class="custom-control custom-radio custom-control-inline px-0">
            <input type="radio" 
                name="product-size-fixed" 
                value="<?= $d['value']; ?>" 
                id="<?= $d['id']; ?>" 
                class="custom-control-input">
                <label for="<?= $d['id']; ?>" class="custom-control-label position-absolute text-center"><?= $d['label']; ?></label>
        </div>
        
        <?php endforeach; ?>
    </div>
</div>