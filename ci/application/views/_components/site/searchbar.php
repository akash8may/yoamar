<?php 
	$this->load->helper('form');
	$action = $action ?? 'search'; 
	$default_category = 'All Categories';
?>
<!--TEMPLATEstart searchbar-->
<?= form_open(site_url($action),array(
	'method'=>'GET',
	'class'=>'flex-grow-1 d-none d-sm-block ',
	'id'=>get_value_or_default(@$id, 'navSearchbar')
)); ?>
<div class="input-group my-searchbar position-relative js-searchbar <?= $class; ?> <?= get_value_or_default(@$extra_class); ?> <?= $size ?? '_thin'; ?>">
    <div class="input-group-prepend d-none d-sm-flex">
        <div class="dropdown btn-group" role="group">

            <button class="btn btn-primary dropdown-toggle border-0 text-truncate" data-toggle="dropdown" aria-expanded="false" type="button">All categories</button>

			<!-- Hidden input ~ stores value of cat -->
			<input type="hidden" name="cat" value="">

			<!-- Dropdown -->
			<?php //*TODO : Move this style into css file with the class yo-dropdown-select being the selector 
			?>
            <div class="dropdown-menu shadow-lg yo-dropdown-select" role="menu"  style="max-height:50vh; overflow-y:scroll;">
				<span class="mx-2"><b>Categories</b></span>
				<a class="dropdown-item" 
					data-value="-1"
					data-value-name="All categories"
					data-input-name="cat"
					href="javascript:void(0)"><?= $default_category; ?></a>
			<?php foreach($categories as $category): ?>
				<span class="mx-2"><b><?= $category->title; ?></b></span>
			<?php
					$subcategories = $this->utility->get_subcategories(array(
						'category_id'=>$category->id,
						'level'=>PRODUCT_FAMILY
					));	
					if(!empty($subcategories)):
						foreach ($subcategories as $subcategory):
			?>
                <a class="dropdown-item" data-value="<?= $subcategory->id; ?>" data-value-name="<?= $subcategory->title; ?>" data-input-name="cat" href="#!"><?= $subcategory->title; ?></a>
			<?php 
						endforeach; #foreach ($subcategories as $subcategory)
					endif; #if(!empty($subcategories))
				endforeach; #foreach($categories as $category)
			?>
            </div>
        </div>
    </div>
    <input class="form-control border-0" name="q" type="search" placeholder="SEARCH PRODUCTS">
    <div class="input-group-append">
        <button class="btn btn-primary border-0" type="submit">
            <i class="fa fa-search"></i>
        </button>
    </div>
</div>
<?= form_close(); ?>
<!--TEMPLATEend searchbar-->
