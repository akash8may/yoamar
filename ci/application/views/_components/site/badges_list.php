<div class="yo-badge-list <?= @$class; ?> bg-secondary26">
    <!-- <a class="badge badge-pill badge-secondary54 mr-2 p-2 mb-2" href="#">Paintings<i class="fa fa-star-o ml-1"></i><i class="fa fa-window-close ml-2 js-removeFilter"></i></a> -->
    <?php
    # <!-- LOOP -->
    $product_groups = array();
    $product_segments = array();
    $product_families = array();

    foreach ($badges as $key => $badge) :
        
    if(! in_array($badge->product_group_title, $product_groups)): ?>

    <a class="badge badge-pill badge-secondary54 mr-2 p-2 _active mb-2"
        id="product_group_title<?= $badge->id ?>"><?= $badge->product_group_title; ?><i class="fa fa-star ml-1"></i></a>
    <?php
        array_push($product_groups, $badge->product_group_title);
    endif;
    
    if(! in_array($badge->product_segment_title, $product_segments)): ?>

    <a class="badge badge-pill badge-secondary54 mr-2 p-2 _active mb-2"
        id="product_segment_title_<?= $badge->id ?>"><?= $badge->product_segment_title; ?><i class="fa fa-dot-circle-o ml-1"></i></a>
    <?php
        array_push($product_segments, $badge->product_segment_title);
    endif;

    if(! in_array($badge->product_family_title, $product_families)): ?>

    <a class="badge badge-pill badge-secondary54 mr-2 p-2 _active mb-2"
        href="#" 
        id="product_family_title<?= $badge->id ?>"><?= $badge->product_family_title; ?></a>
    
    <?php
        array_push($product_families, $badge->product_family_title);
    endif;
    
    endforeach;
    # <!-- LOOPend -->
    ?>
</div>