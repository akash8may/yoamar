<div class="category-list <?= @$class; ?> ">
<?php if(!empty(@$categories)): ?>
	<div class="pb-1 mb-1 category-list-header position-relative">
		<h6 class="d-inline-block yo-title-sm">CATEGORIES</h6>
		<a href="<?= site_url('categories'); ?>" class="btn btn-primary my-btn-primary _small btn-outline float-right py-2">VIEW
			ALL</a>
	</div>
	<ul class="list-group">
		<!--ts  -->
		<?php 
		$count = 0;
		foreach($categories as $category):
			$subcategories = $this->utility->get_subcategories(array(
				'category_id'=>$category->id,
				'level'=>PRODUCT_FAMILY
			));	
			$accordion_class = 'item-'.$category->id;
			$should_collapse =  ($count === 0);
			if(!empty($subcategories)):
		?>
		<li class="list-group-item p-0 border-right-0 border-left-0 border-top-0">
			<div role="tablist" id="sidebar-category-list-<?= $category->id;?>">
				<div class="card border-0">
					<div class="card-header border-0 p-0" role="tab">
						<h5 class="mb-0 p-0">
							<a data-toggle="collapse" 
								aria-expanded="<?= $should_collapse ? 'true':'false'; ?>" 
								aria-controls="sidebar-category-list-<?= $category->id;?> <?= '.'.$accordion_class?>" 
								href="#sidebar-category-list-<?= $category->id;?> <?= '.'.$accordion_class?>"
								class="d-block"><b><?= $category->title; ?></b></a>
						</h5>
					</div>
					<div class="collapse <?= $accordion_class?> <?= get_if_not_empty($should_collapse,'show'); ?>" role="tabpanel" data-parent="#sidebar-category-list-<?= $category->id;?>">
						<div class="card-body pr-0 py-0">
							<ul class="list-group list-group-flush">
                                <!--ts  -->
                                <?php foreach($subcategories as $subcategory):?>
								<li class="list-group-item p-0">
									<a href="<?= site_url('category/'.$subcategory->id.'/products'); ?>" class="d-block"><?= $subcategory->title; ?></a>
								</li>
                                <?php endforeach; ?>
                                <!--te  -->
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</li>
		<?php 
			endif;
			$count++;	
		endforeach; 
		?>
		<!--te  -->
		
	</ul>
	<!-- TODO: Add filters -->
<?php else: # No categories found ?>
	<p class="lead text-muted">
		No categories found
	</p> 
<?php endif;?>
</div>
