<?php 
	$is_dark = $is_dark ?? TRUE;
	$has_margin = $has_margin ?? TRUE;
	$extra_classes = $extra_classes ?? '';
	$extra_classes .= $has_margin ? ' mr-5':'';
	$_icon_color = $is_dark ? 'text-primary-dark' : 'text-white';
	$_feedback_bubble_color = $is_dark ? 'bg-primary-dark text-white' : 'bg-white text-primary-dark';
	
?>

<span class="<?= get_value_or_default(@$extra_classes).' '.$_icon_color; ?> yo-cart-icon js-cartIcon position-relative d-inline">
	<i class="fa <?= get_value_or_default(@$icon,'fa-shopping-cart');?>"></i>
	<?php 
		if(!empty($total)){
		echo '<span class="position-absolute '.$_feedback_bubble_color.' text-center js-cartTotalItems">'.@$total.'</span>';
		}
		else
		echo '<span class="position-absolute '.$_feedback_bubble_color.' text-center js-cartTotalItems" style="display:none">'.@$total.'</span>';
	?>
	<!-- <?= 
		get_if_not_empty(@$total,'<span class="position-absolute '.$_feedback_bubble_color.' text-center js-cartTotalItems">'.@$total.'</span>','<span class="'.$_feedback_bubble_color.'" style="width:0.5rem; height:0.5rem;border-radius:100%; margin-top:0.75rem;"></span>'); 
	?> 
	<?=
		get_if_empty(@$total,'<span class="position-absolute '.$_feedback_bubble_color.' text-center js-cartTotalItems">'.@$total.'</span>','<span class="'.$_feedback_bubble_color.'" style="width:0.5rem; height:0.5rem;border-radius:100%; margin-top:0.75rem;"></span>'); 
	?> -->
</span>

<style>
.cart-header a.cart-title span.yo-cart-icon{font-size:18px;top:0;}
.cart-header a.cart-title span{font-size:10px;}
</style>