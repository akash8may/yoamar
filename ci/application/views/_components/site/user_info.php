<?php 
    $dismissable = $dismissable ?? FALSE;
    $subtitle = $subtitle ?? FALSE;
    $user_img = @$picture_url ?? base_url(PRODUCT_PLACEHOLDER_IMG);
 ?>
<div class="user-item <?= ((@$size === 'large' ? 'item-large' : 'item-small')).' '.@$extra_classes; ?>" <?= @$attributes ?>>
    <div class="user-container">
        <a href="#" class="user-avatar">
            <img class="rounded-circle img-fluid" 
                src="<?= $user_img ?>" 
                alt="Profile of Mark Smith Peterson"></a>
        <p class="user-name">
            <a href="#"><?= $first_name ?></a>
            <?php if($subtitle !== FALSE) : ?>
            <span class="d-block"><?= $subtitle; ?></span></p>
        <?php 
        endif;
        if($dismissable) :
            ?>
        <a href="#" class="user-delete text-center position-absolute"><i class="fa fa-remove"></i></a>
        <?php endif; ?>
    </div>
</div>