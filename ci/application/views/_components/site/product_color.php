<div class="form-group yo-product-variable yo-color /*d-inline-block mr-3*/ my-1"><label class="text-primary-dark mb-1 d-block">Colors available</label>
    <div class="yo-color-list js-ui-overflow-x py-1">
    <?php ?>
        <div class="custom-control custom-radio custom-control-inline px-0">
            <input type="radio" 
                name="product-color" 
                value="332e32" 
                checked="" 
                id="color332e32" 
                class="custom-control-input border-0">
                    <label for="color332e32" 
                        class="custom-control-label position-absolute text-center" 
                        data-product-label="#fec313"
                        style="background-color:#fec313;"></label>
        </div>
    <?php ?>

        <div class="custom-control custom-radio custom-control-inline px-0"><input type="radio" name="product-color" value="13fed4" id="color13fed4" class="custom-control-input border-0"><label for="color13fed4" class="custom-control-label position-absolute text-center" data-product-label="#fec313"
                style="background-color:#13fed4;"></label></div>

    </div>
</div>