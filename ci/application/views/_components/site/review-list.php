<?php
if(empty($reviews) && isset($id)) {
    $reviews = $this->utility->get_product_reviews($id);
}
if(!empty($reviews)):
?>

<div class="row mx-0" id="review-<?= @$id ?>">
    <div class="col-md-12 col-lg-6 review-header">
        <div class="mb-2">
            <h1><span class="text-primary"><?= count($reviews) ?></span> Review<?= count($reviews) !== 0 ? 's' : '' ?></h1>
            <div class="pb-2 border-bottom">
                <?php
                $average_rating = $this->utility->get_average_rating($id) ?? 0;
                $this->load->view('_components/ratings', array(
                    'rating' => (float)$average_rating,
                    'extra_classes' => 'd-block',
                    'show_text' => TRUE
                ));
                ?>
                </div>
        </div>
    </div>

    <div class="col-12">
        <div class="row flex-column-reverse flex-sm-row">
            <div class="col-12 col-sm-6 col-md-8 col-lg-9">
                <div class="row">
                <?php foreach($reviews as $key => $review): ?>
                <!-- LOOP START -->
                    <div class="col-12 col-md-6 col-lg-4 pl-0">
                    <?php
                        // <!-- TEMPLATE START -->
                        $this->load->view('_components/site/review-item', array(
                            'is_highlight' => TRUE,
                            'comment' => array($review->comment, 'text-primary-dark text-center'),
                            'extra_classes' => 'p-4',
                            'rating' => array(
                                'rating' => (float) $review->rating,
                                'show_text' => FALSE,
                                'extra_classes' => 'text-center d-block mb-2',
                            ),
                            'user' => array(
                                'name' => $review->buyer_first_name.' '.$review->buyer_last_name,
                                'date' => $review->date_updated,
                            )));
                        // <!-- TEMPLATE END -->
                    ?>
                    </div>
                <!-- LOOP END -->
                <?php endforeach; ?>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 p-0 d-none">
                <?php
                    // <!-- TEMPLATE START -->
                    $this->load->view('_components/site/review-item', array(
                        'is_highlight' => TRUE,
                        'review' => array($review, 'text-primary-dark text-center'),
                        'extra_classes' => 'bg-secondary26 p-4',
                        'rating' => array(
                            'rating' => 2.7,
                            'show-text' => FALSE,
                            'extra_classes' => 'text-center d-block mb-2',
                        ),
                        'user' => array(
                            'name' => 'Mark Smith Peterson',
                            'location' => 'Nairobi, KE',
                            'date' => '12th August 2018',
                        )
                    ));
                    // <!-- TEMPLATE END -->
                ?>
            </div>
        </div>
    </div>
</div>
<?php else: 
$this->load->view('_templates/empty_message',array(
    'title'=>'Product not yet rated',
    'message'=>'',
    'button'=>[],
    'extra_classes'=>'bg-light border-top border-bottom mb-5'
));

endif; ?>