<div class="product-img-list">
    <form class="form-inline" id="productImagesList" data-list-for="<?= $id ?>">
        <?php 
        // <!-- LOOP -->
        $selected_image = @$selected_image ?? 0;
        foreach($images as $key => $image): 
            $product_thumb_link = @$image->thumbnail_link ?? base_url(PRODUCT_PLACEHOLDER_IMG);
            $product_img_link = @$image->img_link ?? base_url(PRODUCT_PLACEHOLDER_IMG);
        ?>
        <div class="custom-control custom-radio custom-control-inline px-0 product-img position-relative">
            <input id="im<?= $image->id ?>"
                type="radio" 
                name="product-image" 
                value="<?= $product_thumb_link ?>" 
                data-value="<?= $product_img_link ?>"
                class="custom-control-input border-0">
                <label for="im<?= $image->id ?>" 
                    class="custom-control-label position-relative text-center bg-secondary" 
                    data-product-label="#im<?= $image->id ?>">
                    <span class="img position-relative">
                        <img src="<?= $product_thumb_link ?>">
                    </span></label>
        </div>
        <?php 
        endforeach;
        // <!-- LOOPend -->
        ?>
    </form>
</div>