<?php 
    $grid = $grid ?? '_even';
    $size = $size ?? '';
    $size = strtolower($size);
    $has_title = $has_title ?? TRUE;
	// $extra_classes = $extra_classes ?? '';
	$placeholder = array(
		(object)array(
			'img_link'=>$bundle_img ?? base_url(PRODUCT_PLACEHOLDER_IMG)
		)
	);

	$images = $images ?? $placeholder;

	// Prevent an error with passing an unexpected value to shuffle()
	if(is_array($images))
	{
		shuffle($images);
	}
// if(is_array($images)):
    //Display large product card
    if(($size === 'horizontal' || $size==='horiz')) :
?>

<div class="bundle-card horiz <?= @$extra_classes; ?>">
    
    <?php if(!empty($images)): ?>
    <div class="bundle-image <?= @$grid; ?>">
        <?php if(count((array)$images) > 1): ?>
        <ul>
        <?php foreach($images as $key => $image): ?>

            <li>
                <img src="<?= ($key === 2 || $key === 0) ? $image->img_link : $image->thumbnail_link ?>" 
                    class="force-center-both">
            </li>
        
        <?php endforeach; ?>
        </ul>
        <?php else: ?>

        <img src="<?= $images[0]->img_link ?>" class="force-center-both">
        <?php endif; ?>
    </div>
    <a <?= empty( @$link) ? '' : 'href="'.@$link .'"' ?> class="align-middle text-center bundle-title d-none"></a>
    <div class="bundle-info"></div>
    <div class="bundle-overlay d-none"></div>
    <?php if(! empty(@$link)): ?>
    <a class="position-absolute w-100 h-100" href="<?= @$link ?>" style="top:0; left:0; z-index: 45;"></a>
    <?php endif; ?>
    <?php endif;?>
</div>

<?php else: ?>
<div class="bundle-card <?= @$extra_classes; ?>">
    <div class="bundle-image <?= @$grid; ?>">
        <?php if(count((array)$images) > 1): ?>
        <ul>
        <?php foreach($images as $key => $image): ?>

            <li>
                <img src="<?= ($key === 2 || $key === 0) ? $image->img_link : $image->thumbnail_link ?>" 
                    class="force-center-both">
            </li>
        
        <?php endforeach; ?>
        </ul>
		<?php else:	?>

        <img src="<?= $images[0]->img_link ?>" class="force-center-both">
        <?php endif; ?>
    </div>
    <div class="bundle-info"></div>
    <?php if($has_title): ?>
    <div class="bundle-overlay d-table">
        <a href="<?= @$link ?>" class="d-table-cell align-middle text-center bundle-title"><?= @$title ?></a>
    </div>
    <?php endif; ?>
    <a class="position-absolute w-100 h-100" href="<?= @$link ?>" style="top:0; left:0; z-index: 45;"></a>
</div>

<?php 
	endif; // if(($size === 'horizontal' || $size==='horiz'))
// endif; // If the images is not an array
?>
