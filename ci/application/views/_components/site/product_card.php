<?php 
	$this->load->library('utility');
    $size = $size ?? '';
    $size = strtolower($size);
	$is_wishlist_item = @$is_wishlist_item ?? FALSE;
	$reload = $reload_on_action_complete ?? FALSE;
    //Display large product card
	$product = $product ?? FALSE;
	
// Only display a product if the product has been set
if($product !== FALSE):
	
	// Product image
	$product_img = $this->utility->get_single_product_image($product->id) ?? FALSE;
	$product_img_link = @$product_img->img_link ?? base_url(PRODUCT_PLACEHOLDER_IMG);
	$product_thumb_link = @$product_img->thumbnail_link ?? base_url(PRODUCT_PLACEHOLDER_IMG);
	$product_img_alt= @$product_img->alt_text ?? $product->description;

	$avg_rating = $this->utility->get_average_rating($product->id) ?? MIN_RATING;
	

	if($size === 'large' || $size==='lg' || empty($size)):
?>
<!--TEMPLATEstart product-card-->
<div data-id="<?= $product->id; ?>" id="pr_<?= $product->id; ?>" class="product-card" <?= @$extra_attributes ?>>
	<div class="pr-image">
		<a href="<?= site_url('product/'.$product->id); ?>" class="position-absolute w-100 h-100"></a>
		<img src="<?= $product_thumb_link; ?>" class="js-im-lazyLoad force-center-both" data-src="<?= $product_thumb_link; ?>" width="100%" height="100%" data-imageorientation="">
	</div>
	<div class="pr-info d-block">
		<div class="info position-relative d-block" data-state="200-301">
			<a href="<?= site_url('product/'.$product->id); ?>" class="pr-title font-weight-bold" data-productid="<?= $product->id; ?>"><?= ucfirst($product->product_name); ?></a>
			<h6 class="pr-prize mb-0"><?= '$'.$product->unit_price; ?></h6>
			<!--TEMPLATEstart rating-->
			<?php 
            $rating_data = array(
                'extra_classes' => 'd-inline-block',
                'rating' => $avg_rating
            );

            $this->load->view('_components/ratings', $rating_data);
            ?>
			<!--TEMPLATEend rating-->
			<div class="pr-about text-truncate">
				<?= $product->description; ?>
			</div>
		</div>
	</div>
	<div class="pr-action">
		<div class="container">
			<a href="#!" class="inline-btn float-right js-infoClose d-none mb-2">View less</a>
			
			<a href="#!" class="inline-btn float-right js-infoOpen mb-2">View more</a>
		</div>

		<div class="row w-100 m-0 pr-action-row my-auto pt-0">
			<div class="col">
				<!-- TEMPLATEstart button-add-to-wishlist-->
				<?php
                    $data = array(
                        'extra_classes' => 'py-2 px-1 p-xl-2 _small btn-outline float-lg-left',
                        'responsive' => FALSE, #Always use caps in Codeigniter for boolean/NULL
                        'attributes' => ''
                    );
                    
                    $this->load->view('_components/site/button_add-to-wishlist.php', $data);
                ?>
					<!-- TEM PLATEend button-add-to-wishlist-->
			</div>
			<div class="col-lg-3 mt-lg-0 mt-2">
				<!-- TEMPLATEstart button-add-to-cart-->
				<?php
                    $data = array(
                        'extra_classes' => 'p-2 _small float-lg-right js-addToCart',
						'responsive' => FALSE,
						'attributes' => ''
                        // 'attributes' => 'onclick="yo.ui.addToCart('.$product->id.', this)"'
                    );
                    
                    $this->load->view('_components/site/button_add-to-cart.php', $data);
                ?>
					<!-- TEMPLATEend button-add-to-cart-->
			</div>
		</div>

	</div>
</div>
<!--TEMPLATEend product-card-->
<?php else: ?>
<!--TEMPLATEstart product_card-small-->
<div data-id="<?= $product->id; ?>" id="pr_<?= $product->id; ?>" class="product-card _small <?= @$extra_classes ?>" <?= @$extra_attributes ?>>
	<div class="pr-image">
		<a href="<?= site_url('product/'.$product->id) ?>" 
			class="position-absolute w-100 h-100"
			title="<?= $product->product_name ?>"></a>
		<img src="<?= $product_thumb_link ?>" 
			class="js-im-lazyLoad force-center-both"
			data-src="<?= $product_img_link ?>" 
			width="100%" height="100%">
	</div>
	<div class="pr-info">
		<div class="info" data-state="200-301">
			<a href="<?= site_url('product/'.$product->id) ?>" 
				class="pr-title"
				title="<?= $product_img_alt ?>"
				data-product-id="<?= $product->id; ?>"><?= ucfirst($product->product_name); ?></a>
			
			<h3 class="pr-price"><?= '$'.$product->unit_price; ?></h3>
			<!--TEMPLATEstart rating-->
			<?php 
            $rating_data = array(
                'extra_classes' => 'd-inline-block mx-auto my-0',
                'rating' => $avg_rating
            );

            $this->load->view('_components/ratings', $rating_data);
            ?>
			<!--TEMPLATEend rating-->
		</div>
	</div>
	<div class="pr-action border-0 rounded-0">
		<div class="row w-100 m-0">
			<div class="col">
				<a href="#!" data-reload="<?= $reload ?>" class="btn-inline text-primary float-left px-1 <?= $is_wishlist_item ? 'js-removeFromWishlist' : 'js-addToWishlist' ?>">
					<i class="fa fa-heart<?= $is_wishlist_item ? '' : '-o' ?>"></i>
				</a>
			</div>
			<div class="col">
				<a href="#" class="btn-inline text-primary float-right px-1 js-addToCart">
					<i class="fa fa-cart-plus"></i>
				</a>
			</div>
		</div>
	</div>
</div>
<!--TEMPLATEend product_card-small-->
<?php 
	endif; 
endif;	
?>
