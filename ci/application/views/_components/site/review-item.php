<?php
$is_highlight = @$is_highlight ?? FALSE;
?>
<div class="review-item <?= @$extra_classes ?>">

    <?php if($is_highlight) {
        $this->load->view('_components/ratings', @$rating);
    } ?>

    <?php if(is_array($comment)): ?>
    <p class="mb-2 review <?= $comment[1] ?>"> <?= $comment[0] ?> </p>
    <?php else: ?>
    <p class="mb-2 review"> <?= $comment ?> </p>
    <?php endif; ?>
    <div class="review-footer pl-2">
        <?php if(! $is_highlight) {
            // $this->load->view('_components/ratings', @$rating);
        } 
        if(isset($user)):?>
        <div class="user-item p-0">
            <div class="user-container">
                <p class="user-name">
                    <a href="#!" class="text-info d-block"><?= @$user['name'] ?></a>
                    <span class="d-inline-block"><?= @$user['location'] ?></span>
                    <span class="p-0"><em><?= @$user['date'] ?></em></span></p>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>