<?php
$product_thumb_link = @$image->thumbnail_link ?? base_url(PRODUCT_PLACEHOLDER_IMG);
?>
<!--TEMPLATE CART -ITEM-->
<div class="cart-item" data-id="<?= $id ?>" id="wi_<?= $id ?>" data-base-amount="<?= $product->unit_price ?>">
	<div class="card d-flex flex-sm-row flex-column flex-nowrap border-0">
		<div class="flex-grow-0 flex-shrink-0 yo-cart-item-image">
			<img class="text-center align-middle float-right h-100" src="<?= $product_thumb_link ?>" />
		</div>
		<div class="card-body flex-grow-0 yo-cart-item-body pl-3 py-2 pr-0">
			<div class="row">
				<div class="col-12">
					<h4 class="card-title pt-1 pt-md-2"><?= $product->product_name ?></h4>
					<h6 class="card-subtitle py-1">$ <span class="d-inline js-baseAmount"><?= $product->unit_price ?></span></h6>
					<div class="card-text cart-item-details mb-1">
						<?= $product->description ?>
						<!-- <span class="ml-1 d-none">
							<i class="fa fa-circle" style="color:#6267dc;"></i>
						</span> -->
					</div>
				</div>
				<div class="col-12">
					<a class="card-link btn btn-sm btn-outline-primary px-2 js-moveTo -f-si-t-c" data-to="cart" data-from="savedItem" href="#!" title="Move to cart">
						<i class="fa fa-cart-plus pr-1 d-none d-lg-inline"></i>Move to Cart
					</a>
					<a class="card-link btn btn-sm btn-primary px-2 js-removeSavedItem" href="#!" title="Delete from your saved items">
						<i class="fa fa-trash pr-1 d-none d-lg-inline"></i>Delete
					</a>
				</div>
			</div>
		</div>
	</div>
</div>