<?php
$product_thumb_link = @$image->thumbnail_link ?? base_url(PRODUCT_PLACEHOLDER_IMG);
?>
<!--TEMPLATE CART -ITEM-->
<div class="cart-item mb20">
	<div class="d-flex">
		<div class="mr-3">
			<div class="cart-img" style='background-image:url(<?=$product_thumb_link?>)'></div>
		</div>
		<div>
			<div class="cart-item-content">
				<div class="row mb20 align-items-center">
					<div class="col-md-6">
						<div class="cart-item-name">
							<h2 class="mt0 mb0"><?=$product->product_name?></h2>
						</div>
					</div>
					<div class="col-md-6">
						<div class="cart-option text-right">
							<ul class="list-inline mb0">
									<li class="list-inline-item">
										<a  href="#" class="circle selected">
											<i class="fa fa-save"></i>
											Save To Later
										</a>
									</li>
									<li class="list-inline-item">
										<a  href="#" class="circle">
											<i class="fa fa-heart"></i>
											Move To Wishlist
										</a>
									</li>
									<li class="list-inline-item">
										<a  href="#" class="circle">
											<i class="fa fa-trash"></i>
											Remove
										</a>
									</li>
								</ul>
						</div>
					</div>
				</div>
				<div class="row mb30">
					<div class="col-md-12">
						<div class="cart-item-content">
							<p class="mb0">
							<?= $product->description?>
							</p>
						</div>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-md-6">
						<div class="select-quantity">
							<div class="d-flex">
								<div class="mr-3">
									<span>Quantity</span>
								</div>
								<div>
									<div class="quantity-dropdown">
										<select class = "js-updateItemNumber" name="number of items">
											<?php 
											for($i = 1; $i <= $data['product']->quantity;$i++){
												$selected='';
												if($i==$data['quantity'])
													$selected='selected';
												echo "<option $selected >$i</option>";	
											}
											?>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="cart-item-total text-right">
							<h4>$ <?=$product->unit_price?></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>