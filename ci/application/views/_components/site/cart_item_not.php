<?php
$product_thumb_link = @$image->thumbnail_link ?? base_url(PRODUCT_PLACEHOLDER_IMG);
$message = $message ?? 'Not available';

?>
<!--TEMPLATE CART -ITEM-->
<div class="cart-item is_not" id="cin_<?= $id ?>" data-id="<?= $id ?>" data-base-amount="<?= $product->unit_price ?>">
    <div class="item-overlay position-absolute d-flex flex-column justify-content-center align-items-center border">
        <h4 class="font-weight-light text-center mb-3 pt-2"> <?= $message ?> </h4>
        <div class="d-inline-block">
            <?php  if(is_array($action)) : foreach($action as $btn):?>
            <a href="#!" class="btn btn-primary my-btn-primary <?= @$btn['extra_classes'] ?>" <?= @$btn['extra_attributes'] ?> >
                <?= @$btn['text'] ?>
            </a>
            <?php endforeach; endif; ?>
        </div>
    </div>
	<div class="card d-flex flex-sm-row flex-column flex-nowrap border-0">
		<div class="flex-grow-0 flex-shrink-0 yo-cart-item-image">
			<img class="text-center align-middle float-right h-100" src="<?= $product_thumb_link ?>" />
		</div>
		<div class="card-body flex-grow-0 yo-cart-item-body pl-3 py-2 pr-0">
			<div class="row">
				<div class="col-12">
					<h4 class="card-title pt-1 pt-md-3"><?= $product->product_name ?></h4>
					<div class="card-text cart-item-details mb-1">
						<?= $product->description ?>
						<!-- <span class="ml-1 d-none">
							<i class="fa fa-circle" style="color:#6267dc;"></i>
						</span> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
