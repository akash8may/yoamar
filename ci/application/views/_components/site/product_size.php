<div class="form-group yo-product-variable yo-size /*d-inline-block mr-3*/ m-0 my-1">
    <label class="text-primary-dark mb-1 d-block">Sizes available</label>
    <select class="form-control w-auto text-primary-dark" 
            name="product-size-number">
        <option value="0" selected="">Select</option>
        
        <?php for($i = 9; $i < 12; $i++):  ?>
        <option value="<?= $i ?>"><?= $i ?></option>
        <?php endfor; ?>
    </select>
</div>