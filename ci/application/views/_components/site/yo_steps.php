<?php 
    $active_step = @$active_step ?? 1;
    $state_success = array(
        'class' => 'btn-success',
        'attributes' => ''
    );
    $state_active = array(
        'class' => 'btn-primary',
        'attributes' => ''
    );
    $state_default = array(
        'class' => 'btn-default',
        'attributes' => "disabled='disabled'"
    );

    switch (@$active_step) {
        case 1:
            $step_1_state = $state_active;
            $step_2_state = $state_default;
            $step_3_state = $state_default;

            break;
        case 2:
            $step_1_state = $state_success;
            $step_2_state = $state_active;
            $step_3_state = $state_default;

            break;
        case 3:
            $step_1_state = $state_success;
            $step_2_state = $state_success;
            $step_3_state = $state_active;
            break;
        
        default:
            $step_1_state = $state_default;
            $step_2_state = $state_default;
            $step_3_state = $state_default;
            break;
    }
?>
<div class="yo-steps d-table position-relative w-100">
    <div class="yo-step-row d-table-row _3-step">
        <div class="yo-step d-table-cell position-relative text-center">
            <button type="button" class="btn <?= $step_1_state['class']; ?> btn-circle" <?= $step_1_state['attributes']; ?>>1</button>
            <p>Login</p>
        </div>
        <div class="yo-step d-table-cell position-relative text-center">
            <button type="button" class="btn <?= $step_2_state['class']; ?> btn-circle" <?= $step_2_state['attributes']; ?>>2</button>
            <p>Shipping </p>
        </div>
        <div class="yo-step d-table-cell position-relative text-center">
            <button type="button" class="btn <?= $step_3_state['class']; ?> btn-circle" <?= $step_3_state['attributes']; ?>>3</button>
            <p>Payment</p>
        </div>
    </div>
</div>