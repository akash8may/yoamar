<?php

// $compArgs = array(
//     'class' => '',
//     'attributes' => '',
//     'responsive' => FALSE #Always use caps in Codeigniter for boolean/NULL
// );
?>
<a href="#" 
    class="btn btn-primary my-btn-primary <?= get_value_or_default(@$extra_classes); ?>" 
    <?= get_value_or_default(@$attributes); ?>>
    <i class="fa fa-cart-plus <?= $responsive ?? 'd-inline-block'; ?>"></i>
    <?= ($responsive ? "<span class='d-md-none'> Add to cart</span>" : "Add to cart"); ?>
</a>