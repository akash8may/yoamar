<?php
$product_thumb_link = @$image->thumbnail_link ?? base_url(PRODUCT_PLACEHOLDER_IMG);
$order = $order ?? NULL;
$product_id = $order->product_id ?? $id ?? NULL;
$reversed_product_count = 0;
$return_period = 0;
if(isset($order->date_delivered))
{
    $date_delivered = new Moment\Moment($order->date_delivered, 'CET');
    $today = $date_delivered->fromNow();
    $return_period = $today->getDays();
}

if(isset($order))
{
    $paid_order = ($order->paid === '1') ? 'PAID '.$order->quantity.' items' : 'PAYMENT PENDING';
    $paid_order_class = ($order->paid === '1') ? 'text-success' : 'text-error';
    $product_id = $order->product_id;
    $reversed_product_count = (int) $order->reversed_quantity;
    $reversed_all_counts = ($reversed_product_count == (int) $order->quantity) ? TRUE : FALSE;
} else {
    return;
}

// TEMPLATEstart CART -ITEMsmall
?>
<div class="cart-item _small" data-id="<?= $product_id; ?>" id="pr_<?= $product_id; ?>">
    <div class="card d-flex flex-row flex-nowrap border-0 dropleft">
        <div class="flex-grow-0 flex-shrink-0 yo-cart-item-image rounded-0">
            <img src="<?= $product_thumb_link ?>" height="100%" class="card-img-top w-auto text-center align-middle">
        </div>
        <div class="card-body flex-grow-0 yo-cart-item-body pt-2 pb-0">
            <h5 class="card-title"><?= $order->product_name ?></h5>
            <h6 class="text-muted card-subtitle mb-2 <?= @$paid_order_class ?> font-italic"><?= @$paid_order ?></h6>
            <h6 class="text-muted card-subtitle mb-2 text-primary-dark">$ <?= ((float)@$order->sales_amount * (int)@$order->quantity) ?> ( $ <?= @$order->sales_amount ?> each )</h6>
            
            <?php if($order->is_cancelled == '1' || $reversed_product_count > 0): ?>
            <h5 class="text-danger text-uppercase">cancelled <?= ($reversed_all_counts) ? '' : $reversed_product_count.' item'.(($reversed_product_count > 1) ? 's' : '') ?></h5>
            <?php endif; 
            if($order->status == 'delivered' && $return_period <= ORDER_REVERSAL_PERIOD): ?>
            <a class="btn btn-outline-primary float-left py-0" href="<?= site_url('u/orders/return/'.@$order->order_id.'/'.$product_id) ?>">return item</a>
                <?php endif; ?>
            
        </div>
        
        <a class="card-action position-absolute text-black-50 px-2" id="dropdownMenu_pr_<?= $product_id; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-ellipsis-v"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu_pr_<?= $product_id; ?>">
            <?php if($order->status == 'delivered'): ?>
            <a class="dropdown-item text-right" href="<?= site_url('u/review/'.$product_id) ?>">Review</a>
            
            <?php endif; if($order->status != 'delivered'): ?>
            <a class="dropdown-item text-right" href="<?= site_url('u/orders/cancel/'.$order->order_id.'/'.$product_id) ?>">Cancel ordered item</a>
            <?php elseif($order->status == 'delivered' && $return_period <= ORDER_REVERSAL_PERIOD): ?>
            <a class="dropdown-item text-right" href="<?= site_url('u/orders/report/'.$order->order_id.'/'.$product_id) ?>">Report</a>
            <?php endif; ?>
        </div>
    </div>
</div>
<!--TEMPLATEsmall CART -ITEM-->