<div class="input-group yo-quantity-changer" data-id="<?= @$product_id ?>">
    <div class="input-group-prepend">
        <a href="#!" class="input-group-text bg-secondary26 text-primary87 js-minusProductQuantity"><i class="fa fa-minus"></i></a>
    </div>
    <input type="number" min="0" inputmode="numeric" class="form-control form-control-sm" value="<?= @$quantity ?>" />
    <div class="input-group-append">
        <a href="#!" class="input-group-text bg-secondary26 text-primary87 js-addProductQuantity"><i class="fa fa-plus"></i></a>
    </div>
</div>