<?php 
	//TODO: Allow customizing of the title and message
?>
<div class="modal" id="confirmDelete">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Confirm Delete Account?</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <p class="lead">Are you sure you want to delete the account? <b>This action is irreversible.</b></p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-warning btnConfirmDelete">Confirm</button>
      </div>
    </div>
  </div>
</div>
