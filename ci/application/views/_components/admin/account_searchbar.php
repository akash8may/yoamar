<?php defined('BASEPATH') OR exit('No direct script access allowed');
	if(FALSE): # Acting as a placeholder commenting out of this code
		$this->load->helper('form'); 
?>
<?= form_open('',['method'=>'GET','class'=>'mb-4']); ?>
<div class="input-group">
	<!-- <div class="input-group-prepend">
		<?php $is_active = $this->input->get('active'); ?>
		<select name="active">
			<option value="" <?= get_if_empty($is_active,'selected'); ?>>All</option>
			<option value="1" <?= get_selected($is_active,'1'); ?>>Active</option>
			<option value="0" <?= get_selected($is_active,'0'); ?>>Inactive</option>
		</select>
	</div> -->
	<input class="form-control" name="q" type="search" placeholder="Search accounts" value="<?= $this->input->get('q') ?? ''; ?>">
	<div class="input-group-append">
		<button class="btn btn-primary" type="submit">
			<i class="d-inline d-md-none fa fa-search"></i>
			<span class="d-none d-md-inline">
				<?= get_value_or_default(@$btn_text,'Search'); ?>
			</span>
		</button>
	</div>
</div> 
<?= form_close(); ?>
<?php endif;?>
