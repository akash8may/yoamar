<?php
/* PROPS
$btn_extra_class
$btn_size
$btn_type
$btn_icon
$btn_link
$btn_title
$btn_text
$btn_id
$btn_extra_props
*/
	$this->load->view('_components/admin/buttons/btn_icon',array(
		'btn_link'=>@$btn_link,
		'btn_type'=>'btn-dark',
		'btn_text'=>'RETURN ORDER',
		'btn_size'=>'',
		'btn_icon'=>'',
		'btn_title'=>get_value_or_default(@$btn_title,'Return this order'),
	));
?>
