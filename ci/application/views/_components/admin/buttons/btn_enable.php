<?php

	// Inherited variables
	$btn_is_enable = $btn_is_enable ?? FALSE; # Whether or not this button is an enable button
	$btn_is_enable = (bool)$btn_is_enable; # Cast to boolean so it is always either TRUE or FALSE
	$btn_js_class = $btn_js_class ?? '';
	$btn_link = $btn_link ?? '#';
	
	//Local variables ~ private variables
	$_btn_text = $btn_is_enable ? 'Enable' : 'Disable'; # Set text based on button type (enable/disable)
	$_btn_icon = $btn_is_enable ? 'fa-check' : 'fa-remove';
	$_btn_title = $btn_is_enable ? 'Enable account' : 'Disable account';
	$_btn_class = $btn_is_enable ? 'btn-success' : 'btn-warning';#Success or warning button

	$this->load->view('_components/admin/buttons/btn_icon',array(
		'btn_is_enable'=>$btn_is_enable,
		'btn_type'=>$_btn_class,
		'btn_type'=>$_btn_class,
		'btn_link'=>@$btn_link,
		'btn_text'=>$_btn_text,
		'btn_size'=>'sm',
		'btn_icon'=>$_btn_icon,
		'btn_extra_class'=>@$btn_extra_class,
		'btn_title'=>$_btn_title,
	));
	
?>
