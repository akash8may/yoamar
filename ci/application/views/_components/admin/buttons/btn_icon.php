<?php // A basic icon button ~ Commonly used in other components 

	//Properties
	/** Any extra classes this button needs */
	$btn_extra_class = $btn_extra_class ?? ''; 

	/** Bootstrap button size: sm(defaults to this),lg - setting this to '' means default size. */
	$btn_size = strtolower($btn_size) ?? 'sm'; 

	/** Bootstrap button color class. Default is 'btn-dark if not specified or left empty'*/
	$btn_type = get_value_or_default(@$btn_type,'btn-dark');
	
	/** Fontawesome icon class for the button. Leaving this blank means no icon */
	$btn_icon = $btn_icon ?? 'fa-exclamation';

	/** Link the button will point to: not setting defaults to href='#'  */
	$btn_link = $btn_link ?? '#'; 

	/** Text used in tooltip. Leaving this blank means no tooltip will be displayed*/
	$btn_title = $btn_title ?? ''; 
	
	/** Text displayed on the button */
	$btn_text = $btn_text ?? ''; 
	
	/** Id of the button. Leaving this blank means the button has no id */
	$btn_id = $btn_id ?? '';

	$btn_extra_props = $btn_extra_props ?? '';
?>
<a href="<?= $btn_link; ?>" class="btn <?= $btn_type; ?> <?= get_if_not_empty($btn_size,'btn-'.$btn_size); ?> ml-lg-1 mb-2 <?= $btn_extra_class; ?>" <?= get_if_not_empty($btn_title,'data-toggle="tooltip" data-placement="bottom" title="'.$btn_title.'"');?> <?= get_if_not_empty($btn_id,'id="'.$btn_id.'"')?> <?= get_value_or_default($btn_extra_props); ?>>
	<?= get_if_not_empty($btn_icon,'<i class="fa '.$btn_icon.'"></i>'); ?>
	<?= get_if_not_empty($btn_text,'<span>'.$btn_text.'</span>'); ?> 
</a>
