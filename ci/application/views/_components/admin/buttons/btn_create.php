<?php
/* PROPS
$btn_extra_class
$btn_size
$btn_type
$btn_icon
$btn_link
$btn_title
$btn_text
$btn_id
$btn_extra_props
*/
	$this->load->view('_components/admin/buttons/btn_icon',array(
		'btn_link'=>@$btn_link,
		'btn_type'=>'btn-success',
		'btn_text'=>@$btn_text,
		'btn_size'=>'',
		'btn_extra_class'=>@$extra_class,
		'btn_extra_props'=>@$attributes,
		'btn_icon'=>'fa-plus',
		'btn_title'=>get_value_or_default(@$btn_title,'Create'),
	));
?>

