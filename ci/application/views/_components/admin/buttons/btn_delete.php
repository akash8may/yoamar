<?php
/* PROPS
$btn_extra_class
$btn_size
$btn_type
$btn_icon
$btn_link
$btn_title
$btn_text
$btn_id
$btn_extra_props
*/
	$this->load->view('_components/admin/buttons/btn_icon',array(
		'btn_type'=>'btn-warning',
		'btn_link'=>'#',
		'btn_size'=> ($btn_size ?? 'sm'),
		'btn_icon'=>'fa-trash',
		'btn_extra_class'=>@$btn_extra_class,
		'btn_id'=>@$btn_id,
		'btn_title'=> ($btn_title ?? 'Delete'),
		'btn_extra_props'=>@$btn_extra_props
	));
?>
