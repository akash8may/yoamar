<?php $stats_change = $stats_change ?? NULL;

	$has_badge = FALSE;
	$_badge_class;  # Bootstrap color class for the badge
	$_badge_icon; # Icon shown in the badge
	
	if(!isset($content_class))
	{
		$content_class = @$has_btn == TRUE ? 'display-4' : 'display-3'; #Class applied to the content in the dashboard card
	}

	if(isset($stats_change) && $stats_change != 0)
	{
		$stats_change = clamp($stats_change,-100,100);
		$_badge_class = $stats_change > 0 ? 'badge-success' : 'badge-warning';
		$_badge_icon = $stats_change > 0 ? 'fa-caret-up' : 'fa-caret-down';
	}
	else if(isset($stats_change) && $stats_change == 0)
	{
		$_badge_class = 'bg-dark26';
		$_badge_icon = 'fa-minus';
	}

	// If the content is numeric, format it to include commas when it exceeds 1000
	$content =  is_numeric($content) ? number_format($content) : $content; #1000 is 1,000 after format
?>
<div class="col-sm-6 col-lg-4 mb-4">
	<div class="card bg-secondary dashboard-card">

		<?php if(isset($card_title)): ?>
		<div class="card-header bg-dark">
			<span class="mr-2">
				<?= $card_title; ?>
			</span>

			<?php if(isset($stats_change) && $has_badge): ?>
			<span class="badge <?= $_badge_class; ?>">
				<i class="fa <?= $_badge_icon; ?> p-1"></i><?= ' '.abs($stats_change).'%' ?>
			</span>
			<?php endif;?>
		</div>
		<?php endif;?>
		<div class="card-body text-dark87 text-center">
			<h1 class="<?= $content_class ?>">
			<?= get_if_not_empty(@$content_icon,'<i class="fa '.@$content_icon.' p-2"></i>') ?> <?= get_value_or_default($content,'-');?></h1>
		</div>
		<?php if(isset($has_btn) && $has_btn == TRUE): ?>
		<div class="bg-dark text-light">
			<a class="btn btn-dark btn-block pt-2 pb-2" role="button" href="<?= get_value_or_default($btn_link,'#'); ?>"><?= get_value_or_default(strtoupper(@$btn_text),'VIEW MORE'); ?></a>
		</div>
		<?php endif;?>
	</div>
</div>
<?php 
// PROPS
// $stats_change
// $card_title
// $content_class
// $content_icon
// $content
// $has_btn
// $btn_link
// $btn_text
?>
