<?php defined('BASEPATH') or exit('No direct script access allowed');
$queries = $queries ?? NULL;

if(empty($queries)):
	$this->load->view('_templates/empty_message',array(
		'title'=>'No search queries found',
		'message'=>'Once users search for items, queries will appear here',
	));
else:
?>
<div class="table-responsive mb-4">
	<table class="table">
		<thead>
			<tr>
				<th>Search query</th>
				<th>Country</th>
				<th>City</th>
				<th>Date searched</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($queries as $query):?>
			<tr>
				<td><?= $query->search_query; ?></td>
				<td><?= !empty($query->country) ? $query->country : '--'; ?></td>
				<td><?= !empty($query->city) ? $query->city : '--'; ?></td>
				<td><?= $query->date_searched; ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
<?php endif; ?>
