<?php 
$account_type = $account_type ?? 'buyer';
$is_deletable = ($account_type == 'admin' || $account_type == 'logistics');
$accounts = $accounts ?? NULL;

$buttons = $buttons ?? [];

// If no accounts were found, display empty message
if(empty($accounts)):
	$this->load->view('_templates/empty_message',array(
		'title'=>'No accounts found',
		'message'=>'Once accounts are added, they will appear here'
	));
else:
?>
<div class="table-responsive my-4">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php 
		// Use this to prevent disabling/enabling of the first admin account
		$first_admin = $this->ion_auth->users(['1'])->row();
		foreach($accounts as $account):
		?>
			<tr class="accountData">
				<td>
					<?= $account->first_name; ?>
				</td>
				<td>
					<?= $account->last_name; ?>
				</td>
				<td>
					<?= $account->email; ?>
				</td>
				<td class="accountStatus">
					<?= $account->active ? 'Enabled' : 'Disabled'; ?>
				</td>
				<td data-account-enabled="<?= $account->active; ?>">
				<?php 
					$btn_is_active = $account->active;
					$btn_text = $btn_is_active ? 'Disable' : 'Enable';
					$btn_class = $btn_is_active ? 'btn-outline-warning' : 'btn-outline-success';
					
					// Do not display enable/disable button for first admin account
					if(!($account->id == $first_admin->id)):
				?>
					<div class="row no-gutters">
						<div class="col">
							<button class="btn btn-block <?= $btn_class; ?> btn btnToggleEnableAccount" data-user_id="<?= $account->id; ?>" data-is_active="<?= $btn_is_active ? 'true' : 'false' ?>"><?= $btn_text; ?></button>
						</div>
						<?php
							// If we are an admin or logistics account, display the delete account button 
							if($is_deletable): 
						?>
						<div class="col">
							<button class="btn btn-block btn-outline-warning btnDeleteAccount ml-1" title="Delete account" data-user_id="<?= $account->id; ?>"  data-toggle="modal"><i class="fa fa-trash"></i> Delete</button>
						</div>
						<?php 
							endif;
						?>
					</div>
					<?php else: # First admin account ~ Display text ?>
					<button class="btn btn-block" title="Cannot modify root admin account status" disabled>--</button>
				
				<?php endif; ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>
<?php endif; ?>
