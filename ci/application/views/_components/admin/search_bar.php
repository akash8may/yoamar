<form class="mb-4 row" type="GET" action="<?= get_value_or_default(@$action_url,current_url()); ?>">
	<?php if(!empty($filter_list)): ?>
	<div class="col-sm-5 col-md-4 col-lg-3 col-xl-2">
		<select class="form-control" name="filter">
			<?php foreach($filter_list as $filter_key=>$filter_item): 
				$filter_key = is_string($filter_key) ? $filter_key : ''; #Prevent numeric indices from being used as opt group
				$value = &$filter_item['value'];
				$text = &$filter_item['text'];
				
				// @ used to allow the $selected property to be optionally excluded when creating a filter item. Suppresses index not found errors
				$selected = (@$filter_item['selected'] == TRUE) ? 'selected' : ''; 
			?>
			<?= get_if_not_empty($filter_key,'<optgroup label="'.$filter_key.'">'); ?>
				<option value="<?= $value; ?>" <?= $selected; ?>>
					<?= $text; ?>
				</option>
			<?= get_if_not_empty($filter_key,'</optgroup>'); ?>
				<?php endforeach; ?>
		</select>
	</div>
	<?php endif; ?>
	<div class="col-sm-7 col-md-8 mt-2 mt-sm-0">
		<div class="input-group">
			<input class="form-control" name="q" type="search" placeholder="<?= get_value_or_default(@$placeholder_text,'Search'); ?>">
			<div class="input-group-append">
				<button class="btn btn-primary" type="submit">
					<i class="d-inline d-md-none fa fa-search"></i>
					<span class="d-none d-md-inline">
						<?= get_value_or_default(@$btn_text,'FILTER'); ?>
					</span>
				</button>
			</div>
		</div>
	</div>
</form>
