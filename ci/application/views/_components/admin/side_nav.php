<div id="sidebar-wrapper" class="bg-primary">
	<ul class="sidebar-nav" style="font-family:Raleway, sans-serif;">
		<li class="sidebar-brand pb-4"> <a href="#" style="font-size:24px;font-weight:bold;"><?= $brand_title ?? 'Admin Panel'; ?></a></li>

		<?php foreach($nav_items as $nav_key=>$nav_item):
			$active = @$nav_item['active'] ?? FALSE;	
			$active = ($active == TRUE) ? 'class="active"' : NULL;
		?>
		<li <?= $active ?? ''; ?>>
			
			<a href="<?= $nav_item['link'];?>"><?= $nav_item['text'];?></a>
		</li>
		<?php endforeach;?>

	</ul>
</div>
