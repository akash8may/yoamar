<?php 
	$is_dynamic = $is_dynamic ?? FALSE;
	$is_readonly = $is_readonly ?? TRUE;

	$id = $id ?? NULL;
	$rating = $rating ?? NULL;

	$show_text = $show_text ?? FALSE;
	$show_text = (bool)$show_text;# Ensure boolean values

	if($rating !== 0)
	{
		$rating = get_if_not_empty($rating,clamp($rating,MIN_RATING,MAX_RATING),MIN_RATING);
	}
?>
<a href="#" class="yo-rating-link mr-2 <?= get_value_or_default(@$extra_classes, 'd-inline-block'); ?>" 
	<?= get_if_not_empty($id,'id="'.$id.'"'); ?>> 
	<?php //TODO: Make this a parameter if it changes ?>
    <ul class="product-ratings list-inline text-primary d-inline-block <?= ($is_readonly) ? '' : 'js-review' ?>" data-rating="<?= $rating; ?>">
		<?php for($i=0;$i<MAX_RATING;$i++): 
			if(($rating-$i)<0.3)
			{
				$star_class = 'fa fa-star-o';
			}
			//Displaying half star if the rating value is close to half
			else if(($rating-$i)>=0.3 && ($rating-$i)<=0.75)	
			{
				$star_class = 'fa fa-star-half-o';
			}
			else
			{
				$star_class = 'fa fa-star';
			}
		?>
        <li class="list-inline-item m-0 p-0" data-index="<?= $i+1; ?>">
			<i class="<?= $star_class; ?> <?= get_value_or_default(@$extra_classes); ?>"></i>
		</li>
		<?php endfor; ?>
    </ul>
	<?php if($show_text === TRUE): ?>
		<span class="text-primary-dark87"><?= $rating.'/'.MAX_RATING; ?></span>
	<?php endif;?>
</a>
<?php if($is_dynamic): ?>
<input type="number" hidden id="input_rating" value="<?= $rating; ?>">
<?php endif;?>