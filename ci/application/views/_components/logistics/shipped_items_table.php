<?php defined('BASEPATH') or exit('No direct script access allowed');
$items = $items ?? NULL;

if(empty($items)):
	$this->load->view('_templates/empty_message',array(
		'title'=>'No shipped items found',
		'message'=>'Once items are shipped, they will appear here',
		'button'=>array(
			'text'=>'Ship items now',
			'link'=>site_url('logistics/deploy')
		)
	));
else:
?>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Tracking ID</th>
						<th>Product name</th>
						<th>Quantity</th>
						<th>Merchant</th>
						<th>Date shipped</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($items as $item):?>
					<tr>
						<td><?= $item->tracking_id; ?></td>
						<td><?= $item->product_name; ?></td>
						<td><?= $item->quantity; ?></td>
						<td><?= $item->merchant_company; ?></td>
						<td><?= $item->date_picked; ?></td>
						<td><?= $item->item_status; ?></td>
						<td><button class="btn btn-dark js-recall-item">Recall</button></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
<?php endif; ?>
