<?php defined('BASEPATH') or exit('No direct script access allowed');
$items = $items ?? NULL;

if(empty($items)):
	$this->load->view('_templates/empty_message',array(
		'title'=>'No warehouse items found',
		'message'=>'Once items are added to the warehouse, they will appear here',
		'button'=>array(
			'text'=>'Add items to warehouse',
			'link'=>site_url('logistics/add')
		)
	));
else:
?>
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>Warehouse ID</th>
				<th>Product name</th>
				<th>Quantity</th>
				<th>Merchant</th>
				<th>Date added</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($items as $item):?>
			<tr>
				<td><?= $item->id; ?></td>
				<td><?= $item->product_name; ?></td>
				<td><?= $item->quantity; ?></td>
				<td><?= $item->merchant_company; ?></td>
				<td><?= $item->date_added; ?></td>
				<td>
					<button class="btn btn-dark" type="button" data-toggle="modal" data-target="#modalConfirmReturnItem" data-id="<?= $item->id; ?>">Return</button>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>
<?php endif; ?>
