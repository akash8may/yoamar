<div class="container">
	<div class="row mb-4 mb-sm-5">
		<?php  
			// Warehouse items
			$this->load->view('_components/admin/dashboard_card',array(
				'stats_change'=> 0,#TODO: Make dynamic
				'content'=> count($warehouse_items),#TODO: Make dynamic
				'card_title'=>'Total warehouse items',
				'content_icon'=>'fa-archive',
				'has_btn'=>TRUE,
				'btn_link'=>site_url('logistics/warehouse'),
				'btn_text'=>'View warehouse items',
			));

			// Items being shipped
			$this->load->view('_components/admin/dashboard_card',array(
				'stats_change'=> 0,#TODO: Make dynamic
				'content'=> count($shipped_items),#TODO: Make dynamic
				'card_title'=>'Total items shipped',
				'content_icon'=>'fa-truck',
				'has_btn'=>TRUE,
				'btn_link'=>site_url('logistics/shipped'),
				'btn_text'=>'View shipped items',
			));

			// Delivered items items
			$this->load->view('_components/admin/dashboard_card',array(
				'stats_change'=> 0,#TODO: Make dynamic
				'content'=> count($delivered_items),#TODO: Make dynamic
				'card_title'=>'Total delivered items',
				'content_icon'=>'fa-list-alt',
				'has_btn'=>TRUE,
				'btn_link'=>site_url('logistics/shipped?delivered=1'),
				'btn_text'=>'View delivered items',
			));
		?>
	</div>
	<div class="form-group">
		<h3>Latest warehouse items</h3>
		<?php 
			$this->load->view('_components/logistics/warehouse_items_table',array(
				'items'=>@$warehouse_items
			));
		?>
	</div>
	<div class="form-group">
		<h3>Latest shipped items</h3>
		<?php 
			$this->load->view('_components/logistics/shipped_items_table',array(
				'items'=>@$shipped_items
			));
		?>
	</div>
</div>
