<div class="container px-xl-0" style="">
	<div class="row">
		<div class="col-12 col-lg-8 offset-lg-2">
			<div class="user-item item-large pb-5">
				<div class="user-container bg-light shadow-none border-0 p-0">
					<p class="text-center mb-0 d-block">
                        <a class="user-avatar float-none text-center d-inline-block mx-auto">
                        <?php $thumbnail_link = @$user->picture_url ?? base_url(PRODUCT_PLACEHOLDER_IMG); ?>
							<img src="<?= $thumbnail_link ?>" class="rounded-circle img-fluid" /></a></p>
					<p class="user-name text-center" style="white-space: normal;">
                        <a class="font-weight-light"><?= @$user->first_name.' '.@$user->last_name ?></a><br>
                        <span><i class="fa fa-map-marker mr-2"></i>Nairobi, KE</span><br/>
						<span><i class="fa fa-building-o mr-2"></i><?= @$user->company ?></span></p>
					
                    <p class="text-center">
                        <a class="btn btn-outline-primary mr-4 rounded btn-lg text-right border-0 my-2 mr-1" role="button" href="<?= site_url('logistics/profile/edit') ?>"><i class="fa fa-edit"></i>edit profile</a>
				</div>
			</div>
		</div>
	</div>
</div>
