<div class="container">
	<div class="form-group">
	<?= form_open('',['method'=>'GET']); ?>
		<div class="input-group w-100">
			<input class="form-control" name="q" type="search" placeholder="Find shipped items" value="<?= $this->input->get('q') ?? '' ;?>" id="jsWarehouseItems" required>
			<div class="input-group-append">
				<button class="btn btn-primary" type="submit">Search</button>
			</div>
		</div>
	<?= form_close(); ?>
	</div>
	<div class="form-group">
		<?php 
			$this->load->view('_components/logistics/warehouse_items_table',array(
				'items'=>@$warehouse_items
			));
		?>
	</div>
</div>
