<div class="container">
	<div class="form-group">
		<div class="alert alert-success" role="alert"><span>Add a product by typing a product name and selecting the desired
				product from the dropdown</span></div>
	</div>
	<div class="form-group">
		<?= form_open('',['method'=>'GET']); ?>
		<div class="row no-gutters">
			<div class="col-7 col-lg-8">
				<div class="form-group px-1 position-relative">
					<input type="text" id="jsWarehouseSearch" placeholder="Enter product name" class="form-control">
					<!-- <div class="shadow yo-dropdown position-absolute show">
						<div class="dropdown-item">
							<p>Ancient timepiece <span>MIGATI5</span></p>
						</div>
					</div> -->
				</div>
			</div>
			<div class="col-5 col-lg-2">
				<div class="form-group px-1">
					<input type="number" placeholder="Quantity" min="1" max="1000" class="form-control">
				</div>
			</div>
			<div class="col-12 col-lg-2">
				<div class="form-group px-1">
					<button class="btn btn-success btn-block" type="button" data-product-id=""><i class="fa fa-plus mr-1"></i>Add product</button>
				</div>
			</div>
		</div>
		<?= form_close(); ?>
	</div>
	<div class="form-group">
		<h3>Product details</h3>
		<div class="row">
			<div class="col">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Product ID</label>
								<p data-form-group="product_id"></p>
								</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Product name</label>
								<p data-form-group="product_name"></p>
								</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Unit Price</label>
								<p data-form-group="unit_price"></p>
								</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Merchant</label>
								<p data-form-group="merchant_company"></p>
								</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label>Description</label>
								<div data-form-group="description"></div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
