<div class="container">
	<div class="form-group">
		<div class="row no-gutters">
			<div class="col-6 col-md-2 px-1">
				<div class="form-group">
					<input type="text" placeholder="Transaction id" class="form-control js-product-name" title="Order transaction id" data-toggle="tooltip" data-placement="bottom">
				</div>
			</div>
			<div class="col-6 px-1">
				<div class="form-group">
					<input type="text" placeholder="Enter product name" class="form-control js-product-name">
				</div>
			</div>
			<div class="col-6 col-md-2 px-1">
				<div class="form-group">
					<select class="form-control" name="status" title="Status of the items" data-toggle="tooltip" data-placement="bottom">
						<optgroup label="Item status">
							<option value="new">New</option>
							<option value="refurbished">Refurbished</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="col-6 col-md-2 px-1">
				<div class="form-group">
					<button class="btn btn-success btn-block" type="button"><i class="fa fa-plus mr-1"></i>Add item</button>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Order ID</th>
						<th>Product name</th>
						<th>Quantity</th>
						<th>Merchant</th>
						<th>Status</th>
						<th>Date added</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Ks2VU-2t</td>
						<td>Samsung note 8</td>
						<td>10</td>
						<td>Samsung Electronics</td>
						<td>Refurbished</td>
						<td>12/12/2018</td>
						<td><button class="btn btn-dark" type="button" data-toggle="modal" data-target="#modalConfirmRemoveItem">Remove</button></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="form-group">
		<p class="text-center"><button class="btn btn-dark btn-lg" type="button" title="Ship the items listed above">SHIP
				ITEMS</button></p>
	</div>
</div>
