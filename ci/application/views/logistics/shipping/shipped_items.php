<div class="container">
	<div class="form-group">
		<?= form_open('',['method'=>'GET']); ?>
		<div class="input-group w-100">
			<input class="form-control" name="q" type="search" placeholder="Find shipped items" value="<?= $this->input->get('q') ?? ''; ?>" id="jsShippedItems" required>
			<?php if($is_delivered = $this->input->get('delivered')): ?>
			<input type="number" name="is_delivered" value="<?= $is_delivered; ?>" hidden>
			<?php endif;?>
			<div class="input-group-append">
				<button class="btn btn-primary" type="submit">Search</button>
			</div>
		</div>
		<?= form_close(); ?>
	</div>
	<div class="form-group">
		<?php 
			$this->load->view('_components/logistics/shipped_items_table',array(
				'items'=>@$shipped_items
			));
		?>
	</div>
</div>
