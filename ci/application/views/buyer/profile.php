<?php
$data = array(
    'has_dropdown' => FALSE
);

$this->load->view('_templates/header_base', $data);
?>
<main>
<?php

$this->load->view('_templates/profile_header', $user);
?>
    <div class="container yo-content px-0">
        <div class="yo-buyer-orders p-2 border my-3" id="orders">
            <h6 class="mb-3">Your orders</h6>
            <div id="buyer_orders">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" role="tab" data-toggle="tab" href="#tabOrders">ACTIVE ORDERS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" role="tab" data-toggle="tab" href="#tabHistory">PURCHASE HISTORY</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="tabOrders">
                        <?php if(!empty($orders) && is_array($orders)): ?>
                        <div class="table-responsive table-borderless border bg-light shadow-sm">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>orders</th>
                                        <th class="d-none d-md-table-cell">receipt number</th>
                                        <th>date ordered</th>
                                        <th>delivery date est.</th>
                                        <th>STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php // LOOP
                                    foreach($orders as $key => $order): 
                                        if($order->status !== 'delivered'):
                                    ?>
                                    <tr>
                                        <td><?= $order->product_name ?></td>
                                        <td class="d-none d-md-table-cell"><?= $order->transaction_id ?></td>
                                        <td><?= humanize_date($order->date_ordered) ?></td>
                                        <td><?= $order->shipping_date ?></td>
                                        <td class="text-primary-dark"><?= $order->status ?></td>
                                    </tr>
                                    <?php if($key > 12): ?>
                                    <tr>
                                        <td colspan="5">
                                            <button class="btn btn-primary my-btn-primary js-viewActiveOrders mx-auto d-block" type="button">view all
                                                <i class="fa fa-chevron-down pl-2"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <?php endif; endif; endforeach; 
                                    // LOOPend 
                                    ?>

                                </tbody>
                            </table>

                        </div>
                        <?php else:
                            $this->load->view('_templates/empty_message', array(
                                'title' => 'You currently have not placed any order',
                                'message' => 'When you checkout on your cart, your orders will appear here.',
                                'button' => ''
                            ));
                        endif; ?>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="tabHistory">
                        <?php if(!empty($orders) && is_array($orders)): ?>
                        <div class="table-responsive table-borderless border bg-light shadow-sm">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>orders</th>
                                        <th class="d-none d-md-table-cell">receipt number</th>
                                        <th>date ordered</th>
                                        <th>delivered date</th>
                                        <!-- <th>STATUS</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                <?php // LOOP
                                    $delivered_orders_count = 0;
                                    foreach($orders as $key => $order): 
                                        if($order->status === 'delivered'):
                                            $delivered_orders_count += 1;
                                    ?>
                                    <tr>
                                        <td><?= $order->product_name ?></td>
                                        <td class="d-none d-md-table-cell"><?= $order->transaction_id ?></td>
                                        <td><?= $order->date_ordered ?></td>
                                        <td><?= $order->shipping_date ?></td>
                                        <td class="text-primary-dark"><?= $order->status ?></td>
                                    </tr>
                                    <?php if($key > 12): ?>
                                    <tr>
                                        <td colspan="5">
                                            <button class="btn btn-primary my-btn-primary js-viewActiveOrders mx-auto d-block" type="button">view all
                                                <i class="fa fa-chevron-down pl-2"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <?php endif; endif; endforeach; 
                                    // LOOPend 
                                    if ($delivered_orders_count === 0) : ?>
                                    <tr><td colspan="5">
                                    <?php
                                        $this->load->view('_templates/empty_message', array(
                                            'title' => 'You currently have no orders delivered to you',
                                            'message' => 'When your shipped orders are delivered, the shipped products will appear here',
                                            'button' => ''
                                        ));
                                    ?>
                                    </td></tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php else:
                            $this->load->view('_templates/empty_message', array(
                                'title' => 'You currently have not placed any order',
                                'message' => 'When you checkout on your cart, your orders will appear here.',
                                'button' => ''
                            ));
                        endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container yo-content px-0">
        <div class="row">
            <div class="col-12 col-lg-8">
                <h6 class="py-2">MY WISHLIST</h6>
                <div class="row">
                    <?php 
                    // LOOP 
                    // var_dump($wishlist);
                    foreach($wishlist as $key => $item):
                        $product = $this->utility->get_single_product($item->product_id);
                        if(!empty($product)):
                    ?>
                    <div class="col-6 col-sm-4 col-md-3 mb-3">
                        <?php 
                        // TEMPLATEstart product-card _small

                        $this->load->view('_components/site/product_card', array(
                            'size' => 'sm',
                            'is_wishlist_item' => TRUE,
                            'reload_on_action_complete' => TRUE,
                            'product' => $product
                        ));

                        // TEMPLATEend product-card _small
                        ?>
                    </div>

                    <?php endif; endforeach; 
                    // LOOPend 
                    ?>
                </div>
            </div>
            <div class="col-12 col-lg-4 p-0">
                <div class="d-block px-3 bg-info12 pb-3 pt-2">
                    <h6 class="pb-3 pt-1">NOTIFICATIONS</h6>
                    <?php if(!empty($new_notifications)) : 

                        $this->load->view('_components/site/notification_list', array(
                            'notifications' => @$new_notifications,
                            'in_timeline' => FALSE,
                            'extra_classes' => '',
                            'type' => 'new' #all||new||old (no need for this though since we have the ->is_read property)
                        )); 
                    
                    else: ?>

                    <div class="row user-list h-75">
                        <div class="col-12">
                            <p class="text-center text-muted py-5">NO NEW NOTIFICATIONS</p>
                        </div>
                    </div>

                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>
