<?php
$data = array();

$this->load->view('_templates/header_base', $data);
?>
<main>
	<div class="container yo-content yo-fluid-container">
		<div class="yo-content-header mb-4">
			<p class="d-block text-primary-dark87 yo-title text-center">CHECKOUT</p>
		</div>
		<div class="row mx-0">

			<div class="col-10 mb-2 offset-lg-1">
				<!-- TEMPLATEstart YO-STEPS -->
				<?php
                $data = array(
                    'active_step' => 2
                );

                $this->load->view('_components/site/yo_steps', $data);
                ?>
				<!-- TEMPLATEend YO-STEPS -->
			</div>
			<?php if(!empty(@$message)) :?> 
			<div class="col-12 col-lg-11 offset-lg-1">
				<div id="infoMessage" class='col-10 offset-1 col-md-8 offset-md-2 p-4 bg-info12 border'><?= $message ?></div>
			</div>
			<?php endif; ?>
			<div class="col-12 col-lg-6 mx-auto offset-lg-1 position-relative">
				<h5 class="py-3 px-2 font-weight-bold">Shipping Information
				</h5>
				
				<?php 
					$input_classes = array('class' => 'form-control col-12 col-sm-9');
					$billing_input_classes = array('class' => 'form-control mb-2');
					echo form_open("u/shipping".@$redirect);
					?>
						<div class="form-row">
							<div class="col-12">
								<h6 class="py-3 px-2 bg-info12">Billing Information
									<br />
								</h6>
							</div>
							<div class="col-12">
								<div class="form-group row mx-0 align-items-center">
									<label class="col-12 col-sm-3 m-0">
										<strong>First Name</strong><br/>
										<span class="text-warning">(required)</span>
									</label>
									<?= form_input(array_merge($first_name, $input_classes, array('required' => 'required'))); ?>
								</div>
								<div class="form-group row mx-0 align-items-center">
									<label class="col-12 col-sm-3 m-0">
										<strong>Last Name</strong><br/>
										<span class="text-warning">(required)</span></label>
									<?= form_input(array_merge($last_name, $input_classes, array('required' => 'required'))); ?>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group row mx-0 align-items-start">
									<div class="col-12 col-sm-3">
										<label class="col-form-label m-0">
											<strong>Country</strong>
											<br />
											<span class="text-warning">(required)</span>
										</label>
									</div>
									<div class="col-12 col-sm-9 p-0">
										<select required class="form-control mb-2" name="address_country">
											<optgroup label="Countries">
											<?php if(! empty($countries)) : 
											foreach($countries as $key => $country): ?>
												<!-- Set Kenya to be default country-->
												<!--TODO: List order in alphabetical order once expansion starts-->
												<option value="<?= $country->id ?>" 
													data-iso="<?= $country->iso ?>" 
													<?= $country->id == '110' ? 'selected' : ''; ?>>
													<?= $country->nicename ?></option>
											<?php endforeach; 
												else:
											?>	
											<option value="-1" disabled>No Countries found</option>
											<?php endif; ?>
											</optgroup>
										</select>
									</div>
								</div>
								<div class="form-group row mx-0 align-items-center">
									<label class="col-12 col-sm-3 m-0">
										<strong>City</strong><br/>
										<span class="text-warning">(required)</span></label>
										<?= form_input(array_merge($address_city, $input_classes, array('required' => 'required'))); ?>
								</div>
								<div class="form-group row mx-0 align-items-center">
									<label class="col-12 col-sm-3 m-0">
										<strong>Address line 1</strong><br/>
										<span class="text-warning">(required)</span></label>
										<?= form_input(array_merge($address_line_1, $input_classes, array('required' => 'required'))); ?>
								</div>
								<div class="form-group row mx-0 align-items-center">
									<label class="col-12 col-sm-3 m-0">
										<strong>Address line 2</strong></label>
										<?= form_input(array_merge($address_line_2, $input_classes)); ?>
								</div>
								
								<div class="form-group row mx-0 align-items-center">
									<label class="col-12 col-sm-3 m-0">
										<strong>Postal</strong><br/>
										<span class="text-warning">(required)</span></label>
										<?= form_input(array_merge($postal_code, $input_classes, array('required' => 'required'))); ?>
								</div>
								<div class="form-group row mx-0 align-items-center">
									<label class="col-12 col-sm-3 m-0">
										<strong>Phone number</strong><br/>
										<span class="text-warning">(required)</span></label>
										<?= form_input(array_merge($phone, $input_classes, array('required' => 'required'))); ?>
								</div>
							</div>
							
							<div class="col-12">
							<hr>
								<div class="form-group row mx-0 align-items-start">
									<div class="col-12 col-sm-3"></div>
									<div class="col-12 col-sm-9 p-0">
										<div class="form-group">
											<input type="checkbox" required name="t-c" class="mr-2" id="agreeToTerms" />
											<label for="agreeToTerms">I agree to the
												<a href="<?= site_url('legal/terms'); ?>"><u>Terms of Use</u></a>
												<br />
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12">
								<p class="text-right">
									<button class="btn btn-primary" type="submit">PROCEED TO PAYMENT</button>
								</p>

							</div>
						</div>
					<?= form_close();?>
			</div>
			<div class="col-12 col-sm-10 col-lg-6 offset-sm-1">

			</div>
		</div>

	</div>
</main>