<?php
    $data = array();

    $this->load->view('_templates/header_base', $data);
    ?>
<main>
	<div class="container yo-fluid-container my-5">
		<div class="row">
			<div class="col-6 col-md-5 offset-md-1 yo-cart">
				<h6>ORDERED PRODUCTS</h6>
			</div>
			<div class="col-6 col-md-5 col-xl-4 ">
			</div>
		</div>
		<div class="row ">
			<?php if(!empty($orders)): ?>
			<div class="col-12 col-md-6 col-xl-4 offset-xl-1 py-2 js-cartList">
				<div class="cart-items">
					<!-- LOOP -->
					<?php foreach($orders as $key => $order):
					$product_image = $this->utility->get_single_product_image($order->product_id);
					// TEMPLATEstart CART ITEM_small
					$this->load->view('_components/site/cart_item_small', array(
						'image' => $product_image,
						'order' => $order
					));
					// TEMPLATEend CART ITEM_small
					?>

					<?php endforeach; ?>
					<!-- LOOPend -->
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-5 col-xl-4 offset-xl-1 pt-4 pb-2">
				<div class="card yo-order-details mb-5 shadow-sm pb">
					<div class="card-body pb-0">
						<h6 class="card-title">ORDER DETAILS</h6>
						<h6 class="text-muted card-subtitle mb-2">Paid <?= @$payment['data']->amount ?> (<?= @$payment['data']->currency_iso ?>)</h6>
						<!-- LOOP -->
						<p class="card-text mb-0">
							<strong>paid on:</strong>
							<?= humanize_date(@$payment['data']->payment_date) ?>
						</p>
						<!-- LOOP STOP -->
					</div>
					<div class="w-75 border-bottom mt-2 mx-auto"></div>
					<div class="card-body">
						<h6 class="card-title">
							SHIPPING DETAILS<br>
						</h6>
						<h6 class="text-muted card-subtitle mb-2">
							Shipping cost: <?= @$shipping_data['shipping_cost'] ?>
						</h6>
						<!-- LOOP -->
						<p class="card-text mb-0">
							<strong>Shipping method:</strong>
							<?= @$shipping_data['method'] ?>
						</p>
						<p class="card-text mb-0">
							<strong>Address:</strong>
							<?= @$shipping_details->address_line ?>
						</p>
						<p class="card-text mb-0">
							<strong>Phone number:</strong>
							<?= @$shipping_details->phone ?>
						</p>
						<p class="card-text mb-0">
							<strong>City:</strong>
							<?= @$shipping_details->address_city ?>
						</p>
						<!-- LOOP STOP -->
					</div>
					<div class="card-footer bg-primary-dark87 text-light">
						<p class="m-0">receipt number:
							<span class="font-weight-light pl-4"><?= @$payment['data']->id.'-'.@$payment['data']->payment_info_id ?></span>
						</p>
					</div>
				</div>
				<div class="border px-3 pt-4 pb-5">
					<h6 class="text-right">Any problems?</h6>
					<a class="btn btn-danger54 mb-1 float-right">Cancel my order</a>
				</div>
			</div>
			<?php else: ?>
			<div class="col-10 offset-1">
			<?php $this->load->view('_templates/empty_message', array(
					'title' => 'The order cannot be found',
					'message' => 'Kindly contact us via <a href="mailto:">help@yoamar.com</a>',
					'button' => ''
				));
			endif; ?>
			</div>
		</div>
	</div>
</main>
