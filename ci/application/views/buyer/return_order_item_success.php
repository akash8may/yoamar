<?php
$data = array();

$this->load->view('_templates/header_base', $data);
if(empty($order)): ?>

<main>
    <?php $this->load->view('_templates/empty_message', array(
        'title' => 'We could not find the order reversal request',
        'message' => 'Kindly contact us'
    ));
    ?>
</main>

<?php else: ?>

<main style="min-height: 100vh;">
    <div class="container-fluid text-center d-flex flex-row justify-content-xl-center align-items-xl-center">
        <div class="row mt-5 pt-4">
            <div class="col-12">
                <h2 class="display-4 text-muted text-center">Your order reversal is being processed</h2>
            </div>
            <div class="col-12">
                <?php 
                    $images = $this->utility->get_product_images($order->product_id);
                    
                    $this->load->view('_components/site/bundle_card', array(
                        'id' => 'return 2232',
                        'size' => 'horizontal',
                        'images' => $images,
                        'grid' => '_even',
                        'extra_classes' => 'mx-auto'
                    ));
                ?>
            </div>
        </div>
    </div>
</main>

<?php endif; ?>