<?php 
$this->load->view('_templates/header_base', []);
?>

<main>
    <div class="container yo-fluid-container">
        <div class="row mb-3">
            <div class="col border-bottom">
                <h4 class="py-2">LINKED ACCOUNTS</h4>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-5"></div>
            <div class="col-6">
                <a href="#!" class="btn btn-primary my-btn-primary js-addPayment float-right" data-toggle="modal" data-target="#modalAddPaymentMethod" data-method="card"><i class="fa fa-credit-card mr-2"></i>ADD ACCOUNT</a>
            </div>
            <?php if(isset($message)): ?>
            <div class="col-10 offset-1 mt-4 p-5 bg-info12" id="infoMessage"> <?= @$message ?> </div>
            <?php endif; ?>
        </div>
        <?php if(!empty(@$payment_options) && is_array(@$payment_options)) : ?>
        <div class="row">
            <?php if(!empty($payment_options['mpesa']) && is_array($payment_options['mpesa'])) : ?>
            <!-- LOOP MPESA-->
            <div class="col-8 pl-md-0 col-md-7 offset-md-1 py-2">
                <h5 class="text-uppercase text-dark font-weight-bold text-capitalize">MPESA ACCOUNTS (<?= count($payment_options['mpesa']) ?>)</h5>
                <ol class="mb-1">
                    <?php // LOOP
                    foreach($payment_options['mpesa'] as $key => $method): ?>
                    <li id="<?= $method->id ?>">
                        <?= $method->number ?>
                        <a href="#" class="ml-3 js-editPayment" data-toggle="modal" data-target="#modalEditPaymentMethod" data-method="mobile2"><i class="fa fa-edit"></i></a>
                    </li>
                    <?php endforeach; 
                    // LOOPend ?>
                </ol>
                <a class="btn btn-primary my-btn-primary _small btn-outline py-2 js-addPayment ml-3" data-toggle="modal" data-target="#modalAddPaymentMethod" data-method="mobile"><i class="fa fa-credit-card d-inline-block pr-3"></i>ADD</a>
            </div>
            <!-- LOOP MPESAend -->
            <?php endif;
            if(!empty($payment_options['paypal']) && is_array($payment_options['paypal'])) : ?>
            <!-- LOOP PAYPAL-->
            <div class="col-8 pl-md-0 col-md-5 offset-md-1 py-2">
                <h5 class="text-uppercase text-dark font-weight-bold text-capitalize">PAYPAL ACCOUNTS (<?= count($payment_options['paypal']) ?>)</h5>
                <ol class="mb-1">
                <?php // LOOP
                    foreach($payment_options['paypal'] as $key => $method): ?>
                    <li id="<?= $method->id ?>">
                        <?= $method->email ?>
                        <a href="#" class="ml-3 js-editPayment" data-toggle="modal" data-target="#modalEditPaymentMethod" data-method="online2"><i class="fa fa-edit"></i></a>
                    </li>
                    <?php endforeach; 
                    // LOOPend ?>
                </ol>
                <a class="btn btn-primary my-btn-primary _small btn-outline py-2 js-addPayment ml-3" data-toggle="modal" data-target="#modalAddPaymentMethod" data-method="online"><i class="fa fa-credit-card d-inline-block pr-3"></i>ADD</a>
            </div>
            <div class="col-4 col-md-6">
                <div class="position-absolute yo-credit-card"></div>
            </div>
            <!-- LOOP PAYPALend -->
            <?php endif;
            if(!empty($payment_options['card']) && is_array($payment_options['card'])) : ?>
            <!-- LOOP CARD-->
            <div class="col-8 pl-md-0 col-md-5 offset-md-1 py-2">
                <h5 class="text-uppercase text-dark font-weight-bold text-capitalize">CREDITCARD ACCOUNTS (<?= count($payment_options['card']) ?>)</h5>
                <ol class="mb-1">
                <?php // LOOP
                    foreach($payment_options['card'] as $key => $method): ?>
                    <li id="<?= $method->id ?>">
                        <?= $method->number ?>
                        <a href="#" class="ml-3 js-editPayment" data-toggle="modal" data-target="#modalEditPaymentMethod" data-method="card2"><i class="fa fa-edit"></i></a>
                    </li>
                    <?php endforeach; 
                    // LOOPend ?>
                </ol>
                <a class="btn btn-primary my-btn-primary _small btn-outline py-2 js-addPayment ml-3" data-toggle="modal" data-target="#modalAddPaymentMethod" data-method="card"><i class="fa fa-credit-card d-inline-block pr-3"></i>ADD</a>
            </div>
            <div class="col-4 col-md-6">
                <div class="position-absolute yo-credit-card visa-card"></div>
            </div>
            <!-- LOOP CARDend -->
            <?php endif; ?>
        </div>
        <?php else:
            $this->load->view('_templates/empty_message', array(
                'image_url' => FALSE,
                'title' => 'You have not added a payment option',
                'message' => 'Quick pay in one tap by adding paypal, credit cards or mpesa payment options',
                'button'=> array(
                    'link' => '#!',
                    'text' => '<i class="fa fa-credit-card mr-2"></i>ADD ACCOUNT',
                    'extra_classes' => 'js-addPayment',
                    'extra_attributes' => 'data-toggle="modal" data-target="#modalAddPaymentMethod" data-method="card"',
                ),
            ));
        
        endif; ?>
    </div>
    <?php $this->load->view('_templates/payment_modal_edit', array(
        'action_url' => site_url('merchant/payment_edit')
    ));
    ?>
</main>