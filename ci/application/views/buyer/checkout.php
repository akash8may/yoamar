<?php
$this->load->view('_templates/header_base', []);
?>
<main>
	<div class="container yo-content yo-fluid-container">
		<div class="yo-content-header mb-4">
			<p class="d-block text-primary-dark87 yo-title text-center">CHECKOUT</p>
		</div>
		<div class="row mx-0">
			<?php if(isset($message) && !empty(@$message)): ?>
			<div class="col-12 bg-info12 p-3" id="ifoMessage"><?= @$message ?></div>
			<?php endif; ?>
			<div class="col-12 col-md-10 mb-2 offset-md-1">
				<!-- TEMPLATEstart YO-STEPS -->
                <?php
                $data = array(
                    'active_step' => 3
                );

                $this->load->view('_components/site/yo_steps', $data);
                ?>
                <!-- TEMPLATEend YO-STEPS -->
			</div>
			<div class="col-12 col-lg-5">
				<div>
					<!-- TEMPLATEstart bundle-card_horizontal -->
				<?php
				$bundle_card_images = array();
				foreach ($cart as $key => $value) {
					$product_images = $this->utility->get_product_images($value->product_id);
					array_push($bundle_card_images, $product_images);
				}

				//flatten the multidimensional array to a one dimensional array
				$bundle_card_images = call_user_func_array('array_merge', $bundle_card_images);

                $data = array(
                    'size'			=> 'horizontal',
					'grid'			=> (count((array)$bundle_card_images) > 2 ? '_odd' : '_even'),
					'images'		=> $bundle_card_images,
					'extra_classes' => 'shadow mx-auto my-5',
                );
                $this->load->view('_components/site/bundle_card', $data);
                ?>
					<!-- TEMPLATEend bundle-card_horizontal -->
				</div>
			</div>
			<div class="col-12 col-sm-10 col-lg-6 offset-sm-1">
				<form class="mb-5" id="checkout-form" method="post" action="<?= site_url('u/checkout') ?>">
					<input type="number" hidden name="shipping_details_id" value="<?= @$shipping_details_id ?>">
					<input type="text" hidden name="payment_method" value="card" />
					<div class="yo-payment-type my-3">
						<div class="bg-info26 p-3 border-bottom pb-2">
							<h6 class="">ITEMS: <span class="font-weight-bold"><?= count((array)$cart) ?> product<?= count((array)$cart) === 1 ? '' : 's' ?></span></h6>
							<h6 class="">TAX: <span class="font-weight-normal"><?= @$checkout_info['sale_amount'] ?></span></h6>
							<h5 class="font-weight-normal pt-1 my-0">Products total: $<span class="font-weight-bold"><?= @$checkout_info['total_amount'] ?></span></h5>
							<h5 class="font-weight-normal pt-2">Total ( including shipping ): $<span class="font-weight-bold js-order-total" data-base-total="<?= @$checkout_info['total_amount'] ?>"><?= (float)@$checkout_info['total_amount'] + (float)@$shipping_details['ship_shipping_cost'] ?></span></h5>
							<div>
								<p class="text-center py-1 m-0 border-left border-right border-top bg-info12">Shipping method</p>
								<ul class="yo-shipping-type py-2 nav nav-pills justify-content-center border-bottom border-left border-right bg-info12">
									<li class="nav-item">
										<a role="tab" data-toggle="pill" href="#tab-6" class="nav-link active">
											<input class="custom-radio radio-tab" type="radio" name="_shipping_method" data-shipping-cost="<?= @$shipping_details['ship_shipping_cost'] ?>" value="ship_shipping_cost" id="formCheck-6" checked/>
											<img src="<?= asset_url('site/img/ship.png'); ?>" height="52px" class="d-block mx-auto mb-2" />
											<span class="d-block text-center">SHIP</span>
											<span class="d-block text-center">(+$<?= @$shipping_details['ship_shipping_cost'] ?>)</span>
										</a>
									</li>
									<li class="nav-item">
										<a role="tab" data-toggle="pill" href="#tab-7" class="nav-link">
											<input class="custom-radio radio-tab" type="radio" name="_shipping_method" data-shipping-cost="<?= @$shipping_details['air_shipping_cost'] ?>" value="air_shipping_cost" id="formCheck-7"/>
											<img src="<?= asset_url('site/img/air.png'); ?>" height="52px" class="d-block mx-auto mb-2" />
											<span class="d-block text-center">AIR</span>
											<span class="d-block text-center">(+$<?= @$shipping_details['air_shipping_cost'] ?>)</span>
										</a>
									</li>
									<input name="shipping_method" type="hidden" value="ship_shipping_cost">
								</ul>
							</div>
						</div>
						<div class="bg-info12 p-3">
							<ul class="yo-payment-type py-2 nav nav-pills justify-content-around border bg-light shadow-sm">
								<li class="nav-item">
									<a role="tab" data-toggle="pill" href="#tab-1" class="nav-link active">
										<input class="custom-radio radio-tab" type="radio" name="_payment_method" value="visa" id="formCheck-1" checked/>
										<img src="<?= asset_url('site/img/Visa.png'); ?>" height="52px" class="d-block mx-auto mb-2" />
										<span class="d-block text-center">VISA</span>
									</a>
								</li>
								<li class="nav-item">
									<a role="tab" data-toggle="pill" href="#tab-2" class="nav-link">
										<input class="custom-radio radio-tab" type="radio" name="_payment_method" value="online" id="formCheck-2"/>
										<img src="<?= asset_url('site/img/imgonline-com-ua-twotoone-iFBhDVLbOVRL.png'); ?>" height="52px" class="d-block mx-auto mb-2" />
										<span class="d-block text-center">PAYPAL</span>
									</a>
								</li>
								<li class="nav-item">
									<a role="tab" data-toggle="pill" href="#tab-1" class="nav-link">
										<input class="custom-radio radio-tab" type="radio" name="_payment_method" value="mastercard" id="formCheck-3"/>
										<img src="<?= asset_url('site/img/Visa%20(2).png'); ?>" height="52px" class="d-block mx-auto mb-2" />
										<span class="d-block text-center">MASTERCARD</span>
									</a>
								</li>
								<li class="nav-item">
									<a role="tab" data-toggle="pill" href="#tab-4" class="nav-link">
										<input class="custom-radio radio-tab" type="radio" name="_payment_method" value="mpesa" id="formCheck-4"/>
										<img src="<?= asset_url('site/img/m-pesa-logo.jpg'); ?>" height="52px" class="d-block mx-auto mb-2" />
										<span class="d-block text-center">m-Pesa</span>
									</a>
								</li>
							</ul>
							<div class="tab-content bg-info12">
								<div class="tab-pane active p-3" id="tab-1">
									<p class="d-none"><i class="fa fa-cc-visa footer-icon mr-2"></i> Visa payment</p>
									<label for="card-element" class="d-block text-center">
										<i class="fa fa-cc-visa footer-icon mr-2"></i>
										<i class="fa fa-cc-mastercard footer-icon mr-2"></i>
										Credit or debit card
									</label>
									<script src="https://js.stripe.com/v3/"></script>
									<div class="form-row">
										<div class="col-12">
											<div id="card-element" class="border py-2 bg-light pl-2">
											<!-- A Stripe Element will be inserted here. -->
											</div>

											<!-- Used to display Element errors. -->
											<div id="card-errors" role="alert"></div>
										</div>
										<div class="col-12 d-none">
											<div class="form-group">
												<label class="pr-3">Card Number</label>
												<input type="text" class="form-control" placeholder="Card number" name="card_number"/>
											</div>
										</div>
										<div class="col-6 d-none">
											<div class="form-group">
												<label class="pr-3">Expiration date</label>

												<div class="form-row">
													<div class="col-6">
														<input type="text" class="form-control" placeholder="MM" name="card_expiry_month"/>
													</div>
													<div class="col-6">
														<input type="text" class="form-control" placeholder="YY" name="card_expiry_year"/>
													</div>
												</div>
											</div>
										</div>
										<div class="col-6 d-none">
											<div class="form-group">
												<label class="pr-3">Security code/CVC
													<i class="fa fa-info-circle px-2" title="This is the extra numbers at the back of the card"></i>
												</label>
												<input type="password" class="form-control" name="cvc"/>
											</div>
										</div>
										
									</div>
								</div>
								<div class="tab-pane p-3" id="tab-2">
									<p class="d-block text-center"><i class="fa fa-cc-paypal footer-icon mr-2"></i> Paypal payment</p>
									<div class="form-row">
										<div class="col">
											<?php $this->load->view('_components/payments/paypal_button'); ?>
										</div>
									</div>
								</div>
								<div class="tab-pane p-3" id="tab-4">
									<p class="d-block text-center">M-pesa payment</p>
									<div class="form-row">
										<div class="col-12 col-md-7">
											<input type="number" class="form-control my-2" name="mpesa_number" placeholder="m-Pesa number" value="<?= @$user->phone ?>"/>
										</div>
										<div class="col-12 col-md-5">
											<div class="form-group">
												<a class="btn btn-success btn-lg btn-block mpesa-btn" href="#">
												Lipa na
												<img class="pl-2" src="<?= asset_url('site/svg/mpesa-logo.svg')?>" height="32"/>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row mt-5">
						<div class="col-12">
							<button type="submit" class="btn btn-lg btn-primary87 text-white d-block w-100 font-weight-bold my-2 mx-auto text-center js-checkOut px-2 shadow js-placeCartOrder text-center mx-auto">PLACE ORDER</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</main>


<!-- Iveri HTML -->
<div id="iveri-litebox"></div>
<form id="Formlitebox" method="post">
	<input type="hidden" name="Lite_Authorisation" id="Lite_Authorisation" value="false">
	<input type="hidden" name="Lite_Currency_AlphaCode" id="Lite_Currency_AlphaCode" value="ZAR">
	<input name='Lite_Merchant_ApplicationID' id='Lite_Merchant_ApplicationID' value='{E2CE3705-F0DF-4C72-9615-E2B1848B9DE7}' />
	<input name='Lite_Order_Amount' id='Lite_Order_Amount' value='20' />
	<?php
	$url= "http://yoamar.yiipro.com/";
	?>
	<input name='Lite_Website_Successful_Url' id='Lite_Website_Successful_Url' value="<?=$url?>" />
	<input name='Lite_Website_Fail_Url' id='Lite_Website_Fail_Url' value="<?=$url?>" />
	<input name='Lite_Website_TryLater_Url' id='Lite_Website_TryLater_Url' value="<?=$url?>" />
	<input name='Lite_Website_Error_Url' id='Lite_Website_Error_Url' value="<?=$url?>" /> 
	<input type="hidden" name="Lite_Website_Return_Url" id="Lite_Website_Return_Url" value="<?=$url?>">
	<input name="Ecom_BillTo_Online_Email" readonly="readonly" type="text" value="eugene@iveri.com" maxlength="50" id="Ecom_BillTo_Online_Email" class="clsInputReadOnlyText"> 
	<input type="hidden" name="Ecom_Payment_Card_Protocols" id="Ecom_Payment_Card_Protocols" value="iVeri">
	<input type="hidden" name="Ecom_TransactionComplete" id="Ecom_TransactionComplete" value="false">

	<input name='Lite_Order_LineItems_Product_1' id='Lite_Order_LineItems_Product_1' value='111' />
	<input name='Lite_Order_LineItems_Quantity_1' id='Lite_Order_LineItems_Quantity_1' value='1' />
	<input name='Lite_Order_LineItems_Amount_1'  id='Lite_Order_LineItems_Amount_1' value='20' />
	<input name='Lite_ConsumerOrderID_PreFix' id='Lite_ConsumerOrderID_PreFix' value='111222' />
	<a id="iveri-litebox-button" >Make Payment</a>
</form>
