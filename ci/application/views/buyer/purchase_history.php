<?php

$this->load->view('_templates/header_base', []);
?>
<main>
<?php
$this->load->view('_templates/profile_header', $data);
?>
	<div class="container yo-content px-0">
		<div class="yo-buyer-orders p-2 my-3">
			<h6 class="mb-3">Purchase history</h6>
			<div class="table-responsive table-borderless border bg-light shadow-sm">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>orders</th>
							<th class="d-none d-md-table-cell">receipt number</th>
							<th>date ordered</th>
							<th>delivered date</th>
							<!-- <th>STATUS</th> -->
						</tr>
					</thead>
					<tbody>
						<!--LOOP -->
						<?php for($i=0;  $i < 2; $i++):?>
						<tr>
							<td>Owinde Eliyoo canvas Painting...</td>
							<td class="d-none d-md-table-cell">2534656yg78/254</td>
							<td>12th July</td>
							<td>12th August</td>
							<!-- <td class="text-primary-dark">shipping</td> -->
						</tr>
						<?php endfor; ?>
						<!--LOOPend  -->

						<tr class="d-none">
							<td colspan="4">
								<button class="btn btn-primary my-btn-primary js-viewActiveOrders mx-auto d-block" type="button">view all
									<i class="fa fa-chevron-down pl-2"></i>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</main>
