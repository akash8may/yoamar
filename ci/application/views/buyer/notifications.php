<?php
$this->load->view('_templates/header_base', []);
?>
<main>
	<?php
    $this->load->view('_templates/profile_header', $data);
    ?>

    <div class="container yo-content px-0">
        <div class="row">
            <div class="col-8 co offset-2 p-0">
                <div class="d-block px-3 pb-3 pt-2">
                    <h4 class="pb-3 pt-1 text-center">NOTIFICATIONS</h4>
                    <div class="row user-list js-newNotifications bg-info12 p-2">
                        <div class="col-12 text-primary-alert-dark54">
                            <h6 class="py-2">new (
                                <span class="text-primary-dark">3</span>)</h6>
                        </div>
                        <!--LOOP -->
                        <?php for($i=0;  $i < 2; $i++):?>

                        <div class="col-12 user-item py-0 ">
                            <!-- TEMPLATEstart user notification-item -->
                            <div class="user-container bg-light">
                                <a href="#" class="user-avatar">
                                    <img class="rounded-circle img-fluid" src="<?= asset_url('site/img/avatar.jpg'); ?>" alt="Profile of Mark Smith Peterson"
                                    width="48" height="48">
                                </a>
                                <p class="user-name">
                                    <a href="#">Maskuti has seen syour review on a product</a>
                                    <span class="d-none">Last seen on 27th Sep 2014 </span>
                                </p>
                                <a href="#" class="user-delete d-none">
                                    <i class="fa fa-remove"></i>
                                </a>
                            </div>
                            <!-- TEMPLATEend user notification-item -->

                        </div>
                        <?php endfor; ?>
                        <!--LOOPend   -->
                    </div>
                    <div class="row user-list js-oldNotifications py-2">
                        <div class="col-12 text-primary-alert-dark54">
                            <h6 class="py-2">seen</h6>
                        </div>
                        <!--LOOP -->
                        <?php for($i=0;  $i < 4; $i++):?>

                        <div class="col-12 user-item py-1 border-bottom">
                            <!-- TEMPLATEstart user notification-item -->
                            <div class="user-container bg-light">
                                <a href="#" class="user-avatar">
                                    <img class="rounded-circle img-fluid" src="<?= asset_url('site/img/avatar.jpg'); ?>" alt="Profile of Mark Smith Peterson"
                                    width="48" height="48">
                                </a>
                                <p class="user-name">
                                    <a href="#">Maskuti has seen syour review on a product</a>
                                    <span class="d-none">Last seen on 27th Sep 2014 </span>
                                </p>
                                <a href="#" class="user-delete d-none">
                                    <i class="fa fa-remove"></i>
                                </a>
                            </div>
                            <!-- TEMPLATEend user notification-item -->

                        </div>
                        <?php endfor; ?>
                        <!--LOOPend   -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
