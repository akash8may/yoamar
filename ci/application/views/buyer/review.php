<?php
if(empty($product))
{
	show_404();
}
$this->load->view('_templates/header_base');
?>
<main>
	<div class="container px-xl-0 px-5">
		<div class="row yo-product">

			<div class="col-12 col-md-6 product-img-container p-0">
				<div class="row mx-0 bg-info12">
					<div class="col-12 px-4">
						<h3 class="yo-title-lg text-primary-dark my-4">
							<?= $product->product_name.' - <small>$'.$product->unit_price.'</small>'; ?>
						</h3>
					</div>
					<div class="col-12 img-holder px-4 pb-5 mx-auto" style="height: auto;">
						<?php 
					$product_img = $this->utility->get_single_product_image($product->id);
					$alt_text = $product_img->alt_text ?? $product->description;
                    ?>
						<img src="<?= $product_img->img_link; ?>" alt="<?= $alt_text; ?>" class="_d-none shadow align-middle"
						 data-im-orientation="vertical" width="100%" height="auto">
					</div>
				</div>
			</div>

			<div class="col-12 col-md-6 product-details-container">
				<h3 class="px-4 text-black-50">Thank you for reviewing this product</h3>
				<div class="d-block">
					<form action="?" method="post" class="px-4" id="formReview">
						<?php
                        $this->load->view('_components/ratings', array(
                            'rating' => $review->rating ?? 0,
                            'extra_classes' => 'fa-2x px-1',
                            'is_dynamic' => TRUE,
                            'is_readonly' => FALSE
                        ));
                        ?>
						<div class="form-group">
							<label class="mb-1">Comment</label>
							<div class="review-editor"></div>
							<textarea name="review" class="form-control form-control-lg" style="min-height: 10rem;" rows="8"><?= get_value_or_default(@$review->comment);?></textarea>
						</div>
						<div class="form-group float-right">
							<?php if(empty(@$review)): ?>
							<button class="btn btn-success text-center py-2" type="button" id="btnAddReview" data-product-id="<?= $product->id; ?>">ADD REVIEW</button>
							<?php else:?>
							<button class="btn btn-success text-center py-2" type="button" id="btnUpdateReview" data-product-id="<?= $product->id; ?>">UPDATE REVIEW</button>
							<button class="btn btn-warning text-center py-2" type="button" id="btnDeleteReview" data-product-id="<?= $product->id; ?>">DELETE REVIEW</button>
							<?php endif;?>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
</main>
