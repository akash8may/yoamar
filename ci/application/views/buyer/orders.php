<?php
    $this->load->view('_templates/header_base', []);
    ?>
<main>

	<div class="container my-5">
        <?php if(! empty($message)): ?>
        <div class='container'>
            <div class='row'>
                <div id="infoMessage" class='col-10 offset-1 col-md-8 offset-md-2 p-4 bg-info12 border mb-3'><?= $message ?></div>
            </div>
        </div>
        <?php endif; ?>
        <h3 class="mb-4">Ordered products</h3>
        <?php if(!empty($orders) && is_array($orders)): ?>
        
        <div class="table-responsive table-borderless border bg-light shadow-sm">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Product name</th>
                        <th>Date ordered</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php // LOOP
                    foreach($orders as $key => $order): 
                    ?>
                    <tr>
                        <td class="text-truncate"><?= $order->product_name ?></td>
                        <td><?= humanize_date($order->date_ordered) ?></td>
                        <td class="text-primary-dark"><?= (int)$order->paid === 0 ? 'unpaid' : $order->status ?></td>
                        <td> 
                            <?php if((int)$order->paid === 0): ?>
                            <a class="btn" href="<?= site_url('u/checkout') ?>" >PAY ORDER</a>
                            <?php else: ?>
                            <a class="btn btn-outline-primary" href="<?= site_url('u/orders/'.$order->order_id) ?>" >VIEW ORDER</a>
                            <?php 
                            $review = $this->utility->get_single_buyer_review($order->product_id, $order->buyer_id);
                            if(empty($review) && $order->status == 'delivered'):
                            ?>
                            <button type="button" class="btn btn-outline-primary" data-product-id="<?= $order->product_id ?>" data-toggle="modal" data-target="#modalReview" data-href="<?= site_url('u/review/'.$order->product_id) ?>" >REVIEW</button>
                            <?php else: ?>
                            <?php endif; endif; ?>
                        </td>
                    </tr>
                    <?php if($key > 12): ?>
                    <tr>
                        <td colspan="5">
                            <button class="btn btn-primary my-btn-primary js-viewActiveOrders mx-auto d-block" type="button">view all
                                <i class="fa fa-chevron-down pl-2"></i>
                            </button>
                        </td>
                    </tr>
                    <?php endif; endforeach; 
                    // LOOPend 
                    ?>

                </tbody>
            </table>

        </div>
        
        <?php 
        $this->load->view('_templates/review_modal');
        else:
            $this->load->view('_templates/empty_message', array(
                'title' => 'You currently have not placed any order',
                'message' => 'When you checkout on your cart, your orders will appear here.',
                'button' => ''
            ));
        endif; ?>
    </div>
</main>
