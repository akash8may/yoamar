<?php
$data = array();

$this->load->view('_templates/header_base', $data);
if(empty($orders)): ?>

<main>
    <?php $this->load->view('_templates/empty_message', array(
        'title' => 'We could not find the order',
        'message' => 'Kindly contact us if you cannot complete this task'
    ));
    ?>
</main>

<?php else:

?>

<main class="bg-secondary26 h-100">
    <div class="container w-100 yo-content position-relative">
        <div class="pt-5"></div>
        <?php if(! empty($message)): ?>
        <div class='container'>
                <div class='row'>
                    <div id="infoMessage" class='col-10 offset-1 col-md-8 offset-md-2 p-4 bg-info12 border'><?= $message ?></div>
                </div>
        </div>
        <?php endif; ?>
        <div class="yo-content-header">
            <p class="d-inline-block text-primary-dark87 yo-title">Return product<br /></p>
        </div>
        <div class="row mt-5 pt-4">
            <div class="col-12 col-lg-6">
                <img src="<?= asset_url('site/svg/package-01.svg') ?>" style="margin-top: -30px" />
            </div>
            <div class="col-12 col-lg-4">
                <div class="row">
                    <div class="col-12">
                        <h2 class="text-muted">Our return policy</h2>
                    </div>
                    <div class="col-12">
                        <p data-default="10rem" data-toggle-read-more>
                        Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, 
                            <span class="yo-text-highlight">dapibus ac facilisis<br /></span> in, egestas eget quam. Donec id elit non mi porta gravida at eget 
                            <span class="yo-text-highlight">dapibus ac facilisis<br /></span>metus.
                        Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, 
                            <span class="yo-text-highlight">dapibus ac facilisis<br /></span> in, egestas eget quam. Donec id elit non mi porta gravida at eget 
                            <span class="yo-text-highlight">dapibus ac facilisis<br /></span>metus.
                        Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, 
                            <span class="yo-text-highlight">dapibus ac facilisis<br /></span> in, egestas eget quam. Donec id elit non mi porta gravida at eget 
                            <span class="yo-text-highlight">dapibus ac facilisis<br /></span>metus.
                        Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, 
                            <span class="yo-text-highlight">dapibus ac facilisis<br /></span> in, egestas eget quam. Donec id elit non mi porta gravida at eget 
                            <span class="yo-text-highlight">dapibus ac facilisis<br /></span>metus.
                        Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, 
                            <span class="yo-text-highlight">dapibus ac facilisis<br /></span> in, egestas eget quam. Donec id elit non mi porta gravida at eget 
                            <span class="yo-text-highlight">dapibus ac facilisis<br /></span>metus.
                        
                        </p>
                        
                        
                        <a class="btn btn-outline-primary btn-sm js-infoOpen" role="button" href="#!">Read more</a>
                        <a class="btn btn-outline-primary btn-sm ml-3 js-infoClose d-none" role="button" href="#!">Read less</a>
                    </div>
                    <div class="col-12 pt-2 mt-4 border-top">
                        <h2 class="text-muted d-none">Return details</h2>
                    </div>
                </div>
                <div class="card shadow-sm rounded mx-auto mb-4 border-0 bg-transparent border-left">
                    <div class="card-body border-bottom bg-info26">
                        <h2 class="text-muted card-title">Payments</h2>
                        <h6 class="text-muted card-subtitle mb-2">Return cost</h6>
                        <p class="text-primary card-text mb-1"><span class="text-muted">Charges 1: </span>$30</p>
                        <p class="text-primary card-text mb-1"><span class="text-muted">Charges 1: </span>$30</p>
                        <p class="text-primary card-text mb-1"><span class="text-muted">Charges 1: </span>$30</p>
                        <h5 class="text-dark card-subtitle mt-3">TOTAL CHARGES</h5>
                        <p class="text-primary m-0">$160</p>
                    </div>
                    <div class="card-body border-bottom bg-info26">
                        <h2 class="text-muted card-title">Products to return (<?= count($orders) ?>)</h2>
                        <?php 
                            $order_count= 0; 
                            foreach($orders as $order):
                            $order_count = $order_count + 1;
                            $product = $this->utility->get_single_product($order->product_id);
                        ?>

                        <h6 class="text-muted card-subtitle mb-2">Product details (<?= $order_count ?>)</h6>

                        <p class="card-text mb-1"><span>Product name: </span>
                            <span class="text-underline"><?= @$product->product_name ?></span></p>

                        <p class="card-text mb-1"><span>Unit price: </span>
                            <span class="text-underline"><?= @$product->unit_price ?></span></p>

                        <p class="card-text mb-1"><span>Quantity ordered: </span>
                            <span class="text-underline"><?= @$order->quantity ?></span></p>

                        <p class="card-text mb-1"><span>Paid: </span>
                            <span class="text-underline"><?= (@$order->paid == '1') ? 'TRUE' : 'FALSE' ?></span></p>

                        <p class="card-text mb-1"><span>Date ordered: </span>
                            <span class="text-underline"><?= humanize_date(@$order->date_ordered) ?></span></p>

                        <p class="card-text mb-1"><span>Date delivered: </span>
                            <span class="text-underline"><?= (!empty(@$order->date_delivered)) ? humanize_date($order->date_delivered) : 'Not yet delivered' ?></span></p>
                        
                        <p class="text-dark card-text mb-1"><span class="text-dark">Total Paid: </span>
                            <span class="text-underline"><?= @$order->order_total ?></span></p>
                        <p class="border-bottom"></p>
                        <?php endforeach; ?>
                    </div>
                    <form method="post" action="<?= site_url('u/orders/return/'.$orders[0]->order_id) ?>" accept-charset="utf-8">
                        <!-- TODO: update hidden inputs for the charges and charges_breakdown -->
                        <div class="card-body border-bottom shadow-none bg-transparent">
                            <h2 class="text-muted card-title">Reversal buyer input</h2>
                            <h6 class="text-muted card-subtitle mb-4">We need you to fill in this data in order for the reversal to be accepted</h6>
                            <input name="charges" value="" type="hidden">
                            <input name="charges_breakdown" value="" type="hidden">
                            
                            <?php foreach($orders as $order): 
                                $product = $this->utility->get_single_product($order->product_id);
                                ?>
                                <div class="form-group">
                                    <label class="m-0">Return how many <?= $product->product_name ?>(s)?</label>
                                    <?php if(count($orders) > 1): ?>
                                    <a href="<?= site_url('u/orders/return/'.$order->order_id.'/'.$order->product_id) ?>" class="btn btn-inline p-0 text-primary26 text-underline">
                                        I want to return only this item</a>
                                    <?php endif; ?>
                                    <select required class="custom-select w-100" name="return_quantity[<?= $order->transaction_id ?>]">

                                        <?php 
                                        $quantity = (int) $order->quantity - (int) $order->reversed_quantity;
                                        
                                        for($opt = 1; $opt <= $quantity; $opt++): ?>
                                        
                                        <option value="<?= $opt ?>" <?= $opt == 1 ? 'selected' : '' ?>>
                                            <?= $opt ?>
                                        </option>
                                        
                                        <?php endfor; ?>

                                    </select>
                                    <?php if($order->quantity == $order->reversed_quantity): ?>
                                    <span class="font-weight-light font-italic text-danger87 d-inline-block">Seems you returned all the quantity you ordered</span>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                            
                            <div class="form-group">
                                <label>Why do you want to return the purchased item(s)?</label>
                                <textarea name="reversal_reason" class="w-100" style="min-height: 6rem;"></textarea>
                            </div>

                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="formCheck-1" name="accept_t_and_c" required/>
                                <label class="form-check-label" for="formCheck-1">I&#39;ve read and accept the terms and conditions <span class="text-primary-dark87">(required)</span></label>
                            </div>
                        </div>
                        <div class="card-body border-top bg-warning54 rounded-bottom">
                            <h5 class="text-white-50 card-subtitle mt-2">TOTAL TO REFUND</h5>
                            <p class="text-uppercase text-white mb-4">$ 230.33</p>
                            <button class="btn btn-primary btn-block btn-lg my-btn-primary w-100" type="submit">RETURN PRODUCT</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</main>

<?php endif;