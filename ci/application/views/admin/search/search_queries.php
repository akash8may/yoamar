<div class="container">
	<div class="form-group">
		<div class="alert alert-success" role="alert"><span>Filter search results by country by selecting the desired country
				from the dropdown</span></div>
	</div>
	<div class="form-group">
		<?= form_open('',['method'=>'GET']); ?>
		<div class="input-group w-100">
			<div class="input-group-prepend">
				<select name="country" >
					<?php $selected_country_id = $this->input->get('country'); ?>
					<option value="" <?= get_if_empty($selected_country_id,'selected'); ?>>All countries</option>
					<?php 
						if(!empty($countries)):
							foreach($countries as $country):
					?>
					<option value="<?= $country->id; ?>" <?= get_selected($country->id,$selected_country_id) ?>>
						<?= $country->nicename; ?>
					</option>
					<?php 
							endforeach;
						endif;
					?>
				</select>
			</div>
			<input class="form-control" name="q" type="search" placeholder="Filter search queries" value="<?= $this->input->get('q') ?? '' ;?>">
			<div class="input-group-append">
				<button class="btn btn-primary" type="submit">Search</button>
			</div>
		</div>
		<?= form_close(); ?>
	</div>
	<div class="form-group">
		<h3>Product details</h3>
		<?php 
			$this->load->view('_components/admin/search_queries_table',array(
				'queries'=>$search_queries
			));
		?>
	</div>
</div>
