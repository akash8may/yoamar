<?php 
$products_to_verify = @$products->to_verify;
$other_products = $products->products;
$products_count = count($other_products) + count($products_to_verify);
if(isset($products_to_verify)): ?>
<div class="container">
    <h5>Verify products</h5>
	<div class="alert alert-info mt-4" role="alert" id="info-message">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<span><?= count($products_to_verify); ?> products found</span>
	</div>
</div>
<div class="container px-5">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th colspan="1">Product name</th>
                    <th>Unit Price</th>
                    <th style="width: 45%">Description</th>
                    <th>Date created</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($products_to_verify as $product): ?>
                <tr>
                    <td><?= @$product->product_name ?></td>
                    <td><?= @$product->unit_price ?></td>
                    <td>
                        <div data-toggle-read-more data-default="50px" style="max-height: 50px">
                            <?= @$product->description ?>
                        </div>
                        <a class="btn btn-inline text-primary54 py-0 mr-2 js-infoOpen" onclick="window.admin_yo.ui.toggleReadMore(this, 'sibling')">Read more</a>
                        <a class="btn btn-inline text-primary54 py-0 mr-2 js-infoClose d-none" onclick="window.admin_yo.ui.toggleReadMore(this, 'sibling')">Read less</a>
                    </td>
                    <td><?= @$product->date_created ?></td>
                    <td><button class="btn btn-outline-success btn-block btnVerifyProduct" data-verify-true="Unverify" data-verify-false="Verify" data-product_id="<?= @$product->id ?>" data-is_verified="false" title="Verify account" type="button">Verify</button></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php endif; 
if(isset($other_products)): ?>
<div class="container">
    <h5>All products</h5>
	<div class="alert alert-info mt-4" role="alert" id="info-message">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<span><?= count($other_products); ?> products found</span>
	</div>
</div>
<div class="container px-5">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th colspan="1">Product name</th>
                    <th>Unit Price</th>
                    <th style="width: 45%">Description</th>
                    <th>Date created</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($other_products as $product): ?>
                <tr>
                    <td><?= @$product->product_name ?></td>
                    <td><?= @$product->unit_price ?></td>
                    <td>
                        <div data-toggle-read-more data-default="50px" style="max-height: 50px">
                            <?= @$product->description ?>
                        </div>
                        <a class="btn btn-inline text-primary54 py-0 mr-2 js-infoOpen" onclick="window.admin_yo.ui.toggleReadMore(this, 'sibling')">Read more</a>
                        <a class="btn btn-inline text-primary54 py-0 mr-2 js-infoClose d-none" onclick="window.admin_yo.ui.toggleReadMore(this, 'sibling')">Read less</a>
                    </td>
                    <td><?= @$product->date_created ?></td>
                    <td>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <ul class="pagination justify-content-center">
        <li class="page-item"><a class="page-link text-info" href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
        <?php $pages = $products_count / 5;
        $active_page = 1;
        for($page = 1; $page < $pages; $page++):  ?>
            <li class="page-item">
                <a class="page-link <?= $active_page == $page ? 'bg-primary-dark12 text-white' : 'text-info' ?>" href="#"><?= $page ?></a>
            </li>
        <?php endfor; ?>
        <li class="page-item"><a class="page-link text-info" href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>
    </ul>
</div>
<?php endif; ?>
