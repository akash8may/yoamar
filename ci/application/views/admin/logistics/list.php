<?php 
	$this->load->view('_components/admin/modals/modal_confirm_delete');
?>
<div class="container">
	<?php if(! empty($message)): ?>
	<div id="infoMessage" class='alert alert-success text-center'>
		<?= $message ?>
	</div>
	<?php endif; ?>
	<div class="my-4">
		<div class="button-group text-right">
			<a href="<?= site_url('admin/accounts/logistics/add'); ?>" class="btn btn-success">Create Account</a>
		</div>
	</div>

	<?php
		// Load the searchbar
		$this->load->view('_components/admin/account_searchbar');

		// Load the table
		$this->load->view('_components/admin/accounts_table',array(
			'accounts'=>$accounts,
			'account_type'=>$account_type
		));
	?>
</div>
