<div class="row">
	<div class="container mt-2 mt-md-4">
		<div class="alert alert-info text-center" id="info_message">Your password is insecure and <b>needs to be changed</b>
			<a href="#">Take action</a>
		</div>
		<div class="row">
			<?php 
				// Buyer accounts
				$this->load->view('_components/admin/dashboard_card',array(
					'stats_change'=> NULL,#TODO: Make dynamic
					'content'=>count($buyer_accounts),
					'card_title'=>'Buyer accounts',
					'content_icon'=>'fa-group',
					'has_btn'=>TRUE,
					'btn_link'=>site_url('admin/accounts/buyer'),
				));

				// Merchant/Seller accounts
				$this->load->view('_components/admin/dashboard_card',array(
					'stats_change'=> NULL,#TODO: Make dynamic
					'content'=>count($merchant_accounts),
					'card_title'=>'Seller accounts',
					'content_icon'=>'fa-group',
					'has_btn'=>TRUE,
					'btn_link'=>site_url('admin/accounts/seller'),
				));

				// Admin accounts
				$this->load->view('_components/admin/dashboard_card',array(
					'stats_change'=> -20,#TODO: Make dynamic
					'content'=>count($admin_accounts),
					'card_title'=>'Admin accounts',
					'content_icon'=>'fa-group',
					'has_btn'=>TRUE,
					'btn_link'=>site_url('admin/accounts/admin'),
				));
				
				// Logistics accounts
				$this->load->view('_components/admin/dashboard_card',array(
					'stats_change'=> 0,#TODO: Make dynamic
					'content'=>count($logistics_accounts),
					'card_title'=>'Logistics accounts',
					'content_icon'=>'fa-truck',
					'has_btn'=>TRUE,
					'btn_link'=>site_url('admin/accounts/logistics'),
				));

				// Search queries
				$this->load->view('_components/admin/dashboard_card',array(
					'stats_change'=> 50,#TODO: Make dynamic
					'content'=>count($search_queries),
					'card_title'=>'Search queries',
					'content_icon'=>FALSE,
					'has_btn'=>TRUE,
					'btn_link'=>site_url('admin/accounts/search_queries'),
				));
				
				// Products added
				$this->load->view('_components/admin/dashboard_card',array(
					'stats_change'=> NULL,#TODO: Make dynamic
					'content'=>count($products),
					'card_title'=>'Products added',
					'content_icon'=>FALSE,
					'has_btn'=>FALSE
				));
				
				// Items ordered
				$this->load->view('_components/admin/dashboard_card',array(
					'stats_change'=> 50,#TODO: Make dynamic
					'content'=>count($orders),
					'card_title'=>'Total orders',
					'content_icon'=>FALSE,
					'has_btn'=>FALSE
				));
				
				// Items shipped
				$this->load->view('_components/admin/dashboard_card',array(
					'stats_change'=> 50,#TODO: Make dynamic
					'content'=>count($items_shipped),
					'card_title'=>'Items shipped',
					'content_icon'=>FALSE,
					'has_btn'=>FALSE
				));

				// Items delivered
				$this->load->view('_components/admin/dashboard_card',array(
					'stats_change'=> 50,#TODO: Make dynamic
					'content'=>count($items_delivered),
					'card_title'=>'Items delivered',
					'content_icon'=>FALSE,
					'has_btn'=>FALSE
				));

			?>
		</div>
		
		<h2 class="mt-sm-4">Recent search queries</h2>
		<?php 
			$this->load->view('_components/admin/search_queries_table',array(
				'queries'=> $search_queries
			));
		?>
	</div>
</div>
