<div class="mb-4 mb-sm-5">
	<div class="container">
		<div class="alert alert-info"><span>All accounts will be created with a default password equal to the [last_name]_[phone]!</span>
		<p><i>For example : User with <b>last name:</b> Doe, <b>Phone:</b> 0712454545</i> will have a password of <b>Doe_0712454545!</b><br> It is recommended that the password is changed upon logging in to increase security.</p>
		</div>
	</div>
	<?php if(!empty($message)): ?>
	<div class='container'>
		<div class='row'>
			<div id="infoMessage" class='col-10 offset-1 col-md-8 offset-md-2 p-4 bg-info12 border'>
				<?= $message ?>
			</div>
		</div>
	</div>
	<?php 
      	endif;
      	$input_classes = array('class' => 'form-control');
      	echo form_open('admin/accounts/'.$account_type.'/add', array(
			'class' => 'd-flex flex-column justify-content-center',
			'id' => 'login-box',
		));
	?>
	<div class='login-box-header px-4 pt-4 pb-0'>
		<h2 class="text-center">
			<?= 'Create '.ucwords($account_type).' Account'; ?>
		</h2>
	</div>
	<div class="d-flex flex-row align-items-center login-box-seperator-container px-4 pb-4">
		<div class="login-box-seperator"></div>
		<div class="login-box-seperator-text">
			<p class="mb-0 px-2 text-gray-dark">
				<?php echo lang('create_user_subheading');?>
			</p>
		</div>
		<div class="login-box-seperator"></div>
	</div>

	<div class='px-4 pb-3'>
		<?php echo lang('create_user_fname_label', 'first_name');
            echo form_input(array_merge($first_name, $input_classes));?>
	</div>
	<div class='px-4 pb-3'>
		<?php echo lang('create_user_lname_label', 'last_name');
            echo form_input(array_merge($last_name, $input_classes));?>
	</div>

	<?php if($identity_column!=='email') : ?>

	<div class='px-4 pb-3'>
		<?php echo lang('create_user_identity_label', 'identity');
            echo '<br />';
            echo form_error('identity'); 
            echo form_input(array_merge($identity, $input_classes)); ?>
	</div>

	<?php endif;?>

	<div class='px-4 pb-3 <?= (isset($user_group) && @$user_group == ' buyer') ? 'd-none' : '' ?>'>
		<?php echo lang('create_user_company_label', 'company');
            echo form_input(array_merge($company, $input_classes));?>
	</div>

	<div class='px-4 pb-3'>
		<?php echo lang('create_user_email_label', 'email');
            echo form_input(array_merge($email, $input_classes));?>
	</div>

	<div class='px-4 pb-3'>
		<?php echo lang('create_user_phone_label', 'phone');
            echo form_input(array_merge($phone, $input_classes));?>
	</div>

	<?php if($account_type == 'logsitics'): ?>
	<!-- Country -->
	<!-- City -->
	<!-- Address -->
	<!-- Postal Code -->
	<?php endif;?>

	<div class='submit-row p-4'>
		<?php echo form_submit('submit', 'Create '.ucwords($account_type).' Account', array(
                  'class' => 'btn btn-primary btn-block',
            ));?>
	</div>
	<?php echo form_close();?>
</div>
