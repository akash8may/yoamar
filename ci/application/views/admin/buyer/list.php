<div class="container">
	<div class="alert alert-info mt-4" role="alert" id="info-message">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<span><?= count($accounts); ?> accounts found</span>
	</div>

	<?php 
		// Load the searchbar
		$this->load->view('_components/admin/account_searchbar');

		// Load the table
		$this->load->view('_components/admin/accounts_table',array(
			'accounts'=>$accounts
		));
	?>
</div>
