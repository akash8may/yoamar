<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th class="text-center">ID</th>
				<th>Name</th>
				<th>Purchases</th>
				<th>Latest purchase</th>
				<th>Reviews</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center">8972354</td>
				<td>John Doe</td>
				<td>20</td>
				<td><a href="#" target="_blank">Some product</a></td>
				<td>5</td>
				<td>
					<?php 
						$this->load->view('_components/admin/buttons/btn_view_details');
					?>
				</td>
			</tr>
			<tr>
				<td class="text-center">8972354</td>
				<td>John Doe</td>
				<td>20</td>
				<td><a href="#" target="_blank">Some product</a></td>
				<td>5</td>
				<td>
					<?php 
						$this->load->view('_components/admin/buttons/btn_view_details');
					?>
				</td>
			</tr>
			<tr>
				<td class="text-center">8972354</td>
				<td>John Doe</td>
				<td>20</td>
				<td><a href="#" target="_blank">Some product</a></td>
				<td>5</td>
				<td>
					<?php 
						$this->load->view('_components/admin/buttons/btn_view_details');
					?>
				</td>
			</tr>
			<tr>
				<td class="text-center">8972354</td>
				<td>John Doe</td>
				<td>20</td>
				<td><a href="#" target="_blank">Some product</a></td>
				<td>5</td>
				<td>
					<?php 
						$this->load->view('_components/admin/buttons/btn_view_details');
					?>
				</td>
			</tr>
			<tr>
				<td class="text-center">8972354</td>
				<td>John Doe</td>
				<td>20</td>
				<td><a href="#" target="_blank">Some product</a></td>
				<td>5</td>
				<td>
					<?php 
						$this->load->view('_components/admin/buttons/btn_view_details');
					?>
				</td>
			</tr>
			<tr>
				<td class="text-center">8972354</td>
				<td>John Doe</td>
				<td>20</td>
				<td><a href="#" target="_blank">Some product</a></td>
				<td>5</td>
				<td>
					<?php 
						$this->load->view('_components/admin/buttons/btn_view_details');
					?>
				</td>
			</tr>
		</tbody>
	</table>
</div>
