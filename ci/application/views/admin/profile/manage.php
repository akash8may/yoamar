<?php if(isset($message)): ?>
<div id="infoMessage" class="bg-info12 p-4 border"><?= @$message ?></div>
<?php endif; ?>
<form action="<?= site_url('admin/profile/edit/') ?>" autocomplete="off" id="profile-edit" method="post" enctype="multipart/form-data">
    <div class="form-row">
        <div class="col-12 col-lg-6 offset-lg-3">
            <div class="form-row border-bottom py-3">
                <div class="col-6">
                    <label>First name</label>
                    <input type="text" name="f_name" value="<?= @$user->first_name ?>" placeholder="First name" class="form-control" />
                </div>
                <div class="col-6">
                    <label>Last name</label>
                    <input type="text" name="l_name" value="<?= @$user->last_name ?>" placeholder="First name" class="form-control" />
                </div>
                <div class="col-6 pt-1">
                    <label>Phone number</label>
                    <input type="number" name="phone" value="<?= @$user->phone ?>" class="form-control" />
                </div>
                <div class="col-6 pt-1">
                    <label>Company</label>
                    <input type="text" name="company" value="<?= @$user->company ?>" placeholder="First name" class="form-control" />
                </div>
                <div class="col-12 pt-1">
                    <label>e-mail</label>
                    <input type="email" name="email" value="<?= @$user->email ?>" class="form-control" />
                </div>
            </div>
            <div class="form-row border-bottom p-2 bg-info12 mt-3">
                <div class="col-12">
                    <p class="text-muted">Change password</p>
                </div>
                <div class="col-12">
                    <label>Old password</label>
                    <input type="password" autocomplete="new-password" name="old_password" class="form-control" />
                </div>
                <div class="col-12">
                    <label>New password</label>
                    <input type="password" name="new_password" class="form-control" />
                </div>
                <div class="col-12 pt-1">
                    <label>Confirm new password</label>
                    <input type="password" name="new_password_confirm" class="form-control" />
                </div>
                <div class="col-12 py-3">
                    <button class="btn btn-primary btn-lg w-100 d-block" type="submit"><i class="fa fa-save pr-3"></i>change password</button>
                </div>
            </div>
            <div class="form-row p-2 mt-3">
                <div class="col-12 py-3">
                    <button class="btn btn-primary btn-lg my-2 d-block mx-auto my-btn-primary py-2 px-5 shadow" type="submit"><i class="fa fa-save pr-3"></i>SAVE</button>
                </div>
            </div>
        </div>
    </div>
</form>