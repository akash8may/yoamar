<?php $is_saved = $is_saved ?? FALSE; 
$total = @$total ?? count((array)$products);

?>
   
<section class="cart-wrapper">
	<div class="container">
		<div class="row mb20">
			<div class="col-md-12">
				<div class="cart-content-panel js-cartList ">
					<div class="cart-panel-heading">
						<h3 class="cart-title">Your Cart <span class='total_count_bracket'>(<span class='js-cartTotalItems'><?=$total?></span>)<span> </h3>
					</div>
					<div class="cart-panel-content">
					
						<?php
					
							foreach($products as $key => $item):	
								//TEMPLATE CART-ITEM
								$product = $this->utility->get_single_product($item->product_id);
								// Only display the product if it was actually found
								if(!empty($product))
								{
									$data = array(
										'id' => $item->product_id,
										'image' => $this->utility->get_single_product_image($item->product_id),
										'product' => $product,
										'quantity' => $item->quantity,
									);
									foreach($data as $key=>$val)
										$$key=$val;

									$is_available=true;
									if($product->is_available == '0' || $product->is_available == FALSE || $product->is_verified == '0' || $product->is_verified == FALSE || (int)$product->quantity == 0)
									{
										$is_available=false;
										$data['message'] = $product->product_name.' is not available';
										$data['action'] = [
											array(
												'text' => 'Move to wishlist',
												'extra_classes' => 'js-moveTo -f-c-t-w',
												'extra_attributes' => 'data-from="cart" data-to="wishlist" data-id="'.$product->id.'"'
											),
											array(
												'text' => 'Move to saved items',
												'extra_classes' => 'js-moveTo -f-c-t-si ml-3',
												'extra_attributes' => 'data-from="cart" data-to="savedItem" data-id="'.$product->id.'"'
											)
										];
									}	
							?>
							<div class="cart-item" id="ci_<?= $id ?>" data-id="<?= $id ?>" data-base-amount="<?= $product->unit_price ?>" style='margin-bottom:5px;'>
                                <div class="d-flex">
                                    <div class="mr-3">
                                        <div class="cart-img" style='background-image:url(<?=$data['image']->img_link?>)'></div>
                                    </div>
                                    <div>
                                        <div class="cart-item-content">
                                            <div class="row mb20 align-items-center">
                                                <div class="col-md-6">
                                                    <div class="cart-item-name">
                                                        <h2 class="mt0 mb0 d-inline-block text-truncate" style="max-width: 250px;" title="<?=$data['product']->product_name?>"><?=$data['product']->product_name?></h2>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="cart-option text-right">

												
															<ul class="list-inline mb0">
                                                                <li class="list-inline-item">
																	<div class="">
																	<a class="circle selected card-link px-1 js-moveTo -f-c-t-si" data-to="savedItem" data-from="cart" href="#!" title="Move to cart">
																		<i class="fa fa-save"></i>	
																		Save for later
																	</a>
																	</div>
																</li>
																<li class="list-inline-item">
																	<div class="">
																	<a class=" circle card-link  px-1 ml-1 js-moveTo -f-c-t-w" data-to="wishlist" data-from="cart" href="#!" title="Move to wishlist">
																	<i class="fa fa-heart"></i>
                                                                        Move To Wishlist
																	</a>
																	</div>
																</li>
																<li class="list-inline-item">
																	<div class="">
																	<a class=" circle card-link  px-1 ml-1 js-removeFromCart" href="#!" data-id="<?= $id ?>" title="Remove product(s) from cart">
																	<i class="fa fa-trash"></i>
                                                                        Remove
																	</a>
																	</div>
																</li>
															</ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb30">
                                                <div class="col-md-12">
                                                    <div class="cart-item-content">
                                                        <p class="mb0">
														<?=$data['product']->description?>
														</p>
                                                    </div>
                                                </div>
											</div>
											<?php if(!$is_available){?>
												<div class="row align-items-center">
                                                    <div class="col-md-12">
                                                        <div class="out-of-stock ">
                                                            <h3 class="mt0 mb0">This item is currently out of stock. 
                                                                <!-- <a href="#" class=""><i class="fa fa-save"></i> Save For Later</a> -->
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
											<?php } else { ?>
                                            <div class="row align-items-center item-amount-details">
                                                <div class="col-md-6">
                                                    <div class="select-quantity">
                                                        <div class="d-flex">
                                                            <div class="mr-3">
                                                                <span>Quantity</span>
                                                            </div>
                                                            <div>
                                                                <div class="quantity-dropdown">
																<form  action="cart/product/update/<?= $id ?>/" method="post">
																	<select class="form-control-sm w-auto js-updateItemNumber" 
																		name="number of items">
																		<?php 
																		$option_numbers = ($product->quantity > 10) ? 10 : $product->quantity;
																		for($i = 1; $i <= $option_numbers; $i++):  ?>
																		
																		<option value="<?= $i ?>" <?= ($i === (int)$quantity) ? 'selected' : '' ?>>
																			<?= (($i == 10) ? '10+': $i); ?>
																		</option>
																		
																		<?php endfor; 
																		if($quantity > 10) : ?>
																		
																		<option value="<?= $quantity ?>" selected disabled class="disabled">
																			<?= $quantity ?>
																		</option>
																		
																		<?php endif; ?>
																	</select>
																	<input type="number" max="<?= @$max; ?>" class="d-none" name="item-number" value="<?= $quantity ?>">
																</form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="cart-item-total text-right">
                                                        <h4>$ <span class="product-amount" ><?=$data['product']->unit_price?></span></h4>
                                                    </div>
                                                </div>
											</div>
											<?php }?>
                                        </div>
                                    </div>
                                </div>
                            </div>

							<?php 
							}
							endforeach; ?>
                        </div>
				</div>
			</div>
		</div>
	</div>	
</section>
<?php /* ?>
<!-- <div id="list"
	class="js-cartList yo-cart-list position-relative">
	

    <div class="cart-header position-relative">
	
        <h2 class="cart-title text-primary-dark">
			<?php 
				$this->load->view('_components/site/my_cart_icon', array(
					'total' => $total,
					'has_margin'=>TRUE,
					'is_dark'=>TRUE,
				));
            ?> MY CART</h2>
    </div>
    <div class="cart-items position-relative">

		<?php 
		if($total === 0) 
		{
			$this->load->view('_templates/empty_message', array(
				'image_url' => FALSE,
				'title' => 'No items were found in your cart',
				'message' => 'Add products to the cart for them to appear here',
				'button'=> array(
					'link' => site_url('explore'),
					'text' => '<i class="fa fa-cart-plus mr-2"></i>EXPLORE  PRODUCTS',
				),
			));
		}
		
		foreach($products as $key => $item):	
		//TEMPLATE CART-ITEM
        $product = $this->utility->get_single_product($item->product_id);

		// Only display the product if it was actually found
		if(!empty($product))
		{
			$data = array(
				'id' => $item->product_id,
				'image' => $this->utility->get_single_product_image($item->product_id),
				'product' => $product,
				'quantity' => $item->quantity,
			);

			if($product->is_available == '0' || $product->is_available == FALSE || $product->is_verified == '0' || $product->is_verified == FALSE || (int)$product->quantity == 0)
			{
				$data['message'] = $product->product_name.' is not available';
				$data['action'] = [
					array(
						'text' => 'Move to wishlist',
						'extra_classes' => 'js-moveTo -f-c-t-w',
						'extra_attributes' => 'data-from="cart" data-to="wishlist" data-id="'.$product->id.'"'
					),
					array(
						'text' => 'Move to saved items',
						'extra_classes' => 'js-moveTo -f-c-t-si ml-3',
						'extra_attributes' => 'data-from="cart" data-to="savedItem" data-id="'.$product->id.'"'
					)
				];
				$this->load->view('_components/site/cart_item_not', $data);
			} else {
				$this->load->view('_components/site/cart_item', $data);
			}
		}
		
		//TEMPLATE CART-ITEM End
		endforeach; ?>
	</div>
</div> -->
<?php */ ?>
