<div class="container yo-page-title px-0">
    <h1 class="/*d-inline-block*/ d-none text-primary-dark87 yo-title-lg">PRODUCTS</h1>
</div>
<div class="container yo-content px-0">
    <div class="yo-content-header">
        <p class="d-inline-block text-primary-dark87 yo-title">ARTS<br></p>
    </div>
    <div class="row mx-0">
        <div class="col-lg-3 p-0 side-bar">
            <!-- TEMPLATEstart category-list-->
            <?php
                $data = array();
                
                $this->load->view('', $data);
            ?>
            <!-- TEMPLATEend category-list-->
        </div>
        <div class="col-lg-9 mx-auto content px-0">
            <div class="row yo-products">
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                    <!-- TEMPLATEstart product_card-->
                    
                    <!-- TEMPLATEend product_card-->
                </div>
            </div>
        </div>
    </div>
</div>