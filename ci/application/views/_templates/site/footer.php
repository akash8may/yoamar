<div class="row bg-secondary26 py-3" style="margin-top: 40px;">
	<div class="row d-flex justify-content-center mx-auto">
		<div class="col-12">
			<h4 class="text-center text-primary-dark87 my-3">We ship overseas and locally</h4>
		</div>
		<div style="margin:0 36px;">
			<img style="opacity:.75" src="<?= asset_url('site/img/air.png'); ?>" height="52px" class="d-block mx-auto mb-2 text-center" />
			<p class="text-center text-muted">Freight/Air</p>
		</div>
		<div style="margin:0 36px;">
			<img style="opacity:.75" src="<?= asset_url('site/img/ship.png'); ?>" height="52px" class="d-block mx-auto mb-2 text-center" />
			<p class="text-center text-muted">Ship</p>
		</div>
        </div>
</div>

<footer class="bg-primary87 text-primary26 pb-3 position-relative <?= @$class; ?>" data-slideout-ignore>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-3">
				<h6>GENERAL</h6>
				<ul class="list-unstyled" style="line-height:32px;">
					<li><a class="text-white" href="<?= site_url('u/orders'); ?>">Track Your Order</a></li>
					<li><a class="text-white" href="<?= site_url('about'); ?>">About</a></li>
					<li><a class="text-white" href="<?= site_url('faq'); ?>">FAQ</a></li>
					<li><a class="text-white" href="#">Contact Us</a></li>
				</ul>
			</div>
			<div class="col-sm-6 col-md-3">
				<h6>SELLER &amp; CONTACT</h6>
				<ul class="list-unstyled" style="line-height:32px;">
					<li><a class="text-white" href="<?= site_url('auth/create_user/merchant'); ?>">Become a seller</a></li>
					<li><a class="text-white" href="<?= site_url('auth/login/merchant'); ?>">Seller login</a></li>
					<li><i class="fa fa-phone-square text-white pr-2"></i>
					<?php 
						$contact_phones = $this->config->item('contact_phones');
						foreach($contact_phones as $phone):?>
					<a class="text-white pr-2" href="tel:<?= $phone; ?>"><?= $phone; ?></a>		
					<?php endforeach; ?>
					</li>
					<li><i class="fa fa-envelope text-white pr-2"></i><a class="text-white" href="mailto:<?= $this->config->item('contact_email'); ?>"><?= $this->config->item('contact_email'); ?></a></li>
				</ul>
			</div>
			<div class="col-sm-6 col-md-3">
				<h6>POLICIES &amp; SITEMAP</h6>
				<ul class="list-unstyled" style="line-height:32px;">
					<li><a class="text-white" href="#">Refund Policy</a></li>
					<li><a class="text-white" href="#">Privacy Policy</a></li>
					<li><a class="text-white" href="#">Terms &amp; Conditions</a></li>
					<li><a class="text-white" href="#">Sitemap</a></li>
				</ul>
			</div>
			<div class="col-sm-6 col-md-3">
				<h6>JOIN US</h6>
				<ul class="list-inline">
					<li class="list-inline-item mx-2"><a class="text-white" href="#" style="font-size:36px;line-height:48px;"><i class="fa fa-facebook-official footer-icon"></i></a></li>
					<li class="list-inline-item mx-2"><a class="text-white" href="#" style="font-size:36px;line-height:48px;"><i class="fa fa-twitter-square footer-icon"></i></a></li>
					<li class="list-inline-item mx-2"><a class="text-white" href="#" style="font-size:36px;line-height:48px;"><i class="fa fa-google-plus-official footer-icon"></i></a></li>
				</ul>
				<h6>PAYMENT METHODS</h6>
				<ul class="list-inline my-2" style="font-size:36px;">
					<li class="list-inline-item mx-2" title="Visa"><i class="fa fa-cc-visa footer-icon"></i></li>
					<li class="list-inline-item mx-2" title="Mastercard"><i class="fa fa-cc-mastercard footer-icon"></i></li>
					<li class="list-inline-item mx-2" title="Mastercard"><i class="fa fa-cc-paypal footer-icon"></i></li>
					<li class="list-inline-item mx-2" id="mpesa-logo" title="MPesa"></li>
				</ul>
			</div>
		</div>
		<p class="text-center text-primary26 pt-4" style="font-size:14px;">© 2018 <a href="<?= site_url(); ?>">YoAMar</a></p>
	</div>
</footer>
