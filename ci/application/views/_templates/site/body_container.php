<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
	
	<meta name="description" content="<?= !empty($page_description) ? $page_description : ucfirst(strtolower(SITE_NAME)).' | One stop shop for exports and imports'; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?= $page_title ?? get_page_title(); ?></title>
    <link rel="stylesheet" href="<?= asset_url('site/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    
	<link rel="stylesheet subresource" href="<?= asset_url('site/css/styles.min.css'); ?>">
	<link rel="stylesheet subresource" href="<?= asset_url('site/css/merged-styles.css'); ?>">
	<link rel="stylesheet subresource" href="<?= asset_url('site/css/yo-styles.css'); ?>">
    <!-- Include any extra header scripts -->
	<?= $extra_header_content ?? ''; ?>
</head>

<body>
    <div id="slideout-body" class="bg-light">
        <?= get_value_or_default(@$page_content); ?>
        <?php $this->load->view('_templates/site/footer', array('class' => 'w-100 pt-5')); ?>
    </div>
    <?php 
    if(@$categories) {
        // $this->load->view('_components/site/slideout', array(
        //     'class' => ''
        // ));
    }
    ?>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="<?= asset_url('site/js/script.min.js'); ?>"></script>
    <script src="<?= asset_url('site/js/yo-es.js'); ?>"></script>
    <script src="<?= asset_url('site/js/zoomple.js'); ?>"></script>
   <script src="<?= asset_url('site/js/jquery.litebox.js'); ?>"></script>

    <script type="text/javascript">
        //portalUrl  pass the appropriate gateway address or endpoint
        if($("#Formlitebox").length){
            liteboxInitialise("https://portal.host.iveri.com/Lite/Authorise.aspx",'Formlitebox');
            //When litebox complete response in this function
            function liteboxComplete(data) {
               console.log(data);
            }
        }
    </script>
    <!-- <script src="<?= asset_url('site/js/slideout.min.js'); ?>"></script>
    <!-- Include any extra scripts -->
	<?= $extra_footer_content ?? ''; ?>
<!--End of Tawk.to Script-->
<!-- Firebase App is always required and must be first -->
<!-- <script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-app.js"></script> -->

<!-- Add additional services that you want to use -->
<!-- <script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-auth.js"></script> -->
<!-- <script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-database.js"></script> -->
<!-- <script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-firestore.js"></script> -->
<!-- <script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-messaging.js"></script> -->

<!-- Comment out (or don't include) services that you don't want to use -->
<!-- <script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-storage.js"></script> -->

    <script type="text/javascript">
        console.log(window.yo);
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),
                s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5c399d11361b3372892fc3fa/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();

        init();
        var yo;
        function init() {
            yo = {
                ui : window.yo,
                base_url : '<?= site_url(); ?>'
            };
            // window.yo = null;
            $('[data-toggle="popover"]').popover();
        };

        window.onload = init;
	</script>
</body>

</html>
