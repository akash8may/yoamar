<?php 
$has_dropdown = @$has_dropdown ?? TRUE;
//logo height = h = h - margin-top - margin-bottom
?>
<nav class="navbar navbar-light navbar-expand navigation-clean-search mx-auto mb-0 border-0 shadow-none rounded-0">
    <div class="container-fluid">
        <a class="navbar-brand" href="<?= site_url(); ?>">

            <img src="<?= asset_url('site/img/yoamar-light-2.png') ?>" class="img-logo" alt="<?= SITE_NAME ?>">
        </a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navcol-1">
            <!--TEMPLATEstart searchbar-->
            <?php
				$this->load->view('_components/site/searchbar', array(
					'class' => 'mx-auto',
					'extra_class' => @$searchbar_extra_class,
					'action'=>'search',
				));
			?>
            <!--TEMPLATEend searchbar-->

           <?php
           $carttotal= 0; 
           if(!empty($cart)){
            foreach($cart as $itom){
                $carttotal += $itom->quantity;
               }
           }
          
             ?>
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item d-sm-none" role="presentation">
                    <a class="nav-link js-toggleSearchbar" href="#!" data-action="open">
                        <i class="fa fa-search"></i>
                    </a>
                </li>
                <li class="nav-item" role="presentation">
                <div class="cart-header position-relative">
                    <a class="cart-title text-primary-dark my-cart yo-cart-icon"
                    <?php 
                    if($user !== FALSE && is_object($user)) echo 'style="margin-right:20px !important;"'; 
                    ?>
                     href="<?= site_url('cart'); ?>">
                        <?php $this->load->view('_components/site/my_cart_icon', array(
                            'total' => $carttotal,
                            'has_margin'=>FALSE,
                            'is_dark'=>FALSE,
                        )); ?>
                        </a>
                    <!-- <a class="nav-link my-cart yo-cart-icon" href="<?= site_url('cart'); ?>">
                        <i class="fa fa-shopping-cart"></i>
                    </a> -->
                </div>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link my-cart yo-wishlist-icon" href="<?= site_url('wishlist'); ?>" title="Wishlist" data-toggle="tooltip"
                     data-placement="bottom">
                        <i class="fa fa-heart-o"></i>
                    </a>
                </li>
                <li class="nav-item d-lg-none position-relative" role="presentation">
                    <?php if($user !== FALSE && is_object($user)) {
                    $component_data = [];
                    if($has_dropdown)
                    {
                        $component_data = array(
                            'extra_classes' => 'dropdown-toggle',
                            'attributes' => 'data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"',
                        );
                    }
                    $this->load->view('_components/site/user_info', array_merge((array)$user, $component_data));
                    if($has_dropdown) {
                        $this->load->view('_templates/header_user_dropdown');
                    }
                
                    } else { ?>
                    <a class="nav-link <?= ($has_dropdown)? 'dropdown-toggle' : '' ?>" href="#!" <?=($has_dropdown)?
                        'data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"' : '' ?>>
                        <i class="fa fa-user-circle"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow-sm">
                        <a class="dropdown-item" href="<?= site_url('auth/login') ?>">Log in</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= site_url('auth/create_user') ?>">Sign up</a>
                    </div>


                <?php } ?>
                </li>
            </ul>
            <?php if($user !== FALSE && is_object($user)) : ?>
            <div class="_dropdown position-relative d-none d-sm-none d-lg-inline-block">
                <?php
                $component_data = [];
                if($has_dropdown)
                {
                    $component_data = array(
                        'extra_classes' => 'dropdown-toggle',
                        'attributes' => 'data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"',
                    );
                }
                $this->load->view('_components/site/user_info', array_merge((array)$user, $component_data));
                
                if($has_dropdown) {
                    $this->load->view('_templates/header_user_dropdown');
                }
                ?>
            </div>
            <?php else : ?>
            <a href="<?= site_url('auth/create_user') ?>" class="btn btn-primary d-none d-sm-none d-lg-inline-block my-btn-primary btn-outline btn-cta bg-light">SIGN-UP</a>
            <a href="<?= site_url('auth/login') ?>" class="btn btn-primary d-none d-sm-none d-lg-inline-block my-btn-primary text-right btn-cta bg-light">LOGIN</a>
            <?php endif; ?>
        </div>
    </div>
</nav>
<div class="js-fixedNavbar position-absolute w-100 px-2 d-none">
    <a class="js-toggleSearchbar ml-2 text-light text-center align-self-center" href="#!" data-action="close">
        <i class="fa fa-close"></i>
    </a>
</div>
