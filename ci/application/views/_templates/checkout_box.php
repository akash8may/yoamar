<div class="cart-checkout-wrapper">
    <div class="cart-content-panel cartcheckout">
            <div class="cart-panel-heading">
                <h3 class="cart-title">Price Detail </h3>
            </div>
            <div class="cart-panel-content">
                <div class="checkout-items">
                    <p class="mb10"> <span class="">Price <span class='cart-itom-count'> (<span class="js-cartTotalItems"><?= $product_counts ?></span> item<?= ($product_counts > 1) ? 's' : '' ?>)</span></span> <span class="total float-right">
                    $ <span  class="js-cartBasePrice" ><span><?= $total_cart_amount ?></span></span>
                    </span>
                    </p>    
                    <p class="mb10"><span class="">Delivery Charges</span> 
                    <span class="total float-right">
                    $ <span class="js-cartDeliveryPrice"><span><?= $Delivery_amount ?></span></span>
                        </span>
                    </p>
                    
                    <div class="dropdown-divider"></div>
                     
                    <p class="mb20"><span class="">Amount Paybale</span><span class="float-right">
                    $ <span class="js-cartTotalPrice"><span><?= $total_cart_amount ?></span></span>
                    </span></p>
                    <a href="<?= site_url('u/checkout'); ?>" class="btn btn-checkout btn-block btn-primary87 text-white d-block w-100 font-weight-bold my-2 mx-auto text-center js-checkOut px-2 shadow">Checkout</a>
                
                    <a href="/" class="btn btn-continue btn-block">Continue Shopping</a>
                </div>
                
            </div>
           
    </div>
</div>


<?php 
/*
?>
<div id="cart<?= $id ?>" class="js-cartListCheckOut yo-checkout-container pr-0">
    <div class="checkout-header pb-1 border-bottom mr-5 ml-4">
        <div class="title-box d-none"></div>
        <h1 class="text-capitalize cart-price m-0">
            <span class="font-weight-bold text-dark54">TOTAL:</span>
            <span class="text-primary54 font-weight-normal pl-3 js-cartTotalPrice">$<span><?= $total_cart_amount ?></span></span>
        </h1>
        <h5 class="text-capitalize text-dark54">
            <span class="js-cartTotalItems"><?= $product_counts ?></span> item<?= ($product_counts > 1) ? 's' : '' ?></h5>
    </div>
    <div class="checkout-body border-left mr-5 pl-4 mt-2">
        <p class="checkout-details m-0 pb-2 d-none"> <?php //SHIPPING INFORMATION ?>
            <span class="font-weight-bold mr-2">Estimated delivery time:</span>
            <span class="font-weight-light" data-toggle="tooltip" data-placement="bottom" title="16 Business days">16 days <i class="fa fa-info-circle"></i></span>
        </p>
        <p class="checkout-details m-0 pb-2 d-none"> <?php //SHIPPING INFORMATION ?>
            <span class="font-weight-bold mr-2">Estimated weight:</span>
            <span class="font-weight-light">19kgs</span>
        </p>
    </div>
    <div class="row checkout-footer border-left mr-5 pl-4 py-4 mx-0">
        <div class="col-12 ml-auto pl-0 border-right d-none"> <?php //DISCOUNT BOX ?>
            <div class="info-box text-center">
                <h1 class="font-weight-bold text-dark26">25% off</h1>
                <h6 class="text-center font-weight-light">on 25 items and above</h6>
            </div>
        </div>
        <div class="col-12 pr-0">
            <a href="<?= site_url('u/checkout'); ?>" class="btn btn-lg btn-primary87 text-white d-block w-100 font-weight-bold my-2 mx-auto text-center js-checkOut px-2 shadow">CHECK OUT</a>
        </div>
    </div>
</div>
<?php */ ?>