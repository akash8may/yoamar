<div role="dialog" tabindex="-1" class="modal fade" id="modalReview">
	<div class="modal-dialog modal-dialog-centered bg-light" role="document" style="pointer-events: auto;">
		<form action="@" method="post" class="px-4 mx-auto" id="formReview">
			<div class="modal-header">
				<h4 class="modal-title">Thank you for reviewing this product</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			</div>
			<div class="modal-body">
				<div class="d-block">
					<?php
                            $this->load->view('_components/ratings', array(
                                'rating' => 0,
                                'extra_classes' => 'fa-2x px-1',
                                'is_dynamic' => TRUE,
                                'is_readonly' => FALSE
                            ));
                            ?>
					<div class="form-group">
						<label class="mb-1">Comment</label>
						<div class="review-editor"></div>
						<textarea name="review" class="form-control form-control-lg" style="min-height: 10rem;" rows="8"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-light" type="button" data-dismiss="modal">Close</button>

				<button class="btn btn-success text-center py-2" type="button" id="btnAddReview" data-product-id="">ADD REVIEW</button>
				<?php ?>
			</div>
		</form>
	</div>
</div>
