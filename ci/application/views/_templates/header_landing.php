<?php 
$has_dropdown = @$has_dropdown ?? TRUE;
?>
<div class="top-bar">
	<div class="container-fluid">
		<div class="row d-flex align-items-center">
			<div class="col-sm-7 d-none d-sm-block">
				<ul class="list-inline mb-0">
				<li class="list-inline-item pr-3 mr-0">
				<i class="fa fa-phone mr-1"></i>
				<?php 
					$contact_phones = $this->config->item('contact_phones');
					foreach($contact_phones as $phone):?>
						<a href="tel:<?= $phone ?>" class="border-right pr-2"><?= $phone; ?></a>
				<?php endforeach; ?>
				</li>
					<!-- <li class="list-inline-item px-3 border-left d-none d-lg-inline-block">Free shipping on orders over Ksh 10,000</li> -->
				</ul>
			</div>
			<div class="col-sm-5 d-flex justify-content-end">
				<!-- Language Dropdown-->
				<div class="dropdown border-right px-3">
                    <a id="langsDropdown" href="https://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle topbar-link">
                        <img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-2/img/united-kingdom.svg" alt="english" class="topbar-flag">
                        English</a>
					<div aria-labelledby="langsDropdown" class="dropdown-menu dropdown-menu-right">
                        <a href="index.html#" class="dropdown-item text-sm"> 
                            <img src="<?= asset_url('site/img/cn.png') ?>" alt="chinese" class="topbar-flag">
                            Chinese</a>
                    </div>
				</div>
				<!-- Currency Dropdown-->
				<div class="dropdown pl-3 ml-0">
                    <a id="currencyDropdown" href="index.html#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle topbar-link">USD</a>
                    <div aria-labelledby="currencyDropdown" class="dropdown-menu dropdown-menu-right">
                        <a href="index.html#" class="dropdown-item text-sm">KES</a>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<header class="yo-header <?= @$header_extra_classes ?>">
	<?php // TEMPLATEstart header_nav
        $data = array(
            'searchbar_extra_class' => 'd-none',
			'has_dropdown' => $has_dropdown,
			'cart'=>empty($cart)?0:$cart
        );
        $this->load->view('_templates/header_nav', $data);
        
        // TEMPLATEend header_nav
    ?>
	<div class="container fold-container">
		<div class="row">
			<div class="col-10 col-xl-8 offset-1 offset-xl-2">
				<h1 class="text-center text-light fold-title">Increasing Africa's exports</h1>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<!--TEMPLATEstart searchbar-->
				<?php 
                        $this->load->view('_components/site/searchbar',array(
                            'id' => 'navSearchbarLanding',
                            'size' => 'large',
                            'class' =>'mx-auto',
                            'extra_class' =>'',
                        ));
                    ?>

			</div>
		</div>
	</div>
	<div class="img-underlay">
		<img async="on" src="<?= asset_url('site/img/pexels-photo-696205_thumb.jpeg') ?>"
		 width="100%"></div>
</header>
