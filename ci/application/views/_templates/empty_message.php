<?php 
	$title = $title ?? 'Missing Information';
	$message = $message ?? 'Could not find the information you were looking for';
	$button = $button ?? NULL;
	$_button_text = '';
	$_button_link = '#';
	$image_url = @$image_url ?? FALSE;

	if(is_array($button) && !empty($button))
	{
		$_button_link = $button['link'] ?? $_button_link;
		$_button_text = $button['text'] ?? $_button_text;
		$_button_class = $button['extra_classes'] ?? '';
		$_button_attributes = $button['extra_attributes'] ?? '';
	}
?>
<div class="jumbotron text-center <?= get_value_or_default(@$extra_classes); ?> <?= ($image_url !== FALSE) ? 'bg' : '' ?>"
	style="<?= ($image_url !== FALSE) ? 'background-image: url(\''.$image_url.'\');' : '' ?>">
	<?php if(!empty($title)): ?>
	<h3><?= $title; ?></h3>
	<?php endif;?>
	
	<p class="lead"><?= $message; ?></p>

	<?php if(!empty($_button_text)): ?>
	<a href="<?= $_button_link ?>" class="btn btn-primary btn-lg font-weight-light text-uppercase <?= $_button_class ?>" <?= $_button_attributes ?>><?= $_button_text; ?></a>
	<?php endif;?>
</div>
