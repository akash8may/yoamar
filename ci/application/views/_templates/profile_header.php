<?php 
$user_img = @$picture_url ?? base_url(PRODUCT_PLACEHOLDER_IMG);
?>

<section id="foldSection" 
    class="shadow-sm mb-5 position-relative d-block bg-light26">
    <div class="container yo-page-title px-0 pb-3">
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="user-item item-large">
                    <div class="user-container pb-1">
                        <a href="#" class="user-avatar">
                            <img class="rounded-circle img-fluid" 
                                src="<?= $user_img ?>"></a>
                        <p class="user-name">
                            <a class="d-block"><?= $user->first_name.' '.$user->last_name ?></a>
                            <span class="text-lowercase mb-2">
                                <i class="fa fa-map-marker mr-2 d-none"></i>
                                <?= $user->username ?></span></p>
                        <a class="btn btn-outline-primary btn-sm border-0 mr-1 text-right bg-white text-primary87" href="<?= site_url('u/notifications') ?>"><i class="fa fa-comments pr-2"></i>2000</a>
                        <a class="btn btn-outline-primary btn-sm border-0 mr-1 text-right bg-white text-primary87" href="<?= site_url('u/payment_options') ?>"><i class="fa fa-credit-card pr-2"></i>3</a>
                        <a class="btn btn-outline-primary btn-sm border-0 mr-1 text-right bg-white text-primary87" href="<?= site_url('u/profile/edit') ?>">
                            <i class="fa fa-edit mr-2"></i>edit profile</a>
                        <a href="#" class="user-delete"> <i class="fa fa-remove"></i></a>
                        </div>
                            
                </div>
            </div>
            <div class="col">
                
            </div>
        </div>
    </div>
    <div class="position-absolute w-50 yo-user-bg h-100"></div>
</section>