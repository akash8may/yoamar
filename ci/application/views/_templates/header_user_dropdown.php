<div class="dropdown-menu dropdown-menu-right shadow-sm">
    <a class="dropdown-item" href="<?= site_url('u/orders') ?>">My orders</a>
    
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="<?= site_url('cart#saved-items') ?>">Saved items</a>
    
    <?php if($user->group == 'buyer'): 
        if(isset($current_order)): ?>
    <div class="dropdown-divider"></div>
    <h6 class="dropdown-header">Track your orders</h6>
    <a class="dropdown-item" href="<?= site_url('u/orders/'.$current_order->order_id) ?>">Current order</a>
    <?php endif; endif; ?>
    
    <div class="dropdown-divider"></div>
    <h6 class="dropdown-header">
        <?= $user->email ?>
    </h6>
    <?php 
    if ($user->group == 'buyer')
    {
        $profile_url = site_url('u/profile');
    } else {
        $profile_url = site_url('merchant/profile');
    }
    ?>
    <a class="dropdown-item" href="<?= $profile_url ?>">Profile</a>

    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="<?= site_url('auth/logout') ?>">Logout</a>
</div>