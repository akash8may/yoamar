<section id="foldSection" 
    class="shadow-sm mb-3 position-relative d-block bg-light26">
    <div class="container yo-page-title px-0 pb-3">
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="user-item item-large">
                    <div class="user-container">
                        <a href="#" class="user-avatar">
                            <img class="rounded-circle img-fluid" 
                                src="<?= asset_url('site/img/3319164835_0740b7ef40_o.jpg'); ?>"></a>
                        <p class="user-name">
                            <a href="#">Mark Smith Peterson</a>
                            <span class="d-block"><i class="fa fa-map-marker mr-2"></i>Nairobi, KE</span></p>
                        <a class="btn btn-outline-primary btn-lg js-addToFavorite text-right border-0 my-2 mr-1" href="#"><i class="fa fa-heart"></i></a>
                        <a class="btn btn-outline-primary btn-lg js-addToFavorite text-right border-0 mr-1" href="#"><i class="fa fa-comments pr-2"></i>2000</a>
                        
                        <a href="#" class="user-delete"> <i class="fa fa-remove"></i></a></div>
                </div>
            </div>
            <div class="col"></div>
            <div class="col-10 offset-xl-1">
                <!-- TEMPLATEstart badges-list-->
                <?php 
                $data = array(
                    'class' => 'p-3'
                );
                
                $this->load->view('_components/site/badges_list', $data);
                
                ?>
                <!-- TEMPLATEend badges-list-->
            </div>
        </div>
    </div>
    <div class="position-absolute w-50 yo-user-bg h-100"></div>
</section>