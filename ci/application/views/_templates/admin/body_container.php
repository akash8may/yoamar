<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $page_title ?? get_page_title(); ?></title>
	<link rel="stylesheet" href="<?= asset_url('admin/bootstrap/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?= asset_url('site/css/yo-styles.css'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    
	<link rel="stylesheet" href="<?= asset_url('admin/css/styles.min.css'); ?>">
	<?= $extra_header_content ?? ''; ?>
	<script type="text/javascript">
    window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","resetIdentity","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
      heap.load("1157807893");
</script>
</head>

<body>
	<div id="wrapper">
		<?php 
			if(!empty(@$sidebar_nav))
			{
				echo $sidebar_nav;
			}
		?>

		<div class="page-content-wrapper">
			<div class="container-fluid">
				<!-- Top nav -->
                <div class="row">
                    <div class="col mb-2 mb-lg-4"><a class="btn btn-link d-lg-none" role="button" href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars text-primary" style="font-size:24px;"></i></a>
                        <div class="d-block float-right d-md-inline justify-content-end" id="top-nav-extra">
							<a class="float-right text-primary54 mr-2" href="<?= site_url($profile_url); ?>" style="font-size:24px;" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="fa fa-user-circle"></i></a>
							<a class="float-right text-primary54 mr-2 mr-md-3 mr-lg-4 yo-notification-icon" href="<?= site_url($notifications_url); ?>" style="font-size:24px;" data-toggle="tooltip" data-placement="bottom" title="Notifications">
								<i class="fa fa-bell"></i>
								<i class="fa fa-circle text-success notification-button <?= empty($new_notifications) ? 'd-none' : '' ?>"></i></a></div>
                    </div>
                </div>
				<div style="min-height: 85vh;">
				<?php
					// Body content 
					if(!empty(@$body_content))
					{
						echo $body_content;
					}
				?>
				</div>
				<?php
					// Footer content
					if(!empty(@$footer_content))
					{
						echo $footer_content;
					}
				?>
			</div>
		</div>
	</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="<?= asset_url('admin/js/script.min.js'); ?>"></script>
	<script src="<?= asset_url('admin/js/bootstrap-colorpicker.min.js'); ?>"></script>
	<!-- Include any extra scripts -->
	<?= $extra_footer_content ?? ''; ?>
	<script type="text/javascript">
		var yo;
		console.log(window.yo_);
		yo = {
			ui : window.yo_,
			base_url : '<?= site_url(); ?>'
		};
	</script>
</body>

</html>
