<?php
$action_url = $action_url ?? site_url('u/payment_edit');
$mpesa = $mpesa ?? TRUE
?>

<div role="dialog" tabindex="-1" class="modal fade" id="modalAddPaymentMethod">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <form class="modal-content" method="post" action="<?= $action_url ?>">
                <div class="modal-header">
                    <h4 class="modal-title">Add a payment method</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                <div class="modal-body">
                    <div>
                        <input type="text" name="payment_method" hidden value="card"/>
                        <ul class="nav nav-pills nav-justified">
                            <li class="nav-item">
                                <a role="tab" data-toggle="pill" href="#card" class="nav-link position-relative active">
                                    <i class="fa fa-credit-card mr-2"></i>CREDIT CARD
                                    <input type="radio" name="_payment_method" value="card" id="_card" class="custom-radio radio-tab"/>
                                </a></li>
                            <li class="nav-item">
                                <a role="tab" data-toggle="pill" href="#online" class="nav-link position-relative ">
                                    <i class="fa fa-paypal mr-2"></i>PAYPAL
                                    <input type="radio" name="_payment_method" value="online" id="_online" class="custom-radio radio-tab"/>
                                </a></li>
                            <?php if($mpesa == TRUE): ?>
                            <li class="nav-item">
                                <a role="tab" data-toggle="pill" href="#mobile" class="nav-link position-relative ">
                                    mPesa
                                    <input type="radio" name="_payment_method" value="mobile" id="_mobile" class="custom-radio radio-tab"/>
                                </a></li>
                            <?php endif; ?>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="card">
                                <div class="row py-3 border-bottom">
                                    <div class="col-12 col-sm-5">
                                        <div class="form-group">
                                            <label>card number</label>
                                            <input type="number" class="col-12 form-control" name="card_number" /></div>
                                    </div>
                                    <div class="col-12 col-sm-4">
                                        <div class="form-group">
                                            <label>expiry date</label>
                                            <input type="date" class="col-12 form-control" name="card_expiry"/></div>
                                    </div>
                                    <div class="col-12 col-sm-3">
                                        <div class="form-group">
                                            <label>CVC</label>
                                            <input type="number" class="col-12 form-control validate" name="card_security_code" min="000" max="999"/></div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="online">
                                <div class="row py-3 border-bottom">
                                    <div class="col-12 col-sm-8 offset-sm-2">
                                        <div class="form-group">
                                            <label>Paypal email address</label>
                                            <input type="email" placeholder="email address" class="form-control form-control-lg col-12" name="paypal_id" /></div>
                                    </div>
                                </div>
                            </div>
                            <?php if($mpesa == TRUE): ?>
                            <div role="tabpanel" class="tab-pane" id="mobile">
                                <div class="row py-3 border-bottom">
                                    <div class="col-12 col-sm-8 offset-sm-2">
                                        <div class="form-group">
                                            <label>safaricom number</label>
                                            <input type="number" class="form-control form-control-lg col-12" placeholder="(+254)" name="mpesa_phone" /></div>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary my-btn-primary" type="submit">ADD PAYMENT METHOD</button>
                </div>
            </form>
        </div>
    </div>
    <div role="dialog" tabindex="-1" class="modal fade" id="modalEditPaymentMethod">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <form class="modal-content" method="post" data-action="<?= $action_url ?>" action="<?= $action_url ?>">
                <div class="modal-header">
                    <h4 class="modal-title">Edit payment method</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                <div class="modal-body">
                    <div>
                    <input type="text" name="payment_method" hidden value="card"/>
                        <ul class="nav nav-pills nav-justified">
                            <li class="nav-item">
                                <a role="tab" data-toggle="pill" href="#card2" class="nav-link position-relative active">
                                    <i class="fa fa-credit-card mr-2"></i>CREDIT CARD
                                    <input type="radio" name="_payment_method" value="card" id="_card" class="custom-radio radio-tab"/>
                                </a></li>
                            <li class="nav-item">
                                <a role="tab" data-toggle="pill" href="#online2" class="nav-link position-relative ">
                                    <i class="fa fa-paypal mr-2"></i>PAYPAL
                                    <input type="radio" name="_payment_method" value="online" id="_online" class="custom-radio radio-tab"/>
                                </a></li>
                            <?php if($mpesa == TRUE): ?>
                            <li class="nav-item">
                                <a role="tab" data-toggle="pill" href="#mobile2" class="nav-link position-relative ">
                                    mPesa
                                    <input type="radio" name="_payment_method" value="mobile" id="_mobile" class="custom-radio radio-tab"/>
                                </a></li>
                            <?php endif; ?>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="card2">
                                <div class="row py-3 border-bottom">
                                    <div class="col-12 col-sm-5">
                                        <div class="form-group">
                                            <label>card number</label>
                                            <input type="number" class="col-12 form-control" name="card_number" /></div>
                                    </div>
                                    <div class="col-12 col-sm-4">
                                        <div class="form-group">
                                            <label>expiry date</label>
                                            <input type="date" class="col-12 form-control" name="card_expiry"/></div>
                                    </div>
                                    <div class="col-12 col-sm-3">
                                        <div class="form-group">
                                            <label>CVC</label>
                                            <input type="number" class="col-12 form-control validate" name="card_security_code" min="000" max="999"/></div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="online2">
                                <div class="row py-3 border-bottom">
                                    <div class="col-12 col-sm-8 offset-sm-2">
                                        <div class="form-group">
                                            <label>Paypal email address</label>
                                            <input type="email" placeholder="email address" class="form-control form-control-lg col-12" name="paypal_id" /></div>
                                    </div>
                                </div>
                            </div>
                            <?php if($mpesa == TRUE): ?>
                            <div role="tabpanel" class="tab-pane" id="mobile2">
                                <div class="row py-3 border-bottom">
                                    <div class="col-12 col-sm-8 offset-sm-2">
                                        <div class="form-group">
                                            <label>safaricom number</label>
                                            <input type="number" class="form-control form-control-lg col-12" placeholder="(+254)" name="mpesa_phone" /></div>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-danger float-left mr-auto js-deletePayment" type="submit">DELETE PAYMENT METHOD</button>
                    <input type="checkbox" name="delete_payment_info" class="js-deletePayment" hidden/>
                    <button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary my-btn-primary" type="submit">UPDATE PAYMENT METHOD</button>
                </div>
            </form>
        </div>
    </div>