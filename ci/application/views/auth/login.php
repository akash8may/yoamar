<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
	<meta name="description" content="<?= !empty($page_description) ? $page_description : ucfirst(strtolower(SITE_NAME)).' | One stop shop for exports and imports'; ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <title><?= $title ?? get_page_title(); ?></title>
      
      <link rel="stylesheet prefetch" href="<?= asset_url('site/bootstrap/css/bootstrap.min.css'); ?>">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    
	<link rel="stylesheet subresource" href="<?= asset_url('site/css/styles.min.css'); ?>">
	<link rel="stylesheet subresource" href="<?= asset_url('site/css/merged-styles.css'); ?>">
	<link rel="stylesheet subresource" href="<?= asset_url('site/css/yo-styles.css'); ?>">

      <script src='https://www.google.com/recaptcha/api.js'></script>
      <script type="text/javascript">
            window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","resetIdentity","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
            heap.load("1157807893");
      </script>
</head>
<body>
      <?php $this->load->view('_templates/header_base', array(
            'header_extra_classes' => 'mb-5'
      )); ?>
<main>
      <?php if(! empty($message)): ?>
      <div class='container'>
            <div class='row'>
                  <div id="infoMessage" class='col-10 offset-1 col-md-8 offset-md-2 p-4 bg-info12 border'><?= $message ?></div>
            </div>
      </div>
      <?php 
      endif;
	$input_classes = array('class' => 'form-control');
	$account_type = $this->uri->segment(3);
	
	$redirect_path = site_url('auth/login/'.$account_type);
      echo form_open($redirect_path, array(
                  'class' => 'd-flex flex-column justify-content-center',
                  'id' => 'login-box',
            ));
      ?>
      <div class='login-box-header p-4'>
            <h1 class="text-center"><?php echo lang('login_heading');?></h1>
            <p class="text-center text-gray"><?php echo lang('login_subheading');?></p>
      </div>

      <div class='px-4 pb-3'>
            <?php echo lang('login_identity_label', 'identity');?>
            <?php 
            echo form_input(array_merge($identity, $input_classes));?>
      </div>
      <div class='px-4 pb-2'>
            <?php echo lang('login_password_label', 'password');?>
            <div class="input-group">
                  <?php echo form_input(array_merge($password, ['class' => 'form-control password-field']));?>
                  <div class="input-group-append">
                        <i class="fa fa-eye password-revealer p-3 bg-secondary54 border-right"></i>
                  </div>
            </div>
      </div>

      <div class='submit-row p-4'>
            <div class='custom-control custom-checkbox pb-3' id="form-check-rememberMe">
                  <?php echo form_checkbox('remember', '1', FALSE, array(
                        'id' => 'remember',
                        'name' => 'remember',
                        'class' => 'custom-control-input',
                  ));
                  ?>
                  <label class="custom-control-label" for="remember">
                        <span class="label-text">Remember Me</span>
                  </label>
            </div>
            <div>
                  <div class="g-recaptcha py-3 mx-auto" data-sitekey="6LfHKY0UAAAAABIGc3ROJjaxzQjHG0Y8_uaXw7i9" data-callback="recaptchaCallback"></div>
            </div>
            <?php echo form_submit('submit', lang('login_submit_btn'), array(
                  'class' => 'btn btn-primary btn-block shadow-sm',
                  // 'disabled' => 'true'
            ));?>
      </div>

      <div class='p-4 border-top'>
            <a href="<?= site_url('auth/forgot_password') ?>"><?php echo lang('login_forgot_password');?></a>
            <?php $is_merchant_login = $is_merchant_login ?? FALSE;
                  if($is_merchant_login == TRUE): ?>
            <a href="<?= site_url('auth/login/buyer') ?>" class='text-right float-right text-primary-dark54'>Buyer Login</a>
                  <?php else: ?>
            <a href="<?= site_url('auth/login/merchant') ?>" class='text-right float-right text-primary-dark54'>Merchant Login</a>
                  <?php endif; ?>
      </div>

<?php echo form_close();?>
</main>
<?php $this->load->view('_templates/site/footer', array('class' => 'w-100 pt-5')); ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="<?= asset_url('site/js/script.min.js'); ?>"></script>
    <script src="<?= asset_url('site/js/yo-es.js'); ?>"></script>
    <script src="<?= asset_url('site/js/forms.js'); ?>"></script>
    <script type="text/javascript">
        var yo = {
            base_url : '<?= site_url(); ?>'
        };
        function recaptchaCallback(g_token) {
            // alert(g_token);
            var _data = {
                  secret: '6LfHKY0UAAAAAIwj7sHG1-IetSKWgOixYix_onCT',
                  response: g_token
            };

            $.ajax({
                  type: "POST",
                  url: "<?= site_url('api/verify_recaptcha'); ?>",
                  data: {
                        '_data': _data
                  },
                  dataType: "json",
                  success: function (response) {
                        alert(response);
                        console.log(response);
                  },
                  error: function (response) {
                        alert('error');
                        console.error(this.responseText);
                        console.error(response);
                  }
            });
        }
	</script>
</body>

</html>
