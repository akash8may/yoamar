<h1><?php echo lang('change_password_heading');?></h1>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/change_password");?>

      <p>
            <?php echo lang('change_password_old_password_label', 'old_password');?> <br />
            <?php echo form_input($old_password);?>
      </p>

      <p>
            <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
            <div class="input-group">
                  <?php echo form_input($new_password, ['class' => 'password-field']);?>
                  <div class="input-group-append">
                        <i class="fa fa-eye password-revealer p-3 bg-secondary54 border-right"></i>
                  </div>
            </div>
      </p>

      <p>
            <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> <br />
            <div class="input-group">
                  <?php echo form_input($new_password_confirm, ['class' => 'password-field']);?>
                  <div class="input-group-append">
                        <i class="fa fa-eye password-revealer p-3 bg-secondary54 border-right"></i>
                  </div>
            </div>
      </p>

      <?php echo form_input($user_id);?>
      <p><?php echo form_submit('submit', lang('change_password_submit_btn'));?></p>

<?php echo form_close();?>
