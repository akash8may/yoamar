<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	
	<meta name="description" content="<?= !empty($page_description) ? $page_description : ucfirst(strtolower(SITE_NAME)).' | One stop shop for exports and imports'; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?= $title ?? get_page_title(); ?></title>
    <link rel="stylesheet prefetch" href="<?= asset_url('site/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    
	<link rel="stylesheet subresource" href="<?= asset_url('site/css/styles.min.css'); ?>">
	<link rel="stylesheet subresource" href="<?= asset_url('site/css/merged-styles.css'); ?>">
	<link rel="stylesheet subresource" href="<?= asset_url('site/css/yo-styles.css'); ?>">
</head>
<body>
      <?php $this->load->view('_templates/header_base', array(
            'header_extra_classes' => 'bg-primary54 mb-5'
      )); ?>
<main>
      <?php if(! empty($message)): ?>
      <div class='container'>
            <div class='row'>
                  <div id="infoMessage" class='col-10 offset-1 col-md-8 offset-md-2 p-4 bg-info12 border'><?= $message ?></div>
            </div>
      </div>
      <?php 
      endif;
	$input_classes = array('class' => 'form-control');
	$account_type = $this->uri->segment(3) ?? '';
	
	$redirect_path = site_url('auth/forgot_password/'.$account_type);
      echo form_open($redirect_path, array(
                  'class' => 'd-flex flex-column justify-content-center',
                  'id' => 'login-box',
            ));
      ?>
      <div class='login-box-header p-4'>
            <h1 class="text-center"><?php echo lang('forgot_password_heading');?></h1>
            <p class="text-center text-gray"><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
      </div>

      <div class='px-4 pb-3'>
            <label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label>
            <?php 
            echo form_input(array_merge($identity, $input_classes));?>
      </div>
      <div class='submit-row p-4'>
            <?php echo form_submit('submit', lang('forgot_password_submit_btn'), array(
                  'class' => 'btn btn-primary btn-block shadow-sm',
            ));?>
      </div>

<?php echo form_close();?>
</main>
<?php $this->load->view('_templates/site/footer', array('class' => 'w-100 mt-5')); ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="<?= asset_url('site/js/script.min.js'); ?>"></script>
    <script src="<?= asset_url('site/js/yo-es.js'); ?>"></script>
    <script src="<?= asset_url('site/js/toastify.js'); ?>"></script>
    <script type="text/javascript">
        var yo = {
            ui : new Ui(),
            async : new Async(),
            base_url : '<?= site_url(); ?>'
        };
	</script>
</body>

</html>
