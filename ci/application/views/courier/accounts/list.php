<div class="row">
	<div class="col">
		<div class="alert alert-info mt-4" role="alert" id="info-message">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<span class="num-buyer-accounts">125,000</span>
			<span>&nbsp;child courier accounts found.
				<strong>Child courier accounts cannot create other courier accounts.</strong>
			</span>
		</div>
	</div>
	<div class="col-md-12 mt-3">
		<div class="row">
			<div class="col-10">
				<?php 
					$this->load->view('_components/admin/search_bar',array(
						'filter_list'=> $filter_list ?? [],
						'btn_text'=>'FILTER',
						'placeholder_text'=>'Filter courier accounts',
					));
				?>
			</div>
			<div class="col-2">
				<div class="float-right">
				<?php 
					$this->load->view('_components/admin/buttons/btn_create', array(
						'btn_link'=>base_url('admin/accounts/courier/add'),
						'btn_extra_class'=>'',
						'btn_title'=>'Create account',
						'btn_text'=>'',#Bugged if this isn't used. Carries value from above ~ probably an unset issue.
						//*TODO: Report this as bug to codeigniter ~ data values replicated across views loaded consecutively
					));
				?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 mt-2 mt-md-4">
		<div class="table-responsive ">
			<table class="table  table-hover">
				<caption class="text-center">
					<a class="btn btn-secondary btn-sm btn-load-more" role="button" href="#">LOAD MORE</a>
				</caption>
				<thead>
					<tr>
						<th class="text-center"></th>
						<th>Name</th>
						<th data-toggle="tooltip" data-placement="bottom" title="No. of registered products">Location</th>
						<th>Email</th>
						<th>Registrant Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">
							<input type="checkbox">
						</td>
						<td>Posta</td>
						<td>Nairobi, Kenya</td>
						<td>john@posta.co.ke</td>
						<td>John Mwau</td>
						<td class="text-center text-white">
							<a class="btn btn-dark btn-sm ml-lg-1 mb-2" data-toggle="tooltip" data-placement="bottom" title="Edit">
								<span>
									<i class="fa fa-pencil"></i>
								</span>
							</a>

							<a class="btn btn-warning btn-sm ml-lg-1 mb-2" data-toggle="tooltip" data-placement="bottom" title="Disable account">
								<i class="fa fa-remove"></i>
								<span class="d-none d-lg-inline">Disable</span>
							</a>
						</td>
					</tr>
					<tr>
						<td class="text-center">
							<input type="checkbox">
						</td>
						<td>DHL</td>
						<td>Nairobi, Kenya</td>
						<td>doe@dhl.com</td>
						<td>Doe Oruca</td>
						<td class="text-center" data-account-enabled="1">
							<?php 
								//Edit button
								$this->load->view('_components/admin/buttons/btn_edit',array(
									'btn_link'=>base_url('admin/accounts/courier/edit/1'),#TODO: Make dynamic
									'btn_extra_class'=>'btn-edit_courier',
								));

								//Enable/Disable button
								$this->load->view('_components/admin/buttons/btn_enable',array(
									'btn_extra_class'=>'btn-toggle_courier_enabled',
								));
							?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
