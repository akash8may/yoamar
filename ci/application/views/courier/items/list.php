<?php 
	$filter_list =  array(#Debug
		[
			'text'=>'In transit',
			'value'=>1
		],
		[
			'text'=>'Processing',
			'value'=>2
		],
		[
			'text'=>'Delivered',
			'value'=>3
		],
	);//TODO: Remove this and use value from controller
	$this->load->view('_components/admin/search_bar',array(
		'filter_list'=> $filter_list ?? [],
		'btn_text'=>'FILTER',
		'placeholder_text'=>'Filter items',
	));
?>
<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th class="text-center">Order ID</th>
				<th class="text-center">Tracking ID</th>
				<th>Customer Name</th>
				<th>Products</th>
				<th>Address</th>
				<th>Date</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php for($i=1; $i<=5; $i++): ?>
			<tr>
				<td class="text-center">OR
					<?= 10-$i; ?>972354</td>
				<td class="text-center">TR7
					<?= $i; ?>32102</td>
				<td>John Doe</td>
				<td>Product 1, Product 2</td>
				<td class="mw-25 text-truncate" title="445 Mount Eden Road, Mount Eden, Auckland">445 Mount Eden Road, Mount Eden, Auckland</td>
				<td>27/07/2018</td>
				<td>
					<?php 
						$this->load->view('_components/admin/buttons/btn_view_details');
						$this->load->view('_components/admin/buttons/btn_confirm',array(
							'btn_link'=>@$btn_link,
							'btn_title'=>'Confirm delivery',
						));
					?>
				</td>
			</tr>
			<?php endfor;?>
		</tbody>
	</table>
</div>
