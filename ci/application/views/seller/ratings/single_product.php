<main>
    <?php
    if(isset($review)):?>
    <div class="container " style="min-height:80vh;">
        <div class="row">
            <div class="review-item bg-secondary12 p-4 review-highlight col-10 col-lg-6 col-md-8 offset-1 offset-lg-3 offset-md-2">
                <?php 
                $this->load->view('_components/ratings', array(
                    'rating' =>(float)$review->rating,
                    'extra_classes' => 'h4 d-block text-center',
                    'show_text' => TRUE,
                ));
                ?>
                <p class="user-name text-center"><span class="text-info d-block"><?= $review->buyer_first_name.' '.$review->buyer_last_name ?></span></p>
                <div class="w-25 border-bottom mb-3 mx-auto"></div>
                <h3 class="mb-2 review"><?= $review->comment ?><br /></h3>
                <p><?= @$review->date_created ?></p>
                <div class="review-footer pt-3">
                    <div class="user-item p-0">
                        <div class="user-container p-2">
                        <a class="btn btn-outline-primary text-capitalize" data-user="<?= $review->buyer_first_name ?>" data-id="<?= $review->id ?>" data-review-id="<?= $review->id ?>" data-toggle="modal" data-target="#modalReviewReply" data-href="<?= site_url('merchant/review/'.$review->id) ?>" href="#!"><i class="fa fa-reply mr-3"></i>REPLY</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        if(isset($review_replies) && !empty($review_replies)):
            $seller_reply_class = 'offset-2 offset-lg-4 offset-md-4 bg-secondary26';
            $buyer_reply_class = 'offset-0 offset-lg-1 offset-md-0 bg-info12';
        ?>
        <!-- review replies start -->
        <div class="row">
            <?php foreach($review_replies as $reply): 
                // var_dump($reply);
                if($user->id == $reply->user_id): ?>

            <div class="review-item p-4 review-highlight col-10 col-lg-6 col-md-8 <?= $seller_reply_class ?>">
                <p class="user-name text-right"><span class="text-black-50 d-block">You</span></p>
                <h5 class="mb-2 review text-right"><?= $reply->reply_text ?><br /></h5>
                <p><?= @$reply->date_created ?></p>
                <div class="review-footer pt-1">
                    <div class="user-item p-0">
                        <div class="user-container p-2">
                        <a class="btn btn-danger text-capitalize float-right ml-2 js-deleteReviewReply" data-id="<?= $reply->id ?>" data-review-id="<?= $reply->review_id ?>" data-href="<?= site_url('merchant/review/'.$reply->id) ?>" href="#!"><i class="fa fa-trash mr-2"></i>DELETE</a>
                        <a class="btn btn-outline-primary text-capitalize float-right" data-user="[self]" data-id="<?= $reply->id ?>" data-review-id="<?= $reply->review_id ?>" data-toggle="modal" data-target="#modalReviewReplyUpdate" data-href="<?= site_url('merchant/review/'.$reply->id) ?>" href="#!">UPDATE</a>
                        </div>
                    </div>
                </div>
            </div>

            <?php else: ?>

            <div class="review-item p-4 review-highlight col-10 col-lg-6 col-md-8 <?= $buyer_reply_class ?>">reply
                <p class="user-name text-left"><span class="text-info d-block"><?= $review->buyer_first_name.' '.$review->buyer_last_name ?></span></p>
                <div class="w-25 border-bottom mb-3 mr-auto"></div>
                <h5 class="mb-2 review text-left"><?= $reply->reply_text ?><br /></h5>
                <p><?= @$reply->date_created ?></p>
                <div class="review-footer pt-3">
                    <div class="user-item p-0">
                        <div class="user-container p-2">
                        <a class="btn btn-outline-primary text-capitalize float-left" data-user="<?= $review->buyer_first_name ?>" data-id="<?= $reply->id ?>" data-review-id="<?= $reply->review_id ?>" data-toggle="modal" data-target="#modalReviewReply" data-href="<?= site_url('merchant/review/'.$reply->id) ?>" href="#!"><i class="fa fa-reply mr-3"></i>REPLY</a>
                        </div>
                    </div>
                </div>
            </div>

            <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <!-- review replies end -->
        <?php endif; ?>
    </div>
    <div role="dialog" tabindex="-1" class="modal fade" id="modalReviewReply">
		<div class="modal-dialog modal-dialog-centered bg-light" role="document" style="pointer-events: auto;">
			<form action="@" method="post" class="px-2 mx-auto col-10 offset-1" id="formReviewReply">
				<div class="modal-header">
					<h4 class="modal-title">Reply to <span class="yo-user"><?= (!isset($review_replies)) ? '{{user}}' : $review->buyer_first_name ?> </span>'s review</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
				<div class="modal-body">
					<div class="d-block">
						<div class="form-group">
							<label class="mb-1">Comment</label>
							<div class="review-editor"></div>
							<textarea name="reply_text" class="form-control form-control-lg" style="height: 10rem;" rows="8"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
					<button class="btn btn-success text-center py-2 js-createReviewReply" type="button" id="btnAddReviewReply" data-review-id="<?= @$review->id ?>">ADD REVIEW</button>
					<?php ?>
				</div>
			</form>
		</div>
	</div>
    <div role="dialog" tabindex="-1" class="modal fade" id="modalReviewReplyUpdate">
		<div class="modal-dialog modal-dialog-centered bg-light" role="document" style="pointer-events: auto;">
			<form action="@" method="post" class="px-2 mx-auto col-10 offset-1" id="formReviewReplyUpdate">
				<div class="modal-header">
					<h4 class="modal-title">Update your review</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
				<div class="modal-body">
					<div class="d-block">
						<div class="form-group">
							<label class="mb-1">Comment</label>
							<div class="review-editor"></div>
							<textarea name="reply_text" class="form-control form-control-lg" style="height: 10rem;" rows="8"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-light" type="button" data-dismiss="modal">Close</button>
					<button class="btn btn-success text-center py-2 js-updateReviewReply" type="button" id="btnUpdateReviewReply" data-id="">UPDATE REVIEW</button>
					<?php ?>
				</div>
			</form>
		</div>
	</div>
    <?php else:

    $this->load->view('_templates/empty_message', array(
        'title' => 'The review could not be found',
        'message' => 'It appeas that the buyer has deleted the review',
        'button' => []
    ));

    endif; ?>
</main>