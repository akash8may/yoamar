<?php if(!empty($reviews)): ?>
<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Customer</th>
				<th>Product</th>
				<th>Rating</th>
				<th>Message</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($reviews as $review): 
		$product = $this->utility->get_single_product($review->product_id);
			?>
			<!--For start-->
			<tr>
				<td>
					<?= $review->buyer_first_name.' '.$review->buyer_last_name ?>
				</td>
				<td><?= $product->product_name ?></td>
				<td>
					<?php 
						$this->load->view('_components/ratings',array(
							'rating'=>$review->rating,
							'show_text'=>TRUE
						));
					?>
				</td>
				<td class="text-truncate mw-50"><?= $review->comment ?></td>
				<td>
					<?php 
						$this->load->view('_components/admin/buttons/btn_view_details', array('btn_link' => site_url('merchant/rating/'.$review->id)));
					?>
				</td>
			</tr>
		<?php endforeach; ?>
			<!--For end-->
		</tbody>
	</table>
</div>
<?php else:

$this->load->view('_templates/empty_message', array(
	'title' => 'No product ratings found',
	'message' => 'None of your products have received ratings yet. Once they do, ratings will appear here.',
	'button' => []
));

endif; ?>
