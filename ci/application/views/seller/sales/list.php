<main>
	<?php if(!empty($sales)): ?>
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Sales Id</th>
					<th>Product</th>
					<th>Quantity</th>
					<th>Unit Price</th>
					<th>Total</th>
					<th>Date</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($sales as $sale): ?>
				<tr>
					<td><?= $sale->id ?></td>
					<td>
						<a href="<?= site_url('product/'.$sale->product_id) ?>" target="_blank"><?= $sale->product_name ?></a>
					</td>
					<td><?= $sale->quantity ?></td>
					<td>$<?= $sale->unit_price ?></td>
					<td>$<?= $sale->sales_amount ?></td>
					<td><?= $sale->date_ordered ?></td>
					<td>
						<?php 
							$this->load->view('_components/admin/buttons/btn_view_details', array(
								'btn_link' => site_url($sale->order_transaction_id),
								'btn_extra_class' => 'js-openShipment',
								'btn_extra_props' => 'data-toggle="modal" data-target="#trackingModal"',
							));
						?>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<div class="modal fade" id="trackingModal" tabindex="-1" role="dialog" aria-labelledby="trackingModalTitle"
	aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="trackingModalTitle">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="yo-tracking-order">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<?php else: 

	$this->load->view('_templates/empty_message', array(
		'title' => 'No sales data found',
		'message' => 'Sales data includes items that were sold to and received by buyers. Once sales data is found, it will appear here.',
		'button' => []
	));

	endif; ?>

</main>
