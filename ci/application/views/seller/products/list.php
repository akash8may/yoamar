<div class="row">
	<div class="col-12">
		<div class="form-group text-right">
			<?php $this->load->view('_components/admin/buttons/btn_create',array(
					'btn_link'=>site_url('merchant/product/add'),
					'btn_title'=>'Create new product',
				));
			?>
			<?php $this->load->view('_components/admin/buttons/btn_delete',array(
					'btn_link'=>'#',
					'btn_size'=>'',
					'btn_extra_class' => 'js-deleteProduct',
					'btn_title'=>'Delete product(s)',
				));
			?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12 col-lg-8 col-xl-9">
		<div class="row">
				
			<div class="col-12">
			<?php if(! empty($products)): ?>
				<h5 class="p-2"><i class="fa fa-list mr-2"></i>Product List</h5>
				<div class="table-responsive">
					<table class="table table-hover">
						<!-- <caption class="text-center"><a class="btn btn-secondary btn-sm btn-load-more" role="button" href="#">LOAD MORE</a></caption> -->
						<thead>
							<tr>
								<th class="text-center">
									<input type="checkbox" class="checbox-select-all"></th>
								<th>Image</th>
								<th data-toggle="tooltip" data-placement="bottom" title="No. of registered products">Product Name</th>
								<th>Category</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($products as $key => $product): 
						$img_link = @$this->utility->get_single_product_image($product->id)->thumbnail_link ?? site_url(PRODUCT_PLACEHOLDER_IMG);
							?>

							<tr>
								<td class="text-center">
									<input type="checkbox" name="product_list_item" id="<?= $product->id ?>" class="checkbox-select-item"></td>
								<td>
									<div class="card product-image">
										<div class="card-body">
											<img class="img-fluid" src="<?= $img_link ?>"></div>
									</div>
								</td>
								<td title="Unit price"><?= $product->product_name; ?></td>
								<td><?= $product->product_family_title; ?></td>
								<td><?= '$'.$product->unit_price; ?></td>
								<td class="text-center">
									<?php $this->load->view('_components/seller/quantity_changer', array(
										'product_id' => @$product->id,
										'quantity' => @$product->quantity ?? 0
									));
									?>
								</td>
								<td><?= (int)$product->is_active == 1 ? 'Enabled' : 'Disabled' ?> </td>
								<td class="text-center">
									<a class="btn btn-dark btn-sm" href="<?= site_url('merchant/product/edit/'.$product->id); ?>" data-toggle="tooltip" data-placement="bottom" title="Edit"><span><i class="fa fa-pencil"></i></span></a></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			<?php else:
				$this->load->view('_templates/empty_message', array(
					'title' => 'Add a product',
					'message' => 'When you add a product, it will appear here',
					'button' => array(
						'text' => 'ADD A PRODUCT',
						'link' => site_url('merchant/product/add')
					)
				));
			endif; ?>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-lg-4 col-xl-3">
		<div class="card filter-list">
			<div class="card-header">
				<h5 class="mb-0"><i class="fa fa-filter mr-2"></i>Filter</h5>
			</div>
			<div class="card-body pl-3 pr-3">
				<?= form_open('',['method'=>'GET']); ?>
					<div class="form-group mb-2">
						<label>Product Name</label>
						<input class="form-control form-control" type="text" name="q" placeholder="Product Name" value="<?= get_value_or_default($this->input->get('q')); ?>">
					</div>
					<div class="form-group mb-2">
						<label>Category</label>
						<select class="form-control form-control" name="cat">
							<option value="">All</option>
							<?php foreach($categories as $category):
								$selected = get_selected($category->id,$this->input->get('cat'));	
							?>
							<option value="<?= $category->id; ?>" <?= $selected; ?>><?= ucfirst($category->title); ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group mb-2"><label>Min Price</label>
						<div class="input-group">
							<div class="input-group-prepend"><span class="input-group-text">$</span></div>
							<input class="form-control form-control" type="number" name="min_price" value="<?= get_value_or_default($this->input->get('min_price')); ?>" placeholder="Min Price" min="0" max="100000"></div>
					</div>
					<div class="form-group mb-2"><label>Max Price</label>
						<div class="input-group">
							<div class="input-group-prepend"><span class="input-group-text">$</span></div>
							<input class="form-control form-control" type="number" name="max_price" value="<?= get_value_or_default($this->input->get('max_price')); ?>" placeholder="Max Price" min="0" max="100000">
						</div>
					</div>
					<div class="form-group mb-2">
						<label>Quantity</label>
						<input class="form-control form-control" type="number" name="qty" placeholder="Quantity" min="0" max="100" value="<?= get_value_or_default($this->input->get('qty')); ?>">
					</div>
					<div class="form-group mb-2">
						<button class="btn btn-outline-primary btn-block" type="submit">FILTER</button>
					</div>
				<?= form_close(); ?>
			</div>
		</div>
		
	</div>
</div>
