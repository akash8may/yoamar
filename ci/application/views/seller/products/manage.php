<div class="row">
	<div class="col-12">
		<div class="form-group text-right">
			<?php 
			if(isset($product)) {
				$button_details = array(
					'btn_link'=>'#!',
					// 'btn_link'=>site_url('merchant/product_create'),
					'btn_text'=>'Update Product',
					'btn_title'=>'Update '.@$product->product_name,
					'extra_class'=>'shadow-sm js-editProduct',
					'attributes'=>'data-url="'.site_url('merchant/product/edit/'.@$product->id).'" data-product-id="'.@$product->id.'"',
				);
			} else {
				$button_details = array(
					'btn_link'=>'#!',
					// 'btn_link'=>site_url('merchant/product_create'),
					'btn_text'=>'Create Product',
					'btn_title'=>'Create a new product',
					'extra_class'=>'shadow-sm js-addProduct',
					'attributes'=>'data-url="'.site_url('merchant/product_create').'"',
				);
			}
				$this->load->view('_components/admin/buttons/btn_create', $button_details);
				$this->load->view('_components/admin/buttons/btn_view_details',array(
					'btn_link'=>site_url('merchant/products'),
					'btn_text'=>'View Products',
					'btn_title'=>'View registered products',
				));
			?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12 mx-auto">

		<div class="card px-4 py-3">
			<div class="card-body">
				<form method="post" action="<?= @$form_action ?>" id="product-form" enctype="multipart/form-data">
					<!-- Include stylesheet -->
					<!-- <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet"> -->
					<link rel="stylesheet" href="<?= asset_url('admin/css/bootstrap-colorpicker.min.css'); ?>">
					<ul class="nav nav-tabs nav-justified">
						<li class="nav-item"><a role="tab" data-toggle="tab" href="#tab-1" class="nav-link active">Basic<br /></a></li>
						<li class="nav-item"><a role="tab" data-toggle="tab" href="#tab-2" class="nav-link">General</a></li>
						<li class="nav-item"><a role="tab" data-toggle="tab" href="#tab-3" class="nav-link">Categories</a></li>
						<li class="nav-item"><a role="tab" data-toggle="tab" href="#tab-4" class="nav-link">Properties</a></li>
						<li class="nav-item"><a role="tab" data-toggle="tab" href="#tab-5" class="nav-link">Images</a></li>
						<li class="nav-item"><a role="tab" data-toggle="tab" href="#tab-6" class="nav-link">Discounts</a></li>
						<li class="nav-item"><a role="tab" data-toggle="tab" href="#tab-7" class="nav-link">Locations</a></li>
						<li class="nav-item"><a role="tab" data-toggle="tab" href="#tab-8" class="nav-link">Shipping &amp; Tax</a></li>
					</ul>
					<div class="tab-content p-4">
						<div role="tabpanel" class="tab-pane active" id="tab-1">
							<div class="form-group form-inline">
								<label for="product_name" class="col-sm-3 col-lg-2 pr-4"><strong>Product name</strong></label>
								<input type="text" name="product_name" placeholder="Product name" required id="product_name" class="form-control col-sm-9 col-lg-10" value="<?= @$product->product_name ?>" />
							</div>
							<div class="form-group form-inline">
								<label for="brand_name" class="col-sm-3 col-lg-2 pr-4"><strong>Brand name</strong></label>
								<input type="text" name="brand_name" placeholder="Brand name" id="brand_name" class="form-control col-sm-9 col-lg-10" value="<?= @$product->brand_name ?>"/>
							</div>
							<div class="form-group form-inline">
								<label for="description" class="col-sm-3 col-lg-2 pr-4"><strong>Description</strong></label>
								<div class="form-control col-sm-9 col-lg-10 border-0 p-0 ">
									<div id="description-editor" class="editor"><?= @$product->description ?></div>
								</div>
								<!-- auto-filled by the on-change quill event -->
								<textarea rows="5" type="hidden" name="description" required class="d-none form-control col-sm-9 col-lg-10" value="<?= @$product->description ?>"><?= @$product->description ?></textarea>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="tab-2">
							<div class="input-group form-group form-inline">
							<?php $unit_price = (float)@$product->unit_price ?? 10; 
								$yoamar_rate = get_commission($unit_price);
								$seller_earnings = get_seller_dues($unit_price)
							?>
								<label for="unit_price" class="col-sm-3 col-lg-2 pr-4">
									<strong>Unit price</strong>
								</label>
								<div class="input-group-prepend">
									<span class="input-group-text">$</span>
								</div>
								<input type="number" data-rate="<?= get_commission_rate(); ?>" data-rate-type="%" name="unit_price" value="<?= $unit_price ?>" placeholder="Unit price" required class="form-control col-sm-9 col-lg-10" />
								<div class="col-sm-9 col-lg-10 offset-lg-2 offset-sm-3 bg-info12 py-2 rounded-bottom js-priceCalculator">
									<p class="m-0">YoAmar's rate: <span class="font-italic"><?= get_commission_rate().'%' ?></span> ( $<span class="js-yoamarPrice"> <?= $yoamar_rate ?> </span> )</p>
									<p class="m-0">Total earnings: $<span class="js-sellerPrice"><?= $seller_earnings ?></span></p></div>
							</div>
							<div class="form-group form-inline"><label for="quantity" class="col-sm-3 col-lg-2 pr-4"><strong>Quantity</strong></label>
								<input type="number" name="quantity" min="0" placeholder="Quantity" value="<?= @$product->quantity ?? 1 ?>" class="form-control col-sm-9 col-lg-10" /></div>
							<div class="form-group form-inline"><label for="min_quantity" class="col-sm-3 col-lg-2 pr-4"><strong>Min Quantity</strong></label>
								<input type="number" name="min_quantity" placeholder="minimum quantity" class="form-control col-sm-9 col-lg-10" value="<?= @$product->min_quantity ?>"/></div>
							<div class="form-group form-inline"><label for="max_quantity" class="col-sm-3 col-lg-2 pr-4"><strong>Max Quantity</strong></label>
								<input type="number" name="max_quantity" placeholder="maximum quantity" min="1" class="form-control col-sm-9 col-lg-10" value="<?= @$product->max_quantity ?>"/></div>
							
							<div class="form-group form-inline"><label for="is_available" class="col-sm-3 col-lg-2 pr-4"><strong>Availability</strong></label>
								<select name="is_available" class="form-control col-sm-9 col-lg-10">
								<?php $product->is_available = (int)@$product->is_available ?? 1 ?>
									<option value="1" <?= $product->is_available === 1 ? 'selected' : '' ?>>Yes</option>
									<option value="0" <?= $product->is_available === 0 ? 'selected' : '' ?>>No</option>
								</select></div>
							<div class="form-group form-inline"><label for="availability_date" class="col-sm-3 col-lg-2 pr-4"><strong>Availability date</strong></label>
								<input type="date" name="availability_date" class="form-control col-sm-9 col-lg-5 datepicker" <?= @$product->availability_date ?>/></div>
							<div class="form-group form-inline"><label for="expiration_date" class="col-sm-3 col-lg-2 pr-4"><strong>Expiration date</strong></label>
								<input type="date" name="expiration_date" id="expiration_date" class="form-control col-sm-9 col-lg-5 datepicker" <?= @$product->expiration_date ?>/></div>
							<div class="form-group form-inline"><label for="product_state" class="col-sm-3 col-lg-2 pr-4"><strong>Product state</strong></label>
								<select name="product_state" class="form-control col-sm-9 col-lg-10">
								<?php $product->product_state = $product->product_state ?? 'new' ?>
									<option value="new" <?= $product->product_state === 'new' ? 'selected' : '' ?>>New</option>
									<option value="used" <?= $product->product_state === 'used' ? 'selected' : '' ?>>Used</option>
									<option value="refurbished" <?= $product->product_state === 'refurbished' ? 'selected' : '' ?>>Refurbished</option>
								</select></div>
						</div>
						<div role="tabpanel" class="tab-pane" id="tab-3">
							<div class="form-group form-inline">
								<label for="product_group" class="col-sm-3 col-lg-2 pr-4">
									<strong>Product Group</strong></label>
								<select name="product_group" class="form-control col-sm-9 col-lg-10" <?=get_if_not_empty(@$product_groups,'','disabled')
								 ?>>
									<?php 
								if(!empty($product_groups)):
									foreach($product_groups as $product_group):
								?>
									<option value="<?= $product_group->id ?>">
										<?= $product_group->title; ?>
									</option>
									<?php 
									endforeach; 
								else:?>
									<option value="-1" selected disabled>No product groups found</option>
									<?php endif;?>
								</select>
							</div>
							<div class="form-group form-inline">
								<label for="product_segment" class="col-sm-3 col-lg-2 pr-4">
									<strong>Product Segment</strong></label>
								<select name="product_segment" class="form-control col-sm-9 col-lg-10" <?=get_if_not_empty(@$product_segments,'','disabled')
								 ?> data-child-select="product_family">
									<?php 
									if(!empty($product_segments)):
										$selected;
										foreach($product_segments as $product_segment):
											if(!isset($selected))
											{
												$selected = $product_segment->id;
											}
									?>
									<option value="<?= $product_segment->id ?>" <?= @$product->product_segment === $product_segment->id?'selected': '' ?>>
										<?= $product_segment->title; ?>
									</option>
									<?php 
										endforeach; 
										unset($selected);
									else:?>
									<option value="-1" selected disabled>No product segments found</option>
									<?php endif;?>
								</select>
							</div>
							<div class="form-group form-inline">
								<label for="product_family" class="col-sm-3 col-lg-2 pr-4">
									<strong>Product Family</strong></label>
								<select name="product_family" class="form-control col-sm-9 col-lg-10" <?=get_if_not_empty(@$product_families,'','disabled')
								 ?> data-child-select="product_class">
									<?php 
								//*TODO: Show this based on what has been selected
								$selected = @$product->product_segment ?? $selected;
									if(!empty($product_families)):
										foreach($product_families as $product_family):
											if(!isset($selected))
											{
												$selected = $product_family->category_id;
											}
									?>
									<option <?= $selected === $product_family->category_id ? '': 'class="d-none"' ?> data-category-id="<?= $product_family->category_id ?>" value="<?= $product_family->id ?>" <?= @$product->product_family === $product_family->id?'selected': '' ?>>
										<?= $product_family->title; ?>
									</option>
									<?php 
										endforeach; 
										unset($selected);
									else:?>
									<option value="-1" selected disabled>No product families found</option>
									<?php endif;?>
								</select>
							</div>
							<div class="form-group form-inline">
								<label for="product_class" class="col-sm-3 col-lg-2 pr-4">
									<strong>Product Class</strong></label>
								<select name="product_class" class="form-control col-sm-9 col-lg-10" <?=get_if_not_empty(@$product_classes,'','disabled')
								 ?>>
									<?php 
								//*TODO: Show this based on what has been selected
									if(!empty($product_classes)):
										$selected = @$product->product_family ?? $selected;
										foreach($product_classes as $product_class):
											if(!isset($selected))
											{
												$selected = $product_class->category_id;
											}
									?>
									<option <?= $selected === $product_class->category_id ? '': 'class="d-none"' ?> data-category-id="<?= $product_class->category_id ?>" value="<?= $product_class->id ?>" <?= @$product->product_class === $product_class->id?'selected': '' ?>>
										<?= $product_class->title; ?>
									</option>
									<?php 
										endforeach;
										unset($selected); 
									else:?>
									<option value="-1" selected disabled>No product classes found</option>
									<?php endif;?>
								</select>
							</div>
							<div class="form-group form-inline">
								<label for="product_type" class="col-sm-3 col-lg-2 pr-4"><strong>Product
										Type</strong></label>
								<select name="product_type" class="form-control col-sm-9 col-lg-10" <?=get_if_not_empty(@$product_types,'','disabled')
								 ?>>
									<?php 
								//*TODO: Show this based on what has been selected
									if(!empty($product_types)):
										foreach($product_types as $product_type):
									?>
									<option data-category-id="<?= $product_type->category_id ?>" value="<?= $product_type->id ?>" <?= @$product->product_type === $product_type->id?'selected': '' ?>>
										<?= $product_type->title; ?>
									</option>
									<?php 
										endforeach; 
									else:?>
									<option value="-1" selected disabled>No product types found</option>
									<?php endif;?>
								</select>
							</div>
							<div class="form-group form-inline">
								<label for="product_sub_type" class="col-sm-3 col-lg-2 pr-4">
									<strong>Product SubType</strong>
								</label>
								<select name="product_sub_type" class="form-control col-sm-9 col-lg-10" <?=get_if_not_empty(@$product_subtypes,'','disabled')
								 ?>>
									<?php 
								if(!empty($product_subtypes)):
									foreach($product_subtypes as $product_subtype):
								?>
									<option value="<?= $product_subtype->id ?>" <?= @$product->product_subtype === $product_subtype->id?'selected': '' ?>>
										<?= $product_subtype->title; ?>
									</option>
									<?php 
									endforeach; 
								else:?>
									<option value="-1" selected disabled>No product subtypes found</option>
									<?php endif;?>
								</select></div>
						</div>
						<div role="tabpanel" class="tab-pane" id="tab-4">
							<div class="form-group form-inline"><label for="age_group" class="col-sm-3 col-lg-2 pr-4"><strong>Age group</strong></label>
								<select name="age_group" value="3" class="form-control col-sm-9 col-lg-10">
									<?php $product->age_group = $product->age_group ?? 'all' ?>
									<option value="all" <?= $product->age_group === 'all'?'selected':''?>>All age groups</option>
									<option value="kids" <?= $product->age_group === 'kids'?'selected':''?>>Kids</option>
									<option value="teens" <?= $product->age_group === 'teens'?'selected':''?>>Teens</option>
									<option value="youth" <?= $product->age_group === 'youth'?'selected':''?>>Youth</option>
									<option value="adults" <?= $product->age_group === 'adults'?'selected':''?>>Adults</option>
								</select></div>
							<div class="form-group form-inline"><label for="color" class="col-sm-3 col-lg-2 pr-4"><strong>Color</strong></label>
								<div class="form-group yo-product-variable yo-color col-sm-3 col-lg-10">
									<div class="yo-color-list">
									<?php
									$colors = @$product->color ?? NULL;
									if(!empty($colors) && is_string($colors)):
										//split the string then loop
										$colors = explode(',', $colors);

										foreach($colors as $key => $color) :
											$id = 'picker-'.$key;
											if(!empty($color)):
									?>
									<!-- TEMPLATE START -->
										
										<div class="float-left custom-control color d-inline-block disabled" 
											style="background-color: <?= $color ?>;"
											data-color="<?= $color ?>" id='<?= $key ?>'>
											<input id="<?= $id ?>" type="text"
												value="<?= $color ?>" 
												class="position-absolute border-0" style="width:36px;height:36px; top: 0; left: 0; opacity: 0;" 
												autofocus="true"/>
											<a><i class="fa fa-close text-primary87"></i></a>
										</div> 

									<!-- TEMPLATE STOP -->
											<?php endif;
											endforeach;
									endif; ?>

										<a href="#!" class="js-addProductColor variable-action d-inline-block float-right">
											<i class="fa fa-plus mr-1"></i>Add a new color option</a>
									</div>
									<input type="hidden" name="color" value="<?= is_string($colors) ? $colors : '' ?>" />
								</div>
							</div>
							<div class="form-group form-inline"><label for="gender" class="col-sm-3 col-lg-2 pr-4"><strong>Gender</strong></label>
								<select name="gender" value="3" class="form-control col-sm-9 col-lg-10">
									<?php $product->gender = $product->gender ?? '' ?>
									<option value="both" <?= $product->gender === 'both'?'selected':''?>>Both</option>
									<option value="male" <?= $product->gender === 'male'?'selected':''?>>Male</option>
									<option value="female" <?= $product->gender === 'female'?'selected':''?>>Female</option>
								</select></div>
							<div class="form-group form-inline"><label for="material" class="col-sm-3 col-lg-2 pr-4"><strong>Material</strong></label>
								<input type="text" name="material" class="form-control col-sm-9 col-lg-10" value="<?= @$product->material ?>"/></div>

							<div class="form-group form-inline"><label for="pattern" class="col-sm-3 col-lg-2 pr-4"><strong>Pattern</strong></label>
								<input type="text" name="pattern" class="form-control col-sm-9 col-lg-10" value="<?= @$product->pattern ?>"/></div>

							<div class="form-group form-inline"><label for="product_size" class="col-sm-3 col-lg-2 pr-4"><strong>Product size</strong></label>
								<input type="text" name="product_size" class="form-control col-sm-9 col-lg-10" value="<?= @$product->size ?>"/></div>
							
							<div class="form-group form-inline"><label for="size_type" class="col-sm-3 col-lg-2 pr-4"><strong>Size type</strong></label>
								<input type="text" name="size_type" class="form-control col-sm-9 col-lg-10" value="<?= @$product->size_type ?>"/></div>
							
							<div class="form-group form-inline"><label for="size_system" class="col-sm-3 col-lg-2 pr-4"><strong>Size system</strong></label>
								<select name="size_system" class="form-control col-sm-9 col-lg-10">
								<?php $product->size_system = $product->size_system ?? '' ?>
									<option value="0" <?= $product->size_system === 'metric'?'selected':''?>>metric</option>
									<option value="1" <?= $product->size_system === 'imperial'?'selected':''?>>imperial</option>
								</select></div>
							<div class="form-group form-inline"><label for="energy_efficiency_class" class="col-sm-3 col-lg-2 pr-4"><strong>Energy efficiency class</strong></label>
								<select name="energy_efficiency_class" class="form-control col-sm-9 col-lg-10">
									<option value="0">option 1</option>
								</select></div>
							
							<div class="form-group form-inline"><label for="energy_efficiency_class_min" class="col-sm-3 col-lg-2 pr-4"><strong>Energy efficiency class min</strong></label>
								<select name="energy_efficiency_class_min" class="form-control col-sm-9 col-lg-10">
									<option value="0">option 1</option>
								</select></div>
							
							<div class="form-group form-inline"><label for="energy_efficiency_class_max" class="col-sm-3 col-lg-2 pr-4"><strong>Energy efficiency class max</strong></label>
								<select name="energy_efficiency_class_max" class="form-control col-sm-9 col-lg-10">
									<option value="0">option 1</option>
								</select></div>
							
							<div class="form-group form-inline"><label for="style" class="col-sm-3 col-lg-2 pr-4"><strong>Style</strong></label>
								<input type="text" name="style" class="form-control col-sm-9 col-lg-10" value="<?= @$product->style ?>"/></div>
							
							<div class="form-group form-inline"><label for="year_made" class="col-sm-3 col-lg-2 pr-4"><strong>Year Made</strong></label>
								<input type="date" name="year_made" class="form-control col-sm-9 col-lg-5 datepicker" value="<?= @$product->year_made ?>"/></div>
							
							<div class="form-group form-inline"><label for="season" class="col-sm-3 col-lg-2 pr-4"><strong>Season</strong></label>
								<select name="season" class="form-control col-sm-9 col-lg-10">
								<?php $product->season = $product->season ?? '' ?>
									<option value="autumn" <?= $product->season === 'autumn'?'selected':''?>>Autumn</option>
									<option value="summer" <?= $product->season === 'summer'?'selected':''?>>Summer</option>
									<option value="spring" <?= $product->season === 'spring'?'selected':''?>>Spring</option>
									<option value="winter" <?= $product->season === 'winter'?'selected':''?>>Winter</option>
								</select></div>
						</div>
						<div role="tabpanel" class="tab-pane" id="tab-5">
							<div class="form-group form-inline">
								<label for="product_images" class="align-self-start col-sm-3 col-lg-2 pr-4"><strong>Product images</strong></label>
								<div class="col-9 col-lg-10 border-0 p-0 custom-file">
									<input type="file" name="product_images" multiple required accept="image/*" class="d-flex col-12 custom-file-input" />
									<label class="custom-file-label" for="customFile">Choose image</label>
								</div>
								<div class="offset-3 offset-lg-2 col-9 col-lg-10">
									<div class="row mt-4 product-image-list" id="product-image-list">
									<?php if(isset($product)):
									$product_images = $this->utility->get_product_images(@$product->id);
									if(!empty($product_images) && is_array($product_images)):
										foreach($product_images as $image):
									?>
										<!-- TEMPLATE START  -->
										<div class="col-lg-3 col-md-4 col-6 image-list mb-4 rounded-top position-relative">
											<img style="opacity:1;" src="<?= $image->thumbnail_link ?>" height="100%" width="100%" class="rounded shadow-sm"/>
											<p class="image-title d-none"><i><?= $image->thumbnail_link ?></i></p>
										</div> 
										<!-- TEMPLATE STOP-->
										<?php endforeach; 
									endif; endif; ?>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="tab-6">
							<div class="input-group form-group form-inline">
							<?php $sale_price = (float)@$product->sale_price ?? 0;
							$yoamar_rate = get_commission($sale_price);
							$seller_earnings = get_seller_dues($sale_price);
							?>
								<label for="sale_price" class="col-sm-3 col-lg-2 pr-4"> <strong>Sale price</strong> </label>
								<div class="input-group-prepend"> <span class="input-group-text">$</span> </div>
								
								<input type="number" name="sale_price" class="form-control col-sm-9 col-lg-10" data-rate="<?= get_commission_rate() ?>" data-rate-type="<?= '%' ?>" value="<?= $sale_price ?>"/>
								<div class="col-sm-9 col-lg-10 offset-lg-2 offset-sm-3 bg-info12 py-2 rounded-bottom js-saleCalculator">
									<p class="m-0">YoAmar's rate: <span class="font-italic"><?= get_commission_rate().'%' ?> of the sale price</span> ( $<span class="js-yoamarPrice"> <?= $yoamar_rate ?> </span> )</p>
									<p class="m-0">Total sale earnings: $<span class="js-sellerPrice"><?= $seller_earnings ?></span></p></div>
							</div>
							<div class="form-group form-inline"><label for="sale_price_effective_date" class="col-sm-3 col-lg-2 pr-4"><strong>Sale price effective date</strong></label>
							<input type="date" name="sale_price_effective_date" class="form-control col-sm-9 col-lg-5 datepicker" value="<?= @$product->sale_price_effective_date ?>"/></div>
						</div>
						<div role="tabpanel" class="tab-pane" id="tab-7">
							<div class="form-group form-inline"><label for="origin_country" class="col-sm-3 col-lg-2 pr-4"><strong>Origin
										Country</strong></label><select name="origin_country" class="form-control col-sm-9 col-lg-10">
									<option value="110" selected>Kenya</option>
								</select></div>
							<div class="form-group form-inline"><label for="origin_state" class="col-sm-3 col-lg-2 pr-4"><strong>Origin
										State</strong></label><select name="origin_state" class="form-control col-sm-9 col-lg-10">
									<option value="110" selected>Kenya</option>
								</select></div>
							<div class="form-group form-inline"><label for="origin_city" class="col-sm-3 col-lg-2 pr-4"><strong>Origin City</strong></label><select
								 name="origin_city" class="form-control col-sm-9 col-lg-10">
									<option value="110" selected>Kenya</option>
								</select></div>
							<div class="form-group form-inline"><label for="availability_country" class="col-sm-3 col-lg-2 pr-4"><strong>Availability
										Country</strong></label><select name="availability_country" class="form-control col-sm-9 col-lg-10">
									<option value="110" selected>Kenya</option>
								</select></div>
							<div class="form-group form-inline"><label for="availability_state" class="col-sm-3 col-lg-2 pr-4"><strong>Availability
										State</strong></label><select name="availability_state" class="form-control col-sm-9 col-lg-10">
									<option value="110" selected>Kenya</option>
								</select></div>
							<div class="form-group form-inline"><label for="availability_city" class="col-sm-3 col-lg-2 pr-4"><strong>Availability
										City</strong></label><select name="availability_city" class="form-control col-sm-9 col-lg-10">
									<option value="110" selected>Kenya</option>
								</select></div>
						</div>
						<div role="tabpanel" class="tab-pane" id="tab-8">
							<div class="form-group form-inline input-group">
								<label for="shipping_weight" class="col-sm-3 col-lg-2 pr-4">
									<strong>Shipping weight</strong> <br /> </label>
								<input type="number" name="shipping_weight" placeholder="Weight" class="form-control col-sm-9 col-lg-10 js-roundOf" value="<?= @$product->shipping_weight ?>"/>
								<div class="input-group-append"> <span class="input-group-text">KG</span> </div>
								<div class="col-sm-9 col-lg-10 offset-lg-2 offset-sm-3 bg-info12 py-2 rounded-bottom">
									<p class="m-0">Will be rounded off to nearest .5 kg</p></div>
							</div>
							<div class="form-group form-inline">
								<label for="shipping_length" class="col-sm-3 col-lg-2 pr-4">
									<strong>Dimensions (L x W x H)</strong> <br /></label>
								<div class="form-control col-md-9 col-lg-10 p-0 border-0">
									<div class="d-inline-flex flex-wrap">
										<div class="input-group form-group col-md-12 col-lg-3">
											<input type="number" name="shipping_length" placeholder="Length" class="form-control" value="<?= @$product->shipping_length ?>"/>
											<div class="input-group-append"> <span class="input-group-text">CM</span> </div>
										</div>
										<div class="input-group form-group col-md-12 col-lg-3">
											<input type="number" name="shipping_width" placeholder="Width" class="form-control" value="<?= @$product->shipping_width ?>"/>
											<div class="input-group-append"> <span class="input-group-text">CM</span> </div>
										</div>
										<div class="input-group form-group col-md-12 col-lg-3">
											<input type="number" name="shipping_height" placeholder="Height" class="form-control" value="<?= @$product->shipping_height ?>"/>
											<div class="input-group-append"> <span class="input-group-text">CM</span> </div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-inline">
								<label for="is_bundle" class="col-sm-3 col-lg-2 pr-4">
									<strong>Is bundle</strong>
									<br />
								</label>
								<input type="checkbox" name="is_bundle" <?= (int)@$product->is_bundle === 1?'checked':'' ?> />
							</div>
							<div class="input-group form-group form-inline">
								<label for="min_handling_time" class="col-sm-3 col-lg-2 pr-4"> <strong>Min handling time</strong><br /> </label>
								<input type="number" name="min_handling_time" placeholder="N/A" min="1" class="form-control col-sm-9 col-lg-10" value="<?= @$product->min_handling_time ?>"/>
								<div class="input-group-append"> <span class="input-group-text">Days</span> </div>
							</div>
							
							<div class="input-group form-group form-inline">
								<label for="max_handling_time" class="col-sm-3 col-lg-2 pr-4"> <strong>Max handling time</strong><br /> </label>
								<input type="number" name="max_handling_time" placeholder="N/A" min="1" class="form-control col-sm-9 col-lg-10" value="<?= @$product->max_handling_time ?>"/>
								<div class="input-group-append"> <span class="input-group-text">Days</span> </div>
							</div>
							
							<div class="input-group form-group form-inline">
								<label for="tax" class="col-sm-3 col-lg-2 pr-4"> <strong>Tax</strong> </label>
								<input type="number" name="tax" placeholder="Tax" min="N/A" class="form-control col-sm-9 col-lg-10" value="<?= @$product->tax ?>"/>
								<div class="input-group-append"> <span class="input-group-text">%</span> </div>
							</div>
							
							<div class="form-group form-inline"><label for="tax_category" class="col-sm-3 col-lg-2 pr-4"><strong>Tax category</strong></label>
								<select name="tax_category" class="form-control col-sm-9 col-lg-10" value="<?= @$product->tax_category ?>">
									<option value="1" selected>Tax category 1</option>
								</select></div>
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>
