<?php if(! empty($orders)) : ?>
<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Tracking ID</th>
				<th>Product</th>
				<th>Quantity</th>
				<th>Unit Price</th>
				<th>Total</th>
				<th>Status</th>
				<th>Date</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($orders as $order): 
		$product = $this->utility->get_single_product($order->product_id);
			?>
			<tr>
				<td><?= $order->tracking_id ?></td>
				<td><a href="#" class="php-data"><?= $product->product_name ?></a></td>
				<td><?= $order->quantity ?></td>
				<td>$<?= 
				
				$product->unit_price ?></td>
				<td>$<?= $order->order_total ?></td>
				<td title="Item delivered"><?= $order->status ?></td>
				<td>
					<span class="text-black-50">Ordered:</span> <span><?= humanize_date($order->date_ordered)  ?></span>
					<span class="text-black-50">Delivered:</span> <span><?= @$order->date_delivered ?? '--' ?></span>
				</td>
				<td>
					<?php $this->load->view('_components/admin/buttons/btn_track_order', array('btn_link' => site_url()));
					?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
<?php else:

$this->load->view('_templates/empty_message', array(
	'title' => 'No orders found',
	'message' => 'Once orders are made, they will appear here.',
	'button' => []
));

endif; ?>
