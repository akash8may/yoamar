<div class="row">
	<div class="container mt-2 mt-md-4">
		<div class="alert alert-info text-center" id="info_message">Important message here
			<a href="#">Take action</a>
		</div>
		<div class="row">
			<?php 
				// Total orders
				$this->load->view('_components/admin/dashboard_card',array(
					'stats_change'=> 0,#TODO: Make dynamic
					'content'=> count($orders),#TODO: Make dynamic
					'card_title'=>'Total orders',
					'content_icon'=>'fa-list-alt',
					'has_btn'=>TRUE,
					'btn_link'=>site_url('merchant/orders'),
					'btn_text'=>'View orders',
				));
				// Total sales
				$this->load->view('_components/admin/dashboard_card',array(
					'stats_change'=> 0,#TODO: Make dynamic
					'content'=> count($sales),#TODO: Make dynamic
					'card_title'=>'Total sales',
					'content_icon'=>'fa-bar-chart',
					'has_btn'=>TRUE,
					'btn_link'=>site_url('merchant/sales'),
					'btn_text'=>'View sales',
				));

				// Total orders
				// $this->load->view('_components/admin/dashboard_card',array(
				// 	'stats_change'=> 0,#TODO: Make dynamic
				// 	'content'=>'10',#TODO: Make dynamic
				// 	'card_title'=>'Total customers',
				// 	'content_icon'=>'fa-group',
				// 	'has_btn'=>TRUE,
				// 	'btn_link'=>site_url('merchant/customers'),
				// 	'btn_text'=>'View customers',
				// ));
			?>
		</div>
		<div class="row">
			<?php if(! empty($orders)): ?>
			<div class="col-lg-10 mx-auto mt-lg-4 mt-2 mb-2 mb-lg-0">
				<h4 class="text-center text-dark54 pb-2">Recent Orders</h4>
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Product</th>
								<th>Quantity</th>
								<th>Unit Price</th>
								<th>Total</th>
								<th>Status</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<a href="#" class="php-data">Ebony Elephant Carving</a>
								</td>
								<td>1</td>
								<td>$30</td>
								<td>$30</td>
								<td title="Item delivered">Delivered</td>
								<td>07/18/2018 (6:30pm)</td>
							</tr>
							<tr>
								<td>
									<a href="#" class="php-data">Oak Giraffe Sculpture</a>
								</td>
								<td>1</td>
								<td>$50</td>
								<td>$50</td>
								<td title="Item is in the process of being delivered">In transit</td>
								<td>07/18/2018 (4:00pm)</td>
							</tr>
							<tr>
								<td>
									<a href="#" class="php-data">African Cultural Painting</a>
								</td>
								<td>1</td>
								<td>$150</td>
								<td>$150</td>
								<td title="Item is being picked from logistics">Processing</td>
								<td>07/18/2018 (11:00am)</td>
							</tr>

						</tbody>
					</table>
				</div>
			</div>
			<?php endif;

			if(! empty($sales)): ?>
			<!-- Divider -->
			<div class="col-lg-10 mx-auto">
				<hr class="p-2">
			</div>
			<!-- Recent sales -->
			<div class="col-lg-10 mx-auto mt-lg-4 mt-2 mb-2 mb-lg-0">
				<h4 class="text-center text-dark54 pb-2">Recent Sales</h4>
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Customer</th>
								<th>Product</th>
								<th>Quantity</th>
								<th>Unit Price</th>
								<th>Total</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<a href="#" class="php-data">10005454</a>
								</td>
								<td>
									<a href="#" class="php-data">Ebony Elephant Carving</a>
								</td>
								<td>5</td>
								<td>$30</td>
								<td>$150</td>
								<td>07/18/2018 (11:00am)</td>
							</tr>
							<tr>
								<td>
									<a href="#" class="php-data">8941515</a>
								</td>
								<td>
									<a href="#" class="php-data">Ebony Elephant Carving</a>
								</td>
								<td>1</td>
								<td>$30</td>
								<td>$30</td>
								<td>07/18/2018 (11:00am)</td>
							</tr>
							<tr>
								<td>
									<a href="#" class="php-data">10005454</a>
								</td>
								<td>
									<a href="#" class="php-data">Ebony Elephant Carving</a>
								</td>
								<td>3</td>
								<td>$30</td>
								<td>$90</td>
								<td>07/18/2018 (11:00am)</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
