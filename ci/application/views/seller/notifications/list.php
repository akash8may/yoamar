<main>
    <div class="container row mx-auto">
        <div class="col-12 col-md-8 col-lg-6 border-bottom mb-4">
            <h2 class="font-weight-light text-center">Notifications</h2>
        </div>
        <div class="col-12">
            <ul class="nav nav-tabs nav-fill">
                <li class="nav-item">
                    <a class="nav-link active text-primary-dark"
                        data-toggle="tab"
                        href="#important-notifications"
                        role="tab" 
                        aria-controls="important-notifications" 
                        aria-selected="true"
                        id="important-notifications-tab">IMPORTANT</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                        data-toggle="tab"
                        href="#notifications"
                        role="tab" 
                        aria-controls="notifications" 
                        aria-selected="false" 
                        id="notifications-tab">notifications</a>
                </li>
            </ul>
            <div class="tab-content py-4" id="notificationsTab">
                <div class="tab-pane fade show active" 
                    id="important-notifications" 
                    role="tabpanel" 
                    aria-labelledby="important-notifications-tab">
                    <?php 
                    if(!empty($important_notifications) && is_array($important_notifications)):
                        $this->load->view('_components/site/notification_list', array(
                            'notifications' => @$important_notifications,
                            'in_timeline' => TRUE,
                            'extra_classes' => '',
                            'type' => 'all' #all||new||old (no need for this though since we have the ->is_read property)
                        ));
                    else:
                        $this->load->view('_templates/empty_message', array(
                            'title' => 'No important notifications found',
                            'message' => 'Important notifications sent to you will appear here',
                            'button' => []
                        ));
                        endif;
                    ?>
                </div>
                <div class="tab-pane fade" 
                    id="notifications" 
                    role="tabpanel" 
                    aria-labelledby="notifications-tab">
                    <?php 
                    if(!empty($notifications) && is_array($notifications)):
                        $this->load->view('_components/site/notification_list', array(
                            'notifications' => @$notifications,
                            'in_timeline' => TRUE,
                            'extra_classes' => '',
                            'type' => 'all' #all||new||old (no need for this though since we have the ->is_read property)
                        ));
                    else:
                        $this->load->view('_templates/empty_message', array(
                            'title' => 'No notifications found',
                            'message' => 'Notifications sent to you will appear here',
                            'button' => []
                        ));
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</main>