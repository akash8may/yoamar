<?php
$this->load->view('_templates/header_base');
?>

<main>
    <div class="container yo-fluid-container mb-4">
        <div class="row">

            <div class="col-md-12">
                <div class="emptycart"  style="display:none">
                    <div class="cart-content-panel js-cartList ">
                        <div class="cart-panel-heading">
                            <h3 class="cart-title">Your Cart <span class='total_count_bracket'>(<span class='js-cartTotalItems'><?=$total?></span>)<span> </h3>
                        </div>
                        <div class="cart-panel-content">
                        
                                <div class="no-item-cart">
                                    <div class="row text-center">
                                        <div class="col-md-12">
                                            <img src="/uploads/img/empty-cart-ico.png" alt="" title=""/>
                                            <h4>There is no item in your cart</h4>
                                            <a href="/"><button type="button" class="btn btn-shop-now">Shop Now</button></a>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>     
            </div>
          
            
            
            <div class="col-12 col-md-8 px-0 notemptycart">
                <!--TEMPLATEstart CART-LIST-->

                <?php
                    $data = array(
                        'products' => $cart,
                        'total' => count((array)$cart)
                    );
					$this->load->view('_templates/cart_list', $data);
                ?>

                <!--TEMPLATEend CART-LIST-->
            </div>
            <div class="col-12 col-md-4 pr-0 notemptycart">
                <!--TEMPLATEstart Checkout-box-->
                <?php
                    $total_cart_amount = 0;
                    $total_cart_items = 0;
                    $id;
                    foreach($cart as $c) {
                        $product = $this->utility->get_single_product($c->product_id);
                        //Only run this if the product was a valid product
                        if(!empty($product))
                        {
                            $product_amount = (float)$product->unit_price;
                            $total_product_amount = $product_amount * (int) $c->quantity;
                            $total_cart_amount += $total_product_amount;
                            $id = $c->last_save_date;
                            $total_cart_items ++;
                        }
                    }

                    $data = array(
                        'id' => @$id,
                        'product_counts' => $total_cart_items,
                        'total_cart_amount' => $total_cart_amount,
                        'Delivery_amount'=>0,
                    );
                    
                    $this->load->view('_templates/checkout_box', $data);
                    
                ?>
                <!--TEMPLATEend Checkout-box-->
            </div>
        </div>
    </div>
    <?php if(! empty($saved_items) && is_array($saved_items)) : ?>
            
                <div class="row mb20">
                    <div class="col-md-12">
                        <div class="cart-content-panel">
                                <div class="cart-panel-heading">
                                    <h3 class="cart-title">Save For Later</h3>
                                </div>
                                <div class="cart-panel-content">
                                        <div class="row">
                                                <?php // <!-- LOOP -->
                                                foreach($saved_items as $key => $item):
                                                    $product = $this->utility->get_single_product($item->product_id);
                                                    //Only run this if the product was a valid product
                                                    if(!empty($product)):
                                                        $data = array(
                                                            'id' => $item->product_id,
                                                            'image' => $this->utility->get_single_product_image($item->product_id),
                                                            'product' => $product,
                                                        );
                                                        $this->load->view('_components/site/saved_item', $data);
                                                    ?>
                                                <?php 
                                                    endif;
                                                // <!-- LOOPend -->
                                                endforeach; ?>

                                        </div>
                                </div>
                        </div>
                    </div>    
                </div>    
            
<?php /* ?>
    <div class="container yo-fluid-container" id="saved-items">
		<h2 class="cart-title text-primary-dark py-2">SAVED ITEMS</h2>
        <div class="row">
            
            <div class="col-12">
                <div class="row">
                    <?php
                    // <!-- LOOP -->
                    foreach($saved_items as $key => $item):
                        $product = $this->utility->get_single_product($item->product_id);

                        //Only run this if the product was a valid product
                        if(!empty($product)):
                    ?>
                    <div class="col-sm-6 col-12 mb-2">
                        <!--TEMPLATEstart SAVED-ITEM CARD-->
                        <?php
                            $data = array(
                                'id' => $item->product_id,
                                'image' => $this->utility->get_single_product_image($item->product_id),
                                'product' => $product,
                            );
                            $this->load->view('_components/site/saved_item', $data);
                        ?>
                        <!--TEMPLATEend SAVED-ITEM CARD--> 
                    </div>
                    <?php 
                        endif;
                    // <!-- LOOPend -->
                    endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <?php 
*/ 
endif;  
?>
</main>
