<div class="jumbotron text-center">
    <h1>About <?= SITE_NAME; ?></h1>
    <p class="lead">Some text goes here about the possible information about yoamar</p>
</div>
<div class="container py-5">
    <h1 class="text-center pb-4">HOW IT WORKS</h1>
    <div class="row">
        <div class="col-12 col-md-4">
            <div class="form-group text-center p-3">
                <h2 class="lh-2">Find products</h2>
                <p class="my-4 text-center"><i class="fa fa-eye fa-5x my-3"></i></p>
                <p class="lead">Find amazing products that have been uploaded to yoamar.</p>
                <p><a class="btn btn-primary font-weight-300" href="<?= site_url('explore'); ?>">Explore products</a></p>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="form-group text-center p-3">
                <h2 class="lh-2">Add to cart</h2>
                <p class="my-4 text-center"><i class="fa fa-cart-plus fa-5x my-3"></i></p>
                <p class="lead">Add items you like to your cart.</p>
                <p><a class="btn btn-primary font-weight-300" href="<?= site_url('cart'); ?>">View your cart</a></p>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="form-group text-center p-3">
                <h2 class="lh-2">Checkout</h2>
                <p class="my-4 text-center"><i class="fa fa-check fa-5x my-3"></i></p>
                <p class="lead">Checkout through our streamlined process.</p>
            </div>
        </div>
    </div>
</div>