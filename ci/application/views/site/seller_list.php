<?php
$this->load->view('_templates/header_base');
?>

<main class="container-fluid">
	<div class="container yo-page-title">
		<h1 class="text-primary-dark87 yo-title-lg">Sellers</h1>
	</div>
	<div class="container yo-content">
		<div class="yo-content-header">
			<h5 class="d-inline-block text-gray pl-2">Sorted by - Most active</h5>
		</div>
		<div class="row mx-0">
			<div class="col-lg-3 p-0 side-bar d-none">
				<!-- TEMPLATEstart category-list-->
				<?php
                    $data = array();
                    $this->load->view('_components/site/category_list', $data);
                ?>
					<!-- TEMPLATEend category-list-->
			</div>
			<div class="col-12 mx-auto px-0">
				<div class="row content mb-5">
					<!-- LOOP -->
					<?php for($c=0;  $c < 4; $c++):?>
					<div class="col-12 col-md-4 pl-l-5 pt-3">
                        <!-- TEMPLATEstart seller_card-->
                        <?php 
                        $this->load->view('_components/site/seller_card', $data);
                        ?>
                        <!-- TEMPLATEend seller_card-->
					</div>
                    <?php endfor; ?>
                    <!-- LOOPend -->
				</div>
			</div>
		</div>
	</div>
</main>
