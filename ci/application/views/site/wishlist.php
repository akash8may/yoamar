<?php
$this->load->view('_templates/header_base');
?>
<div class="form-group">
	<ul class="nav nav-pills justify-content-center">
		<li class="nav-item">
			<a class="nav-link <?= get_active_nav(!$is_private); ?>" href="<?= site_url('wishlist'); ?>">Public <span class="d-none d-md-inline">Wishlist</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link <?= get_active_nav($is_private); ?>" href="<?= site_url('wishlist/'.TRUE); ?>">Private <span class="d-none d-md-inline">Wishlist</span></a>
		</li>
	</ul>
</div>

<div class="container">
	<?php if(!empty($user) || $is_private == FALSE): ?>
	<div class="form-group">
		<h6 class="py-2">MY WISHLIST (
			<?= $wishlist_type; ?>)</h6>
		<?php 
			// LOOP 
			foreach($wishlist as $item):
				$product = $this->utility->get_single_product($item->product_id);
				if(!empty($product) && (bool) $item->is_public !== $is_private):
		?>
		<div class="form-group">
			<?php 
				// TEMPLATEstart wishlist_item
				$this->load->view('_components/site/wishlist_item', array(
					'id' => $item->product_id,
					'private'=>$is_private,
					'image' => $this->utility->get_single_product_image($item->product_id),
						'product' => $product,
					));
				// TEMPLATEend wishlist_item
			?>
		</div>

		<?php endif; endforeach; 
			// LOOPend 
		?>
	</div>
	<?php else :
	// prompt login

	$this->load->view('_templates/empty_message', array(
		'title'=>'Access restricted',
		'message'=>'You need to login to view your private wishlist',
		'button' => array(
			'text' => 'Log in',
			'extra_classes' => 'my-btn-primary bg-primary',
			'link' => site_url('auth/login')
		)
	));

	endif; ?>

</div>
</div>
