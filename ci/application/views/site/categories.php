<?php
$this->load->view('_templates/header_base');
?>

<main class="container-fluid">
    <div class="container yo-page-title">
        <h1 class="text-primary-dark87 yo-title-lg">CATEGORIES</h1>
    </div>
    <div class="container yo-content">
        <div class="yo-content-header">
            <h5 class="d-inline-block text-gray pl-2"></h5>
        </div>
        <div class="row mx-0">
            <div class="col-12 mx-auto content px-0">

			<?php // Ensure that the code below only runs if categories were found
			if(!empty(@$categories)): 
			?>

                <!-- LOOP -->
				<?php foreach($categories as $category):
						$sub_categories = $this->utility->get_category_subcategories($category->id);
				?>
                <div class="row yo-category pb-3 border-bottom" data-category-id="<?= $category->id; ?>">
                    <div class="col-12">
                        <h5 class="d-inline-block text-gray pl-5 pt-3 mb-0 text-capitalize"><?= $category->title; ?></h5>
                    </div>
                    <?php 
                    // LOOP
                    foreach($sub_categories as $sub_category): ?>

                    <div class="col-sm-6 col-md-4 col-xl-3 pl-l-5 pt-3">
                        <!-- TEMPLATEstart bundle_card-->
                        <?php
                            $this->load->view('_components/site/bundle_card', array(
                                'id' => $sub_category->id,
                                'link' => site_url('category/'.$sub_category->id.'/products'),
                                'grid' => '_even',
                                'size' => 'square',
                                'bundle_img' => @$sub_category->img_link,
                                'title' => $sub_category->title,
                                'size' => 'square',
                                'extra_classes' => 'mx-auto'
                            ));
                        ?>
                        <!-- TEMPLATEend bundle_card-->
                    </div>
                    <?php endforeach; 
                    // LOOPend
                    ?>
                </div>
                <?php endforeach; ?>
				<!-- LOOPend -->
			<?php else:?>
				<p class="text-muted">No categories found</p>
			<?php endif;?>
            </div>
        </div>
    </div>


</main>
