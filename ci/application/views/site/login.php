<main>
	<div class="row mt-4 d-none">
		<div class="col-md-6 col-lg-4 mx-auto">
			<section>
				<div class="login-card shadow-sm bg-secondary26">
					<h1 class="profile-img-card text-center mx-auto">YOAMAR</h1>
					<img class="profile-img-card d-none" src="assets/img/logo.png">
					<div class="row mb-2">
						<div class="col-6 px-0">
							<button class="btn btn-primary btn-sm bg-info px-1" type="button">
								<i class="fab fa-google pr-2"></i>GOOGLE LOGIN</button>
						</div>
						<div class="col-6 px-0">
							<button class="btn btn-primary btn-sm bg-info px-1 float-right" type="button">
								<i class="fab fa-facebook-f pr-2"></i>FACEBOOK LOGIN</button>
						</div>
					</div>
					<form class="form-login" method="post">
						<span class="yo-auth-email"> </span>
						<input class="form-control" type="email" required="" placeholder="username, e-mail" autofocus="" id="inputEmail">
						<input class="form-control" type="password" required="" placeholder="password" id="inputPassword">
						<div class="checkbox"></div>
						<button class="btn btn-primary btn-block btn-lg btn-signin my-btn-primary" type="submit">Log in</button>
					</form>
					<div class="row">
						<div class="col">
							<a href="#" class="inline-btn js-infoOpen text-right float-left text-primary js-forgotPass">forgot password</a>
						</div>
						<div class="col-12 py-3">
							<a href="#" class="js-signin text-right float-right text-primary p-2">sign-in
								<i class="fa fa-sign-in ml-1"></i>
							</a>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
	
	<form class="d-flex flex-column justify-content-center" id="login-box" method="post" action="auth/login" >
		<div class="login-box-header p-4">
			<h4 class="mb-0 font-weight-bold">Login</h4>
		</div>
		<div class="login-box-content mb-4 row px-4">
			<div class="col-6">
				<div class="fb-login box-shadow d-flex flex-row align-items-center" style="background-color: #1a538a;">
					<a class="social-login-link btn text-light text-left flex-grow-1" href="auth/login?provider=Facebook">
						<i class="fa fa-facebook ml-0" style="padding-right:18px;padding-left:8px;"></i>Facebook</a>
				</div>
			</div>
			<div class="col-6">
				<div class="gp-login box-shadow d-flex flex-row align-items-center" style="background-color: #db4437;">
					<a class=" social-login-link btn text-light text-left flex-grow-1" href="auth/login?provider=Google">
						<i class="fa fa-google ml-0" style="padding-right:18px;padding-left:8px;"></i>Google</a>
				</div>
			</div>
		</div>
		<div class="d-flex flex-row align-items-center login-box-seperator-container px-4">
			<div class="login-box-seperator"></div>
			<div class="login-box-seperator-text">
				<p class="mb-0 px-2" style="color:rgb(201,201,201);">or</p>
			</div>
			<div class="login-box-seperator"></div>
		</div>
		<div class="email-login p-4 bg-light">
			<input type="email" required="" placeholder="Email" minlength="6" class="email-imput form-control" style="margin-top:10px;">
			<input type="password" required="" placeholder="Password" minlength="6" class="password-input form-control" style="margin-top:10px;">
		</div>
		<div class="submit-row p-4" style="margin-bottom:8px;padding-top:0px;">
			<button class="btn btn-primary btn-block box-shadow" type="submit" id="submit-id-submit">Login</button>
			<div class="d-flex justify-content-between py-2">
				<div class="form-check form-check-inline" id="form-check-rememberMe">
					<input class="form-check-input" type="checkbox" name="check" id="formCheck-1" for="remember" style="cursor:pointer;">
					<label class="form-check-label" for="formCheck-1">
						<span class="label-text">Remember Me</span>
					</label>
				</div>
				<a href="#" id="forgot-password-link">Forgot Password?</a>
			</div>
		</div>
		<div id="login-box-footer" class="px-4 pt-3 pb-4 border-top">
			<p style="margin-bottom:0px;">Don't you have an account?
				<a href="#" id="register-link">Sign Up!</a>
			</p>
		</div>
	</div>
</main>