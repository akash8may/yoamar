<?php
$this->load->view('_templates/header_base');
?>

<main class="container-fluid">
    <div class="row">
        <div class="col-12 col-md-10 offset-md-1">
            <div class="row mb-5 ad-container border">
                <div class="col-12 col-md-3 p-4">
                    <h1 class="yo-title-lg font-weight-light">On sale products this month</h1>
                    <h6 class="font-weight-light mt-2">
                        From bags, to wall hangings, get notified on sale seasons</h6>
                </div>
                <div class="col p-0 m-0">
                    <div class="row bg-info12 py-3 m-0 h-100">
                    <?php if(!empty($ad_products)): 
                    if(is_array($ad_products)):
                        foreach($ad_products as $ad_product):
                            // COMPONENT PRODUCT-CARD _small
                        ?>
                        <div class="col-md-4 col-12">
                            <?php 
                            $ad_product->unit_price = $ad_product->sale_price;

                            $this->load->view('_components/site/product_card', array(
                                'size' => 'sm',
                                'product' => $ad_product,
                                'extra_classes' => 'my-3 shadow',
                                'extra_attributes' => 'style="max-width: 244px;"',
                            ));
                            ?>
                        </div>
                    <?php endforeach;
                    else: ?>
                        <div class="col">
                            <!-- COMPONENT BANNER-CARD -->
                        </div>
                    <?php endif;
                    endif; ?>
                    </div>
                </div>
            </div>
        </div>
    
        <div class="col-12">
            <div class="yo-page-title">
                <h1 class="d-none text-primary-dark87 yo-title-lg">PRODUCTS</h1>
            </div>
        </div>
        <div class="col-12">
            <div class="yo-content">
                <div class="row mx-0 yo-content-header">
                    <div class="col-12 col-lg-9 offset-lg-3 p-0">
                        <div class="yo-content-header">
                            <h5 class="d-inline-block text-gray pl-2">Products</h5>
                        </div>
                    </div>
                </div>
                <div class="row mx-0">
                    <div class="col-lg-3 p-0 side-bar">
                        <!-- TEMPLATEstart category-list-->
                        <?php
                            $data = array();
                            $this->load->view('_components/site/category_list', $data);
                        ?>
                            <!-- TEMPLATEend category-list-->
                    </div>
                    <div class="col-lg-9 mx-auto content px-0">
                        <div class="row yo-products">
						<?php 
							if(empty($products)):
						?>
							<div class="col-12">
								<?php 
									$this->load->view('_templates/empty_message', array(
										'title'=>'No products found',
										'message'=>'Once products are added, they will appear here.'
									));
									
								?>
							</div>
						<?php 
							else:
								foreach($products as $product):
						?>
                            <!-- LOOP -->
                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                <!-- TEMPLATEstart product_card-->
                                <?php 
                                    $this->load->view('_components/site/product_card', array(
                                        'product'=>$product,
                                        'size' => 'lg',
                                        'extra_classes' => '',
                                        'extra_attributes' => '',
                                    ));
                                ?>
                                <!-- TEMPLATEend product_card-->
                            </div>
                            <!-- LOOPend -->
						<?php 
								endforeach;
							endif;
						?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>