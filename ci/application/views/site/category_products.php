<?php
$this->load->view('_templates/header_base');
$category_title = $category->title ?? '-';
$subcategory_title = $subcategory->title ?? '-';
$product_count = count(@$products) ?? 0;
?>

<main>
	<div class="container yo-page-title px-0">
		<h1 class="/*d-inline-block*/ d-none text-primary-dark87 yo-title-lg">PRODUCTS</h1>
	</div>
	<div class="container-fluid yo-content">
		<div class="yo-content-header">
			<p class="d-inline-block text-primary-dark87 yo-title">CATEGORIES / 
                <a href="#"><?= strtoupper($category_title); ?></a> /
				<a href="#"><?= strtoupper($subcategory_title); ?></a>
			</p>
		</div>
		<?php if(!empty($product_classes)): ?>
		<div class="container-fluid my-2">
			<div class="row">
				<ul class="list-inline">
				<?php foreach($product_classes as $product_class):
					$badge_class = ($product_class->id == $this->input->get('sub'))	? 'badge-success' : 'badge-secondary';
				?> 
					<li class="list-inline-item my-2">
						<a href="?sub=<?= $product_class->id; ?>"><span class="badge badge-pill <?= $badge_class; ?> p-2"><?= $product_class->title ?></span></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</div>
		</div>
		<?php endif;?>
		<div class="row mx-0">
			<div class="col-lg-3 p-0 side-bar">
				<!-- TEMPLATEstart category-list-->
				<?php
                $data = array();
                
                $this->load->view('_components/site/category_list', $data);
            	?>
				<!-- TEMPLATEend category-list-->
			</div>
			<div class="col-lg-9 mx-auto content px-0">
				<div class="row yo-products">
				<?php if(!empty($products)): ?>
				<div class="col-12">
					<p class="my-2 alert alert-secondary text-center text-muted"><?= $product_count; ?> products found</p>
				</div>

					<!-- LOOP -->
					<?php foreach($products as $product):?>
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
						<?php 
						// TEMPLATEstart product_card
                            $this->load->view('_components/site/product_card', array(
								'product' => $product
                            ));
						// TEMPLATEend product_card
                        ?>
					</div>
					<?php endforeach; ?>
					<!-- LOOPend -->
				<?php else:?>
					<div class="col-12">
						<?php 
							$this->load->view('_templates/empty_message',array(
								'title'=>'No matching products found',
								'message'=>'Once products are added, they will appear here',
								'button'=>array(
									'text'=>'EXPLORE PRODUCTS',//TODO: Localize
									'link'=>site_url('explore'),
								),
							));
						?>
					</div>
				<?php endif;?>
				</div>
			</div>
		</div>
	</div>
</main>
