<?php
$this->load->view('_templates/header_base');
?>
<main>
	<?php if(!empty($product)): ?>

	<div class="container px-sm-0 px-5"> 
        <div class="row yo-product" id="pr_<?= $product->id ?>">
            <div class="col product-img-container p-0">
                <?php 
                $images = $this->utility->get_product_images($product->id);
                $product_thumb_link = @$images[0]->thumbnail_link ?? base_url(PRODUCT_PLACEHOLDER_IMG);
                $product_img_link = @$images[0]->img_link ?? base_url(PRODUCT_PLACEHOLDER_IMG);
                // var_dump($images);
                ?>
                <div class="row mx-0 bg-secondary11">
                    <div class="col img-holder p-0">
                        <a href="<?= $product_img_link ?>" class="zoomple js-imgZoomer">
                            <img src="<?= $product_thumb_link ?>" async
                            data-src="<?= $product_img_link ?>" 
                            class="_d-none float-right align-middle" 
                            data-im-orientation="vertical">
                        </a>
                    </div>
                </div>
                <!-- TEMPLATEstart image-list -->
                <?php
                $data = array(
                    'images' => $images,
                    'carousel' => FALSE,
                    'id' => $product->id,
                );
                $this->load->view('_components/site/single_product_image-list', $data);
                ?>
                    <!-- TEMPLATEend image-list -->
            </div>
            <div class="col product-details-container">
                <div class="js-zoomer zoom-lens d-none"></div>
                <div class="row border-bottom pb-2">
                    <div class="col product-details p-0 w-not-responsive">
                        <h2 class="yo-title-lg text-primary-dark"><?= $product->product_name ?></h2>
                        <p class="product-price text-primary-dark mb-0">$ <?= $product->unit_price ?></p>
                        <!-- TEMPLATEstart rating -->
                        <?php 
                        $avg_rating = $this->utility->get_average_rating($product->id) ?? MIN_RATING;
                        $data = array(
                            'extra_classes' => 'product-rate',
                            'rating' => $avg_rating
                        );
                        $this->load->view('_components/ratings', $data);
                    ?>
                        <!-- TEMPLATEend rating -->
                        <div class="product-specs text-primary-dark87"><?= $product->style ?></div>
                        <div class="product-about m-0 d-none">
                        
                        </div>
                        <a href="#!" class="inline-btn js-infoOpen text-right float-right text-primary my-2">read more</a>
                        <a href="#!" class="inline-btn js-infoClose text-right float-right text-primary my-2 d-none">read less</a>
                    </div>

                    <div class="col product-variables p-0 d-none">
                        <form action="product/vars" method="post" class="d-none">
                            <!-- TEMPLATEstart product-size-number -->
                            <?php
								$this->load->view('_components/site/product_size', array(
								    'selected' => NULL
								));
								// TEMPLATEend product-size-number
								
								// TEMPLATEstart product-size-fixed
								$this->load->view('_components/site/product_size-fixed', array(
									'selected' => NULL
								));
								// TEMPLATEend product-size-fixed
                        	?>
                                    <!-- TEMPLATEstart product-color-->
                                    <?php
                            $this->load->view('_components/site/product_color', array(
                                'selected' => NULL
                            ));
                        ?>
                                        <!-- TEMPLATEend product-color-->
                        </form>
                    </div>
                    <div class="col product-details p-0">
                        <div class="product-about m-0" data-toggle-read-more>
                            <?= $product->description ?>
                        </div>
                        <a href="#!"
                            class="inline-btn js-infoOpen text-right float-right text-primary my-2">Read more</a>
                        <a href="#!"
                            class="inline-btn js-infoClose text-right float-right text-primary my-2 d-none">Read less</a>
                    </div>
                    <div class="col product-cta p-0 _position-absolute _position-fixed _fixed-bottom">
                        <div class="row mx-0 bg-light py-1">
                            <div class="col product-variables p-0 my-2">
                                <!-- Or test it. http://localhost/yoamar/public/admin/accounts/admin -->

                                <button class="btn btn-primary btn-lg my-btn-primary js-addToCart text-right text-center py-2 px-4 float-right js-btn-absolute" type="button">
                                    <i class="fa fa-cart-plus"></i>
                                </button>

                                <!-- TEMPLATEstart button-add-to-cart-->
                                <?php
                                    $data = array(
                                        'extra_classes' => 'btn-lg py-2 js-addToCart',
                                        'responsive' => FALSE,
                                        'attributes' => 'data-id="'.$product->id.'"'
                                    );
                                    
                                    $this->load->view('_components/site/button_add-to-cart', $data);
                                ?>
                                    <!-- TEMPLATEend button-add-to-cart-->
                            </div>
                            <div class="col p-0 mt-2">
                                <!-- TEMPLATEstart button-add-to-wishlist-->
                                <?php
                                    $data = array(
                                        'extra_classes' => 'btn-outline float-right py-2 px-4',
                                        'responsive' => TRUE, #Always use caps in Codeigniter for boolean/NULL
                                        'attributes' => 'data-id="'.$product->id.'"'
                                    );
                                    
                                    $this->load->view('_components/site/button_add-to-wishlist', $data);
                                ?>
                                    <!-- TEMPLATEend button-add-to-wishlist-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mb-5">
		<?php if(empty($related_products)): 
			$this->load->view('_templates/empty_message',array(
				'title'=>'No related products found', //TODO: Add lang file
				'message'=>'Once related products are found, they will appear here.', //TODO: Add lang file
				'extra_classes'=>'',
				'button'=>array(
					'text'=>'EXPLORE PRODUCTS', //TODO: Add lang files
					'link'=>site_url('category/'.$product->product_family.'/products')
				)
			));
			else:
		?>
        <div class="row">
            <div class="col-12">
                <h4 class="mb-3">Related products<br /></h4>
            </div>
			<?php foreach($related_products as $product):?>
            <div class="col-12 col-md-4 col-xl-3 pr-0">
            <?php
                // <!-- COMPONENT PRODUCT-CARD -->
                $this->load->view('_components/site/product_card', array(
                    'product' => $product
                ));
			?>
            </div>
			<?php endforeach; ?>
        </div>
		<?php endif;?>
    </div>
    <div class="container-fluid px-xl-0 mb-5 px-5">
    <?php
        $this->load->view('_components/site/review-list', array(
            'id' => $product->id,
            'reviews' => [],
        ));
        ?>
    </div>
	<?php else:
		$this->load->view('_templates/empty_message',array(
			'title'=>'Product not found',
			'message'=>'We could not find the product you are looking for. In the mean time, you can explore other products on '.SITE_NAME,
			'button'=>array(
				'text'=>'Explore products',
				'link'=>site_url('explore')
			),
			'extra_classes'=>'mh-100'
		));
	endif;?>
</main>
