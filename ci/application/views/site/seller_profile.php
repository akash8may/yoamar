<?php
$data = array();

$this->load->view('_templates/header_base', $data);
?>
<main>
<?php

$this->load->view('_templates/admin/profile_header', $data);
?>
    <div class="container-fluid yo-content">
		<div class="yo-content-header">
            <h5 class="d-inline-block text-gray pl-2"></h5>
		</div>
		<div class="row mx-0">
			<div class="col-lg-3 p-0 side-bar border-right">
                <!-- TEMPLATEstart category-list-->
				<?php
                $data = array();
                $this->load->view('_components/site/category_list', $data);
                ?>
                <!-- TEMPLATEend category-list-->
			</div>
			<div class="col-lg-9 mx-auto content px-0">
				<div class="row yo-products">
					<?php for($i=1;  $i < 6; $i++):?>

					<!-- LOOP -->
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
						<!-- TEMPLATEstart product_card-->
						<?php 
                                    $this->load->view('_components/site/product_card', array(
										'id' => 'pr'.($i+2/0.3),
										'product_id' => $i
                                    ));
                                ?>
						<!-- TEMPLATEend product_card-->
					</div>
					<!-- LOOPend -->
					<?php endfor; ?>
				</div>
			</div>
		</div>
	</div>
</main>
