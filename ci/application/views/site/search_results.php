<?php
$search_title_string = 'Search results for ';
$search_title_string .= "'".get_value_or_default(@$search_query,'products')."'";
$this->load->view('_templates/header_base');
$category_products = @$category_products ?? NULL;
?>

<main class="container-fluid">
	<div class="container-fluid yo-content">
		<div class="w-100 py-4 mb-4 border-bottom bg-white text-center">
			<div class="yo-page-title">
				<h2 class="text-primary-dark87 yo-title-lg">
					<i class="font-weight-light"> <?= count($products) > 0 ? $search_title_string : @$search_query ?></i>
				</h2>
			</div>
			<div class="yo-content-header">
				<h5 class="d-inline-block text-gray">Found <?= count($products) ?> result<?= count($products) > 1 || count($products) === 0 ? 's' : '' ?></h5>
			</div>
		</div>
		<div class="row mx-0">
			<div class="col-lg-3 p-0 side-bar border-right">
                <?php
				if(! empty($products)):
                # <!-- TEMPLATEstart badges-list-->
                $data = array(
					'class' => 'p-1 py-2 my-1',
					'badges' => $products,
                );
                $this->load->view('_components/site/badges_list', $data);
                # <!-- TEMPLATEend badges-list-->
				endif;
				# <!-- TEMPLATEstart category-list-->
                $data = array();
                $this->load->view('_components/site/category_list', $data);
                # <!-- TEMPLATEend category-list-->
                ?>
			</div>
			<div class="col-lg-9 mx-auto content px-0">
				<?php
				if($category_products && is_array($category_products)) : 
				?>
				<div class="row yo-products">
					<div class="col-12 pb-0 pt-3">
						<h5 class="yo-content-header text-primary-dark font-weight-light m-0">Similar products from <?= $category_products[0]->product_segment_title ?> category</h5>
					</div>
					<!-- LOOP -->
					<?php 
					foreach($category_products as $key => $product):?>

					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
						<!-- TEMPLATEstart product_card-->
						<?php 
							$this->load->view('_components/site/product_card', array(
								'product' => $product,
							));
						?>
						<!-- TEMPLATEend product_card-->
					</div>
						<?php endforeach; ?>
					<!-- LOOPend -->
				</div>
				<?php endif; ?>
				<div class="row yo-products">
					<!-- LOOP -->
					<?php 
					if(! empty($products)) :
					foreach($products as $key => $product):?>

					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
						<!-- TEMPLATEstart product_card-->
						<?php 
							$this->load->view('_components/site/product_card', array(
								'product' => $product,
							));
						?>
						<!-- TEMPLATEend product_card-->
					</div>
					<?php 
					endforeach; 
					endif;?>
					<!-- LOOPend -->
				</div>
			</div>
		</div>
	</div>


</main>
