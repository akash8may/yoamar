<?php
$cart= empty($cart)?array():$cart;
$this->load->view('_templates/header_landing',$cart);
?>

<main class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="yo-page-title">
                <h1 class="d-none text-primary-dark87 yo-title-lg">PRODUCTS</h1>
            </div>
        </div>
        <div class="col-12">
            <div class="yo-content">
                <div class="row mx-0 yo-content-header">
                    <div class="col-12 col-lg-9 offset-lg-3 p-0">
                        <div class="yo-content-header">
                            <h5 class="d-inline-block text-gray pl-2">Products</h5>
                        </div>
                    </div>
                </div>
                <div class="row mx-0">
                    <div class="col-lg-3 p-0 side-bar">
                        <!-- TEMPLATEstart category-list-->
                        <?php
                            $data = array();
                            $this->load->view('_components/site/category_list', $data);
                        ?>
                            <!-- TEMPLATEend category-list-->
                    </div>
                    <div class="col-lg-9 mx-auto content px-0">
                        <div class="row yo-products">
						<?php 
							if(empty($products)):
						?>
							<div class="col-12">
								<?php 
									$this->load->view('_templates/empty_message', array(
										'title'=>'No products found',
										'message'=>'Once products are added, they will appear here.'
									));
									
								?>
							</div>
						<?php 
							else:
								foreach($products as $product):
						?>
                            <!-- LOOP -->
                            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                <!-- TEMPLATEstart product_card-->
                                <?php 
                                    $this->load->view('_components/site/product_card', array(
                                        'product'=>$product
                                    ));
                                ?>
                                <!-- TEMPLATEend product_card-->
                            </div>
                            <!-- LOOPend -->
						<?php 
								endforeach;
							endif;
						?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</main>
