<main>
	<div class="container-fluid text-center d-flex flex-row justify-content-xl-center align-items-xl-center w-100 yo-fluid-container bg-info54 new-info-container">
		<div class="row mt-5 pt-4">
			<div class="col">
				<h1 class="display-3 text-muted text-center">Karibu merchant</h1>
			</div>
			<div class="col-12">
				<div class="user-item item-large card mx-auto bg-transparent border-0">
					<div class="user-container shadow-none border-0 mx-auto">
						<p class="text-center mb-0 d-block">
							<a class="user-avatar float-none text-center d-inline-block mx-auto">
								<img src="<?= get_value_or_default($new_user->profile_url, base_url(MERCHANT_PLACEHOLDER_IMG))?>" class="rounded-circle img-fluid"/>
							</a>
						</p>
						<p class="lead text-center user-name mt-1 mb-3"><a><?= @$new_user->first_name .' '.@$new_user->last_name ?></a></p>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="card rounded shadow-sm mx-auto mb-4 border-0">
					<div class="card-body">
						<h4 class="card-title">To easy online store</h4>
						<h6 class="text-muted card-subtitle mb-2">Subtitle</h6>
						<p class="card-text">Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
					</div>
				</div>
				<div class="card rounded shadow-sm mx-auto mb-4 border-0 bg-primary12">
					<div class="card-body">
						<h4 class="card-title">Title</h4>
						<h6 class="text-muted card-subtitle mb-2">Subtitle</h6>
						<p class="card-text">Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
						<div class="form-check">
							<input type="checkbox" name="add-to-newsletter" class="form-check-input" id="formCheck-1" />
							<label class="form-check-label" for="formCheck-1">Keep me informed</label>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 pb-5 pt-3">
				<button class="btn btn-primary my-btn-primary btn-outline text-center mx-auto" type="button">
					<i class="fa fa-check"></i>SOUNDS GOOD!</button>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
			</div>
		</div>
	</div>
</main>