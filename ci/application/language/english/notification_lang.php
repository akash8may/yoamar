<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//ORDERS
$lang['product_new_order']              = 'New order for [product_name]! Please deliver it to the YoAmar\'s offices at [yoamar_location].';
$lang['product_new_order_reminder']     = 'Hey [name], [product_name] has just been bought! Please it to YoAmar\s offices at [yoamar_location]';
$lang['order_status_processing']        = 'Your order for [product] is being processed.';
$lang['order_status_in_transit']        = 'Your order for [product] is being shipped.';
$lang['order_status_delivered']         = '[product] has just been delivered. Please confirm';
$lang['order_status_error']             = 'There has been an hitch in your recent ordered [product]. We are working hard to fix this situation';

$lang['order_reversal_processing']      = 'The order reversal request is being processed';
$lang['order_reversal_success']         = 'The order reversal request has been Granted. Reversal has been successful.';
$lang['order_reversal_error']           = 'The order reversal request has been Declined. Reversal has failed. \n [message]';


//PAYMENT
$lang['payment_success']                = 'Payment for [product] successful';
$lang['payment_success']                = 'Payment for [product] declined.\n  [message]';
$lang['payment_mpesa_success']          = 'M-pesa payment for [product] successful';
$lang['payment_mpesa_error']            = 'M-pesa payment for [product] declined.\n  [message]';
$lang['payment_paypal_success']         = 'Paypal payment for [product] successful';
$lang['payment_paypal_error']           = 'Paypal payment for [product] declined.\n  [message]';
$lang['payment_card_success']           = 'Card payment for [product] successful';
$lang['payment_card_error']             = 'Card payment for [product] declined.\n [message]';
$lang['payment_refund_processing']      = 'Payment refund for [product] is being processed';
$lang['payment_refund_success']         = 'Payment refund for [product] successful';
$lang['payment_refund_error']           = 'Payment refund for [product] declined.\n  [Message]';


//PRODUCT
$lang['product_has_error']              = '[product_name] details needs fixing. [message]';
$lang['product_image_has_error']        = '[product_name]\'s images needs [message]';
$lang['product_needs_update']           = '[product_name] needs to be updated';
$lang['product_image_needs_update']     = '[product_name] images needs to be updated. \n [message]';

$lang['product_request_review']         = 'How would you like to review [product_name]?';
$lang['product_request_review_teen']    = 'Do you like [product_name]? Give us your review!';
$lang['product_request_review_parent']  = '1 to 5, how much does your kid love [product_name]?';
$lang['product_new_review']             = 'New review from [name]';
$lang['product_new_review_2']           = '[product_name] has a new review';
$lang['product_new_review_reply']       = 'New reply to your review!';

$lang['product_sale_promotion']         = '[product] is now [sale] on sale!';
$lang['product_promotion']              = 'yoAmar has an ongoing salee promotion. Visit [yoamar_site]' ;


//PROFILE/ACCOUNT
$lang['profile_error']                  = 'Profile error, \n [message]';
$lang['profile_update']                 = 'We recommend that you update your profile. Add a smiley face or something!';
$lang['profile_add_payment_method']     = 'Adding a payment method makes your checkout faster! Add one!';

$lang['profile_wishlist_reminder']      = 'Message from [product]: "Remember me?"';
$lang['profile_saved_items_reminder']   = 'Message from [product]: "Remember me?"';

$lang['account_suspect']                = 'Please confirm [message]';

//ADMIN
$lang['new_seller_account_title']       = 'New seller account';
$lang['new_seller_account_message']     = 'A new seller account from needs to be verified';