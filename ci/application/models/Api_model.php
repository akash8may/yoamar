<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface Api_interface
{
	#region Api keys
	/** Create an api key */
	public function create_api_key(array $data): boolean;

	/** update an api key */
	public function update_api_key(array $filter,array $data): boolean;

	/** delete an api key */
	public function delete_api_key(array $filter): boolean;

	/** get a single api key */
	// //public function get_single_api_key(array $filter): array;

	/** get a list of api keys */
	public function get_api_keys(array $filter): array;

	#endregion

	#region Api requests
	/** Create an api request */
	public function create_api_request(array $data): boolean;

	/** get an api request */
	// //public function get_single_api_request(array $filter): array;

	/** get a list of api request */
	public function get_api_requests(array $filter): array;

	/** update an api request */
	public function update_api_request(array $filter,array $data): boolean;

	/** delete an api request */
	public function delete_api_request(array $filter): boolean;

	#endregion
}

// Api keys, Api requests
class Api_model extends MY_Model implements Api_interface
{
	// Constructor - call parent constructor (loads database)
	public function __construct()
	{
		parent::__construct();
	}

#region Api keys
	/** Create an api key */
	public function create_api_key(array $data): boolean
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_API_KEY,'id');
				$this->db->or_where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->db->update_api_key(
					array($_field_id=>$data['id']),
					$data
				);
			}
			
			//If we get to this point it means the record was not found ~ create a new one
			return $this->db->insert(TBL_API_KEY,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$api_model->create_api_key()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** update an api key */
	public function update_api_key(array $filter,array $data): boolean
	{
		try
		{	
			unset($data['id']);
			$this->db->or_where($filter);
			return $this->db->update(TBL_API_KEYS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$api_model->update_api_key()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** get a list of api keys */
	public function get_api_keys(array $filter): array
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_API_KEYS)
			);
			$this->db->from(TBL_API_KEYS);

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->or_where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$api_model->get_api_keys()]\n".$this->db->error());
			return NULL;
		}
	}

	/** delete an api key */
	public function delete_api_key(array $filter): boolean
	{
		try
		{	
			$this->db->or_where($filter);
			return $this->db->delete(TBL_API_KEYS);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$api_model->delete_api_key()]\n".$this->db->error());
			return FALSE;
		}
	}
	
	#endregion

	#region Api requests
	/** Create an api request */
	public function create_api_request(array $data): boolean
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_API_REQUESTS,'id');
				$this->db->or_where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->db->update_api_request(
					array($_field_id=>$data['id']),
					$data
				);
			}
			
			//If we get to this point it means the record was not found ~ create a new one
			return $this->db->insert(TBL_API_REQUESTS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$api_model->create_api_request()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** get an api request */
	// //public function get_single_api_request(array $filter): array;

	/** get a list of api request */
	public function get_api_requests(array $filter): array
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_API_REQUESTS)
			);
			$this->db->from(TBL_API_REQUESTS);

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->or_where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$api_model->get_api_requests()]\n".$this->db->error());
			return NULL;
		}
	}

	/** update an api request */
	public function update_api_request(array $filter,array $data): boolean
	{
		try
		{	
			unset($data['id']);
			$this->db->or_where($filter);
			return $this->db->update(TBL_API_REQUESTS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$api_model->update_api_request()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** delete an api request */
	public function delete_api_request(array $filter): boolean
	{
		try
		{	
			$this->db->or_where($filter);
			return $this->db->delete(TBL_API_REQUESTS);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$api_model->delete_api_request()()]\n".$this->db->error());
			return FALSE;
		}
	}

	#endregion
}

/* End of file Api_model.php  */
