<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface Order_interface
{
	// ORDERS
	#region Orders
	/** Create order with the `$data` provided  */
	function create_order(array $data);

	/** Get single order matching the `$filter` provided  */
	// function get_single_order(array $filter): array;

	/** Get orders matching the `$filter` provided */
	function get_orders(array $filter);

	/** Update order matching the `$filter` using the `$data` provided */
	function update_order(array $filter,array $data);

	/** Action order with the `$data` provided  */
	function delete_order(array $filter);

	#endregion

	// ORDER REVERSALS
	#region Order reversals
	/** Create order reversal with the `$data` provided  */
	function create_order_reversal(array $data);

	/** Get order reversals matching the `$filter` provided  */
	function get_order_reversals(array $filter);

	/** Get single order reversal matching the `$filter` provided  */
	// function get_single_order_reversal(array $filter): array;

	/** Update order reversal matching the `$filter` using the `$data` provided */
	function update_order_reversal(array $filter,array $data);

	/** Delete order reversal matching the `$filter` provided  */
	function delete_order_reversal(array $filter);

	#endregion

}

// Orders, shipments, sales
class Order_model extends MY_Model implements Order_interface
{
	// Constructor - call parent constructor (loads database)
	public function __construct()
	{
		parent::__construct();
	}

// ORDERS
	#region Orders
	/** Create order with the `$data` provided. Allows entry of multiple items at once */
	function create_order(array $data)
	{
		try
		{
			$data = _filter_order_data($data);
			//If we get to this point it means the record was not found ~ create a new one
			return $this->db->insert(TBL_ORDERS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->create_order()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Get orders matching the `$filter` provided */
	function get_orders(array $filter)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_ORDERS,'*,')

				.$this->get_table_field(TBL_PRODUCTS,'total_price AS product_price,')
				.$this->get_table_field(TBL_PRODUCTS,'product_name,')
				.$this->get_table_field(TBL_PRODUCTS,'description AS product_description,')
				
				// Get buyer first name and last name too
				.$this->get_table_field(TBL_USERS,'first_name AS buyer_first_name,')
				.$this->get_table_field(TBL_USERS,'last_name AS buyer_last_name,')

				.$this->get_table_field(TBL_ORDER_REVERSALS,'quantity AS reversed_quantity,')
				.$this->get_table_field(TBL_ORDER_REVERSALS,'charges AS reversed_charges,')
				.$this->get_table_field(TBL_ORDER_REVERSALS,'item_condition AS reversed_item_condition,')
				.$this->get_table_field(TBL_ORDER_REVERSALS,'logistics_received AS reversed_logistics_received,')
			);
			$this->db->from(TBL_ORDERS);

			// Product joins
			$this->db->join(TBL_PRODUCTS,
				$this->get_table_field(TBL_ORDERS,'product_id').'='.$this->get_table_field(TBL_PRODUCTS,'id')
			);

			// Buyer info joins ~ we'll do another query if we need to get seller information
			$this->db->join(TBL_USERS,
				$this->get_table_field(TBL_ORDERS,'buyer_id').'='.$this->get_table_field(TBL_USERS,'id')
			);

			// order_returns joins
			$this->db->join(TBL_ORDER_REVERSALS,
				$this->get_table_field(TBL_ORDERS,'transaction_id').'='.$this->get_table_field(TBL_ORDER_REVERSALS,'transaction_id'),'left'
			);

			//If there is a filter set, add a where clause to only select records matching the filter

			if(!empty($filter))
			{
				$this->db->where($filter);
			}

			$this->db->order_by('date_ordered', 'DESC');
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->get_orders()]\n".$this->db->error());
			return NULL;
		}
	}

	/** Update order matching the `$filter` using the `$data` provided */
	function update_order(array $filter,array $data)
	{
		try
		{	
			unset($data['order_id']);
			$this->db->where($filter);
			return $this->db->update(TBL_ORDERS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->update_order()]\n".$this->db->error());
			return FALSE;
		}	
	}

	/** Action order with the `$data` provided  */
	function delete_order(array $filter)
	{
		try
		{	
			$this->db->where($filter);
			return $this->db->delete(TBL_ORDERS);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->delete_order()]\n".$this->db->error());
			return FALSE;
		}
	}

	#endregion

	// SHIPMENTS
	#region Shipments
	/** Create shipment details with the `$data` provided  */
	function create_shipping_details(array $data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->update_shipping_details(
					array('id'=>$data['id']),
					$data
				);
			}

			//If we get to this point it means the record was not found ~ create a new one
			//If we get to this point it means it was insert not an update
			$data['id'] = $this->generate_int_id(TBL_SHIPPING_DETAILS,'id');
			$insert_status = $this->db->insert(TBL_SHIPPING_DETAILS,$data);

			return $insert_status ? $data['id'] : FALSE;
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->create_shipment_details()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Get shipment details matching the `$filter` provided */
	public function get_shipping_details(array $filter)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_SHIPPING_DETAILS)
			);
			$this->db->from(TBL_SHIPPING_DETAILS);

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->or_where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->get_shipments()]\n".$this->db->error());
			return NULL;
		}
	}

	
	/** Update shipping details matching the `$filter` with the `$data` provided  */
	function update_shipping_details(array $filter,array $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->where($filter);
			return $this->db->update(TBL_SHIPPING_DETAILS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->update_shipping_details()]\n".$this->db->error());
			return FALSE;
		}	
	}



	#endregion

	// ORDER REVERSALS
	#region Order reversals
	/** Create order reversal with the `$data` provided  */
	function create_order_reversal(array $data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->update_order_reversal(
					array('id' => $data['id']),
					$data
				);
			}
			
			//If we get to this point it means the record was not found ~ create a new one
			$data['id'] = $this->generate_int_id(TBL_ORDER_REVERSALS);
			return $this->db->insert(TBL_ORDER_REVERSALS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->create_order_reversal()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Get order reversals matching the `$filter` provided  */
	function get_order_reversals(array $filter)
	{
		try
		{
			//TODO: Use joins here
			$this->db->select(
				$this->get_table_field(TBL_ORDER_REVERSALS)
			);
			$this->db->from(TBL_ORDER_REVERSALS);

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->get_order_reversals()]\n".$this->db->error());
			return NULL;
		}
	}

	/** Update order reversal matching the `$filter` using the `$data` provided */
	function update_order_reversal(array $filter,array $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->where($filter);
			return $this->db->update(TBL_ORDER_REVERSALS, $data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->update_order_reversals()]\n".$this->db->error());
			return FALSE;
		}	
	}

	/** Delete order reversal matching the `$filter` provided  */
	function delete_order_reversal(array $filter)
	{
		try
		{	
			$this->db->where($filter);
			return $this->db->delete(TBL_ORDER_REVERSALS);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->delete_order_reversals()]\n".$this->db->error());
			return FALSE;
		}
	}

	#endregion
	
}

/* End of file Order_model.php */
