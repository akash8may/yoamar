<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface Product_interface
{
	// PRODUCTS
	#region Products
	/** Create a single product */
	function create_product(array $data);
	
	/** Get a single product based on the `$filter` provided.
	 * If multiple products are available, returns the first instance found.
	*/
	// function get_single_product(array $filter);
	
	/** Get all products matching the `$filter` provided */
	function get_products(array $filter,bool $in_stock=TRUE,$search_filters);

	/** Update products matching `$filter` with the `$data` provided */
	function update_product(array $filter,array $data);
	
	/** Delete products based on the $filter provided */
	function delete_products(array $filter);
	
	#endregion

	// PRODUCT IMAGES
	#region Product images
	/** Create a single product image*/
	function create_product_image(array $data);
	
	/** Get all products matching the `$filter` provided */
	function get_product_images(array $filter);

	/** Update products matching `$filter` with the `$data` provided */
	function update_product_images(array $filter,array $data);
	
	/** Delete products based on the $filter provided */
	function delete_product_images(array $filter);
	
	#endregion

	// CATEGORY GROUPS
	#region Category groups
	/** Create a single category group */
	function create_category_group(array $data);
	
	/** Get a single category group based on the `$filter` provided.
	 * If multiple category groups are available, returns the first instance found.
	*/
	// function get_single_category_group(array $filter);

	/** Get all category groups matching the `$filter` provided */
	function get_category_groups(array $filter);

	/** Update category_groups matching `$filter` with the `$data` provided */
	function update_category_group(array $filter,array $data);
	
	/** Delete category_groups based on the $filter provided */
	function delete_category_group(array $filter);
	
	#endregion

	// CATEGORIES 
	#region Categories
	/** Create a single category */
	function create_category(array $data);
	
	/** Get a single category group based on the `$filter` provided.
	 * If multiple category groups are available, returns the first instance found.
	*/
	// function get_single_category(array $filter);
		
	/** Get all categories matching the `$filter` provided */
	function get_categories(array $filter);

	/** Update categories matching `$filter` with the `$data` provided */
	function update_category(array $filter,array $data);
	
	/** Delete categories based on the $filter provided */
	function delete_category(array $filter);

	#endregion

	// SUBCATEGORIES
	#region Subcategories
	/** Create a single subcategory */
	function create_subcategory(array $data);
	
	/** Get a single subcategory group based on the `$filter` provided.
	 * If multiple subcategory groups are available, returns the first instance found.
	*/
	// function get_single_subcategory(array $filter);
		
	/** Get all subcategories matching the `$filter` provided */
	function get_subcategories(array $filter);

	/** Update subcategories matching `$filter` with the `$data` provided */
	function update_subcategory(array $filter,array $data);
	
	/** Delete subcategories based on the $filter provided */
	function delete_subcategory(array $filter);

	#endregion
}

// Handles products, categories, subcategories & category groups
class Product_model extends MY_Model implements Product_interface
{
	// Constructor - call parent constructor (loads database)
	public function __construct()
	{
		parent::__construct();
	}

	//Selects and joins for products table
	private function _product_joins()
	{
		// Select
		$this->db->select(
			$this->get_table_field(TBL_PRODUCTS,'*,') # Select all product table fields

			.$this->get_table_field(TBL_CATEGORY_GROUPS,'title AS product_group_title,')
			.$this->get_table_field(TBL_CATEGORY_GROUPS,'description AS product_group_description,')
			.$this->get_table_field(TBL_CATEGORY_GROUPS,'date_created AS product_group_date_created,')

			.$this->get_table_field(TBL_CATEGORIES,'title AS product_segment_title,')
			.$this->get_table_field(TBL_CATEGORIES,'description AS product_segment_description,')
			.$this->get_table_field(TBL_CATEGORIES,'img_link AS product_segment_date_created,')

			.$this->get_table_field(TBL_SUBCATEGORIES,'title AS product_family_title,')
			.$this->get_table_field(TBL_SUBCATEGORIES,'description AS product_family_description,')
			.$this->get_table_field(TBL_SUBCATEGORIES,'img_link AS product_family_date_created,')
			
			.$this->get_table_field(TBL_USERS,'first_name,')
			.$this->get_table_field(TBL_USERS,'last_name,')
			.$this->get_table_field(TBL_USERS,'company')

		);

		// Category group joins
		$this->db->join(TBL_CATEGORY_GROUPS,
			$this->get_table_field(TBL_PRODUCTS,'product_group').'='.$this->get_table_field(TBL_CATEGORY_GROUPS,'id')
		);

		// Category joins
		$this->db->join(TBL_CATEGORIES,
			$this->get_table_field(TBL_PRODUCTS,'product_segment').'='.$this->get_table_field(TBL_CATEGORIES,'id')
		);

		// Subcategory joins
		$this->db->join(TBL_SUBCATEGORIES,
			$this->get_table_field(TBL_PRODUCTS,'product_family').'='.$this->get_table_field(TBL_SUBCATEGORIES,'id')
		);

		// uSER joins
		$this->db->join(TBL_USERS,
			$this->get_table_field(TBL_PRODUCTS,'seller_id').'='.$this->get_table_field(TBL_USERS,'id'),
			'LEFT'
		);

		$this->db->from(TBL_PRODUCTS);
		// * Product class, type and subtype will be retrieved through separate queries when needbe since we don't currently have a way to individually select them in one select statement
	}

	//Selects and joins for category group table
	private function _category_group_selects()
	{
		// Select
		$this->db->select(
			$this->get_table_field(TBL_CATEGORY_GROUPS)
		);
		$this->db->from(TBL_CATEGORY_GROUPS);
	}

	//Selects and joins for category table
	private function _category_joins()
	{
		// Select
		$this->db->select(
			$this->get_table_field(TBL_CATEGORIES,'*,') # Select all categories table fields

			.$this->get_table_field(TBL_CATEGORY_GROUPS,'title AS category_group_title,')
			.$this->get_table_field(TBL_CATEGORY_GROUPS,'description AS category_group_description,')
		);

		$this->db->from(TBL_CATEGORIES);

		// Category group joins 
		$this->db->join(TBL_CATEGORY_GROUPS,
			$this->get_table_field(TBL_CATEGORIES,'category_group_id').'='.$this->get_table_field(TBL_CATEGORY_GROUPS,'id')
		);
	}

	//Selects and joins for category table
	private function _subcategory_joins()
	{
		// Select
		$this->db->select(
			$this->get_table_field(TBL_SUBCATEGORIES,'*,') # Select all subcategories table fields
		);
		$this->db->from(TBL_SUBCATEGORIES);
	}

	// PRODUCTS	
	#region Products
	/** Create a single product */
	function create_product(array $data)
	{
		try
		{
			$product_data = _filter_product_data($data);

			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_PRODUCTS,'id');
				$this->db->where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->db->update_product(
					array($_field_id=>$data['id']),
					$product_data
				);
			}
			
			//If we get to this point it means it was insert not an update
			$product_data['id'] = $this->generate_string_id(TBL_PRODUCTS,'id');

			// If the records were successfully inserted, return the id of the inserted record
			$insert_status = $this->db->insert(TBL_PRODUCTS,$product_data);
			return $insert_status ? $product_data['id'] : FALSE;
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->create_product()]\n".$this->db->error());
			return FALSE;
		}
	}
	
	/** Get all products matching the `$filter` provided */
	function get_products(array $filter=NULL,bool $in_stock=TRUE,$search_filters=NULL,$limit=NULL)
	{
		try
		{
			$this->_product_joins();
			
			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->where($filter);
			}
			
			// Only display items that are in stock if we have set the option to TRUE
			if($in_stock == TRUE)
			{
				$_field_qty	 = $this->get_table_field(TBL_PRODUCTS,'quantity');
				$this->db->where($_field_qty.' >','0');
			}
			
			$this->use_search_filters($search_filters);

			// Set the limit if any is provided
			if(!empty($limit))
			{
				$this->db->limit($limit);
			}

			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->get_products()]\n".$this->db->error());
			return NULL;
		}
	}

	/** Update products matching `$filter` with the `$data` provided */
	function update_product(array $filter,array $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->where($filter);
			return $this->db->update(TBL_PRODUCTS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->update_product()]\n".$this->db->error());
			return FALSE;
		}
	}
	
	/** Delete products based on the $filter provided */
	function delete_products(array $filter)
	{
		try
		{	
			$this->db->where($filter);
			return $this->db->delete(TBL_PRODUCTS);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->delete_products()]\n".$this->db->error());
			return FALSE;
		}
	}
	
	#endregion

	// PRODUCT IMAGES
	#region Product images
	/** Create a single product image*/
	function create_product_image(array $data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_PRODUCT_IMAGES,'id');
				$this->db->where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->db->update_product_images(
					array($_field_id=>$data['id']),
					$data
				);
			}
			
			//If we get to this point it means it was insert not an update
			$data['id'] = $this->generate_int_id(TBL_PRODUCT_IMAGES,'id');
			return $this->db->insert(TBL_PRODUCT_IMAGES,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->create_product()]\n".$this->db->error());
			return FALSE;
		}
	}
	
	/** Get all products matching the `$filter` provided */
	function get_product_images(array $filter)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_PRODUCT_IMAGES)
			);

			$this->db->from(TBL_PRODUCT_IMAGES);
			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->get_product_images()]\n".$this->db->error());
			return NULL;
		}
	}

	/** Update products matching `$filter` with the `$data` provided */
	function update_product_images(array $filter,array $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->where($filter);
			return $this->db->update(TBL_PRODUCT_IMAGES,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->update_product_images()]\n".$this->db->error());
			return FALSE;
		}	
	}
	
	/** Delete products based on the $filter provided */
	function delete_product_images(array $filter)
	{
		try
		{	
			$this->db->where($filter);
			return $this->db->delete(TBL_PRODUCT_IMAGES);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->delete_product_images()]\n".$this->db->error());
			return FALSE;
		}
	}
	
	#endregion
	
	// CATEGORY GROUPS
	#region Category groups
	/** Create a single category group */
	function create_category_group(array $data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_CATEGORY_GROUPS,'id');
				$this->db->where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->db->update_category_group(
					array($_field_id=>$data['id']),
					$data
				);
			}
			
			//If we get to this point it means it was insert not an update
			return $this->db->insert(TBL_CATEGORY_GROUPS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->create_category_group()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Get all category groups matching the `$filter` provided */
	function get_category_groups(array $filter)
	{
		try
		{
			$this->_category_group_selects();

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->get_category_groups()]\n".$this->db->error());
			return NULL;
		}
	}

	/** Update category_groups matching `$filter` with the `$data` provided */
	function update_category_group(array $filter,array $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->where($filter);
			return $this->db->update(TBL_CATEGORY_GROUPS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->update_category_group()]\n".$this->db->error());
			return FALSE;
		}
	}
	
	/** Delete category_groups based on the $filter provided */
	function delete_category_group(array $filter)
	{
		try
		{	
			$this->db->where($filter);
			return $this->db->delete(TBL_CATEGORY_GROUPS);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->delete_category_group()]\n".$this->db->error());
			return FALSE;
		}
	}
	
	#endregion

	// CATEGORIES 
	#region Categories
	/** Create a single category */
	function create_category(array $data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_CATEGORIES,'id');
				$this->db->where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->db->update_category(
					array($_field_id=>$data['id']),
					$data
				);
			}
			
			//If we get to this point it means it was insert not an update
			return $this->db->insert(TBL_CATEGORIES,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->create_category()]\n".$this->db->error());
			return FALSE;
		}
	}
		
	/** Get all categories matching the `$filter` provided */
	function get_categories(array $filter)
	{
		try
		{
			$this->_category_joins();

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->get_categories()]\n".$this->db->error());
			return NULL;
		}
	}

	/** Update categories matching `$filter` with the `$data` provided */
	function update_category(array $filter,array $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->where($filter);
			return $this->db->update(TBL_CATEGORIES,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->update_category()]\n".$this->db->error());
			return FALSE;
		}
	}
	
	/** Delete categories based on the $filter provided */
	function delete_category(array $filter)
	{
		try
		{	
			$this->db->where($filter);
			return $this->db->delete(TBL_CATEGORIES);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->delete_category()]\n".$this->db->error());
			return FALSE;
		}
	}

	#endregion

	// SUBCATEGORIES
	#region Subcategories
	/** Create a single subcategory */
	function create_subcategory(array $data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_SUBCATEGORIES,'id');
				$this->db->where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->db->update_category(
					array($_field_id=>$data['id']),
					$data
				);
			}
			
			//If we get to this point it means it was insert not an update
			return $this->db->insert(TBL_SUBCATEGORIES,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->create_category()]\n".$this->db->error());
			return FALSE;
		}
	}
		
	/** Get all subcategories matching the `$filter` provided */
	function get_subcategories(array $filter)
	{
		try
		{
			$this->_subcategory_joins();

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->get_subcategories()]\n".$this->db->error());
			return NULL;
		}
	}

	/** Update subcategories matching `$filter` with the `$data` provided */
	function update_subcategory(array $filter,array $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->where($filter);
			return $this->db->update(TBL_SUBCATEGORIES,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->update_subcategory()]\n".$this->db->error());
			return FALSE;
		}
	}
	
	/** Delete subcategories based on the $filter provided */
	function delete_subcategory(array $filter)
	{
		try
		{	
			$this->db->where($filter);
			return $this->db->delete(TBL_SUBCATEGORIES);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$product_model->delete_subcategory()]\n".$this->db->error());
			return FALSE;
		}
	}

	#endregion
}

/* End of file Product_model.php */
