<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface Payment_interface
{
	// PAYMENT
	#region Payments
	/** Create payment with the `$data` provided*/
	function create_payment(array $data);

	/** Get single payment matching the `$filter` provided*/
	// // function get_single_payment(array $filter): array;

	/** Get payment matching the `$filter` provided*/
	function get_payments(array $filter);

	/** Update payment matching the `$filter` provided*/
	function update_payment(array $filter,array $data);

	/** Delete payment with the `$filter` provided*/
	function delete_payment(array $filter): boolean;

	#endregion

	// PAYMENT INFO
	#region Paymente info
	/** Create payment with the `$filter` provided*/
	function create_payment_info(array $data, $return_id);

	/** Get single payment matching the `$filter` provided*/
	// // function get_single_payment_info(array $filter): array;

	/** Get payment matching the `$filter` provided*/
	function get_payment_info(array $filter);

	/** Update payment matching the `$filter` provided*/
	function update_payment_info($data, array $filter);

	/** Delete payment with the `$filter` provided*/
	function delete_payment_info(array $filter, array $filter2);

	#endregion
}

// Payments, payment info
class Payment_model extends MY_Model implements Payment_interface
{
	// Constructor - call parent constructor (loads database)
	public function __construct()
	{
		parent::__construct();
	}

	// PAYMENT
	#region Payments
	/** Create payment with the `$data` provided*/
	function create_payment(array $data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_PAYMENTS,'id');
				$this->db->or_where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->db->update_payment(
					array($_field_id=>$data['id']),
					$data
				);
			}
			
			//If we get to this point it means the record was not found ~ create a new one
			$data['id'] = $this->generate_string_id(TBL_PAYMENTS);
			return $this->db->insert(TBL_PAYMENTS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$payment_model->create_payment()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Get payment matching the `$filter` provided*/
	function get_payments(array $filter)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_PAYMENTS)
			);
			$this->db->from(TBL_PAYMENTS);

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->or_where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$payment_model->get_payments()]\n".$this->db->error());
			return NULL;
		}
	}

	/** Update payment matching the `$filter` provided*/
	function update_payment(array $filter,array $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->or_where($filter);
			return $this->db->update(TBL_PAYMENTS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$payment_model->update_payment()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Delete payment with the `$filter` provided*/
	function delete_payment(array $filter): boolean
	{
		try
		{	
			$this->db->or_where($filter);
			return $this->db->delete(TBL_PAYMENTS);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$payment_model->delete_payment()]\n".$this->db->error());
			return FALSE;
		}
	}

	#endregion

	// PAYMENT INFO
	#region Paymente info
	/** Create payment with the `$filter` provided*/
	function create_payment_info(array $data, $return_id=FALSE)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_PAYMENT_INFO,'id');
				$this->db->or_where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->update_payment_info($data, array(
					'id' => $data['id'],
				));
			}
			//If we get to this point it means the record was not found ~ create a new one

			$data['id'] = $this->generate_string_id(TBL_PAYMENT_INFO);
			$response = $this->db->insert(TBL_PAYMENT_INFO,$data);

			if($return_id && $response)
			{
				return $data['id'];
			}
			return $response;
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$payment_model->create_payment_info()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Get payment matching the `$filter` provided*/
	function get_payment_info(array $filter)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_PAYMENT_INFO)
			);
			$this->db->from(TBL_PAYMENT_INFO);

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->where($filter);
			}
			$this->db->group_by(array("card_number", "card_expiry", "card_security_code", "mpesa_phone", "paypal_id"));
			$this->db->order_by("card_security_code", "asc");
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$payment_model->get_payment_info()]\n".$this->db->error());
			return NULL;
		}
	}

	/** Update payment matching the `$filter` provided*/
	function update_payment_info($data, array $filter)
	{
		try
		{	
			unset($data['id']);
			$this->db->or_where($filter);
			return $this->db->update(TBL_PAYMENT_INFO,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$payment_model->update_payment_info()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Delete payment with the `$filter` provided*/
	function delete_payment_info(array $filter, array $filter2 = [])
	{
		try
		{	
			// var_dump($filter);
			if(isset($filter2) && !empty($filter2))
			{
				unset($filter['id']);
				$filter_merge = array_merge($filter, $filter2);
				$this->db->where($filter_merge);
			} else {
				$this->db->where($filter);
			}

			return $this->db->delete(TBL_PAYMENT_INFO);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$payment_model->delete_payment_info()]\n".$this->db->error());
			return FALSE;
		}		
	}

	#endregion

}

/* End of file Payment_model.php */
