<?php defined('BASEPATH') or exit('No direct script access allowed');

class Logistics_model extends MY_Model
{
    // Constructor
    public function __construct()
    {
        parent::__construct();
    }

	#region Warehouse
    /** Add product to warehouse ~ only allows for single item entry **/
    public function add_warehouse_item($data)
    {
		if(empty($data))
		{
			return FALSE;
		}

		try
		{
			$data['id'] = 'WH-'.$this->generate_int_id(TBL_WAREHOUSE);
			return $this->db->insert(TBL_WAREHOUSE,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$logistics_model->get_shipments()]\n".$this->db->error());
			return NULL;
		}

    }

    /** Update warehouse items**/
    public function update_warehouse_items($filters,$data)
    {
		try
		{
			$this->db->where($filters);
			return $this->db->update(TBL_WAREHOUSE,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$logistics_model->get_shipments()]\n".$this->db->error());
			return NULL;
		}

    }

    /** Get warehouse items from the database **/
    public function get_warehouse_items($filters=NULL,$search_filters=NULL,$limit=NULL,$select_active_only=FALSE)
    {
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_WAREHOUSE,'*,')
				
				.$this->get_table_field(TBL_PRODUCTS,'seller_id,')
				.$this->get_table_field(TBL_PRODUCTS,'product_name,')
				.$this->get_table_field(TBL_PRODUCTS,'shipping_weight,')
				.$this->get_table_field(TBL_PRODUCTS,'is_active AS product_is_active,')
				
				.$this->get_table_field(TBL_USERS,'first_name AS merchant_first_name,')
				.$this->get_table_field(TBL_USERS,'last_name AS merchant_last_name,')
				.$this->get_table_field(TBL_USERS,'company AS merchant_company')
			);
			$this->db->from(TBL_WAREHOUSE);
			
			// Only select products that are active ~ if we are only selecting active records
			if($select_active_only)
			{
				$this->db->where(
					$this->get_table_field(TBL_PRODUCTS,'is_active=').TRUE
				); #TODO: Debug this ~ may need to be changed to `product_is_active` from `is_active`
			}

			// Product joins
			$this->db->join(TBL_PRODUCTS,
				$this->get_table_field(TBL_WAREHOUSE,'product_id=').$this->get_table_field(TBL_PRODUCTS,'id')
			);
			
			// User joins
			$this->db->join(TBL_USERS,
				$this->get_table_field(TBL_PRODUCTS,'seller_id=').$this->get_table_field(TBL_USERS,'id')
			);

			if(!empty($filters))
			{
				$this->db->where($filters);
			}

			$this->use_search_filters($search_filters);

			// If a limit has been provided ~ limit to the number of records requested
			if(!empty($limit))
			{
				$this->db->limit($limit);
			}

			$this->db->order_by(
				$this->get_table_field(TBL_WAREHOUSE,'date_added'),
				'DESC'
			);

			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$logistics_model->get_warehouse_items()]\n".$this->db->error());
			return NULL;
		}
    }

    /** Delete warehouse items from the database **/
    public function delete_warehouse_item($filters)
    {
		try
		{
			$this->db->where($filters);
			return $this->db->delete(TBL_WAREHOUSE);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$logistics_model->delete_shipments()]\n".$this->db->error());
			return NULL;
		}
	}
	#endregion Warehouse

	#region Shipping

	/** Create shipments with the `$data` provided  
	 * @param array $shipments An array of associative arrays containing shipment information
	*/
	function create_shipments(array $shipments)
	{ 
		try
		{			
			for($i=0; $i<count($shipments); $i++)
			{
				// Generate id for the shipment item
				$shipments[$i]['id'] = $this->generate_string_id(TBL_SHIPMENTS);
			}

			$shipment_insert_status = $this->db->insert_batch(TBL_SHIPMENTS,$shipments);
			return $shipment_insert_status;
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->create_shipment()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Get shipments matching the `$filter` provided  */
	function get_shipments($filter=NULL,$search_filters=NULL,$limit=NULL)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_SHIPMENTS,'*,')
				
				.$this->get_table_field(TBL_ORDERS,'product_id,')
				.$this->get_table_field(TBL_ORDERS,'quantity,')
				.$this->get_table_field(TBL_ORDERS,'sales_amount,')
				.$this->get_table_field(TBL_ORDERS,'status AS order_status,')
				.$this->get_table_field(TBL_ORDERS,'shipping_method,')
				.$this->get_table_field(TBL_ORDERS,'tracking_id,')
				.$this->get_table_field(TBL_ORDERS,'tracking_link,')
				.$this->get_table_field(TBL_ORDERS,'date_ordered,')
				
				.$this->get_table_field(TBL_USERS,'first_name AS merchant_first_name,')
				.$this->get_table_field(TBL_USERS,'last_name AS merchant_last_name,')
				.$this->get_table_field(TBL_USERS,'company AS merchant_company,')

				.$this->get_table_field(TBL_PRODUCTS,'product_name,')
				.$this->get_table_field(TBL_PRODUCTS,'unit_price,')
				.$this->get_table_field(TBL_PRODUCTS,'seller_id,')
				
				.$this->get_table_field(TBL_WAREHOUSE,'logistics_user_id,')
				.$this->get_table_field(TBL_WAREHOUSE,'status AS item_status,')
				.$this->get_table_field(TBL_WAREHOUSE,'is_active')
			);
			$this->db->from(TBL_SHIPMENTS);
			
			// Orders joins
			$this->db->join(TBL_ORDERS,
				$this->get_table_field(TBL_ORDERS,'transaction_id =').$this->get_table_field(TBL_SHIPMENTS,'order_transaction_id ')
			);
			
			// Products joins
			$this->db->join(TBL_PRODUCTS,
				$this->get_table_field(TBL_PRODUCTS,'id =').$this->get_table_field(TBL_ORDERS,'product_id')
			);
			
			// Warehouse joins
			$this->db->join(TBL_WAREHOUSE,
				$this->get_table_field(TBL_WAREHOUSE,'id =').$this->get_table_field(TBL_SHIPMENTS,'warehouse_id')
			);
			
			// User joins
			$this->db->join(TBL_USERS,
				$this->get_table_field(TBL_PRODUCTS,'seller_id=').$this->get_table_field(TBL_USERS,'id')
			);


			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter))
			{
				$this->db->where($filter);
			}
			
			$this->use_search_filters($search_filters);

			// If a limit has been provided ~ limit to the number of records requested
			if(!empty($limit))
			{
				$this->db->limit($limit);
			}

			$this->db->order_by(
				$this->get_table_field(TBL_SHIPMENTS,'date_picked'),
				'DESC'
			);
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->get_shipments()]\n".$this->db->error());
			return NULL;
		}
	}

	/** Update shipment matching the `$filter` with the `$data` provided  */
	function update_shipment($filter,array $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->where($filter);
			return $this->db->update(TBL_SHIPMENTS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->update_shipment()]\n".$this->db->error());
			return FALSE;
		}	
	}
	
	/** Delete shipment matching the `$filter` provided  */
	function delete_shipments(array $filter)
	{
		try
		{	
			$this->db->or_where($filter);
			return $this->db->delete(TBL_SHIPMENTS);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$order_model->delete_shipment()]\n".$this->db->error());
			return FALSE;
		}
	}
	#endregion Shipping
}
/* End of file Logistics_model.php */
