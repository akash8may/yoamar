<?php defined('BASEPATH') or exit('No direct script access allowed');

class Search_model extends MY_Model
{
	// Constructor
	public function __construct()
	{
		parent::__construct();
	}
	/** Create search_query **/
	public function create_search_query($data)
	{
		return $this->db->insert(TBL_SEARCH_QUERIES,$data);
	}
	
	/** Update search_query items**/
	public function update_search_query($filters,$data)
	{
		$this->db->where($filters);
		return $this->db->update(TBL_SEARCH_QUERIES,$data);
	}
	
	/** Get search_query items from the database **/
	public function get_search_queries($filters,$search_filters=NULL,$limit=NULL)
	{
		$this->db->select(
			$this->get_table_field(TBL_SEARCH_QUERIES,'*,')
			
			.$this->get_table_field(TBL_COUNTRIES,'nicename AS country,')
		);
		$this->db->from(TBL_SEARCH_QUERIES);

		$this->db->join(TBL_COUNTRIES,
			$this->get_table_field(TBL_COUNTRIES,'id=').$this->get_table_field(TBL_SEARCH_QUERIES,'country_id')
		);
		if(!empty($filters))
		{	$this->db->where($filters);	}

		$this->use_search_filters($search_filters);

		// If a limit has been provided, limit the selection
		if(!empty($limit))
		{	$this->db->limit($limit);	}

		$this->db->order_by($this->get_table_field(TBL_SEARCH_QUERIES,'date_searched'),'DESC');

		return $this->db->get();
	}
	
	/** Delete search_query items from the database **/
	public function delete_search_query($filters)
	{
		$this->db->where($filters);
		return $this->db->delete(TBL_SEARCH_QUERIES);
	}
}
/* End of file Search_model.php */
