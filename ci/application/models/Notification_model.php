<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface Notification_interface
{
	// NOTIFICATIONS
    #region Notifications
    /** Create a single notification */
    function create_notification(array $data);

	/** Get a single Notification based on the `$filter` provided
     * If multiple notifications are available, returns the first instance found.
     */
	// function get_single_notification(array $filter): array;
	
	/** Get a list of notifications matching the `$filter` provided */
    function get_notifications(array $filter);
    
    /** Update notifications matching `$filter` with the `$data` provided */
    function update_notification(array $filter, array $data);
    
    /** Delete notifications based on the $filter provided */
    function delete_notification(array $filter);
    
	#endregion
}

// Handles notifications
class Notification_model extends MY_Model implements Notification_interface
{
	// Constructor - call parent constructor (loads database)
	public function __construct()
	{
		parent::__construct();
	}

	// NOTIFICATIONS
    #region Notifications
    /** Create a single notification */
	function create_notification(array $data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_NOTIFICATIONS,'id');
				$this->db->or_where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->db->update_notification(
					array($_field_id=>$data['id']),
					$data
				);
			}
			
			$data['id'] = $this->generate_string_id(TBL_NOTIFICATIONS);
			//If we get to this point it means the record was not found ~ create a new one
			return $this->db->insert(TBL_NOTIFICATIONS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$notification_model->create_notification()]\n".$this->db->error());
			return FALSE;
		}
	}

	
	/** Get a list of notifications matching the `$filter` provided */
	function get_notifications(array $filter)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_NOTIFICATIONS)
			);
			$this->db->from(TBL_NOTIFICATIONS);

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->where($filter);
			}
			$this->db->order_by('date_sent', 'ASC');
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$notification_model->get_notifications()]\n".$this->db->error());
			return NULL;
		}
	}
    
    /** Update notifications matching `$filter` with the `$data` provided */
	function update_notification(array $filter, array $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->or_where($filter);
			return $this->db->update(TBL_NOTIFICATIONS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$notification_model->update_notification()]\n".$this->db->error());
			return FALSE;
		}
	}
    
    /** Delete notifications based on the $filter provided */
	function delete_notification(array $filter)
	{
		try
		{	
			$this->db->or_where($filter);
			return $this->db->delete(TBL_NOTIFICATIONS);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$notification_model->delete_notification()]\n".$this->db->error());
			return FALSE;
		}
	}
    
	#endregion
}

/* End of file Notification_model.php */
