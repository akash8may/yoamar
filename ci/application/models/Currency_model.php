<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface Currency_interface
{
	// CURRENCIES
	#region Currencies
	/** Get a list of currencies
     *  Default is all supported currencies
     * `$filter` is optional.
	*/
	function get_currencies(array $filter=NULL): array;
	
	#endregion
}

// Handles currencies
class Currency_model extends MY_Model implements Currency_interface
{
	// Constructor - call parent constructor (loads database)
	public function __construct()
	{
		parent::__construct();
	}

	#region Currencies
	
	/** Get a list of currencies
     *  Default is all supported currencies
     * `$filter` is optional.
	*/
	function get_currencies(array $filter=NULL): array
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_CURRENCIES)
			);
			$this->db->from(TBL_CURRENCIES);

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->or_where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$country_model->get_countries()]\n".$this->db->error());
			return NULL;
		}
	}
	
	#endregion
}

/* End of file Currency_model.php */
