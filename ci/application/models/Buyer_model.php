<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface Buyer_interface
{
	// BUYER 
	#region Buyer
	/** Create a buyer account */
	function create_buyer(array $data);

	// /** Get a buyer account matching `$filter` */
	// // function get_single_buyer(array $filter)

	/** Get buyer accounts matching `$filter` provided */
	function get_buyers(array $filter);

	/** Update buyer accounts matching `$filter` with the `$data` provided */
	function update_buyer(array $filter,array $data);

	/**  */
	function delete_buyer(array $filter);

	#endregion

	// CART
	#region Cart
	/** Add a single cart item */
	function add_to_cart(array $data);

	/** Update cart items matching `$filter` with `$data` provided */
	function update_cart(array $filter,array $data);
	
	/** Get cart items matching the `$filter` provided */
	function get_cart_items(array $filter, bool $only_active=TRUE);
	
	/** Remove cart items matching the `$filter` provided - retains records in database */
	function remove_cart_item(array $filter);
	
	/** Delete cart items matching the `$filter` provided */
	function delete_cart_item(array $filter);
	#endregion

	// WISHLIST
	#region Wishlist
	/** Add wishlist item */
	function add_to_wishlist(array $data);

	/** Get all wishlist items matching the $filter provided */
	function get_wishlist_items(array $filter, bool $only_active=TRUE);

	/** Update wishlist items matching the $filter provided with $data */
	function update_wishlist_item(array $filter,array $data);

	/** Delete wishlist item(s) matching $filter provided */
	function delete_wishlist_items(array $filter);

	#endregion
}

// Buyer specific functionality - carts, wishlist, buyer accounts
class Buyer_model extends MY_Model implements Buyer_interface
{
	// Constructor - call parent constructor (loads database)
	public function __construct()
	{
		parent::__construct();
	}

	// BUYER
	#region Buyer
	/** Create a buyer account */
	function create_buyer(array $data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_BUYER,'id');
				$this->db->or_where($_field_id,$data['buyer_id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $user_update_status && $this->db->update_buyer(
					array($_field_id=>$data['id']),
					$data
				);
			}
			
			//If we get to this point it means the record was not found ~ create a new one
			return $this->db->insert(TBL_BUYER,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->create_buyer()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Update buyer accounts matching `$filter` with the `$data` provided */
	function update_buyer(array $filter,array $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->or_where($filter);
			$update_buyer_status = $this->ionauth->update($data['user_id'],$data['user_info']);
			return $update_buyer_status && $this->db->update(TBL_NOTIFICATIONS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$notification_model->update_notification()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Get buyer accounts matching `$filter` provided */
	function get_buyers(array $filter)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_BUYER)
			);
			$this->db->from(TBL_BUYER);

			//*TODO: Get data from the user table 
			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->or_where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->get_buyers()]\n".$this->db->error());
			return NULL;
		}
	}


	/**  */
	function delete_buyer(array $filter)
	{
		try
		{	
			$this->db->or_where($filter);
			$delete_user_status = $this->ionauth->delete_user(@$filter['user_id']);
			return $delete_user_status && $this->db->delete(TBL_BUYER);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->delete_cart_item()]\n".$this->db->error());
			return FALSE;
		}
	}

	#endregion

	// CART
	#region Cart
	/** Add a single cart item */
	function add_to_cart(array $data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_CART,'id');
				$this->db->or_where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->update_cart(
					array($_field_id=>$data['id']),
					$data
				);
			}
			
			//If we get to this point it means the record was not found ~ create a new one
			$data['id'] = $this->generate_string_id(TBL_CART);
			return $this->db->insert(TBL_CART,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->add_to_cart()]\n".$this->db->error());
			return FALSE;
		}
	}
	
	/** Get cart items matching the `$filter` provided */
	function get_cart_items(array $filter,bool $only_active=TRUE)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_CART)
			);
			$this->db->from(TBL_CART);
			
			// Only get the active cart items
			if($only_active === TRUE)
			{
				$_field_is_active = $this->get_table_field(TBL_CART,'is_active');
				$this->db->where($_field_is_active,TRUE);
			}

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->get_wishlist_items()]\n".$this->db->error());
			return NULL;
		}
	}

	/** Update cart items matching `$filter` with `$data` provided */
	function update_cart(array $filter,array $data)
	{
		try
		{	
			unset($data['product_id']);
			$this->db->or_where($filter);
			return $this->db->update(TBL_CART,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->update_cart()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Remove cart items matching the `$filter` provided - retains records in database */
	function remove_cart_item(array $filter)
	{
		try
		{	
			return $this->update_cart($filter,array(
				'is_active'=>FALSE
			));
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->remove_cart_item()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Delete cart items matching the `$filter` provided */
	function delete_cart_item(array $filter)
	{
		try
		{	
			$this->db->where($filter);
			return $this->db->delete(TBL_CART);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->delete_cart_item()]\n".$this->db->error());
			return FALSE;
		}
	}

	#endregion

	// WISHLIST
	#region Wishlist
	/** Add wishlist item */
	function add_to_wishlist(array $data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_WISHLIST,'id');
				$this->db->where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->db->update_wishlist_item(
					array($_field_id=>$data['id']),
					$data
				);
			}

			//If we get to this point it means the record was not found ~ create a new one
			$data['id'] = $this->generate_string_id(TBL_WISHLIST);
			return $this->db->insert(TBL_WISHLIST,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->add_to_wishlist()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Get all wishlist items matching the $filter provided */
	function get_wishlist_items(array $filter, bool $only_active=TRUE)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_WISHLIST)
			);
			$this->db->from(TBL_WISHLIST);

			// Only get the active cart items
			if($only_active === TRUE)
			{
				$_field_is_active = $this->get_table_field(TBL_WISHLIST,'is_active');
				$this->db->where($_field_is_active,TRUE);
			}

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->or_where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->get_wishlist_items()]\n".$this->db->error());
			return NULL;
		}
	}

	/** Update wishlist items matching the $filter provided with $data */
	function update_wishlist_item(array $filter,array $data)
	{
		try
		{	
			unset($data['product_id']);
			$this->db->or_where($filter);
			return $this->db->update(TBL_WISHLIST,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->update_wishlist_item()]\n".$this->db->error());
			return FALSE;
		}
	}

	/** Remove wishlist items matching the `$filter` provided - retains records in database */
	function remove_wishlist_item(array $filter)
	{
		try
		{	
			return $this->update_wishlist_item($filter,array(
				'is_active'=>FALSE
			));
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->remove_cart_item()]\n".$this->db->error());
			return FALSE;
		}
	}
	/** Delete wishlist item(s) matching $filter provided */
	function delete_wishlist_items(array $filter)
	{
		try
		{	
			$this->db->or_where($filter);
			return $this->db->delete(TBL_WISHLIST);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$buyer_model->delete_wishlist_items()]\n".$this->db->error());
			return FALSE;
		}
	}

	#endregion
}

/* End of file Buyer_model.php */
