<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface Country_interface
{
	// COUNTRIES
	#region Countries
	/** Get a list of countries
     *  @param $filter  Default is all supported countries
     * `$filter` is optional.
	*/
	function get_countries(array $filter=NULL);
	
	#endregion

	// STATES
	#region States
	
	/** Get a list of states
     *  @param $filter  Default is all supported states
     * `$filter` is optional.
	*/
	function get_states($filter=NULL);
	
	#endregion
}

// Handles countries
class Country_model extends MY_Model implements Country_interface
{
	// Constructor - call parent constructor (loads database)
	public function __construct()
	{
		parent::__construct();
	}

	#region Countries
	/** Get a list of countries
     *  @param $filter  Default is all supported countries
     * `$filter` is optional.
	*/
	function get_countries(array $filter=NULL)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_COUNTRIES)
			);
			$this->db->from(TBL_COUNTRIES);

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->or_where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$country_model->get_countries()]\n".$this->db->error());
			return NULL;
		}
	}
	#endregion

	#region States
	/** Get a list of states
     *  @param $filter  Default is all supported states
     * `$filter` is optional.
	*/
	function get_states($filter=NULL)
	{
		try
		{
			$this->db->select($this->get_table_field(TBL_STATES));
			$this->db->from(TBL_STATES);

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->or_where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$country_model->get_states()]\n".$this->db->error());
			return NULL;
		}
	}
	
	#endregion
}

/* End of file Country_model.php */
