<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends MY_Model {

	// Get users based on user group
	public function get_users(string $user_group,$is_active=NULL, $search_filters=NULL,$limit=NULL)
	{
		//* Not doing anything as far as I know [AJ] but intent is to select distinct columns
		$this->db->distinct();

		$this->db->select(
			$this->get_table_field(TBL_USER_GROUPS,'user_id,')
			.$this->get_table_field(TBL_USER_GROUPS,'group_id,')
			
			.$this->get_table_field(TBL_USERS,'*,')
			
			
			.$this->get_table_field(TBL_GROUPS,'name,')
		);

		$this->db->from(TBL_USER_GROUPS);

		// Group Joins
		$this->db->join(TBL_GROUPS,
			$this->get_table_field(TBL_USER_GROUPS,'group_id=')
			.$this->get_table_field(TBL_GROUPS,'id')
		);

		// User Joins
		$this->db->join(TBL_USERS,
			$this->get_table_field(TBL_USER_GROUPS,'user_id=')
			.$this->get_table_field(TBL_USERS,'id')
		);

		//* TODO: Fix filtering ~ currently returns all users matching search query disregarding filter
		// Filters
		$filters = array(
			$this->get_table_field(TBL_GROUPS,'name') => $user_group
		);

		if(isset($is_active))
		{	$filters['active'] = (bool)$is_active; }
		
		$this->db->where($filters);
		
		//* TODO: Fix search ~ currently returns all users matching search query disregarding filter
		$this->use_search_filters($search_filters);

		if(!empty($limit))
		{	$this->db->limit($limit);	}

		return $this->db->get();
	}

}

/* End of file Admin_model.php */
