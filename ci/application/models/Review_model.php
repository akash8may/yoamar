<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface Review_interface
{
	// REVIEWS
    #region Reviews
    /** Create a single review */
    function create_review($data);

	/** Get a single Review based on the `$filter` provided
     * If multiple reviews are available, returns the first instance found.
     */
	// function get_single_review($filter);
	
	/** Get a list of reviews matching the `$filter` provided */
    function get_reviews($filter);
    
    /** Update reviews matching `$filter` with the `$data` provided */
    function update_review($filter, $data);
    
    /** Delete reviews based on the $filter provided */
    function delete_review($filter);
    
	#endregion

	// REVIEWS
    #region Reviews
	/** Create a single review_replies */
	function create_review_replies($data);

	/** Get a single review_replies based on the `$filter` provided
	 * If multiple review_replies are available, returns the first instance found.
	 */
	// function get_single_review_replies($filter);
	
	/** Get a list of review_replies matching the `$filter` provided */
	function get_review_replies($filter);
	
	/** Update review_replies matching `$filter` with the `$data` provided */
	function update_review_replies($filter, $data);
	
	/** Delete review_replies based on the $filter provided */
	function delete_review_replies($filter);

	#endregion
}

// Handles reviews
class Review_model extends MY_Model implements Review_interface
{
	// Constructor - call parent constructor (loads database)
	public function __construct()
	{
		parent::__construct();
	}

	// REVIEWS
    #region Reviews
    /** Create a single review */
	function create_review($data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_REVIEWS,'id');
				$this->db->where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->db->update_notification(
					array($_field_id=>$data['id']),
					$data
				);
			}
			//If we get to this point it means the record was not found ~ create a new one
			$data['id'] = $this->generate_string_id(TBL_REVIEWS);
			return $this->db->insert(TBL_REVIEWS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$review_model->create_review()]\n".$this->db->error());
			return FALSE;
		}
	}
	
	/** Get a list of reviews matching the `$filter` provided */
	function get_reviews($filter)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_REVIEWS,'*,')

				// Get buyer first name and last name too
				.$this->get_table_field(TBL_USERS,'first_name AS buyer_first_name,')
				.$this->get_table_field(TBL_USERS,'last_name AS buyer_last_name')
			);
			$this->db->from(TBL_REVIEWS);

			// Buyer info joins ~ we'll do another query if we need to get seller information
			$this->db->join(TBL_USERS,
				$this->get_table_field(TBL_REVIEWS,'buyer_id').'='.$this->get_table_field(TBL_USERS,'id')
			);

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$review_model->get_reviews()]\n".$this->db->error());
			return NULL;
		}
	}
    
    /** Update reviews matching `$filter` with the `$data` provided */
	function update_review($filter, $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->or_where($filter);
			return $this->db->update(TBL_REVIEWS,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$review_model->update_review()]\n".$this->db->error());
			return FALSE;
		}
	}
    
    /** Delete reviews based on the $filter provided */
	function delete_review($filter)
	{
		try
		{	
			//* Delete review replies belonging to the current review
			$review_reply_delete_status = TRUE;
			// If the id of the review has been provided as part of the filter ~ delete review replies
			if(!empty($filter['id']))
			{
				$review_reply_delete_status = $this->delete_review_replies(array(
					'review_id'=>$filter['id'] # Delete all review replies matching the current review_id
				));
			}

			$this->db->where($filter);
			return $review_reply_delete_status && $this->db->delete(TBL_REVIEWS);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$review_model->delete_review()]\n".$this->db->error());
			return FALSE;
		}
	}
    
	#endregion


	// REVIEWS
    #region Reviews
	/** Create a single review_replies */
	function create_review_replies($data)
	{
		try
		{
			// If an id has been provided as part of the data ~ means the record might already exist
			if(isset($data['id'])) # Handle gracefully
			{
				$_field_id = $this->get_table_field(TBL_REVIEW_REPLIES,'id');
				$this->db->where($_field_id,$data['id']);

				//Unset the id as it is not going to be part of the data being used to update the table
				return $this->db->update(
					array($_field_id=>$data['id']),
					$data
				);
			}
			$data['id'] = $this->generate_string_id(TBL_REVIEW_REPLIES);
			//If we get to this point it means the record was not found ~ create a new one
			return $this->db->insert(TBL_REVIEW_REPLIES,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$review_model->create_review_reply()]\n".$this->db->error());
			return FALSE;
		}		
	}

	/** Get a single review_replies based on the `$filter` provided
	 * If multiple review_replies are available, returns the first instance found.
	 */
	// function get_single_review_replies($filter);
	
	/** Get a list of review_replies matching the `$filter` provided */
	function get_review_replies($filter)
	{
		try
		{
			$this->db->select(
				$this->get_table_field(TBL_REVIEW_REPLIES)
			);
			$this->db->from(TBL_REVIEW_REPLIES);

			//If there is a filter set, add a where clause to only select records matching the filter
			if(!empty($filter) && is_array($filter))
			{
				$this->db->or_where($filter);
			}
			
			return $this->db->get();
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$review_model->get_review_replies()]\n".$this->db->error());
			return NULL;
		}		
	}
	
	/** Update review_replies matching `$filter` with the `$data` provided */
	function update_review_replies($filter, $data)
	{
		try
		{	
			unset($data['id']);
			$this->db->where($filter);
			return $this->db->update(TBL_REVIEW_REPLIES,$data);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$review_model->update_review_replies()]\n".$this->db->error());
			return FALSE;
		}		
	}
	
	/** Delete review_replies based on the $filter provided */
	function delete_review_replies($filter)
	{
		try
		{	
			$this->db->where($filter);
			return $this->db->delete(TBL_REVIEW_REPLIES);
		}
		catch(Exception $e)
		{
			log_message('error',"[Failed @ \$review_model->delete_review_replies()]\n".$this->db->error());
			return FALSE;
		}		
	}
	
	#endregion
}

/* End of file Review_model.php */
