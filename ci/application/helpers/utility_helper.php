<?php defined('BASEPATH') or exit('No direct script access allowed');

/** Returns a page title with the name of the site appended to it  */
function get_page_title($page_title='')
{
	$page_title = ucwords($page_title);
	return empty($page_title)? SITE_NAME : $page_title.' | '.SITE_NAME;
}


/** Returns `$content` if `$var` is not empty. Otherwise returns `$return_value`. 
 * @param $var Variable that should not be empty for the content to be returned
 * @param string $content Content to be returned if not empty
 * @param string $return_value Value to be returned if $var is empty. Empty string by default
 * @return string $content - Content provided in the parameter if $var is not empty, otherwise returns an empty string or $return_value if specified
 */
//TODO: Find a better/shorter name for this function
function get_if_not_empty($var,$content,$return_value='')
{
	return !empty($var) ? $content : $return_value;
}

/** Returns `$content` if `$var` is empty. Otherwise returns `$return_value`. 
 * @param $var Variable that should not be empty for the content to be returned
 * @param string $content Content to be returned if not empty
 * @param string $return_value Value to be returned if $var is empty. Empty string by default
 * @return string $content - Content provided in the parameter if $var is not empty, otherwise returns an empty string or $return_value if specified
 */
//TODO: Find a better/shorter name for this function
function get_if_empty($var,$content,$return_value='')
{
	return empty($var) ? $content : $return_value;
}

/** Returns `$value` if `$value` is not empty. Otherwise returns `$default_value`. 
 * @param string $value Value to be set. If this is empty, $default_value will be used 
 * @param string $default_value Value returned if $value is empty. Empty string by default
 * @return string $content - Content provided in the parameter if $value is not empty, otherwise returns $default_value
 */
function get_value_or_default($value,$default_value='')
{
	return get_if_not_empty($value,$value,$default_value);
}


/** Returns a value within the range of $min - $max if either of the values is exceeded
 * @param int $value Value to be clamped
 * @param int $min Min clamp value
 * @param int $max Max clamp value
 * @return int $clamped $value if it does not exceed the range. $min if $value < $min and $max if $value>$max
 */
function clamp($value, $min, $max)
{
	$clamped = &$value;

	if($clamped<$min)
	{
		$clamped = $min;
	}
	else if ($clamped>$max )
	{
		$clamped = $max;
	}
	return $clamped;
}

//Parse a message that contains placeholder data eg. [attribute] and return the parsed message
function parse_msg(string $message,array $replacements)
{
    //If the replacements are not an array ~ return the message
    if(!is_array($replacements))
    {   return $message;    }

    //Otherwise, loop through the replacements and replace them in the message provided
    foreach ($replacements as $key => $value) 
    {
        $value = (isset($value) && !empty($value)) ? $value : '[Not set]';
        // replace the $key with the value
        $replace_str = '['.(string)$key.']';;# The string to be replaced
        $message = str_replace($replace_str,$value,$message);
    }

    return $message;
}

/** Denests one level the nested arrays within the given array. */
function flatten_array(array $array)
{ //TODO: Support multi-level denesting
	$flattened = [];
	for($i=0; $i<count($array); $i++)
	{
		foreach($array[$i] as $key=>$value)
		{//* Does not support duplicate keys
			$flattened[$key] = $value;
		}
	}
	
	return $flattened;
}

// Returns active if $value is 'truthy'
function get_active_nav($value)
{
	return ($value === TRUE) ? 'active' : '';
}

// Get whether an item should be selected or not
function get_selected($value,$comparison_value='')
{
	return ($value == $comparison_value) ? 'selected': ''; 
}

// Get whether an item should be checked or not
function get_checked($value)
{
	return ($value == TRUE) ? 'checked': ''; 
}

/* Returns an array of filters given expected keys & values
	* @param array $filters An array of filters containing keys(url_param names) and values (database names)
*/
function get_search_filters($filters)
{
	$search_filter_array = [];

	$ci =&get_instance();
	foreach ($filters as $filter) 
	{
		$value = $ci->input->get($filter['url_param']);
		$search_filter = array(
			$filter['db_column']=>$value
		);

		if(!empty($value))
		{
			$search_filter_array[] = $search_filter;
		}
	}

	return $search_filter_array;
}

/**
 * @param String date to be formatted
 * @param String format default is 'dS F Y H:i'
 */
function humanize_date($date, $format = 'dS F Y H:i')
{
	$m = new \Moment\Moment($date, 'UTC'); // default is "now" UTC
	return $m->format($format);
}