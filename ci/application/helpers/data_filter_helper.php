<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Filters product data
function _filter_product_data($data)
{
	$filtered = array(
		'seller_id'=>@$data['seller_id'],
		'product_name'=>@$data['product_name'],
		'brand_name'=>@$data['brand_name'],
		'description'=>@$data['description'],
		'unit_price'=>@$data['unit_price'],
		'yoamar_commission'=>@$data['yoamar_commission'],
		'seller_dues'=>@$data['seller_dues'],
		'quantity'=>@$data['quantity'],
		'sale_price'=>@$data['sale_price'],
		'sale_price_effective_date'=>@$data['sale_price_effective_date'],
		'total_price'=>@$data['total_price'],
		'is_available'=>@$data['is_available'],
		'availability_date'=>@$data['availability_date'],
		'expiration_date'=>@$data['expiration_date'],
		'product_group'=>@$data['product_group'],
		'product_segment'=>@$data['product_segment'],
		'product_family'=>@$data['product_family'],
		'product_class'=>@$data['product_class'],
		'product_type'=>@$data['product_type'],
		'product_sub_type'=>@$data['product_sub_type'],
		'product_state'=>@$data['product_state'],
		'age_group'=>@$data['age_group'],
		'adult'=>@$data['adult'],
		'color'=>@$data['color'],
		'gender'=>@$data['gender'],
		'material'=>@$data['material'],
		'pattern'=>@$data['pattern'],
		'product_size'=>@$data['product_size'],
		'size_type'=>@$data['size_type'],
		'size_system'=>@$data['size_system'],
		'shipping_weight'=>@$data['shipping_weight'],
		'shipping_length'=>@$data['shipping_length'],
		'shipping_width'=>@$data['shipping_width'],
		'shipping_height'=>@$data['shipping_height'],
		'is_bundle'=>@$data['is_bundle'],
		'energy_efficiency_class'=>@$data['energy_efficiency_class'],
		'energy_efficiency_class_min'=>@$data['energy_efficiency_class_min'],
		'energy_efficiency_class_max'=>@$data['energy_efficiency_class_max'],
		'min_handling_time'=>@$data['min_handling_time'],
		'max_handling_time'=>@$data['max_handling_time'],
		'tax'=>@$data['tax'],
		'tax_category'=>@$data['tax_category'],
		'model_number'=>@$data['model_number'],
		'style'=>@$data['style'],
		'year_made'=>@$data['year_made'],
		'season'=>@$data['season'],
		'origin_country'=>@$data['origin_country'],
		'origin_state'=>@$data['origin_state'],
		'origin_city'=>@$data['origin_city'],
		'views'=>@$data['views'],
		'likes'=>@$data['likes'],
		'availability_country'=>@$data['availability_country'],
		'availability_state'=>@$data['availability_state'],
		'availability_city'=>@$data['availability_city'],
		'min_quantity'=>@$data['min_quantity'],
		'max_quantity'=>@$data['max_quantity'],
		'available_quanitity'=>@$data['available_quanitity'],
		// 'is_active'=>@$data['is_active'],
		// 'is_featured'=>@$data['is_featured']
	);
	return $filtered;
}

// Filter order data
function _filter_order_data($data)
{
	$filtered = array(
		'transaction_id'=>@$data['transaction_id'],
		'order_id'=>@$data['order_id'],
		'product_id'=>@$data['product_id'],
		'quantity'=>@$data['quantity'],
		'sales_amount'=>@$data['sales_amount'],
		'sales_tax1'=>@$data['sales_tax1'],
		'sales_tax2'=>@$data['sales_tax2'],
		'sales_tax3'=>@$data['sales_tax3'],
		'order_total'=>@$data['order_total'],
		'paid'=>@$data['paid'],
		'seller_id'=>@$data['seller_id'],
		'coupon_name'=>@$data['coupon_name'],
		'buyer_id'=>@$data['buyer_id'],
		'status'=>@$data['status'],
		'shipping_method'=>@$data['shipping_method'],
		'shipping_charges'=>@$data['shipping_charges'],
		'shipping_date'=>@$data['shipping_date'],
		'import_fees_deposit'=>@$data['import_fees_deposit'],
	);
	if(!isset($filtered['status']))
	{
		unset($filtered['status']);
	}
	if(!isset($filtered['shipping_method']))
	{
		unset($filtered['shipping_method']);
	}
	return $filtered;
}
/* End of file data_filter_helper.php */
