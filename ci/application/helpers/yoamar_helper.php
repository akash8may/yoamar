<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

// Returns the commission based on the provided unit price that will go to yoamar
function get_commission($unit_price)
{
	$ci =& get_instance();
	
	$ci->config->load('ecommerce', TRUE);
	// Convert the commission rate to a decimal representation of the percentage value
	$_yoamar_commission = ($ci->config->item('commission', 'ecommerce'))/100;
	
	$commission = ($_yoamar_commission * $unit_price);
	return round($commission,2);
}

// Returns the seller dues based on the provided unit price
function get_seller_dues($unit_price)
{
	$ci =& get_instance();
	
	$ci->config->load('ecommerce', TRUE);
	$_yoamar_commission = ($ci->config->item('commission', 'ecommerce'));
	$_seller_percentage = clamp((100-$_yoamar_commission),0,100);
	
	// Convert the commission rate to a decimal representation of the percentage value
	$_seller_percentage = $_seller_percentage/100;
	
	$seller_dues = ($_seller_percentage * $unit_price);
	return round($seller_dues,2);
}

// Get the commission rate for yoamar
function get_commission_rate()
{
	$ci =& get_instance();
	
	$ci->config->load('ecommerce', TRUE);
	return ($ci->config->item('commission', 'ecommerce'));
}
/* End of file yoamar_helper.php */

