<?php // Helps with getting geolocation data
defined('BASEPATH') OR exit('No direct script access allowed');

/** Get geodata of the current site visitor  */
function get_visitor_geodata($get_country_id=TRUE)
{
	// Get the geodata based on ip address
	$api_url = 'http://ip-api.com/php/';
	
	$geo = file_get_contents($api_url);
	$geo = unserialize($geo);

	// Getting country id, only using this if we have a countries table in db
	if(!empty($geo) && $get_country_id == TRUE)
	{
		$country_name = trim(strtoupper($geo['country']));

		$ci = &get_instance();
		// Using model because we do not have a function for getting a single country
		$country = $ci->utility->get_single_country(array(
			TBL_COUNTRIES.'.name'=> $country_name
		));

		$geo['country_id'] = $country->id;
	}
	return $geo;
}

/** Returns a preformatted string representing the location information of the current visitor */
function get_visitor_location_string()
{
	$geo = get_visitor_geodata() ?? NULL;
	$city = $geo['city'] ?? 'Unknown city';
	$country = $geo['country'] ?? 'Unknown country';


	return $city.', '.$country;
}

/* End of file geo_helper.php */
