<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Buyer_lib
{
	protected $ci;

	// Buyer related
	protected $current_buyer;
	protected $current_buyer_id;
	protected $buyer_logged_in;

	public function __construct()
	{
		$this->ci =& get_instance();

		// Load the buyer model since we'll be using this
		$this->ci->load->model('buyer_model');
		
		//load libraries
		// Gives us access to user related functions
		$this->ci->load->library(array('payment', 'order'));
		$this->buyer_logged_in = (bool)$this->ci->yo_user->buyer_logged_in();
		$this->current_buyer = $this->ci->yo_user->get_current_buyer();
		$this->current_buyer_id = $this->current_buyer['buyer']->id ?? NULL;
	}

	/** Create a buyer account */
	public function create_buyer(string $username,string $password,string $email,array $additional_data=NULL): bool
	{
		$user_id = $this->ion_auth->register($username, $password, $email, $additional_data, 'buyer');

		// Add the buyer information to the buyer table
		return $this->ci->buyer_model->create_buyer(array(
			'user_id'=>$user_id
		));
	}
	
	/**  Update buyer account 
	 *  @param $user_data  Data belonging to the user table
	 *  @param $buyer_data  Data belonging to the buyer table
	*/
	public function update_buyer(int $user_id,array $user_data=NULL,array $buyer_data=NULL): bool 
	{
		$user_update_status = $this->ion_auth->update($user_id,$user_data);
		$buyer_update_status = $this->ci->buyer_model->update_buyer(array(
			'user_id'=>$user_id,
		), $buyer_data);

		return $user_update_status && $buyer_update_status;
	}

	/** Update loyalty points for the buyer specified */
	protected function _update_loyalty_points(int $loyalty_points, int $user_id=NULL):bool
	{
		$buyer = $this->ci->buyer_model->get_buyers( array('user_id'=>$user_id) )->row_object();
		
		// Make sure the buyer is available
		if(empty($buyer))
		{
			return FALSE;
		}
		
		$total_loyalty_points = $buyer->loyalty_points + $loyalty_points; 
		$total_loyalty_points = clamp($total_loyalty_points,0,MAX_LOYALTY_POINTS);

		return $this->ci->buyer_model->update_buyer(array(
			'user_id'=>$user_id,
		), array(
			'loyalty_points'=>$total_loyalty_points
		));
	}

	/** Add the specified loyalty points to the buyer specified */
	public function add_loyalty_points(int $loyalty_points,int $buyer_id=NULL): bool 
	{
		return $this->_update_loyalty_points($loyalty_points,$buyer_id);
	}
	
	/** Remove the loyalty points specified from the buyer provided */
	public function remove_loyalty_points(int $loyalty_points,int $buyer_id=NULL): bool 
	{
		return $this->_update_loyalty_points(-$loyalty_points,$buyer_id);
	}
	/** Create an order using the current cart items */
	public function make_order($products, $data): bool 
	{
		return $this->ci->order->make_order($products, $data,$this->current_buyer_id);
		//*TODO: Add implementation - relies on Order library & Posta library
	}
	
	/** Defaults to getting all the current(active) buyer orders */
	public function get_current_orders($buyer_id=NULL): bool 
	{
		//*TODO: Add implementation - relies on Order library & Posta library
	}
	
	/** Get a single order based on the order_id provided */
	public function get_single_order($order_id): bool 
	{
		//*TODO: Add implementation - relies on Order library & Posta library
	}
	
	/** Get the current buyer's purchase_history */
	public function get_purchase_history($buyer_id=NULL): bool 
	{
		//*TODO: Add implementation - relies on Order library & Posta library
	}
	
	/** Update the entire order. Only allow this to happen if the order has not yet been processed. ie. if the order is in pending state */
	public function update_order($order_id,$data): bool 
	{
		//*TODO: Add implementation - relies on Order library & Posta library
	}
	
	/** Update single order item. Only allow this to happen if the order has not yet been processed. ie. if the order is in pending state */
	public function update_single_order_item($order_transaction_id,$data): bool 
	{
		//*TODO: Add implementation - relies on Order library & Posta library
	}
	
	/** Confirm an order when it is delivered */
	public function confirm_order_delivery($order_id,$buyer_id=NULL): bool 
	{
		//*TODO: Add implementation - relies on Order library & Posta library
	}
	
	/** Cancel the order */
	public function cancel_order($order_id): bool 
	{
		//*TODO: Add implementation - relies on Order library & Posta library
	}
	
	/** review the product as the currently logged in buyer. Returns true on success and false on failure */
	public function review_product($product_id,$rating,$comment): bool 
	{
		//* If user is not logged in ~ return false (can't anonymous review)
		if(empty($this->current_buyer_id))
		{
			return FALSE;
		}

		// User logged in ~ proceed to review
		$this->ci->load->model('review_model');
		return $this->ci->review_model->create_review(array(
			'product_id'=>$product_id,
			'buyer_id'=>$this->current_buyer_id,
			'rating'=>clamp($rating,MIN_RATING,MAX_RATING),
			'comment'=>$comment
		));
	}
	
	/** Update the rating with $rating & $comment provided */
	public function update_review($product_id,$rating=NULL,$comment=NULL): bool 
	{
		$this->ci->load->model('review_model');

		// Filter used to retrieve and update reviews
		$filter = array(
			'product_id'=>$product_id,
			'buyer_id'=>$this->current_buyer_id
		);

		$review = $this->ci->review_model->get_reviews($filter)->row_object();
		
		// If the review does not exist ~ failed to update
		if(empty($review))
		{
			return FALSE;
		}

		$reply_belongs_to_buyer = ($review->buyer_id == $this->current_buyer_id);
		
		//* Only allow the review to be updated if it belongs to the current user
		if($reply_belongs_to_buyer)
		{
			// Data to update
			$data = array();

			// If the rating is set ~ update it
			if(!empty($rating))
			{
				$data['rating'] = clamp($rating,MIN_RATING,MAX_RATING);
			}

			// If the comment is set ~ update it
			if(!empty($comment))
			{
				$data['comment'] = (string)$comment;
			}

			return $this->ci->review_model->update_review($filter,$data);
		}
		else // Review does not belong to current user ~ don't allow updating
		{
			log_message('debug',"Attempt to review item that doesn't belong to current user [buyer id: $this->current_buyer_id]");
			return FALSE;
		}
	}
	
	/** Remove the rating. Check if it belongs to the currently logged in user or if an admin is trying to access it, otherwise decline */
	public function remove_review($product_id): bool 
	{
		$this->ci->load->model('review_model');

		return $this->ci->review_model->delete_review(array(
			'product_id'=>$product_id,
			'buyer_id'=>$this->current_buyer_id
		));
	}

	/** Reply to a review  */
	public function reply_to_review($review_id,$reply_text)
	{
		$this->load->library('yo_review');
		return $this->ci->yo_review->reply_to_review($this->current_buyer_id,$reply_id,$reply_text);
	}

	/** Update a review reply */
	public function update_review_reply($reply_id,$reply_text)
	{
		$this->load->library('yo_review');
		return $this->ci->yo_review->update_review_reply($this->current_buyer_id,$reply_id,$reply_text);
	}

	/** Delete a review reply */
	public function delete_review_reply($reply_id,$reply_text)
	{
		$this->load->library('yo_review');
		return $this->ci->yo_review->delete_review_reply($this->current_buyer_id,$reply_id);
	}

}

/* End of file Buyer.php */
