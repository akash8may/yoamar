<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Yo_cart
{
	protected $ci;

	// Cookie names
	protected $cart_cookie_name;
	protected $wishlist_cookie_name;
	protected $saved_items_cookie_name;

	// Buyer related
	protected $current_buyer;
	protected $current_buyer_id;
	protected $buyer_logged_in;

	// Constructor
	public function __construct()
	{		
		$this->ci =& get_instance();
	
		// Load the cookie helper ~ allows us to work with cookies
		$this->ci->load->helper('cookie');

		// Cookie names
		$this->cart_cookie_name = 'cart';
		$this->wishlist_cookie_name = 'wishlist';
		$this->saved_items_cookie_name = 'saved_items';

		// Load the buyer model since we'll be using this
		$this->ci->load->model('buyer_model');

		// Gives us access to user related functions
		$this->ci->load->library('yo_user');
		$this->buyer_logged_in = (bool)$this->ci->yo_user->buyer_logged_in();
		$this->current_buyer = $this->ci->yo_user->get_current_buyer();
		$this->current_buyer_id = $this->current_buyer['buyer']->id ?? NULL;
	}


	#region Cart
	// [shoes, laptop, shirt]
	
	/** Get the array index of the product matching the $product_id provided in a cookie 
	 * @return $index if the product was found in the cookie and `FALSE` if the product was not found in the cookie array
	*/
	private function _get_product_cookie_index(string $cookie_name,$product_id)
	{
		$cookie_records = (array)json_decode(get_cookie($cookie_name));
		if(empty($cookie_records) || empty($cookie_name) || empty($product_id))
		{
			//* Unable to remove cookie record as some information was missing
			log_message('error',"[Cookie name : $cookie_name, Product id: $product_id] Unable to get cookie records as some information was missing");
			return FALSE;
		}
		
		// Return the index if found
		for ($i=0; $i < count($cookie_records); $i++) { 
			// If the cookie record index does not exist ~ 
			if(empty($cookie_records[@$i]))
			{
				continue;
			}

			if($cookie_records[$i]->product_id == $product_id)
			{
				// Return an explicit integer ~ this will be used to confirm it is a valid index
			  	return (int)$i;
			}
		}

		// No index was found if we get to this point ~ return `FALSE`
		return FALSE;
	}

	/** Checks and returns whether a product exists in a cookie array */
	private function _product_exists_in_cookie($cookie_index)
	{
		return ($cookie_index !== FALSE) && is_int($cookie_index);
	}

	/** Update a product cookie with default config */
	private function _update_product_cookie($cookie_name,$cookie_value)
	{
		return set_cookie(
			$cookie_name,
			json_encode($cookie_value),
			COOKIE_PRODUCT_EXPIRE
		);
	}

	/** Adds cookie information on a product ~ shared between cart and wishlist for the cookie area */
	private function _add_product_cookie(string $cookie_name,array $data)
	{
		// Get the cart array cookie ~ present if there are items in the cart
		$item_cookie = json_decode(get_cookie($cookie_name));
		if(!is_array($data))
		{
			$data = array();
		}
		
		// Set the buyer id if there is a buyer logged in
		if($this->buyer_logged_in)
		{
			$data['buyer_id'] = $this->current_buyer_id;
		}

		// Store the date the cookie was added as a timestamp
		$data['last_save_date'] = now();

		// Check if the cookie already exists ~ add records to it if it does
		if(!empty($item_cookie) && is_array($item_cookie))
		{
			$data = (object)$data;
			
			$item_exists_in_cookie = FALSE;
			$item_cookie_index = NULL;//Index the item was found at in the array

			//Check if the item cookie exists in the cookie
			for($i=0;$i<count($item_cookie);$i++)
			{
				$cookie_found = $item_cookie[$i];
				if($cookie_found->product_id == $data->product_id)
				{
					$item_cookie_index = $i;
					$item_exists_in_cookie = TRUE;
					break; #End for loop
				}	
			}

			// Duplicate cookie data into the final cookie value ~ which we'll be modifying
			$final_cookie_value = $item_cookie;

			// If the item exists in the cookie ~ update it
			if($item_exists_in_cookie)
			{
				$final_cookie_value[$item_cookie_index] = $data;
			}
			else //* If item does not exist  append item to the current cookie
			{
				array_push($final_cookie_value,$data);
			}

		}
		else // Otherwise, create a new item cookie
		{
			$final_cookie_value = array($data);
		}

		// Add to cookies
		$this->_update_product_cookie($cookie_name,$final_cookie_value);
	}

	/** Removes product cookie information from cookie */
	private function _remove_product_from_cookie(string $cookie_name, $product_id)
	{
		$cookie_records = json_decode(get_cookie($cookie_name));
		$cookie_index = $this->_get_product_cookie_index($cookie_name,$product_id);

		if($this->_product_exists_in_cookie($cookie_index))// Product found in cookie
		{
			array_splice($cookie_records,$cookie_index,1);
			$cookie_records = array_values($cookie_records);
			return $this->_update_product_cookie($cookie_name,$cookie_records);
		}
		else 
		{
			return FALSE;
		}
	}

	// Get a single cart item based on the product id 
	private function get_single_cart_item($product_id,$buyer_id=NULL)
	{
		//~ Assumes current buyer if no buyer id is provided
		$buyer_id = $buyer_id ?? $this->current_buyer_id;
		return $this->ci->buyer_model->get_cart_items(array(
			'product_id'=>$product_id,
			'buyer_id'=>$buyer_id
		))->row_object();
	}

	/** Currently logged in buyer's cart */
	public function add_to_cart($product_id,$quantity)
	{
		//* If either of the required fields have not been set, return FALSE 
		if(empty($product_id) || empty($quantity))
		{
			// echo 'Missing product id or quantity';//!debug
			return FALSE;
		}
		$product = $this->ci->utility->get_single_product($product_id);

		// Check if the product is verified ~ prevent add to cart if not verified
		// if($product->is_verified == '0' || $product->is_verified == false || $product->is_available == '0' || $product->is_available == false || $product->quantity == '0')
		// {
			// return FALSE;
		// }

		$data = array(
			'product_id'=>$product_id,
			'quantity'=>$quantity,
		);
		// Add cookie
		$this->_add_product_cookie($this->cart_cookie_name,$data);

		// Store the user ip ~ for the database
		$data['user_ip'] = $this->ci->input->ip_address();

		$db_add_status = TRUE;
		//Only add records to database if there is a logged in buyer
		if($this->buyer_logged_in)
		{
			$data['buyer_id'] = $this->current_buyer_id;
			// Add the records to database
			$item_count = $this->ci->buyer_model->get_cart_items(array(
				'product_id' => $data['product_id'],
				'buyer_id' => $data['buyer_id']
			))->num_rows();

			if($item_count > 0)
			{
				$db_add_status = TRUE;
			} else {
				$db_add_status = $this->ci->buyer_model->add_to_cart($data);
			}
		}

		return $db_add_status || !empty($this->get_cart_cookie());
	}


	public function get_cart(){

		$item_cookie = json_decode(get_cookie($this->cart_cookie_name));
		return $item_cookie;
	}

	
	// Get the cart cookie array
	private function get_cart_cookie()
	{
		return json_decode(get_cookie($this->cart_cookie_name));
	}
	
	// Get the wishlist cookie array
	private function get_wishlist_cookie()
	{
		return json_decode(get_cookie($this->wishlist_cookie_name));
	}
	
	// Checks which record is more recent, db or cookie one
	private function _is_db_more_recent($db_record_time,$cookie_record_time)
	{
		//* Field for last save date MUST be named `last_save_date` in both db and cookie items
		// If the db time is set and the cookie time is not, then the db is automatically considered more recent
		if(!empty($db_record_time) && empty($cookie_record_time))
		{
			return TRUE;
		}

		// If both cookie and db items exist, compare dates
		if(!empty($db_record_time) && !empty($cookie_record_time))
		{
			$db_date = strtotime($db_record_time);
			$cookie_date = strtotime($cookie_record_time);

			return ($db_date > $cookie_date);
		}	
		else // If this happens, it means the cookie record exists and the database one does not
		{
			return FALSE;
		}
	}

	//* Field for last save date MUST be named `last_save_date` in both db and cookie items
	private function _get_item_to_use($db_item=NULL,$cookie_item=NULL)
	{
		// TODO: Check for possible bug with casting $cookie_item from array to object
		$db_is_more_recent = $this->_is_db_more_recent(@$db_item[(count(@$db_item) - 1)]->last_save_date,@$cookie_item->last_save_date);

		return $db_is_more_recent ? @$db_item : @$cookie_item;
	}

	// Get the saved items cookie array
	public function get_saved_items()
	{
		return json_decode(get_cookie($this->saved_items_cookie_name));
	}

     /**Get avalible cart itom list*/
	public function getAvailableCart($buyer_id=NULL)
	{
		$this->ci->load->library('utility');
		$cartData =	$this->get_buyer_cart($buyer_id);
		$projects = $this->ci->utility->get_products(NULL,FALSE); #Get data from model
		$cart=array();
		$projects =  array_combine(array_column((array)$projects, 'id'),$projects); 
		foreach($cartData  as $itom){
			if(!empty($projects[$itom->product_id])){
				$product = $projects[$itom->product_id];
				if(!($product->is_available == '0' || $product->is_available == FALSE || $product->is_verified == '0' || $product->is_verified == FALSE || (int)$product->quantity == 0)){
						$cart[] =$itom;
				}
			}
		}
		return $cart;
	}

	  /**Get avalible cart itom list*/
	public function getAvailableCartProducts($buyer_id=NULL)
	{
		$this->ci->load->library('utility');
		$cartData =	$this->get_buyer_cart($buyer_id);
		$projects = $this->ci->utility->get_products(NULL,FALSE); #Get data from model
		$cart=array();
		$projects =  array_combine(array_column((array)$projects, 'id'),$projects); 
		foreach($cartData  as $itom){
			if(!empty($projects[$itom->product_id])){
				$product = $projects[$itom->product_id];
				if(!($product->is_available == '0' || $product->is_available == FALSE || $product->is_verified == '0' || $product->is_verified == FALSE || (int)$product->quantity == 0)){
						$cart[] = array_merge((array) $projects[$itom->product_id], (array) $itom );
				}
			}
		}
		return $cart;
	}

	






	/** Defaults to currently logged in buyer's cart */
	public function get_buyer_cart($buyer_id=NULL) 
	{
		// Get the cart from the cookie ~ cast to object for consistency
		$cart_from_cookie = (object)(
			$this->get_cart_cookie()
		);
		
		// If the buyer is not logged in ~ get the cart cookie
		if(!$this->buyer_logged_in)
		{
			return $cart_from_cookie;
		}

		// If no buyer id is provided, attempt to get the current buyer's id
		$buyer_id = $buyer_id ?? $this->current_buyer_id;

		// Get the cart from the database
		$cart_from_db = $this->ci->buyer_model->get_cart_items(array(
			'buyer_id'=>$buyer_id
		))->result_object();

		return $this->_get_item_to_use(@$cart_from_db,@$cart_from_cookie);
	}

	// Update cart item (cookie) matching the cart item id provide
	private function _update_cart_item_cookie($product_id,$quantity)
	{
		// Update the cookie
		$cart = $this->get_cart_cookie();
		$cookie_index = $this->_get_product_cookie_index($this->cart_cookie_name,$product_id);
		
		if($this->_product_exists_in_cookie($cookie_index))// Product found in cookie
		{
			$cart[$cookie_index]->quantity = $quantity;
			if(is_array($this->_update_product_cookie($this->cart_cookie_name,$cart))) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
		else 
		{
			return FALSE;
		}

	}

	/** Update the cart item quantity of a single cart item  */
	public function update_cart_item($product_id,$quantity)
	{
		// Update the cookie
		$cookie_update_status = $this->_update_cart_item_cookie($product_id,$quantity);
		
		// Attempt updating the database
		$cart_from_db = $this->get_single_cart_item($product_id);

		$db_update_status = TRUE;

		$cart_belongs_to_buyer = (@$cart_from_db->buyer_id == $this->current_buyer_id);

		//* Cart item owner is the same as the logged in user ~ update db record
		if($cart_belongs_to_buyer) 
		{
			$db_update_status = $this->ci->buyer_model->update_cart(array(
				'product_id'=>$product_id
			),array(
				'quantity'=>$quantity
			));
		}
		//* Otherwise,if cart does not belong to current logged in user in db, don't waste resources running unecessary queries

		return TRUE && $db_update_status;
	}
	
	/** Saves currently logged in buyer's cart item */
	public function save_cart_item($product_id)
	{
		// TODO: Add implementation
	}
	
	/** Remove product from cart (currently logged in user's cart) */
	public function remove_cart_item($product_id)
	{
		//* If either of the required fields have not been set, return FALSE 
		if(empty($product_id))
		{
			return FALSE;
		}

		//* Remove cart item from cookies
		$this->_remove_product_from_cookie($this->cart_cookie_name,$product_id);
		
		//* Remove cart item from database
		$cart_item_from_db = $this->get_single_cart_item($product_id);
		$cart_belongs_to_buyer = (@$cart_item_from_db->buyer_id == $this->current_buyer_id);

		// If the item belongs to the current buyer remove it from db
		if($cart_belongs_to_buyer)
		{
			$db_remove_status = $this->ci->buyer_model->remove_cart_item(array(
				'product_id'=>$product_id,
				'buyer_id'=>$this->current_buyer_id
			));
			return $db_remove_status;
		}

		//* This will return true, whether or not the item was found in the cart. 
		//*TODO: Return `true` if the item is no longer in the cart & `false` if the item is in the cart
		return TRUE;
	}

	public function delete_buyer_cart($buyer_id=NULL)
	{
		$buyer_id = $buyer_id ?? $this->current_buyer_id;
		
		// $cart = $this->get_buyer_cart($buyer_id);

		// if(!empty($cart) && (is_array($cart) || is_object($cart)))
		// {
		
		$this->ci->load->library('utility');
		
		delete_cookie($this->cart_cookie_name);
		
		return $this->ci->buyer_model->delete_cart_item(array(
			'is_saved_item' => FALSE,
			'user_ip' => $this->ci->input->ip_address()
		));
		
		// foreach($cart as $cart_item)
		// {
		// 	$product = $this->ci->utility->get_single_product($cart_item->product_id, TRUE);
		// 	$this->remove_cart_item(@$product->id);
		// }
		// }
	}

	public function get_cart_amount($buyer_id=NULL)
	{
		$buyer_id = $buyer_id ?? $this->current_buyer_id;
		
		$cart = $this->get_buyer_cart($buyer_id);
		
		$total = array(
			'total_amount' => 0.00,
			'sale_amount' => 0.00,
		);

		if(!empty($cart) && (is_array($cart) || is_object($cart)))
		{
			
			$this->ci->load->library('utility');
			foreach($cart as $key => $item)
			{
				$product = $this->ci->utility->get_single_product($item->product_id, TRUE);

				// Product was found
				if(!empty($product))
				{
					$total['total_amount'] = $total['total_amount'] + ((float)$product->unit_price * (int)$item->quantity);
					$product->sale_price = (float)$product->sale_price ?? 0.00;
					$total['sale_amount'] = $total['sale_amount'] + ((float)$product->sale_price * (int)$item->quantity);
				}
			}

		}
		return $total;
	}
	#endregion Cart

	#region Wishlist
	/** Add an item to a wishlist & specify whether or not it should be added as public or protected */
	private function _add_to_wishlist($product_id,bool $is_public=TRUE)
	{
		// If either of the required fields have not been set, return FALSE 
		if(empty($product_id))
		{
			return FALSE;
		}

		$data = array(
			'product_id'=>$product_id,
			'is_public'=>(bool)$is_public
		);
		// Add cookie
		$this->_add_product_cookie($this->wishlist_cookie_name,$data);

		// Store the user ip ~ for the database
		$data['user_ip'] = $this->ci->input->ip_address();

		$db_add_status = FALSE;
		if($this->buyer_logged_in)
		{
			$data['buyer_id'] = $this->current_buyer_id;
			// Add the records to database
			$db_add_status = $this->ci->buyer_model->add_to_wishlist($data);
		}

		return $db_add_status || !empty($this->get_wishlist_cookie());
	}

	/** Add a product to the currently logged in buyer's wishlist */
	public function add_to_public_wishlist($product_id)
	{
		return $this->_add_to_wishlist($product_id,TRUE);
	}

	/** Add a product to the currently logged in buyer's wishlist */
	public function add_to_private_wishlist($product_id)
	{
		return $this->_add_to_wishlist($product_id,FALSE);
	}
	
	/** Defaults to currently logged in buyer's wishlist */
	public function get_buyer_wishlist($buyer_id=NULL,$private=FALSE)
	{
		// Get the wishlist from the cookie ~ cast to object for consistency
		$wishlist_from_cookie = (object)(
			$this->get_wishlist_cookie()
		);

		// If the buyer is not logged in ~ get the cart cookie
		if(!$this->buyer_logged_in)
		{
			//TODO: Filter by privacy 
			return $wishlist_from_cookie;
		}

		// If no buyer id is provided, attempt to get the current buyer's id
		$buyer_id = $buyer_id ?? $this->current_buyer_id;

		$db_filter = array(
			'buyer_id'=>$buyer_id,
			'is_public'=>!$private
		);
		
		// Get the wishlist from the database
		$wishlist_from_db = $this->ci->buyer_model->get_wishlist_items($db_filter)->result_object();
		return $this->_get_item_to_use(@$wishlist_from_db,@$wishlist_from_cookie);
	}
	
	protected function get_single_wishlist_item($product_id,$buyer_id=NULL)
	{
		//* Assumes current buyer if no buyer id is provided
		$buyer_id = $buyer_id ?? $this->current_buyer_id;
		return $this->ci->buyer_model->get_wishlist_items(array(
			'product_id'=>$product_id,
			'buyer_id'=>$buyer_id
		))->row_object();
	}
	
	/** Remove the item from the currently logged in buyer's wishlist */
	public function remove_wishlist_item($product_id)
	{
		//* If either of the required fields have not been set, return FALSE 
		if(empty($product_id))
		{
			return FALSE;
		}

		//* Remove wishlist from cookies
		$this->_remove_product_from_cookie($this->wishlist_cookie_name,$product_id);
		
		//* Remove wishlist from database
		$wishlist_item_from_db = $this->get_single_wishlist_item($product_id);
		$wishlist_belongs_to_buyer = (@$wishlist_item_from_db->buyer_id == $this->current_buyer_id);

		// If the item belongs to the current buyer remove it from db
		if($wishlist_belongs_to_buyer)
		{
			$db_remove_status = $this->ci->buyer_model->remove_wishlist_item(array(
				'product_id'=>$product_id,
				'buyer_id'=>$this->current_buyer_id
			));
			return $db_remove_status;
		}

		//* This will return true, whether or not the item was found in the wishlist. 
		//*TODO: Return `true` if the item is no longer in the cart & `false` if the item is in the wishlist
		return TRUE;
	}
	
	/** Removes the entire wishlist belonging to the current buyer  */
	public function remove_wishlist() 
	{
		//* Remove wishlist cookie
		$cookie_remove_status = delete_cookie($this->wishlist_cookie_name);

		//* Remove wishlist from db
		$wishlist_belongs_to_buyer = (@$wishlist_item_from_db->buyer_id == $this->current_buyer_id);

		// If the item belongs to the current buyer remove it from db
		if($wishlist_belongs_to_buyer)
		{
			$db_remove_status = $this->ci->buyer_model->remove_wishlist_item(array(
				'buyer_id'=>$this->current_buyer_id
			));
			return $cookie_remove_status && $db_remove_status;
		}
		
		return $cookie_remove_status;
	}
	
	// Set the item saved status of an item in the db
	private function _set_item_saved($product_id,$is_saved_item)
	{
		//* If either of the required fields have not been set, return FALSE 
		if(empty($product_id) || empty($is_saved_item))
		{
			return FALSE;
		}

		return $this->ci->buyer_model->update_cart(array(
			'product_id'=>$product_id, # Not used $data defined above as these are different entities and do not need DRYing
		),array(
			'is_saved_item'=>(bool)$is_saved_item
		));
	}

	/** Save item for later */
	public function save_item_for_later($product_id)
	{
		//* Add to cookie
		$saved_items = $this->get_saved_items();
		// Check if the product exists in cookies
		$cookie_index = $this->_get_product_cookie_index($this->saved_items_cookie_name,$product_id);
		FALSE;

		// If the item hasn't already been saved, save it in the saved items cookie
		if(!$this->_product_exists_in_cookie($cookie_index))
		{
			$data = array(
				'product_id'=>$product_id
			);
			$this->_add_product_cookie($this->saved_items_cookie_name,$data);
		}

		//* Add to database
		$db_add_status = $this->_set_item_saved($product_id,TRUE);
		$item_saved_successfully = $db_add_status;

		// If the item was successfully added to 
		if($item_saved_successfully)
		{
			$this->remove_cart_item($product_id);
		}
		return $item_saved_successfully;
	}

	/** Remove saved item */
	public function remove_saved_item($product_id)
	{
		$this->_remove_product_from_cookie($this->saved_items_cookie_name,$product_id);

		//* Remove the saved item from database if a user is logged in
		if($this->buyer_logged_in)
		{
			$db_remove_status = $this->_set_item_saved($product_id,FALSE);
			return $db_remove_status;
		}
		return TRUE;
	}
	
	#endregion Wishlist	
}

/* End of file Yo_cart.php */
