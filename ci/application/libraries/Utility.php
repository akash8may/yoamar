<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Utility
{
	protected $ci;
	
	public function __construct()
	{
		$this->ci =& get_instance();

		// Load product model here (keep code DRY) since it is used by most functions in the library
		$this->ci->load->model('product_model');
	}

	// Get single product
	public function get_single_product($product_id,$in_stock=FALSE)
	{
		$product = $this->ci->product_model->get_products(array(
			TBL_PRODUCTS.'.id'=>$product_id
		),$in_stock);
		return $product->row_object();
	}

	// Get all products - for home page
	public function get_products($filters=NULL,$in_stock=TRUE,$allow_search_filters=FALSE,$limit=NULL)
	{	
		// Set filters
		$filters = get_value_or_default($filters,[]);
		if(!empty($_GET))
		{
			// Category filter
			if($cat_id = $this->ci->input->get('cat'))
			{	$filters[TBL_PRODUCTS.'.product_family'] = $cat_id;	}
	
			// Min price filter
			if($min_price = $this->ci->input->get('min_price'))
			{	$filters[TBL_PRODUCTS.'.unit_price>='] = $min_price;	}
	
			// Max price filter
			if($max_price = $this->ci->input->get('max_price'))
			{	$filters[TBL_PRODUCTS.'.unit_price<='] = $max_price;	}
	
			// Quantity filter
			if($quantity = $this->ci->input->get('qty'))
			{	$filters[TBL_PRODUCTS.'.quantity>='] = $quantity;	}

		}

		$search_filters = NULL;
		// Set search filters
		if($allow_search_filters === TRUE)
		{
			$search_filters = get_search_filters(array(
				[
					'url_param'=>'q',
					'db_column'=>TBL_PRODUCTS.'.product_name',
				],
				[
					'url_param'=>'q',
					'db_column'=>TBL_USERS.'.company',
				],
				[
					'url_param'=>'q',
					'db_column'=>TBL_USERS.'.first_name',
				],
				[
					'url_param'=>'q',
					'db_column'=>TBL_USERS.'.last_name',
				],
				[
					'url_param'=>'merchant_id',
					'db_column'=>TBL_PRODUCTS.'.seller_id',
				],
			));
		}
		
		$products = $this->ci->product_model->get_products($filters,$in_stock,$search_filters,$limit);
		return $products->result(QUERY_RETURN_TYPE);
	}

	// Get product images
	public function get_product_images($product_id)
	{
		return $this->ci->product_model->get_product_images(array(
			'product_id'=>$product_id
		))->result_object();
	}

	// Get single product image
	public function get_single_product_image($product_id)
	{
		$product = $this->get_single_product($product_id);

		if(empty($product))
		{
			return FALSE;
		}

		$img_from_db = $this->ci->product_model->get_product_images(array(
			'product_id'=>$product_id
		))->row_object();
		$product_img = $img_from_db;
		if(empty($img_from_db))
		{
			$product_img = (object)array(
				'id'=>-1,
				'product_id'=>$product_id,
				'img_link'=>base_url(PRODUCT_PLACEHOLDER_IMG),
				'thumbnail_link'=>base_url(PRODUCT_PLACEHOLDER_IMG),
				'alt_text'=>@$product->description,
			);
		}
		return  $product_img;
	}

	// Get products based on featured 
	private function _get_products_by_featured(bool $is_featured,array $filters=array())
	{
		$filters = $filters ?? array();
		$filters['is_featured'] = (bool)$is_featured;

		$products = $this->ci->product_model->get_products($filters);
		return $products->result(QUERY_RETURN_TYPE);
	}

	// Get featured products
	public function get_featured_products(array $filters=array())
	{
		return $this->_get_products_by_featured(TRUE,$filters);
	}

	// Get other products - for home page
	public function get_other_products(array $filters=array())
	{
		return $this->_get_products_by_featured(FALSE,$filters);
	}

	// Get category products - products in a specific category
	public function get_category_products($category_id)
	{
		$products = $this->ci->product_model->get_products(
			array(
				TBL_PRODUCTS.'.product_group'=>(int)$category_id
			)
		);
		return $products->result(QUERY_RETURN_TYPE);
	}

	// Get subcategory products - products in a specific subcategory
	public function get_subcategory_products($subcategory_id)
	{
		$products = $this->ci->product_model->get_products(
			array(
				TBL_PRODUCTS.'.product_segment'=>(int)$subcategory_id
			)
		);
		return $products->result(QUERY_RETURN_TYPE);
	}
	
	// Get subcategory 1 products - products in a specific subcategory
	public function get_category_subcategory_products($subcategory_id)
	{
		$products = $this->ci->product_model->get_products(
			array(
				TBL_PRODUCTS.'.product_family'=>(int)$subcategory_id
			)
		);
		return $products->result(QUERY_RETURN_TYPE);
	}

	// Get specific seller products
	public function get_seller_products($seller_id)
	{
		$products = $this->ci->product_model->get_products(
			array(
				TBL_PRODUCTS.'.seller_id'=>(int)$seller_id
			)
		);
		return $products->result(QUERY_RETURN_TYPE);
	}

	// Get all category groups
	public function get_category_groups($filters=array())
	{
		$category_groups = $this->ci->product_model->get_category_groups($filters);
		return $category_groups->result(QUERY_RETURN_TYPE);
	}

	// Get all categories
	public function get_categories($filters=array())
	{
		$categories = $this->ci->product_model->get_categories($filters);
		return $categories->result(QUERY_RETURN_TYPE);
	}
	
	public function get_single_category($filters=array())
	{
		$category = $this->ci->product_model->get_categories($filters);
		return $category->row_object();
	}

	public function get_single_subcategory($filters=array())
	{
		$subcategory = $this->ci->product_model->get_subcategories($filters);
		return $subcategory->row_object();
	}

	
	// Get a specific category's subcategories 
	public function get_category_subcategories($category_id,$level=1)
	{
		$filters = array( 
			'category_id'=>$category_id,
			'level'=>$level,
		);
		return $this->get_subcategories($filters);
	}

	// Get all subcategories - By default only gets subcategories in the first level
	public function get_subcategories($filters=array('level'=>1))
	{
		$subcategories = $this->ci->product_model->get_subcategories($filters);
		return $subcategories->result(QUERY_RETURN_TYPE);
	}
	
	// Get average rating
	public function get_average_rating($product_id)
	{
		$this->ci->load->model('review_model');
		$reviews = $this->ci->review_model->get_reviews(array(
			'product_id'=>$product_id
		))->result_object();
		$total_rating = 0.0;

		// Calculate the total rating
		$count = 0;
		foreach($reviews as $review)
		{
			$total_rating += (float)$review->rating;
			$count++;
		}

		// Calculate the average product rating
		$average_rating = $total_rating>0 ? round(($total_rating/$count),2) : 0;
		$average_rating = clamp($average_rating,0,MAX_RATING);
		return $average_rating;
	}
	
	// Get single review
	public function get_single_buyer_review($product_id,$buyer_id)
	{
		$this->ci->load->model('review_model');
		return $this->ci->review_model->get_reviews(array(
			'product_id'=>$product_id,
			'buyer_id'=>$buyer_id
		))->row_object();
	}
	
	// Get single review
	public function get_single_review($review_id)
	{
		$this->ci->load->model('review_model');
		return $this->ci->review_model->get_reviews(array(
			TBL_REVIEWS.'.id'=>$review_id,
		))->row_object();
	}
	// Get product reviews
	public function get_product_reviews($product_id)
	{
		$this->ci->load->model('review_model');
		$reviews = $this->ci->review_model->get_reviews(array(
			'product_id'=>$product_id
		));

		//TODO - Handle review replies
		return $reviews->result(QUERY_RETURN_TYPE);
	}

	// Get product review replies
	public function get_review_replies($review_id)
	{
		$this->ci->load->model('review_model');
		$review_replies = $this->ci->review_model->get_review_replies(array(
			'review_id'=>(int)$review_id
		));

		//TODO - Handle review replies
		return $review_replies->result(QUERY_RETURN_TYPE);
	}
	
	// Get single country. If $is_active is TRUE, return only active countries
	public function get_single_country($filters,$is_active=TRUE)
	{
		$filters = $filters ?? [];

		if($is_active === TRUE)
		{
			$filters['is_active'] = TRUE;
		}
		
		$this->ci->load->model('country_model');
		$countries = $this->ci->country_model->get_countries($filters);
		return $countries->row_object();
	}

	// Get all countries
	public function get_countries($filters= array('is_active'=>TRUE))
	{
		$this->ci->load->model('country_model');
		$countries = $this->ci->country_model->get_countries($filters);
		return $countries->result(QUERY_RETURN_TYPE);
	}

	// Get all states
	public function get_states($filters)
	{
		$this->ci->load->model('country_model');
		$states = $this->ci->country_model->get_states($filters);
		return $states->result(QUERY_RETURN_TYPE);
	}
}

/* End of file Utility.php */
