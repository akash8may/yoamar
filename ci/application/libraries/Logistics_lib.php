<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Logistics_lib
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->model('logistics_model');
		//TODO: Get logged in logistics & use it's id in these creations
	}

	#region Warehouse
	
	// Returns the current logistics user id if no id is provided or if the id provided is invalid
	private function _get_logistics_user_id($logistics_user_id)
	{		
		if(empty($logistics_user_id))
		{
			$this->ci->load->library('yo_user');
			$current_logistics = $this->ci->yo_user->get_current_logistics();
			$logistics_user_id = $current_logistics->id ?? NULL;
		}
		return $logistics_user_id;
	}

	// Add warehouse item
	public function add_warehouse_item($data,$logistics_user_id=NULL)
	{
		$data = $data ?? [];
		$logistics_user_id = $this->_get_logistics_user_id($logistics_user_id);
		$data['logistics_user_id'] = $logistics_user_id;

		return $this->ci->logistics_model->add_warehouse_item($data);
	}
	
	// Get warehouse items
	public function get_warehouse_items($filters,$limit=NULL,$logistics_user_id=NULL)
	{
		$filters = $filters ?? [];

		$search_filters = get_search_filters(array(
			[
				'url_param'=>'q',
				'db_column'=>TBL_PRODUCTS.'.product_name',
			],
			[
				'url_param'=>'q',
				'db_column'=>TBL_USERS.'.company',
			],
			[
				'url_param'=>'merchant_id',
				'db_column'=>TBL_PRODUCTS.'.seller_id',
			],
			[
				'url_param'=>'status',
				'db_column'=>TBL_WAREHOUSE.'.status',
			]
		));

		$logistics_user_id = $logistics_user_id ?? $this->_get_logistics_user_id($logistics_user_id);

		$filters['logistics_user_id'] = $logistics_user_id;

		return $this->ci->logistics_model->get_warehouse_items($filters,$search_filters,$limit);
	}
	
	// Update shipments
	public function update_warehouse_items($filters,$data,$logistics_user_id=NULL)
	{
		$filters = $filters ?? [];
		$logistics_user_id = $this->_get_logistics_user_id($logistics_user_id);
		$filters['logistics_user_id'] = $logistics_user_id;

		return $this->ci->logistics_model->update_warehouse_items($filters,$data);
	}
	
	// Delete warehouse item
	public function delete_warehouse_item($filters)
	{
		$filters = $filters ?? [];
		$logistics_user_id = $this->_get_logistics_user_id($logistics_user_id);
		$filters['logistics_user_id'] = $logistics_user_id;

		return $this->ci->logistics_model->delete_warehouse_item($filters);
	}
	#endregion Warehouse

	#region Shipments
	// Create shipments
	public function create_shipments($shipments)
	{
		return $this->ci->logistics_model->create_shipments($shipments);
	}
	
	// Get shipments
	public function get_shipments($filters,$limit=NULL,$logistics_user_id=NULL)
	{
		$filters = $filters ?? [];

		$search_filters = get_search_filters(array(
			[
				'url_param'=>'product',
				'db_column'=>TBL_PRODUCTS.'.product_name',
			],
			[
				'url_param'=>'status',
				'db_column'=>TBL_WAREHOUSE.'.status',
			],
			[
				'url_param'=>'merchant_id',
				'db_column'=>TBL_ORDERS.'.seller_id',
			],
			[
				'url_param'=>'delivered',
				'db_column'=>TBL_SHIPMENTS.'.is_delivered',
			],
			[
				'url_param'=>'status',
				'db_column'=>TBL_WAREHOUSE.'.status',
			]
		));

		$logistics_user_id = $this->_get_logistics_user_id($logistics_user_id);
		$filters['logistics_user_id'] = $logistics_user_id;

		return $this->ci->logistics_model->get_shipments($filters,$search_filters,$limit);
	}

	// Update shipments
	public function update_shipments($filters,$data)
	{
		return $this->ci->logistics_model->create_shipments($filters,$data);
	}

	// Delete shipments
	public function delete_shipments($filters)
	{
		return $this->ci->logistics_model->delete_shipments($filters);
	}
	#endregion Shipments
}

/* End of file Logistics_lib.php */
