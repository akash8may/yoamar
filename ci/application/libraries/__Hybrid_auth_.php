<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('xdebug.max_nesting_level', 512);
/**
 * Hybridauth Class
 */
class Hybrid_auth {
  /**
   * Reference to the Hybrid_Auth object
   *
   * @var Hybrid_Auth
   */
  public $HA;
  /**
   * Reference to CodeIgniter instance
   *
   * @var CI_Controller
   */
  protected $CI;
  /**
   * Class constructor
   *
   * @param array $config
   */
  public function __construct($config = array())
  {
    $this->CI =& get_instance();
    // Load the HA config.
    if (!$this->CI->load->config('hybrid_auth'))
    {
      log_message('error', 'Hybridauth config does not exist.');
      return;
    }
    // Get HA config.
    $config = $this->CI->config->item('hybridauth');
    // Specify base url to HA Controller.
    $config['base_url'] = $this->CI->config->site_url('auth/endpoint');

    try
    {
      // Load HA library.
      $this->_init();
      // Initialize Hybrid_Auth.
      log_message('info', 'Hybridauth Class is initialized.');
    }
    catch (Exception $e)
    {
      show_error($e->getMessage());
    }
  }
  /**
   * Process the HA request
   */
  public function process()
  {
    $this->_init('Hybrid_Endpoint');
    Hybrid_Endpoint::process();
  }
  /**
   * Initialize HA library
   *
   * @param string $class_name The class name to initialize
   *
   * @throws \Exception
   */
  protected function _init($class_name = 'Hybrid_Auth')
  {
    list($dir, $filename) = explode('_', $class_name);
    if (class_exists($class_name))
    {
      // Nothing to do here. Most probably the class is loaded by composer_autoload.
    }
    elseif (file_exists(APPPATH . "libraries/vendor/hybridauth/hybridauth/{$dir}/{$filename}.php"))
    {
      // In case when the library is placed in third_party/hybridauth.
      require_once APPPATH . "libraries/vendor/hybridauth/hybridauth/{$dir}/{$filename}.php";
    }
    if (!class_exists($class_name))
    {
      throw new Exception("Could not load the {$class_name} class.");
    }
    log_message('info', "{$class_name} class is loaded.");
  }
}