<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// ini_set('xdebug.max_nesting_level', 1000);
// require APPPATH . "libraries/vendor/hybridauth/hybridauth/Hybrid/Auth.php";
/**
 * Hybridauth Class
 */
class Hauth {
  /**
   * Reference to the Hybrid_Auth object
   *
   * @var Hauth
   */
  public $HA;
  /**
   * Reference to CodeIgniter instance
   *
   * @var CI_Controller
   */
  protected $CI;
  /**
   * Class constructor
   *
   * @param array $config
   */
  public function __construct($config = array(), $_HA = FALSE)
  {
    $this->CI =& get_instance();
    
    // require_once APPPATH . "libraries/vendor/hybridauth/hybridauth/hybridauth/Hybrid/Auth.php";
    // require_once APPPATH . "libraries/vendor/hybridauth/hybridauth/Hybrid/Endpoint.php";
    // Load the HA config.
    if (!$this->CI->load->config('hybrid_auth'))
    {
      log_message('error', 'Hybridauth config does not exist.');
      return;
    }
    // Get HA config.
    $config = $this->CI->config->item('hybridauth');
    // Specify base url to HA Controller.
    $config['base_url'] = $this->CI->config->site_url('auth/endpoint');
    // var_dump($config);
    // return;
    try
    {
      // Initialize Hybrid_Auth.
      if(! $_HA) {
        $this->HA = new Hauth($config, TRUE);
      }
      log_message('info', 'Hybridauth Class is initialized.');
    }
    catch (Exception $e)
    {
      show_error($e->getMessage());
    }
  }
  /**
   * Process the HA request
   */
  public function process()
  {
    require_once APPPATH . "libraries/vendor/hybridauth/hybridauth/Hybrid/Endpoint.php";
    Hybrid_Endpoint::process();
  }

  public function authenticate($provider_id, $params)
  {
    // var_dump(class_exists(Hybrid_Auth));
    echo 'áuthing....';
    // $pro = new Hybrid_Auth\Provider\Facebook;
    return Hybrid_Auth::authenticate($provider_id, $params);
  }
}