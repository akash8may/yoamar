<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Omnipay\Omnipay;
use Omnipay\Stripe;
use Omnipay\Paypal;

class Omnipay_gateway extends Omnipay
{
	protected $ci;
	private $pay;
	private $card;

	public function __construct()
	{
        $this->ci =& get_instance();
        $this->ci->load->config('omnipay');
	}

	function set_card($value)
	{
        try{
            $card = [
				'firstName' => $value['firstname'],
				'lastName' => $value['lastname'],
                'number' => $value['card'],
                'expiryMonth' => $value['expiremonth'],
                'expiryYear' => $value['expireyear'],
                'cvv' => $value['cvv'] ?? $value['cvv']
            ];
            $credit_card = new CreditCard($card);
            $credit_card->validate();
            $this->card = $card;
            return TRUE;
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
    }
 
	public function make_card_payment($value, $method)
	{
        try{
            if ($method == 'stripe' || $method == 'card') :
                $response = $this->make_stripe_payment($value);
            elseif($method == 'paypal') :
                $response = $this->make_paypal_card_payment($value);
                    
			endif;
			
            // Process response
            if ($response->isSuccessful()) {
                return $response->getData();
                // return "Thankyou for your payment";
 
            } elseif ($response->isRedirect()) {
                redirect($response->getRedirectUrl());
                // Redirect to offsite payment gateway
                // return $response->getMessage();
 
            } else {
               // Payment failed
               return $response->getMessage();
            }
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    public function authorize_paypal_payment($value)
    {
        // initialize gateway
        $this->paypal_initalize();

        try {
            $transaction = $this->pay->authorize(array(
                'amount'        => $value['amount'],
                'currency'      => $value['currency'],
                'description'   => $value['description'] ?? 'This is a test authorize transaction.',
                'returnUrl'     => site_url('api/complete_paypal_payment'),
                'cancelUrl'     => site_url('u/checkout'),
            ));
            $response = $transaction->send();
            
            // Process response
            if ($response->isSuccessful()) {
				return [
                    'ok' => TRUE,
                    'data' => $response->getData()
                ];
                // return "Thankyou for your payment";
 
            } elseif ($response->isRedirect()) {
                // Redirect to offsite payment gateway
                return $response->getMessage();
 
            } else {
                // Payment failed
               return [
                   'ok' => FALSE,
                   'message' => $response->getMessage(),
                   'data' => $response->getData()
                ];
            }
            
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function complete_paypal_payment($transaction)
    {
        // initialize gateway
        $this->paypal_initalize();
        try {
            $paymentID = $transaction['paymentID'];
            $payerID = $transaction['payerID'];
    
            // Once the transaction has been approved, we need to complete it.
            $transaction = $this->pay->completePurchase(array(
                'payer_id'             => $payerID,
                'transactionReference' => $paymentID,
            ));
            $response = $transaction->send();
            if ($response->isSuccessful()) {
                // The customer has successfully paid.
                return [
                    'ok' => TRUE,
                ];
            } else {
                // There was an error returned by completePurchase().  You should
                // check the error code and message from PayPal, which may be something
                // like "card declined", etc.
                return [
                    'ok' => FALSE,
                    'message' => $response->getMessage()
                ];
            }
        } catch (\Exception $e) {
            return [
                'ok' => FALSE,
                'message' => $e->getMessage()
            ];
        }
        
    }

    public function make_paypal_card_payment($value)
    {
        $this->paypal_initalize();
        $this->set_card();

        // Do a purchase transaction on the gateway
       try {
           $transaction = $gateway->purchase(array(
               'amount'        => $value['amount'],
               'currency'      => $value['currency'],
               'description'   => 'This is a test paypal direct card purchase transaction.',
               'card'          => $this->card,
           ));

           $response = $transaction->send();
           
           return $response;
       } catch (\Exception $e) {
           echo "Exception caught while attempting authorize.\n";
           echo "Exception type == " . get_class($e) . "\n";
           echo "Message == " . $e->getMessage() . "\n";
       }
    }

    public function make_stripe_payment($value)
    {
        try {
            // Setup payment Gateway
            $this->pay = Omnipay::create('Stripe');
            // $stripe_config = array(
            //     "publishable_key"      => $this->ci->config->item("stripe_publishable_key"),
            //     "secret_key" => $this->ci->config->item("stripe_secret_key"),
            // );
            
            $this->pay->setApiKey($this->ci->config->item("stripe_secret_key"));
            // Send purchase request
            return $this->pay->purchase(
                [
                    'amount' => $value['amount'],
                    'currency' => $value['currency'],
                    'token' => $value['token']
                ]
            )->send();

        } catch (\Exception $e) {
            echo "Exception caught while attempting authorize.\n";
            echo "Exception type == " . get_class($e) . "\n";
            echo "Message == " . $e->getMessage() . "\n";
        }
    }

    private function paypal_initalize()
    {
        $this->pay = Omnipay::create('PayPal_Rest');
        $this->pay->initialize(array(
            'clientID' => $this->ci->config->item("paypal_client_id"),
            'secret'   => $this->ci->config->item("paypal_secret_key"),
            'testMode' => true, // Or false when you are ready for live transactions
        ));
    }
}
