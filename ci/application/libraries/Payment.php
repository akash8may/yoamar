<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library('order');
		$this->ci->load->model('payment_model');
	}

	//
	function get_payment_methods($user_id,$filter=NULL)
	{
		$filter['user_id'] = (int)$user_id;

		return $this->ci->payment_model->get_payment_info($filter)->result('object');
	}

	//
	function add_payment_method($user_id,$method,$details=NULL,$return_id=FALSE)
	{
		
		if(!is_array($details) || empty($details)) {
			switch ($method) {
				case 'mpesa':
				case 'mobile':
				$details = array(
					'type' => 'mobile',
					'mpesa_phone' => $this->ci->input->post('mpesa_phone'),
				);
				break;
				case 'bank':
				case 'visa':
				case 'mastercard':
				case 'card':

					$card_expiry_date = $this->ci->input->post('card_expiry') ?? strtotime('20'.$this->ci->input->post('card_expiry_year').'-'.$this->ci->input->post('card_expiry_month').'-00');
					$m = new \Moment\Moment($card_expiry_date); // default is "now" UTC

					$details = array(
						'type' => 'card',
						'card_number' => $this->ci->input->post('card_number'),
						'card_expiry' => $m->format('Y-m-d',$card_expiry_date),
						'card_security_code' => $this->ci->input->post('cvc') ?? $this->ci->input->post('card_security_code'),
					);
					break;
				case 'online':
					$details = array(
						'type' => 'online',
						'paypal_id' => $this->ci->input->post('paypal_id'),
					);
					break;
			}
		}
		
		$details['user_id'] = $user_id;

		return $this->ci->payment_model->create_payment_info($details, $return_id);
	}

	//
	function update_payment_method($payment_method_id, $user_id)
	{
		switch ($this->ci->input->post('payment_method')) {
			case 'mobile':
			$details = array(
				'mpesa_phone' => $this->ci->input->post('mpesa_phone'),
			);
			break;
			case 'bank':
			case 'card':
				$details = array(
					'card_number' => $this->ci->input->post('card_number'),
					'card_expiry' => $this->ci->input->post('card_expiry'),
					'card_security_code' => $this->ci->input->post('card_security_code') ?? $this->input->post('cvc'),
				);
				break;
			case 'online':
				$details = array(
					'paypal_id' => $this->ci->input->post('paypal_id'),
				);
				break;
		};

		$details['id'] = $payment_method_id;
		$details['user_id'] = $user_id;
		$details['paypal_id'] = $this->ci->input->post('paypal_id');
		$details['type'] = $this->ci->input->post('payment_method');

		return $this->ci->payment_model->update_payment_info($details, array(
			'id'=> $details['id'],
		));
	}

	//
	function delete_payment_method($payment_method_id, $user_id)
	{
		$method = $this->get_payment_methods($user_id, array(
			'id' => $payment_method_id
		))[0];
		// var_dump($method);
		switch ($method->type) {
			case 'bank':
			case 'card':
				$card_array = array(
					'card_number' => $this->ci->input->post('card_number'),
					'card_expiry' => $this->ci->input->post('card_expiry'),
					'card_security_code' => $this->ci->input->post('card_security_code'),
				);
				break;
			case 'online':
				$card_array = array(
					'paypal_id' => $this->ci->input->post('paypal_id'),
				);
				break;
			case 'mobile':
				$card_array = array(
					'mpesa_phone' => $this->ci->input->post('mpesa_phone'),
				);
			default:
				$card_array = [];
				break;
		}
		return $this->ci->payment_model->delete_payment_info(array(
			'id' => $payment_method_id,
			'user_id' => $user_id,
		), $card_array);
	}

	// Calculate the total amount for an order
	private function _get_order_total($order_id)
	{
		// Get order details
		$orders = $this->ci->order->get_orders(array(
			'order_id'=>$order_id
		))->result_object();
		if(empty($orders))
		{
			return 0.0;
		}

		$amount = 0.0;
		
		foreach($orders as $order)
		{
			$amount = $amount + $order->order_total;
		}
		return $amount;
	}

	// Process an mpesa payment
	protected function process_mpesa_payment($payment_info,$amount,$account_reference=NULL,$description=NULL)
	{
		$this->ci->load->library('mpesa_lib');
		
		$this->ci->mpesa_lib->lipa_na_mpesa($payment_info->mpesa_phone,$amount,$account_reference,$description);
		
		return TRUE;
	}

	// Process a card payment
	protected function process_card_payment($amount, $token, $currency='USD')
	{
		$this->ci->load->library('omnipay_gateway');
		$response = $this->ci->omnipay_gateway->make_card_payment(array(
			'currency' => $currency,
			'token' => $token ?? 'tok_1DTNG1BvVVXDm5WfZzV8YDau',
			'amount' => $amount,
		), 'stripe');
		
		if($response===TRUE)
		{
			//should return data??
			return $response;
		} else {
			return $response;
		}
	}

	// Process a paypal payment
	protected function process_paypal_payment($order,$payment_info,$amount,$currency='USD')
	{
		$this->ci->load->library(['omnipay_gateway', 'yo_user']);

		$user = $this->ci->yo_user->get_current_buyer()['buyer'];

		$response = $this->ci->omnipay_gateway->make_card_payment(array(
			'firstname' => $user->first_name,
			'lastname' => $user->last_name,
			'card' => 4032034192889551,
			'expiremonth' => 12,
			'expireyear' => 2023,
			'cvv' => 000,
			'currency' => $currency,
			'amount' => $amount,
		), 'paypal');

		if($response===TRUE)
		{
			//should return data?
			return $response;
		} else {
			return null;
		}
	}

	// Make a payment
	function make_payment($payment_method,$order_id, $shipping_cost, $payment_description='', $stripeToken='')
	{
		// Get order details
		$order = $this->ci->order->get_orders(array(
			'order_id'=>$order_id
		));

		// Stores the response for a payment
		$response = array(
			'ok'=>FALSE,
		);

		// If we could not find the response ~ terminate execution
		if(empty($order) || empty($payment_method))
		{
			return $response;
		}

		// Get the order total
		$amount = $this->_get_order_total($order_id) + $shipping_cost;
		// Check if mobile or card
		switch($payment_method)
		{
			case 'mpesa':
				$account_reference = ucfirst($orders[0]->first_name).ucfirst($orders[0]->last_name);
				$response['data'] = $this->process_mpesa_payment($payment_info,$amount,$account_reference,$payment_description);
			break;
			
			case 'visa':
			case 'mastercard':
			case 'card':
				$response['data'] = $this->process_card_payment($amount,$stripeToken);
			break;
				
			case 'online':
				$response['data'] = ['paid' => TRUE];
				// $response['data'] = $this->process_paypal_payment($order,$payment_info,$amount);
			break;
		}
		
		if(is_array($response['data']) === FALSE)
		{
			$response['ok'] = FALSE;
		} else {
			$response['data']['amount'] = $amount;
			$response['ok'] = TRUE;
		}
		return $response;
	}

	//
	function add_payment($data)
	{
		return $this->ci->payment_model->create_payment($data);
	}

	//
	function get_payment($filter)
	{
		return $this->ci->payment_model->get_payments($filter)->result_object();
	}

	//
	function reverse_payment($payment_id)
	{

	}

	//
	function withdraw_funds($user_id,$amount): bool
	{

	}

	public function payment_method_edit($payment_info_id, $user_id, $gateway=FALSE)
	{
		//validate form
		if($this->ci->input->post('delete_payment_info') !== 'on')
		{
			$payment_validation_response = $this->validate_payment($gateway);
		} else {
			$payment_validation_response = TRUE;
		}
		
		if (!$payment_validation_response)
		{
			// set the flash data error message if there is one
			$this->ci->session->set_flashdata('message', validation_errors());
			$message = (validation_errors()) ? validation_errors() : $this->ci->session->flashdata('message');
		} else {
			//if payment_method_id is set, do an update
			if(!empty($payment_info_id)) 
			{
				//do a delete
				if($this->ci->input->post('delete_payment_info') === 'on')
				{
					$response = $this->delete_payment_method($payment_info_id, $user_id);
					$message = $response===TRUE? 'Payment information successffuly deleted':'Internal error while deleting payment information';
				} 
				else 
				{
					$response = $this->update_payment_method($payment_info_id, $user_id);
					$message = $response===TRUE? 'Payment information successffuly updated':'Internal error while updating payment information';
				}
				
				$this->ci->session->set_flashdata('message', $message);
			} 
			else
			{
				$response = $this->add_payment_method(
					$user_id,
					$this->ci->input->post('payment_method'));
				
				$message = $response===TRUE? 'Payment information successfuly added':'Internal error while creating payment information';
				$this->ci->session->set_flashdata('message', $message);
			}
		}
		return $message;
	}

	/** get all payment infos */
	public function get_payment_info($user_id, $filter=NULL)
	{
		$payments_sort = array(
			'card' => [],
			'paypal' => [],
			'mpesa' => [],
		);
		$payments = $this->get_payment_methods($user_id,$filter);

		//arrange the data by payment type
		if(!empty($payments))
		{
			for($i = 0; $i < count($payments); $i++)
			{
				switch ($payments[$i]->type) {
					case 'mobile':
						$card = new \stdClass;
						$card->id = $payments[$i]->id;
						$card->number = $payments[$i]->mpesa_phone;

						array_push($payments_sort['mpesa'], $card);
						break;
					case 'bank':
					case 'card':
						$card = new \stdClass;
						$card->id = $payments[$i]->id;
						$card->number = $payments[$i]->card_number;
						$card->expiry_date = $payments[$i]->card_expiry;
						$card->cvc = $payments[$i]->card_security_code;

						array_push($payments_sort['card'], $card);
						break;
					case 'online':
						$card = new \stdClass;
						$card->id = $payments[$i]->id;
						$card->email = $payments[$i]->paypal_id;

						array_push($payments_sort['paypal'], $card);
						break;
					
					default:
						$payments_sort[$payments[$i]->type] = $payments[$i];
						break;
				}
			}
			$payments = $payments_sort;
		}
		// var_dump($payments);
		return $payments;
	}

	public function validate_payment(bool $gateway = FALSE)
	{
		$this->ci->form_validation->set_rules('payment_method', 'Payment type', 'required');
		switch($this->ci->input->post('payment_method')) {
			case 'online':
				// no need for validating paypal payments. We are using it's api
				$this->ci->form_validation->set_rules('paypal_id', 'Paypal email address', 'trim');
				break;
			case 'bank':
			case 'visa':
			case 'mastercard':
			case 'card':
				if($gateway == TRUE)
				{
					$this->ci->form_validation->set_rules('stripeToken', 'Card number', 'required');
				} else {
					$this->ci->form_validation->set_rules('card_number', 'Card number', 'required');
					$this->ci->form_validation->set_rules('card_expiry', 'Card expiry ', 'required');
					$this->ci->form_validation->set_rules('card_security_code', 'cvc number', 'required');
				}
				break;
			case 'mobile':
				$this->ci->form_validation->set_rules('mpesa_phone', 'mPesa phone number', 'trim|required');
				break;
		}

		return $this->ci->form_validation->run();
	}
}

/* End of file Payment.php */
