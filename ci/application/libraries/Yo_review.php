<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Yo_review
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->model('review_model');
	}

	/** Reply to a review  */
	public function reply_to_review($user_id,$review_id,$reply_text)
	{
		$filter = array(
			TBL_REVIEWS.'.id'=>$review_id
		);
		$review = $this->ci->review_model->get_reviews($filter)->row_object();
		// If the review does not exist ~ failed to reply
		if(empty($review))
		{
			return FALSE;
		}

		// Review exists ~ proceed to check if we can reply of it
		//*TODO: Check whether or not the reviewed product belongs to the current user
		$reply_belongs_to_buyer = ($review->buyer_id == $user_id);
		//*TODO: Check if the reply belongs to the current seller ~ allow for reply
		$reply_belongs_to_seller = TRUE;#TODO: Check if the product being reviewed belongs to seller

		//* Only allow the user to comment if they are the review owner
		if($reply_belongs_to_buyer || $reply_belongs_to_seller)
		{
			return $this->ci->review_model->create_review_replies(array(
				'review_id'=>$review_id,
				'reply_text'=>$reply_text,
				'user_id'=>$user_id,
				'date_updated'=>now(),
			));
		}
		else
		{
			return FALSE;
		}
	}
	
	/** Update a review reply */
	public function update_review_reply($user_id,$reply_id,$reply_text)
	{
		$filter = array(
			TBL_REVIEW_REPLIES.'.id'=>$reply_id
		);
		$review_reply = $this->ci->review_model->get_review_replies($filter)->row_object();
		
		// If the review does not exist ~ failed to reply
		if(empty($review_reply))
		{
			return FALSE;
		}

		// Review reply exists 
		//*TODO: Check whether or not the reviewed product belongs to the current user
		$reply_belongs_to_user = ($review_reply->user_id == $user_id);
		
		//* Only allow the user to update comment if they are the review owner
		if($reply_belongs_to_user)
		{
			return $this->ci->review_model->update_review_replies($filter,array(
				'reply_text'=>$reply_text,
				'date_updated'=>now(), #Update the last updated date
			));
		}
		else
		{
			return FALSE;
		}
	}

	/** Delete the review reply */
	public function delete_review_reply($user_id,$reply_id)
	{
		$filter = array(
			TBL_REVIEW_REPLIES.'.id'=>$reply_id
		);
		$review_reply = $this->ci->review_model->get_review_replies($filter)->row_object();
		
		// If the review does not exist ~ failed to reply
		if(empty($review_reply))
		{
			return FALSE;
		}

		// Review reply exists 
		//*TODO: Check whether or not the reviewed product belongs to the current user
		$reply_belongs_to_user = ($review_reply->user_id == $user_id);
		
		//* Only allow the user to delete comment if they are the review owner
		if($reply_belongs_to_user)
		{
			return $this->ci->review_model->delete_review_replies($filter);
		}
		else
		{
			return FALSE;
		}
	}

	public function get_product_reviews($product_id)
	{
		$reviews = $this->ci->review_model->get_reviews(array(
			'product_id' => $product_id
		))->result_object();

		return $reviews;
	}
}

/* End of file Yo_review.php */
