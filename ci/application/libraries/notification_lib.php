<?php defined('BASEPATH') or exit('No direct script access allowed');

class Notification_lib
{
    protected $ci;

	protected $request_headers;

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->model('notification_model');
        
    }

    public function get_new_notifications($user_id, $filter = array())
    {
        $user_id = $user_id ?? @$this->ci->yo_user->get_current_user()->id;

        $filter['user_id'] = (int)$user_id;
        $filter['is_read'] = FALSE;

        return $this->ci->notification_model->get_notifications($filter)->result_object();
    }

    public function get_notifications($user_id, $filter = array())
    {
        $filter['user_id'] = $user_id;
        return $this->ci->notification_model->get_notifications($filter)->result_object();
    }
    
    public function get_single_notification($notification_id)
    {
        return $this->ci->notification_model->get_notifications(array(
            'id' => $notification_id
        ))->result_object();
    }

    public function mark_notification_read($notification_id)
    {
        return $this->ci->notification_model->update_notification(array(
            'id' => $notification_id,
        ), array(
            'is_read' => TRUE
        ));
    }

    public function add_notification(array $data)
    {
        $moment = new \Moment\Moment();
        $data['date_sent'] = $moment->format(\Moment\Moment::NO_TZ_MYSQL);
        return $this->ci->notification_model->create_notification($data);
    }
}