<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Yo_user
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library('ion_auth');
	}

	// 
	public function user_logged_in()
	{
		return $this->ci->ion_auth->logged_in();
	}

	// 
	public function get_current_user()
	{
		$user = $this->ci->ion_auth->user()->row_array();
		if($this->ci->ion_auth->in_group('buyer')) {
			$user['group'] = 'buyer';
		} elseif($this->ci->ion_auth->in_group('merchant')) {
			$user['group'] = 'merchant';
		} else {
			$user['group'] = 'admin';
		}
		return (object) $user;
	}
	
	// Returns true if there is a logged in user belonging to the group specified
	private function _user_in_group_logged_in($group)
	{
		return $this->ci->ion_auth->in_group($group);
	}

	/**  Get and return the current user in a certain group */
	private function _get_user_in_group($group_name)
	{
		$current_user = $this->get_current_user();
		
		// If the user is not logged in, return null
		if(!$this->user_logged_in())
		{	return NULL;	}
		
		// Suppress errors incase current user could not be retrieved
		if($this->ci->ion_auth->in_group($group_name, $current_user->id))
		{
			return $current_user;
		}
		else
		{
			return NULL;
		}
	}

	// 
	public function buyer_logged_in(): bool
	{
		return $this->_user_in_group_logged_in('buyer');
	}

	// 
	public function get_current_buyer()
	{
		$buyer = $this->_get_user_in_group('buyer');

		$buyer_details = array(
			'buyer'=>$buyer,
			'details'=>NULL
		);

		// If we found a buyer ~ get the buyer details
		if(!empty($buyer))
		{
			return $buyer_details;
			//I'm not sure why we shouldn't already return the buyer details already
			$this->ci->load->model('buyer_model');
			$details = $this->ci->buyer_model->get_buyers(array('id'=> $buyer->id))->row(0,QUERY_RETURN_TYPE);
			$buyer_details['details'] = $details;
		}

		return $buyer_details;
	}

	// 
	public function seller_logged_in(): bool
	{
		return $this->_user_in_group_logged_in('merchant');
	}

	// Alias of seller_logged_in()
	public function merchant_logged_in(): bool
	{
		return $this->seller_logged_in();
	}

	// 
	public function get_current_seller()
	{
		return $this->_get_user_in_group('merchant');
	}

	// Alias of get_current_seller()
	public function get_current_merchant()
	{
		return $this->get_current_seller();
	}

	// 
	public function admin_logged_in(): bool
	{
		return $this->_user_in_group_logged_in('admin');
	}

	// 
	public function get_current_admin()
	{
		return $this->_get_user_in_group('admin');
	}

	// 
	public function logistics_logged_in(): bool
	{
		return $this->_user_in_group_logged_in('logistics');
	}

	// 
	public function get_current_logistics()
	{
		return $this->_get_user_in_group('logistics');
	}

	/** Update user */
	public function update_user($user_id,$data,$restrict_to_logged_in_user=FALSE): bool
	{
		// If access is restricted, don't allow update if the user id provided is not the current user id
		if(($this->get_current_user()->id != $user_id) && ($restrict_to_logged_in_user == TRUE))
		{
			return FALSE;
		}

		$current_user = $this->ci->ion_auth->user($user_id)->row();

		$new_user_image = @$_FILES['profile_image'];
		// var_dump($_FILES);
		if(!empty($new_user_image) && @$new_user_image['error'] !== 4) {
			//upload
			$this->ci->load->library(array('upload', 'my_upload'));
			$upload_result = $this->ci->my_upload->image_upload('profile_image', USER_IMG_PATH, [
				'width' => 240,
				'height' => 240
			]);
			$thumbnail_result = $this->ci->my_upload->img_upload_thumb($upload_result, USER_THUMB_PATH);
			if($thumbnail_result === TRUE) {
				$data['picture_url'] = base_url(USER_THUMB_PATH.$upload_result['raw_name'].'_thumb'.$upload_result['file_ext']);
			}

			if(!empty($current_user->picture_url) && $thumbnail_result === TRUE )
			{
				$image_url = $current_user->picture_url;
				$this->ci->my_upload->delete_images($image_url);
			}
		}
		return $this->ci->ion_auth->update($user_id,$data);
	}

	/** Update profile
	 * @param int $user_id Id of the user whose profile we are updating
	 * @param string $redirect_url Url to redirect to on success
	  */
	public function update_profile($user_id,$redirect_url)
	{
		$this->ci->load->library('form_validation');
		$user = $this->get_current_user();
		$old_password = $this->ci->input->post('old_password');
		$password_error = '';
		$old_password_has_errors = FALSE;
		// Message displayed on profile update ~ errors or success
		$message = '';

		//validate the form
		$this->ci->form_validation->set_rules('f_name', $this->ci->lang->line('edit_user_validation_fname_label'), 'trim|required');
		$this->ci->form_validation->set_rules('l_name', $this->ci->lang->line('edit_user_validation_lname_label'), 'trim|required');
		$this->ci->form_validation->set_rules('email', $this->ci->lang->line('edit_user_validation_email_label'), 'trim|required|valid_email');
		$this->ci->form_validation->set_rules('company', $this->ci->lang->line('edit_user_validation_company_label'), 'trim');
		$this->ci->form_validation->set_rules('phone', $this->ci->lang->line('edit_user_validation_phone_label'), 'trim|required');
		if($this->ci->input->post('old_password') && $this->ci->input->post('new_password_confirm'))
		{
			$this->ci->form_validation->set_rules('new_password', 'New password', 					'min_length[' . $this->ci->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->ci->config->item('max_password_length', 'ion_auth') . ']|required');
			$this->ci->form_validation->set_rules('new_password_confirm', 'Confirm new password', 	'min_length[' . $this->ci->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->ci->config->item('max_password_length', 'ion_auth') . ']|required|matches[new_password]');
			// validate old password
			if (password_verify($old_password, $user->password) == FALSE)
			{
				$password_error .= '<p>You entered the wrong current password</p>';
				$old_password_has_errors = TRUE;
			}
		}

		if ($this->ci->form_validation->run() === TRUE && $old_password_has_errors === FALSE)
		{
			$form_data = array(
				'first_name' => $this->ci->input->post('f_name'),
				'last_name' => $this->ci->input->post('l_name'),
				'phone' => $this->ci->input->post('phone'),
				'company' => $this->ci->input->post('company'),
			);

			if($this->ci->input->post('old_password') && $this->ci->input->post('new_password_confirm'))
			{
				$form_data['password'] = $this->ci->input->post('new_password_confirm');
			}

			$result = $this->update_user($user_id, $form_data);

			if($result) {
				$this->ci->session->set_flashdata('message', "Updated successfully");
				$message = "Updated successfully";
				redirect(site_url($redirect_url), 'refresh');
			}
		} else {
			$message = (validation_errors() ? validation_errors() : ($this->ci->ion_auth->errors() ? $this->ci->ion_auth->errors() : $this->ci->session->flashdata('message')));
		}

		$message .= $password_error;
		return $message;
	}

	// Get users
	public function get_users($user_group,$limit=NULL)
	{
		// If a non-admin tries to use this function ~ return FALSE
		if(!$this->ci->ion_auth->is_admin())
		{	return FALSE;	}

		$is_active = $this->ci->input->get('active');
		
		$is_active = ($is_active == '' || $is_active == NULL) ? NULL : $is_active;

		$search_filters = get_search_filters(array(
			[
				'url_param'=>'q',
				'db_column'=>TBL_USERS.'.first_name',
			],
			[
				'url_param'=>'q',
				'db_column'=>TBL_USERS.'.last_name',
			],
			[
				'url_param'=>'q',
				'db_column'=>TBL_USERS.'.email',
			]
		));
		
		$this->ci->load->model('admin_model');
		$users = $this->ci->admin_model->get_users($user_group,$is_active,$search_filters)->result_object();

		return $users;
	}

	private function old_password_check($password)
	{
		
	}
}

/* End of file Yo_user.php */
