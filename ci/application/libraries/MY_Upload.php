<?php

class MY_Upload extends CI_Upload
{
    public function __construct()
    {
        parent::__construct();
	}
	
	/** Create  a directory if it does not exist */
	private function _create_dir($file_dir='')
	{
		if(is_dir($file_dir))
			return;

		// Otherwise create the directory
		return mkdir($file_dir);
	}

    public function image_upload($field_name='',$file_dir='', $resize = array())
    {
        $file_dir = (isset($file_dir) && !empty($file_dir))?$file_dir:UPLOAD_BASE_PATH;
		
		$this->_create_dir($file_dir);

        //Update the configuration for the upload ~ update the upload path
        $config = array(
            'upload_path'=>$file_dir,
            'allowed_types'=>'gif|jpg|jpeg|png',
            'max_size'=>MAX_IMAGE_SIZE,
            'min_height'=>MIN_IMAGE_HEIGHT,
            'min_width'=>MIN_IMAGE_WIDTH,
            'encrypt_name'=>TRUE
        );
        
        //Initialize the configuration
        parent::initialize($config);
        
        //Return the upload status
        if(! parent::do_upload($field_name,FALSE)) {
            return parent::display_errors();
        } else {
            return parent::data();
        }
    }

    public function img_upload_thumb($img, $file_dir='',$thumbnail_width=300)
    {
		if(!is_array($img))
		{
			return  FALSE;
		}
		
        $this->_CI->load->library('image_lib');
        $this->_CI->image_lib->clear();
        $file_dir = (isset($file_dir) && !empty($file_dir)) ? $file_dir:UPLOAD_BASE_THUMB_PATH;

        $this->_create_dir($file_dir);
		
        //Update the configuration for the resize
        $config = array(
            'allowed_types'=>'gif|jpg|jpeg|png',
            'encrypt_name'=>TRUE,
            'create_thumb'=> TRUE,
            'thumb_marker'=>'_thumb',
            'quality'=>'95%',
            // 'master_dim'=>($img['image_width']>$img['image_height']?'height':'width'),
            'new_image'=>$file_dir,
            'source_image'=>$img['full_path'],
            'maintain_ratio'=>TRUE,
            'width'=> (int)$thumbnail_width,
            'height'=> (int)$thumbnail_width
        );

        //Initialize the configuration
        $this->_CI->image_lib->initialize($config);
        
        //Return the upload status
        if(! $this->_CI->image_lib->resize()) {
            return $this->_CI->image_lib->display_errors();
        } else {
            return TRUE;
        }
	}
	
	// Get and return a relative path when provided with a path for the server
	public function get_relative_path($absolute_path,$to_replace=NULL) 
	{ // TODO: Re-evaluate this if we choose to store images with another server
		$to_replace = empty($to_replace) ? base_url() : $to_replace;
		$replaced_string = str_replace($to_replace,'',$absolute_path);
		
		return $replaced_string;
	}

	// Delete images from the server
	public function delete_images($img_link,$thumbnail_link='')
	{
		// Delete the image from the server
		$img_path = $this->get_relative_path($img_link);
		$image_delete_status = unlink($img_path);

		// Delete the thumbnail from the server ~ if no thumbnail was found, defaults to true
		$thumbnail_path = $this->get_relative_path($thumbnail_link);
		$thumbnail_delete_status = !empty($thumbnail_path) ? unlink($thumbnail_path) : TRUE;

		return $image_delete_status && $thumbnail_delete_status;
	}
}
