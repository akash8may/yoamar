<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_lib
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->model('admin_model');
    }

    public function verify_product($product_id, $verify = TRUE)
    {
        $this->ci->load->model('Product_model');

        return $this->ci->product_model->update_product(array(
            'id' => $product_id
        ), array(
            'is_verified' => $verify
        ));
    }

}