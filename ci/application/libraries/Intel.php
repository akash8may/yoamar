<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Intel
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
	}	

	// Get products related to the product with the product id of $product_id
	public function get_related_products($product_id,$limit=NULL)
	{
		$product = $this->ci->utility->get_single_product($product_id,TRUE);

		if(empty($product))
		{	return;	}
		$this->ci->load->model('product_model');

		$products_found = $this->ci->product_model->get_products(array(
			TBL_PRODUCTS.'.product_family'=>$product->product_family,
		),TRUE,NULL,$limit)->result_object();


		for ($i=0; $i < count($products_found); $i++) { 
			if($products_found[$i]->id == $product_id)
			{
				unset($products_found[$i]);
			}
		}

		return $products_found;
	}
}

/* End of file Intel.php */
