<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Order
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->model('order_model');
	}

	#region Orders
	/** Make an order with the products provided. Each product has details such as product & quantity  */
	public function make_order($ordered_products, $data,$buyer_id,$return_order_id=FALSE)
	{
		$order_count = $this->get_orders(array(
			'buyer_id'=>$buyer_id,
			'paid'=>FALSE
			))->num_rows();
		
		// Check if the current buyer has unpaid orders 
		if(!empty($order_count) || @$order_count > 0)
		{ //~ Prevent them from creating new orders if they have unpaid orders
			return FALSE;
		}
		//TODO: Consider allowing them to add extra items to their previous orders
		
		// If parameter(s) provided are not valid ~ don't waste time running queries
		if(!is_array($data) || !isset($data))
		{
			return FALSE; 
		}
		$this->ci->load->config('shipping');
		$this->ci->load->library(['notification_lib', 'seller_lib']);
		$this->ci->load->language('notification_lang');

		// Add relevant internally used data
		$data['buyer_id'] = $buyer_id;
		$data['paid'] = $data['paid'] ?? FALSE; # Defaults to false, because an order hasn't been paid for when it is made
		$data['shipping_method'] = $data['shipping_method'] ?? 'ship_shipping_cost'; # Defaults to false, because an order hasn't been paid for when it is made
		
		//Generate the order id ~ collective id for all the items in the current order
		// Give every product in the current order the same order id
		$generated_order_id = $this->ci->order_model->generate_string_id(TBL_ORDERS,'order_id');
		$data['order_id'] = $generated_order_id;
		$order_response = [];
		
		//loop through the cart ordered products
		foreach ($ordered_products as $ordered_product) {
			$data['quantity'] = (int)$ordered_product->quantity;
			// get the real product object just incase it's the cookie cart object passed
			$product = $this->ci->utility->get_single_product($ordered_product->product_id);

			$data['sales_tax1'] = (float)$product->tax ?? 0.00;
			$data['seller_id'] = (int)$product->seller_id;
			$data['product_id'] = $product->id;
			$data['sales_amount'] = (float)$product->unit_price;
			$data['order_total'] = ((float)$product->unit_price * (int)$data['quantity']) + $product->tax;
			//Generate a transaction id for the individual items
			$data['transaction_id'] = $this->ci->order_model->generate_string_id(TBL_ORDERS,'transaction_id');
			
			$response = $this->ci->order_model->create_order($data);
			
			//add notificatioon
			if($response === TRUE)
			{
				//reduce the product quantity
				$new_product_quantity = (int) $product->quantity - (int) $data['quantity'];
				$product_updated = $this->ci->seller_lib->update_product($product->id, array(
					'quantity' => $new_product_quantity
				));

				$notification_text = parse_msg(lang('product_new_order'), array(
					'product_name' => $product->product_name,
					'yoamar_location' => 'the nearest location' #should change to an actual place
				));
				$notification_response = $this->ci->notification_lib->add_notification(array(
					'title' => 'New purchase!',
					'user_id' => (int)$product->seller_id,
					'description' => $notification_text,
					'is_important' => TRUE,
				));
			}
			array_push($order_response, $response);
		}
		
		if(in_array(FALSE, $order_response))
		{
			return FALSE;
		}
		
		if($return_order_id)
		{
			return $generated_order_id;
		}
		return TRUE;
 	}

	/** Get orders matching the filter provided
	 * @return object An object that can be extracted using ->row() or ->result()
	 */
	public function get_orders($filters=[])
	{
		return $this->ci->order_model->get_orders($filters);
	}

	/** Update order details */
	private function _update_order_details(array $filter,array $data):bool
	{
		// If parameter(s) provided are not valid ~ don't waste time running queries
		if(!is_array($data) || empty($data) || !is_array($filter) || empty($filter))
		{
			return FALSE;
		}

		return  $this->ci->order_model->update_order($filter,$data);
	}

	//! We may not need this method ~ review if this method is necessary
	/** Update the order with the id of $order_id with the $data provided */
	public function update_order($order_id,array $data)
	{
		$this->_update_order_details(array('order_id'=>$order_id),$data);
	}

	/** Update the tramsaction item with the id of $transaction_id with the $data provided */
	public function update_transaction_item($transaction_id,array $data): bool
	{
		return $this->_update_order_details(array('transaction_id'=>$transaction_id),$data);
	}

	/** Cancel an order */
	public function cancel_order(int $order_id): bool
	{
		// If parameter(s) provided are not valid ~ don't waste time running queries
		if(!is_array($data) || empty($data) || empty($order_id))
		{
			return NULL;
		}

		return  $this->ci->order_model->update_order(
			array('order_id'=>$order_id),
			array('is_cancelled'=>TRUE)
		);
	}
	#endregion Orders

	#region order_reversals
	public function create_order_reversal($order_id, array $data)
	{
		$return_quantity = $data['quantity'];
		unset($data['return_quantity']);
		
		if(isset($data['transaction_id']))
		{
			$orders = $this->ci->order_model->get_orders(array(
				'transaction_id' => $data['transaction_id']
			))->result_object();
			unset($data['transaction_id']);
		} else {
			$orders = $this->ci->order_model->get_orders(array(
				'order_id' => $order_id
			))->result_object();
		}
		
		// key is the transaction_id since the array is return_quantity[transaction_id] = quantity
		foreach ($return_quantity as $key => $quantity) {
			$order = $this->ci->order_model->get_orders(array(
				'transaction_id' => $key
			))->row_object(0);
			$reversal_is_valid = $this->order_reversal_is_valid($order->transaction_id);

			if(!empty($order) && $reversal_is_valid)
			{
				$order_reversal = $this->get_order_reversals(array(
					'transaction_id' => $order->transaction_id
				))->row_object(0);
				$data['transaction_id'] = $order->transaction_id;
				$data['quantity'] = (int) $quantity ?? 0;
				
				if(empty($order_reversal))
				{
					$result = $this->ci->order_model->create_order_reversal($data);
				} else 
				{
					$data['quantity'] = (int) $order_reversal->quantity + (int) $data['quantity'];

					$result = $this->update_order_reversal(array(
						'id' => $order_reversal->id
					), array(
						'quantity' => $data['quantity']
					));

				}
	
				if($result == TRUE)
				{
					//update the order table
					$new_order_quantity = (int) $order->quantity - (int) $data['quantity'];

					if($new_order_quantity <= 0)
					{
						//cancel ordered item
						$result2 = $this->update_transaction_item($order->transaction_id, array(
							'is_cancelled' => TRUE,
						));
					}
				}
			}
		}
		return $result || $result2;
	}

	public function order_reversal_is_valid($transaction_id)
	{
		$order = $this->ci->order_model->get_orders(array(
			'transaction_id' => $transaction_id
		))->row_object(0);
		
		if(empty($order))
		{
			//ccan't find the order id
			return FALSE;
		} elseif(empty($order->date_delivered)) {
			//item has not been delivered
			return TRUE;
		} else {
			//check see if the ordered item is within the reversal period
			$date_delivered = new Moment\Moment($order->date_delivered, 'CET');
			$today = $date_delivered->fromNow();
			$return_period = $today->getDays();

			if($return_period <= ORDER_REVERSAL_PERIOD) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	public function update_order_reversal(array $filter, array $data)
	{
		return $this->ci->order_model->update_order_reversal($filter, $data);
	}

	public function get_order_reversals(array $filter)
	{
		return $this->ci->order_model->get_order_reversals($filter);
	}

	/**
	 * Generate the costs to be refunded and other related data e.g date to be refunded, instructions if it was delivered,
	 * return [
	 * 		reversal_costs => ['humanized text'=> 'cost(changed to appropriate currency)', ...],
	 * 		refund_data => ['humanized text'=> 'humanized text data', ...]
	 * ]
	 */
	public function order_reversal_data($order_id, $product_id=NULL)
	{
		# code...
	}

	#endregion order_reversals

	#region Shipments
	/** Create shipments matching the data provided */
	public function create_shipment(int $transaction_id, string $tracking_id=NULL,string $tracking_url=NULL,int $warehouse_id=NULL, string $condition='good'): boolean
	{
		$data = array(
			'transaction_id'=>$transaction_id,
			'warehouse_id'=>$warehouse_id,
			'condition'=>strtolower($condition),
			'tracking_id'=>$tracking_id,
			'tracking_url'=>$tracking_url,
		);
		$this->ci->load->model('logistics_model');
		return $this->ci->logistics_model->create_shipment($data);
		
	}

	/** Get shipments matching the filter  */
	public function get_shipments(array $filter): array
	{
		// If parameter(s) provided are not valid ~ don't waste time running queries
		if(!is_array($filter) || empty($filter))
		{
			return NULL;
		}

		$this->ci->load->model('logistics_model');
		return $this->ci->logistics_model->get_shipments($filter);
	}

	/** Update the shipment with the shipment id of $shipment id with the $data provided */
	public function update_shipment(int $shipment_id,array $data): boolean
	{
		// If parameters provided are not valid ~ don't waste time running queries
		if(!is_array($data) || !isset($shipment_id,$data))
		{
			return FALSE;
		}

		$this->ci->load->model('logistics_model');
		return $this->ci->logistics_model->update_shipment(
			array('id'=>$shipment_id),
			$data
		);
	}

	/** Delete a shipment */
	public function delete_shipment(array $filter): array
	{
		//! Be careful when using this one 
		//!We generally won't delete shipment data 
		//* ~ Not implemented in buyer/seller libraries
		// If parameter(s) provided are not valid ~ don't waste time running queries
		if(!is_array($filter) || empty($filter))
		{
			return FALSE;
		}

		$this->ci->load->model('logistics_model');
		return $this->ci->logistics_model->delete_shipment($filter);
	}
	
	#endregion Shipments

	#region Shipping details
	/** Remove saved item */
	public function add_shipping_details($id=NULL)
	{
		$this->ci->load->library('yo_user');
		$details = array(
			'first_name' => $this->ci->input->post('first_name'),
			'last_name' => $this->ci->input->post('last_name'),
			'buyer_id' => $this->ci->yo_user->get_current_buyer()['buyer']->id,
			'address_country_id' => $this->ci->input->post('address_country'),
			'address_line' => $this->ci->input->post('address_line_1'),
			'address_city' => $this->ci->input->post('address_city'),
			'postal_code' => $this->ci->input->post('postal_code'),
			'phone' => $this->ci->input->post('phone'),
			'company' => $this->ci->input->post('company'),
			'vat' => $this->ci->input->post('vat'),
		);
		if((isset($id) || $id !== NULL)) {
			$details = array_merge($details, array('id'=> $id));
		}

		//add to the database
		$this->ci->load->model('order_model');

		return $this->ci->order_model->create_shipping_details($details);
	}

	public function get_shipping_details($filter)
	{
		//add to the database
		$this->ci->load->model('order_model');

		return $this->ci->order_model->get_shipping_details($filter)->result_object();
	}

	public function update_shipping_details($filter, $data)
	{
		//add to the database
		$this->ci->load->model('order_model');

		return $this->ci->order_model->update_shipping_details($filter, $data);
	}

	#endregion Shipping details
}

/* End of file Order.php */
