<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Seller_lib
{
	protected $ci;

	// Buyer related
	protected $current_seller;
	protected $current_seller_id;
	protected $seller_logged_in;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->model('product_model');
		$this->ci->load->library(array('utility','upload','image_lib','my_upload'));
		$this->ci->load->helper('yoamar');

		$this->seller_logged_in = (bool)$this->ci->yo_user->seller_logged_in();
		$this->current_seller = $this->ci->yo_user->get_current_seller();
		$this->current_seller_id = $this->current_seller->id ?? NULL;

		if($this->seller_logged_in == FALSE)
		{
			return FALSE;
		}
	}

	// 
	public function create_seller(string $username,string $password,string $email,array $additional_data=NULL): bool
	{
		return $this->ion_auth->register($username, $password, $email, $additional_data, 'seller');
	}

	// Remove seller from public visibility but retain record in database
	public function deactivate_seller($seller_id): bool
	{
		return $this->ci->ion_auth($seller_id,array(
			'is_active'=>FALSE
		));
	}

	// Delete seller records from database ~ only use when seller wants to delete all their records
	public function delete_seller($seller_id): object
	{
		return $this->ci->ion_auth->delete_user($seller_id);
	}	

	//
	public function create_product($data)
	{
		$data['seller_id'] = $this->ci->yo_user->get_current_seller()->id;
		$product_images = $_FILES;
		// Calculate commission and seller dues
		$data['yoamar_commission'] = get_commission($data['unit_price']);
		$data['seller_dues'] = get_seller_dues($data['unit_price']);

		$created_product_id = $this->ci->product_model->create_product($data);
		
		// Data
		$product_description = $data['description'];

		$create_product_images_status = TRUE;
		//Check if there are any product images and add them if any
		if(is_array($product_images) && !empty($product_images))
		{
			$create_product_images_status &= $this->_product_images_upload_handler($product_images, $created_product_id, $product_description);
		}
		else // No product images uploaded ~ use placeholder
		{
			$this->ci->product_model->create_product_image(array(
				'product_id' => $created_product_id,
				'img_link' => base_url(PRODUCT_PLACEHOLDER_IMG),
				'thumbnail_link' => base_url(PRODUCT_PLACEHOLDER_IMG),
				'alt_text' => $product_description,				
			));
		}
		return ($create_product_images_status && ($created_product_id !== FALSE));
	}
	//
	public function update_product($product_id,array $data)	
	{
		return $this->ci->product_model->update_product([
			'id' => $product_id
		], $data);
	}

	private function _product_images_upload_handler($product_images, $product_id, $product_description, $is_update=FALSE)
	{
		$images = array();
		$upload_response = array();
		$i = 0;
		//delete the previous images
		if($is_update === TRUE)
		{
			$previous_product_images = $this->ci->utility->get_product_images($product_id);

			foreach($previous_product_images as $key => $previous_image)
			{
				$delete_image_response[$key] = $this->delete_product_image($previous_image, $product_id);
			}
		}
		foreach ($product_images as $key => $product_image) {
			
			// Check if it is a valid product image ~ if it is, set it, otherwise continue to next iteration
			if(is_array($product_image) && !empty($product_image))
			{
				$response = $this->ci->my_upload->image_upload($key, PRODUCT_IMG_PATH);
				$images[$key] = array(
					'image'=>		$response,
					'thumbnail'=>	NULL,
				);
				
				// if the image has been uploaded, add it to the database
				if(is_array($response)) {
					$thumbnail_response = $this->ci->my_upload->img_upload_thumb($images[$key]['image'], PRODUCT_THUMB_PATH,$this->ci->config->item('product_thumbnail_width'));
					$images[$key]['thumbnail'] = $thumbnail_response;

					$image_link = base_url(PRODUCT_IMG_PATH.$images[$key]['image']['file_name']);
					$image_thumbnail_link = base_url(PRODUCT_THUMB_PATH.$images[$key]['image']['raw_name'].'_thumb'.$images[$key]['image']['file_ext']);
					

					$img_data = array(
						'product_id' => $product_id,
						'img_link' => $image_link,
						'thumbnail_link' => $image_thumbnail_link,
						'alt_text' => $product_description,
					);
					
					$images['key']['ok'] = $this->ci->product_model->create_product_image($img_data);
				}
			}
			else
			{
				continue;
			}
			
		}
		
		return [$images, @$delete_image_response];
	}

	private function _update_product_stock(int $product_id,int $quantity): bool
	{
		$product = $this->ci->utility->get_single_product($product);

		// If the product could not be found, return false
		if(empty($product))
		{
			return FALSE;
		}

		$new_stock = $product->quantity + $quantity;

		return $this->ci->product_model->update_product(array(
			'id'=>$product_id
		),array(
			'quantity'=>$new_stock
		));
	}

	//
	public function add_product_stock(int $product_id,int $quantity): bool
	{
		return $this->_update_product_stock($product_id,$quantity);
	}

	//
	public function remove_product_stock(int $product_id,int $quantity): bool
	{
		return $this->_update_product_stock($product_id,-$quantity);
	}

	//
	public function delete_product($product_id,$data)
	{
		$product_images = $this->ci->utility->get_product_images($product_id);
		
		foreach($product_images as $key => $image)
		{
			$this->delete_product_image($image, $product_id);
		}
		return $this->ci->product_model->delete_products(array(
			'id'=>$product_id
		), $data);
	}
	
	private function delete_product_image($image,$product_id, $on_db=TRUE)
	{
		$img_link = $image->img_link;
		$thumb_link = $image->thumbnail_link;
		$img_link = explode('/', $image->img_link);
		$thumb_link = explode('/', $image->thumbnail_link);
		$img_link = array_pop($img_link);
		$thumb_link = array_pop($thumb_link);

		$this->ci->load->helper('file');
		try {
			if(file_exists(FCPATH.PRODUCT_IMG_PATH.$img_link)) {
				if(unlink(FCPATH.PRODUCT_IMG_PATH.$img_link) == TRUE && unlink(FCPATH.PRODUCT_THUMB_PATH.$thumb_link) == TRUE)
				{
					if($on_db)
					{
						//delete from db
						return $this->ci->product_model->delete_product_images(array(
							'id' => $image->id
						));
					} else {
						return TRUE;
					}
				}
			} else {
				if($on_db)
				{
					//delete from db
					return $this->ci->product_model->delete_product_images(array(
						'id' => $image->id
					));
				} else {
					return TRUE;
				}
			}
		} catch(Exception $e)
		{
			log_message('error',"[Failed @ \$seller_lib->delete_product_image()]\n".$e);
			return FALSE;
		}
		
	}

	//
	public function reply_to_review($review_id,$comment)
	{
		//Load the user library to get the current seller
		$this->ci->load->library('yo_user');
		$current_seller = $this->ci->yo_user->get_current_seller();

		// If there is no currently logged in seller
		if(empty($current_seller))
		{
			return FALSE;
		}

		// Otherwise means that we have a currently logged in seller
		$this->ci->load->model('review_model');
		$this->ci->review_model->create_review_replies(array(
			'review_id'=>$review_id,
			'reply_text'=>(string)$comment,
			'user_id'=>$current_seller->id,
		));
	}

	//
	public function get_sales($filters=NULL,$seller_id=NULL)
	{
		$filters = get_value_or_default($filters,[]);
		$filters[TBL_SHIPMENTS.'.is_delivered'] = TRUE;
		
		$seller_id = get_value_or_default($seller_id,$this->current_seller_id);
		$filters[TBL_PRODUCTS.'.seller_id'] = $seller_id;

		$this->ci->load->model('logistics_model');
		$sales = $this->ci->logistics_model->get_shipments($filters);

		return $sales->result(QUERY_RETURN_TYPE);
	}

	//
	public function get_product_sales($product_id)
	{
		$this->ci->load->library('yo_user');
		$order_transaction_id = $this->ci->order->get_orders(array(
			TBL_ORDERS.'.seller_id' => $this->ci->yo_user->get_current_seller()->id,
			TBL_ORDERS.'.product_id' => $product_id
		));

		return $this->get_sales(array(
			'order_transaction_id'=>$order_transaction_id,
		));
	}

	// Includes ability to track product order progress
	public function get_product_orders($product_id) 
	{
		$this->ci->load->model('order_model');
		$orders = $this->ci->order_model->get_orders(array(
			'product_id'=>$product_id
		));

		return $orders->result(QUERY_RETURN_TYPE);
	}

	//
	public function get_payments_history($limit=NULL)
	{
		//Load the user library to get the current seller
		$this->ci->load->library('yo_user');
		$current_seller = $this->ci->yo_user->get_current_seller();

		// If there is no currently logged in seller
		if(empty($current_seller))
		{
			return NULL;
		}

		$this->ci->load->model('payment_model');
		$payments = $this->ci->payment_model->get_payments(array(
			'user_id'=> $current_seller->id
		));

		return $payments->result(QUERY_RETURN_TYPE);
	}

	//
	public function get_payment_details($payment_id)
	{
		$this->ci->load->model('payment_model');
		$payments = $this->ci->payment_model->get_payments(array(
			'id'=>$payment_id
		));

		return $payments->result(QUERY_RETURN_TYPE);
	}

	//*TODO: Review reply related functionality
}

/* End of file Seller.php */
