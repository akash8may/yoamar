<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Search
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->model('product_model');
	}

	// Return search results based on filters provided for products
	public function get_product_search_results()
	{
		$query = $this->ci->input->get('q');
		$category_id = $this->ci->input->get('cat') ?? NULL;
		$filter = empty($category_id) ? NULL : array('product_family'=>$category_id);
		$search_filters = get_search_filters(array(
			[
				'url_param'=>'q',
				'db_column'=>TBL_PRODUCTS.'.product_name',
			],
			[
				'url_param'=>'q',
				'db_column'=>TBL_USERS.'.company',
			],
			[
				'url_param'=>'merchant_id',
				'db_column'=>TBL_PRODUCTS.'.seller_id',
			]
		));

		$products = $this->ci->product_model->get_products($filter,TRUE,$search_filters);

		if(!empty($query))
		{
			// Store the search query in the database
			$this->ci->load->model('search_model');

			$this->ci->load->helper('geo');
			$geo_data = get_visitor_geodata();
			// Store search queries in database
			$this->ci->search_model->create_search_query(array(
				'search_query'=>$query,
				'user_ip'=>$this->ci->input->ip_address(),
				'country_id'=>$geo_data['country'],
				'city'=>$geo_data['city']
			));
		}
		
		return $products->result(QUERY_RETURN_TYPE);
	}

	// Get search queries
	public function get_search_queries($filters=NULL,$limit=NULL)
	{
		// If we try to access this as a non-admin, return NULL
		if(!$this->ci->ion_auth->is_admin())
		{	return FALSE;	}

		$search_filters = get_search_filters(array(
			[
				'url_param'=>'q',
				'db_column'=>TBL_SEARCH_QUERIES.'.search_query'
			],
			[
				'url_param'=>'q',
				'db_column'=>TBL_COUNTRIES.'.nicename'
			],
			[
				'url_param'=>'q',
				'db_column'=>TBL_SEARCH_QUERIES.'.city'
			]
		));

		$this->ci->load->model('search_model');
		return $this->ci->search_model->get_search_queries($filters,$search_filters,$limit)->result_object();
	}
}

/* End of file Search.php */
