<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Seller extends Admin_Controller {
	
	protected $data;
	protected $quill_css_link;
	protected $quill_js_link;
	protected $current_seller;
	protected $current_seller_id;
	protected $yo_js_link;

	public function __construct()
	{
		// __construct($default_navigation,$profile_url, $notifications_url, $default_container='_templates/admin/body_container',$default_footer_view='_templates/admin/footer')
		parent::__construct('_components/admin/side_nav','merchant/profile','merchant/notifications');

		// Loading appropriate libraries
		$this->load->library(array('seller_lib', 'form_validation', 'notification_lib'));
		$this->load->helper('yoamar');
		
		// Setting script paths
		$this->yo_js_link = $this->get_script_string('merchant/js/yo-es.js');
		$this->quill_css_link = $this->get_css_string('//cdn.quilljs.com/1.3.6/quill.snow.css',FALSE,FALSE);
		$this->quill_js_link = $this->get_script_string('//cdn.quilljs.com/1.3.6/quill.min.js',FALSE);
		$this->quill_js_link .= $this->yo_js_link;
		// Setting data
		$this->data['brand_title'] = 'Seller Panel';
		$this->current_seller = $this->yo_user->get_current_seller() ?? NULL;
		$this->current_seller_id = $this->current_seller->id ?? NULL;
		
		$this->redirect_to_login($this->current_seller,'merchant');
		$this->data['new_notifications'] = $this->notification_lib->get_new_notifications($this->current_seller_id);
		$this->data['nav_items'] = array(
			'dashboard'=> array(
				'link'=>site_url('merchant/dashboard'),
				'text'=>'Dashboard',
			),
			'products'=> array(
				'link'=>site_url('merchant/products'),
				'text'=>'Products',
			),
			'sales'=> array(
				'link'=>site_url('merchant/sales'),
				'text'=>'Sales',
			),
			'orders'=> array(
				'link'=>site_url('merchant/orders'),
				'text'=>'Orders',
			),
			'ratings'=> array(
				'link'=>site_url('merchant/ratings'),
				'text'=>'Ratings',
			),
			'profile'=> array(
				'link'=>site_url('merchant/profile'),
				'text'=>'Profile',
			),
			'logout'=> array(
				'link'=>site_url('auth/logout'),
				'text'=>'Logout',
			),
		);
	}
	
	public function index()
	{
		$this->dashboard();	
	}

	// Dashboard ~ current seller's dashboard
	public function dashboard()
	{
		$this->data['page_title'] = get_page_title('Dashboard');
		$this->data['nav_items']['dashboard']['active'] = TRUE;

		//load order library
		$this->load->library('order');

		$this->data['orders'] = $this->order->get_orders(array(
			TBL_ORDERS.'.seller_id' => @$this->current_seller->id
		))->result();

		$this->data['sales'] = $this->seller_lib->get_sales(array(
			TBL_ORDERS.'.seller_id'=>$this->current_seller->id
		));
		//TODO: Load necessary models & populate data
		$this->_render_page('seller/dashboard/dashboard',$this->data,NULL,$this->yo_js_link);
	}

	// Products - View all seller's products in the market
	public function products() // Filter by all, in stock , out of stock
	{
		$this->data['page_title'] = get_page_title('Products');
		$this->data['nav_items']['products']['active'] = TRUE;
		
		// Product filters ~ using these to filter through products in the list
		$filters = array(
			'seller_id' => @$this->current_seller->id ?? -1
		);

		$this->data['categories'] = $this->utility->get_subcategories();

		$this->data['products'] = $this->utility->get_products($filters,FALSE,TRUE);

		// var_dump($this->data['products']);
		//TODO: Load necessary models & populate data
		$this->_render_page('seller/products/list',$this->data,NULL,$this->yo_js_link);
	}

	private function _get_product_categories()
	{
		$this->data['product_groups'] = $this->utility->get_category_groups();
		$this->data['product_segments'] = $this->utility->get_categories(array(
			'category_group_id'=>'1'
		));
		$this->data['product_families'] = $this->utility->get_subcategories(array(
			'level'=>PRODUCT_FAMILY
		));
		$this->data['product_classes'] = $this->utility->get_subcategories(array(
				'level'=>PRODUCT_CLASS
		));
		$this->data['product_types'] = $this->utility->get_subcategories(array(
			'level'=>PRODUCT_TYPE
		));
		$this->data['product_subtypes'] = $this->utility->get_subcategories(array(
			'level'=>PRODOCUT_SUBTYPE
		));
	}

	// Products - View all seller's products in the market
	public function product_add()
	{
		$this->data['page_title'] = get_page_title('Add Product');
		$this->data['nav_items']['products']['active'] = TRUE;
		$data['form_action'] = 'merchant/product_create';
		$this->_get_product_categories();
		//TODO: Load necessary models & populate data
		$this->_render_page('seller/products/manage',$this->data,$this->quill_css_link,$this->quill_js_link);
	}

	// Products - Edit product with the product id of $product_id
	public function product_edit($product_id)
	{
		$this->data['page_title'] = get_page_title('Edit Product');
		$this->data['nav_items']['products']['active'] = TRUE;

		$this->data['edit'] = TRUE; # Load edit related functionality in the view
		$this->data['edit_active_tab'] = 1; # Load the active edit tab in the view
		$this->data['product_groups'] = $this->utility->get_category_groups();
		$this->data['product_segments'] = $this->utility->get_categories(array(
			'category_group_id'=>'1'
		));
		$this->data['product_families'] = $this->utility->get_subcategories(array(
			'level'=>PRODUCT_FAMILY
		));
		$this->data['product_classes'] = $this->utility->get_subcategories(array(
				'level'=>PRODUCT_CLASS
		));
		$this->data['product_types'] = $this->utility->get_subcategories(array(
			'level'=>PRODUCT_TYPE
		));
		$this->data['product_subtypes'] = $this->utility->get_subcategories(array(
			'level'=>PRODOCUT_SUBTYPE
		));
		
		if($seller = $this->yo_user->get_current_seller()) {

		}
		//TODO: Load necessary models & populate data
		if($product_id === NULL)
		{
			//TODO: Add flashdata of sth like 'product not found' message
			redirect(site_url('merchant/products'), 'location', 301);
			return;
		} else {
			$this->data['product'] = $this->utility->get_single_product($product_id,FALSE);
			$this->data['form_action'] = 'merchant/product_update/'.$product_id;
		}
		//TODO: Check if the product belongs to the current seller ~ Seller library
		$this->_render_page('seller/products/manage',$this->data,$this->quill_css_link,$this->quill_js_link);
	}

	public function product_delete($product_id)
	{
		if(! $this->input->is_ajax_request()) {
			redirect(site_url('merchant/products'), 'location', 301);
		}
		$product = $this->utility->get_single_product($product_id);
		$result = $this->seller_lib->delete_product($product_id, $_POST);
		$product_images = $this->utility->get_product_images($product_id);

		if(!isset($product)) {
			$result = FALSE;
		}
		if($result === TRUE)
		{
			foreach ($product_images as $key => $image) {
				// delete image from assets folder and db
				$image_file_result = $this->seller_lib->delete_product_image($image, $product_id, TRUE);
			}
		}

		echo json_encode([
			'ok' => $result,
			'message' => $result ? @$product->product_name." has been deleted": "There was an error while trying to delete ".@$product->product_name
			]);
	}

	public function product_review($product_id)
	{

	}
	// Sales over a timeframe. Filter for timeframe is available
	public function sales()
	{
		$this->data['page_title'] = get_page_title('Sales');
		$this->data['nav_items']['sales']['active'] = TRUE;

		$this->data['sales'] = $this->seller_lib->get_sales(array(
			TBL_ORDERS.'.seller_id'=>$this->current_seller->id
		));
		$this->_render_page('seller/sales/list',$this->data,NULL,$this->yo_js_link);
	}

	// Orders made on products by the current seller. Sorted from active to completed to returned
	public function orders($filter=NULL)
	{
		$this->load->library('order');

		$this->data['page_title'] = get_page_title('Orders');
		$this->data['nav_items']['orders']['active'] = TRUE;
		
		/* Filters
		- Completed
		- Pending/Processing - Orders that may be in transit/Yet to be confirmed by the logistics center
		- Returned - Orders that may have been cancelled by a user and returned to manufacturer. Stock should be automatically updated once items are received
		> (**Important:** Make sure no personal user info is sent to manufacturers)
		*/
		$this->data['orders'] = $this->order->get_orders(array(
			TBL_ORDERS.'.seller_id' => @$this->current_seller->id,
		))->result();
		
		//TODO: Load necessary models & populate data
		//TODO: Check for filters ~ Seller library/search library
		$this->_render_page('seller/orders/list',$this->data,NULL,$this->yo_js_link);
	}

	// Orders over a timeframe. Filter for timeframe is available
	public function order_details($order_id)
	{
		$this->load->library('order');
		$this->data['page_title'] = get_page_title('Order '.$order_id.' details');
		$this->data['nav_items']['orders']['active'] = TRUE;
		//TODO: Get order with $order_id

		//TODO: Load necessary models & populate data
		//TODO: Check for filters ~ Seller library/search library
		$this->_render_page('seller/orders/details',$this->data,NULL,$this->yo_js_link);
	}
	
	// A list of customers for the current seller
	public function customers()
	{
		$this->data['page_title'] = get_page_title('Customers');
		$this->data['nav_items']['customers']['active'] = TRUE;
		// *TODO: [Icebox] Allow for viewing customer details (purchases from specific customer). Possibly privacy breech. This will be a future consideration for future devs
		
		//TODO: Load necessary models & populate data
		$this->_render_page('seller/customers/list',$this->data,NULL,$this->yo_js_link);
	}
	
	// Reports ~ a summary of reports for the current seller
	public function reports()
	{
		$this->data['page_title'] = get_page_title('Reports');
		$this->data['nav_items']['reports']['active'] = TRUE;

		//TODO: Load necessary models & populate data
		$this->_render_page('seller/reports/summary',$this->data,NULL,$this->yo_js_link);
	}
	
	// Reports ~ a summary of sales reports for the current seller
	public function reports_sales()
	{
		$this->data['page_title'] = get_page_title('Sales Reports');
		$this->data['nav_items']['reports']['active'] = TRUE;

		//TODO: Load necessary models & populate data
		$this->_render_page('seller/reports/sales',$this->data,NULL,$this->yo_js_link);
	}

	// Reports ~ a summary of order reports for the current seller
	public function reports_orders()
	{
		$this->data['page_title'] = get_page_title('Order Reports');
		$this->data['nav_items']['reports']['active'] = TRUE;

		//TODO: Load necessary models & populate data
		$this->_render_page('seller/reports/orders',$this->data,NULL,$this->yo_js_link);
	}
	
	// Profile
	public function profile()
	{
		$this->data['page_title'] = get_page_title('Profile');
		$this->data['nav_items']['profile']['active'] = TRUE;
		$this->data['user'] = $this->current_seller;
		
		//TODO: Load necessary models & populate data
		$this->_render_page('seller/profile/basic',$this->data,NULL,$this->yo_js_link);
	}
	
	// Profile edit
	public function profile_edit()
	{
		$this->data['page_title'] = get_page_title('Edit profile');
		$this->data['nav_items']['profile']['active'] = TRUE;
		$this->data['user'] = $this->current_seller;

		if($this->input->post()) {
			$this->data['message'] = $this->yo_user->update_profile($this->current_seller->id,'merchant/profile/edit');
		}
		$this->data['message'] = $this->session->flashdata('message');
		//TODO: Load necessary models & populate data
		$this->_render_page('seller/profile/manage',$this->data,NULL,$this->yo_js_link);
	}
	
	// Payment methods
	public function profile_payment_methods()
	{
		$this->data['page_title'] = get_page_title('Payment Methods');
		$this->data['nav_items']['profile']['active'] = TRUE;

		$this->load->library('payment');
		// Get current user payment options
		$this->data['payment_options'] = $this->payment->get_payment_info($this->current_seller_id);#TODO: Get from model/library
		
		$this->_render_page('seller/profile/payment_methods',$this->data,NULL,$this->yo_js_link);
	}

	public function payment_edit($payment_info_id)
	{
		$this->load->library('payment');
		$message_result = $this->payment->payment_method_edit($payment_info_id, $this->current_seller_id);

		$this->session->set_flashdata('message', $message_result);
		$this->session->keep_flashdata('message');
		redirect(site_url('merchant/profile/payment_methods'), 'location', 301);
	}

	// Ratings on the current seller's products
	public function ratings()
	{
		$this->data['page_title'] = get_page_title('Ratings');
		$this->data['nav_items']['ratings']['active'] = TRUE;
		$this->data['reviews'] = [];

		$seller_products = $this->utility->get_products(array(
			'seller_id' => @$this->current_seller->id ?? 1
		));

		foreach($seller_products as $product)
		{
			$product_reviews = $this->utility->get_product_reviews($product->id);
			array_push($this->data['reviews'], $product_reviews);
		}
		if(! empty($this->data['reviews']))
		{
			$this->data['reviews'] = call_user_func_array('array_merge', $this->data['reviews']);
		}
		
		//TODO: Add redirect link to actual product ratings on site
		//TODO: Load necessary models & populate data
		$this->_render_page('seller/ratings/list',$this->data);
	}
	public function product_ratings($review_id)
	{
		$this->data['nav_items']['ratings']['active'] = TRUE;
		
		$this->data['review'] = $this->utility->get_single_review($review_id);
		$product = $this->utility->get_single_product($this->data['review']->product_id);
		$this->data['page_title'] = get_page_title('Review ratings for '.$product->product_name);
		$this->data['user'] = $this->current_seller;
		$this->data['review_replies'] = $this->utility->get_review_replies($review_id);
		$this->_render_page('seller/ratings/single_product',$this->data,NULL,$this->yo_js_link);
	}
	// Current seller notifications
	public function notifications()
	{
		$this->load->library('notification_lib');
		$this->data['page_title'] = get_page_title('Notifications');
		$this->data['new_notifications'] = $this->notification_lib->get_new_notifications($this->current_seller_id);
		$this->data['important_notifications'] = $this->notification_lib->get_notifications($this->current_seller_id, array(
			'is_important' => TRUE
		));
		$this->data['notifications'] = $this->notification_lib->get_notifications($this->current_seller_id, array(
			'is_important' => FALSE
		));
		
		$this->_render_page('seller/notifications/list',$this->data,NULL,$this->yo_js_link);
	}
	
	public function product_create() 
	{
		if(! $this->input->is_ajax_request()) {
			redirect(site_url('merchant/product/add'), 'location', 301);
			return;
		}
		$data = $this->input->post();
		echo json_encode(['success' => $this->seller_lib->create_product($data)]);
	}
	
	public function product_update($product_id) 
	{
		if(! $this->input->is_ajax_request()) {
			redirect(site_url('merchant/product/edit'.$product_id), 'location', 301);
			return;
		}
		$data = $this->input->post();
		// Calculate commission and seller dues
		$data['yoamar_commission'] = get_commission($data['unit_price']);
		$data['seller_dues'] = get_seller_dues($data['unit_price']);

		$product_images = $_FILES;
		
		$product_description = $data['description'];

		$update_product_status = $this->product_model->update_product(array(
			'id'=>$product_id
		),$data);

		$create_product_images_status = TRUE;
		//Check if there are any product images and add them if any
		if(is_array($product_images) && !empty($product_images))
		{
			$update_product_image_status = $this->seller_lib->_product_images_upload_handler($product_images, $product_id, $product_description, TRUE);
		} else {
			//update only the image alt_text
			$update_product_image_status = $this->product_model->update_product_images(array(
				'product_id' => $product_id
			), array(
				'alt_text' => $product_description
			));
		}

		echo json_encode(['success' => ($update_product_status && $update_product_image_status)]);
	}
}

/* End of file Seller.php */
