<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller 
{
	protected $data;	
	protected $current_admin;
	protected $yo_js_link;

	public function __construct()
	{
		// __construct($default_navigation,$profile_url, $notifications_url, $default_container='_templates/admin/body_container',$default_footer_view='_templates/admin/footer')
		parent::__construct('_components/admin/side_nav','admin/profile','admin/notifications');
		
		$this->current_admin = $this->yo_user->get_current_admin() ?? NULL;
		$this->redirect_to_login($this->current_admin,'admin');

		$this->data['brand_title'] = 'Admin Panel';

		//Navigation items
		$this->data['nav_items'] = array(
			'dashboard'=> array(
				'active'=>FALSE,
				'link'=>site_url('admin/dashboard'),
				'text'=>'Dashboard',
			),
			'products'=> array(
				'active'=>FALSE,
				'link'=>site_url('admin/products'),
				'text'=>'Products',
			),
			'search_queries'=> array(
				'active'=>FALSE,
				'link'=>site_url('admin/search_queries'),
				'text'=>'Search Queries',
			),
			'buyer_accounts'=> array(
				'active'=>FALSE,
				'link'=>site_url('admin/accounts/buyer'),
				'text'=>'Buyer Accounts',
			),
			'seller_accounts'=> array(
				'active'=>FALSE,
				'link'=>site_url('admin/accounts/seller'),
				'text'=>'Seller Accounts',
			),
			'logistics_accounts'=> array(
				'active'=>FALSE,
				'link'=>site_url('admin/accounts/logistics'),
				'text'=>'Logistics Accounts',
			),
			'admin_accounts'=> array(
				'active'=>FALSE,
				'link'=>site_url('admin/accounts/admin'),
				'text'=>'Admin Accounts',
			),
			'profile'=> array(
				'active'=>FALSE,
				'link'=>site_url('admin/profile'),
				'text'=>'Profile',
			),
			'logout'=> array(
				'active'=>FALSE,
				'link'=>site_url('auth/logout'),
				'text'=>'Logout',
			),
		);
		$this->yo_js_link = $this->get_script_string('admin/js/yo-es.js');
	}
	
	// Index - Admin Dashboard
	public function index()
	{
		$this->dashboard();
	}

	// Dashboard 
	public function dashboard()
	{
		$this->data['page_title'] = get_page_title('Admin Dashboard');
		$this->data['nav_items']['dashboard']['active'] = TRUE;
		
		// Get search query data
		$this->load->library('search');
		$this->data['search_queries'] = $this->search->get_search_queries(NULL,10);

		// Get account data
		$this->data['buyer_accounts'] = $this->yo_user->get_users('buyer');
		$this->data['merchant_accounts'] = $this->yo_user->get_users('merchant');
		$this->data['admin_accounts'] = $this->yo_user->get_users('admin');
		$this->data['logistics_accounts'] = $this->yo_user->get_users('logistics');

		// Get product data
		$this->data['products'] = $this->utility->get_products(NULL,FALSE);

		// Get order and shipment data
		$this->load->model('logistics_model');
		$this->data['items_shipped'] = $this->logistics_model->get_warehouse_items()->result_object();
		$this->data['items_delivered'] = $this->logistics_model->get_shipments(array(
			TBL_SHIPMENTS.'.is_delivered'=>TRUE
		))->result_object();

		$this->load->library('order');
		$this->data['orders'] = $this->order->get_orders()->result_object();

		$this->_render_page('admin/dashboard/dashboard',$this->data,NULL,$this->yo_js_link);
	}

	public function products()
	{
		$this->data['page_title'] = get_page_title('Products');
		$this->data['nav_items']['products']['active'] = TRUE;

		$this->data['products']['to_verify'] = $this->utility->get_products(array(
			'is_verified' => FALSE
		), FALSE);

		$this->data['products']['products'] = $this->utility->get_products(array(
			'is_verified' => TRUE
		), FALSE, NULL, 5);

		$this->data['products'] = (object) $this->data['products'];
		$this->_render_page('admin/seller/products/list',$this->data,NULL,$this->yo_js_link);
	}
	
	// Search queries
	public function search_queries()
	{
		$this->data['page_title'] = get_page_title('Admin Dashboard');
		$this->data['nav_items']['search_queries']['active'] = TRUE;
		
		// Get search query data
		$this->load->library('search');
		$filters = [];
		$country_id = $this->input->get('country');
		
		if(!empty($country_id))
		{	$filters[TBL_SEARCH_QUERIES.'.country_id'] = $country_id;	}

		$this->data['search_queries'] = $this->search->get_search_queries($filters,10);

		$this->load->helper('form');
		$this->data['countries'] = $this->utility->get_countries(NULL);

		$this->_render_page('admin/search/search_queries',$this->data,NULL,$this->yo_js_link);
	}
	

	// Buyer accounts 
	public function buyer_accounts()
	{
		$this->data['page_title'] = get_page_title('Buyer Accounts');
		$this->data['nav_items']['buyer_accounts']['active'] = TRUE;

		$this->data['accounts'] = $this->yo_user->get_users('buyer');

		$this->_render_page('admin/buyer/list',$this->data,NULL,$this->yo_js_link);
	}

	// View seller accounts 
	public function seller_accounts()
	{
		$this->data['page_title'] = get_page_title('Seller Accounts');
		$this->data['nav_items']['seller_accounts']['active'] = TRUE;

		$this->data['accounts'] = $this->yo_user->get_users('merchant');

		$this->_render_page('admin/seller/list',$this->data,NULL,$this->yo_js_link);
	}

	// View logistics accounts 
	public function logistics_accounts()
	{
		$this->data['page_title'] = get_page_title('Logistics Accounts');
		$this->data['nav_items']['logistics_accounts']['active'] = TRUE;

		$this->data['accounts'] = $this->yo_user->get_users('logistics');
		$this->data['message'] = $this->session->flashdata('message');

		$this->data['account_type'] = 'logistics';

		$this->_render_page('admin/logistics/list',$this->data,NULL,$this->yo_js_link);
	}

	#region User registration
	// Set the various form validation rules for validating registration form input
	private function _validate_registration($user_group,$tables,$identity_column)
	{
		$this->load->library('form_validation');
		
		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'trim|required');
		$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'trim|required');

		$email_validation_messages = array(
			'is_unique'=>'An account with that email address already exists. Please try another one <a href="">link</a>'
		);

		// If the email is the identity column, validate the identity
		if ($identity_column !== 'email')
		{
			$this->form_validation->set_rules('identity', $this->lang->line('create_user_validation_identity_label'), 'trim|required|is_unique[' . $tables['users'] . '.' . $identity_column . ']',$email_validation_messages);
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email');
		}
		else
		{
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]',$email_validation_messages);
		}

		$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
		
		$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim'.($user_group == 'merchant' ? '|required':''));
	}
	
	// Select password
	private function _generate_password($user_data)
	{
		return $user_data['last_name'].'_'.$user_data['phone'].'!';
	}

	// If registration information was found in POST variables, handle the registration
	private function _handle_registration($user_group)
	{
		// Convert to lowercase ~ meaning we can enter the user group in any case we desire
		$user_group = strtolower($user_group); # If we want to be strict with the case, remove this

		//TODO: Consider refactoring the 
		// Set the relevant data
		$tables = $this->config->item('tables', 'ion_auth');
		$identity_column = $this->config->item('identity', 'ion_auth');

		$this->_validate_registration($user_group,$tables,$identity_column);

		// If the form was successfully validated
		if ($this->form_validation->run() === TRUE)
		{
			$email = strtolower($this->input->post('email'));
			$identity = ($identity_column === 'email') ? $email : $this->input->post('identity');

			$this->load->helper('geo');
			$geo_data = get_visitor_geodata();

			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'company' => $this->input->post('company'),
				'phone' => $this->input->post('phone'),
				'country_id' => $this->input->post('country_id') ?? @$geo_data['country_id'],
				'city' => $this->input->post('city') ?? @$geo_data['city']
			);
			
			// Generate password
			$password = $this->_generate_password($additional_data);

			switch($user_group)
			{
				case 'admin': 
					$group = [1];
				break;
				case 'logistics':
					$group = [4];
				break;
			}
		}

		// Registration was successful
		if ($this->form_validation->run() === TRUE && $this->ion_auth->register($identity, $password, $email, $additional_data, $group))
		{
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("admin/accounts/".$user_group, 'refresh');
		}
		else
		{// If form validation failed, set the value of the form inputs to their previous values
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = array(
				'name' => 'first_name',
				'id' => 'first_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('first_name'),
			);
			$this->data['last_name'] = array(
				'name' => 'last_name',
				'id' => 'last_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('last_name'),
			);
			$this->data['identity'] = array(
				'name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['email'] = array(
				'name' => 'email',
				'id' => 'email',
				'type' => 'text',
				'value' => $this->form_validation->set_value('email'),
			);
			$this->data['company'] = array(
				'name' => 'company',
				'id' => 'company',
				'type' => 'text',
				'value' => $this->form_validation->set_value('company'),
			);
			$this->data['phone'] = array(
				'name' => 'phone',
				'id' => 'phone',
				'type' => 'text',
				'value' => $this->form_validation->set_value('phone'),
			);
			$this->data['password'] = array(
				'name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'value' => $this->form_validation->set_value('password'),
			);
			$this->data['password_confirm'] = array(
				'name' => 'password_confirm',
				'id' => 'password_confirm',
				'type' => 'password',
				'value' => $this->form_validation->set_value('password_confirm'),
			);
		}
	}
	// Set account data
	private function _set_account_data()
	{
		$this->load->library('form_validation');

		$tables = $this->config->item('tables', 'ion_auth');
		$identity_column = $this->config->item('identity', 'ion_auth');
		$this->data['identity_column'] = $identity_column;
	}
	#endregion User registration

	// Add logistics account 
	public function logistics_add()
	{
		$this->data['page_title'] = get_page_title('Add Logistics Account');
		$this->data['nav_items']['logistics_accounts']['active'] = TRUE;

		$this->data['account_type'] = 'logistics';
		
		$this->load->helper('form');
		$this->lang->load('auth');

		$this->_set_account_data();
		$this->_handle_registration('logistics');

		$this->_render_page('admin/accounts/create_account',$this->data,NULL,$this->yo_js_link);
	}

	// View admin accounts 
	public function admin_accounts()
	{
		$this->data['page_title'] = get_page_title('Admin Accounts');
		$this->data['nav_items']['admin_accounts']['active'] = TRUE;

		$this->data['account_type'] = 'admin';

		$this->data['message'] = $this->session->flashdata('message');
		$this->data['accounts'] = $this->yo_user->get_users('admin');

		$this->_render_page('admin/admin/list',$this->data,NULL,$this->yo_js_link);
	}
	
	// Add admin account 
	public function admin_add()
	{
		$this->data['page_title'] = get_page_title('Add Admin Account');
		$this->data['nav_items']['admin_accounts']['active'] = TRUE;
				
		// Redirect to this url on form submit
		$this->data['account_type'] = 'admin';
		
		$this->load->helper('form');
		$this->lang->load('auth');

		$this->_set_account_data();
		$this->_handle_registration('admin');

		$this->_render_page('admin/accounts/create_account',$this->data,NULL,$this->yo_js_link);
	}

	// Admin profile
	public function profile()
	{
		$this->data['page_title'] = get_page_title('Profile');
		$this->data['nav_items']['profile']['active'] = TRUE;
		$this->data['user'] = $this->current_admin;

		//TODO: Load necessary models
		$this->_render_page('admin/profile/basic',$this->data,NULL,$this->yo_js_link);
	}
	
	// Profile edit
	public function profile_edit()
	{
		$this->data['page_title'] = get_page_title('Edit profile');
		$this->data['nav_items']['profile']['active'] = TRUE;
		$this->data['user'] = $this->current_admin;

		if($this->input->post()) {
			$this->data['message'] = $this->yo_user->update_profile($this->current_admin->id,'admin/profile/edit');
		} else {
			$this->data['message'] = $this->session->flashdata('message');
		}
		//TODO: Load necessary models & populate data
		$this->_render_page('admin/profile/manage',$this->data,NULL,$this->yo_js_link);
	}

	public function logout()
	{
		redirect(site_url('auth/logout'), 'location', 301);
	}
}

/* End of file Admin.php */
