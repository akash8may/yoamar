<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	protected $current_user;
	protected $current_user_id;

	// Constructor
	public function __construct()
	{
		parent::__construct();

		// Load libraries
		$this->load->library(array( 'api_lib', 'yo_cart', 'buyer_lib', 'yo_review' ));

		// Initialize members
		$this->current_user = $this->yo_user->get_current_user() ?? NULL;
		$this->current_user_id = @$this->current_user->id ?? NULL;

		// TODO: Check if an API key has been provided. Reject request if not
		if(!$this->api_lib->is_valid_request())
		{
			redirect(site_url('api/error'),'location','403');
		}
	}

	//	get current cart
	public function get_cart()
	{

		$cookie = $this->yo_cart->get_cart();
		$TotalProducts = $this->utility->get_products(NULL,FALSE);# Get data from model
		$TotalProducts= array_combine(array_column((array) $TotalProducts, 'id'),$TotalProducts); 
		$totalitom=0;
		foreach($cookie as $itom){

			$product = $TotalProducts[$itom->product_id];
			if(!($product->is_available == '0' || $product->is_available == FALSE || $product->is_verified == '0' || $product->is_verified == FALSE || (int)$product->quantity == 0)){
					$totalitom += $itom->quantity;
			}
		}
		print_r($totalitom);
		exit;
		//return $this->api_lib->get_response(true,'',$data);
	}


	#region cart
	// Add to cart
	public function add_to_cart()
	{
		//* Expected params - $product_id,$quantity
		$product_id = $this->input->post('product_id');
		$quantity = (int)$this->input->post('quantity');

		$is_success = $this->yo_cart->add_to_cart($product_id,$quantity);
		
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully added item to cart' : 'Failed to add item to cart'
		);
	}
	// Update cart item
	public function update_cart_item()
	{
		//* Expected params - $product_id,$quantity
		$product_id = $this->input->post('product_id');
		$quantity = $this->input->post('quantity');

		$is_success = $this->yo_cart->update_cart_item($product_id,$quantity);
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully updated item from cart' : 'Failed to update item to cart'
		);
	}
	// Remove from cart
	public function remove_cart_item()
	{
		//* Expected params - $product_id
		$product_id = $this->input->post_get('product_id');
				
		$is_success = $this->yo_cart->remove_cart_item($product_id);
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully removed item from cart' : 'Failed to remove item to cart'
		);
	}

	// Save for later
	public function save_item_for_later()
	{
		//* Expected params - $product_id
		$product_id = $this->input->post_get('product_id');

		$is_success = $this->yo_cart->save_item_for_later($product_id);
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully saved item for later' : 'Failed to save item for later'
		);
	}
	
	// Remove saved item
	public function remove_saved_item()
	{
		//* Expected params - $product_id
		$product_id = $this->input->post_get('product_id');

		$is_success = $this->yo_cart->remove_saved_item($product_id);
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully removed saved item' : 'Failed to remove saved item'
		);
	}
	#endregion

	#region wishlist
	// Add to wishlist
	public function add_to_wishlist()
	{
		//* Expected params - $product_id,$is_private
		$product_id = $this->input->post_get('product_id');
		$is_private = json_decode($this->input->post_get('is_private')) ?? TRUE;

		if($is_private)
		{
			$is_success = $this->yo_cart->add_to_private_wishlist($product_id);
		} else 
		{
			$is_success = $this->yo_cart->add_to_public_wishlist($product_id);
		}
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully added item to '.(($is_private) ? 'private' : 'public').' wishlist' : 'Failed to add item to wishlist'
		);
	}
	// Remove from wishlist
	public function remove_wishlist_item()
	{
		//* Expected params - $product_id
		$product_id = $this->input->post_get('product_id');

		$is_success = $this->yo_cart->remove_wishlist_item($product_id);
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully removed item to wishlist' : 'Failed to remove item to wishlist'
		);
	}
	#endregion

	#region Reviews
	// Review product
	public function review_product()
	{
		//* Expected params - $product_id, $rating, $comment
		$product_id = $this->input->post_get('product_id');
		$rating = $this->input->post_get('rating');
		$comment = $this->input->post_get('comment') ?? '';

		$is_success = $this->buyer_lib->review_product($product_id,$rating,$comment);
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully reviewed product' : 'Failed to review product'
		);
	}
	// Update review
	public function update_review()
	{
		//* Expected params - $product_id, $rating, $comment
		$product_id = $this->input->post('product_id');
		$rating = $this->input->post('rating') ?? NULL;
		$comment = $this->input->post('comment') ?? NULL;

		$is_success = $this->buyer_lib->update_review($product_id,$rating,$comment);
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully updated product review' : 'Failed to update product review'
		);
	}
	// Remove review
	public function remove_review()
	{
		//* Expected params - $product_id
		$product_id = $this->input->post('product_id');

		$is_success = $this->buyer_lib->remove_review($product_id);
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully deleted product review' : 'Failed to delete product review'
		);
	}
	#endregion

	#region review replies
	// Reply to review
	public function reply_to_review()
	{
		//* Expected params - $review_id,$reply_text
		$user_id = $this->current_user_id ?? NULL;
		$review_id = $this->input->post_get('review_id');
		$reply_text = $this->input->post_get('reply_text');

		$is_success = $this->yo_review->reply_to_review($user_id,$review_id,$reply_text);
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully replied to review' : 'Failed to reply to review'
		);
	}
	// Update review reply
	public function update_review_reply()
	{
		//* Expected params - $review_id,$reply_text
		$user_id = $this->current_user_id;
		$reply_id = $this->input->post_get('review_id');
		$reply_text = $this->input->post_get('reply_text');

		$is_success = $this->yo_review->update_review_reply($user_id,$reply_id,$reply_text);
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully updated the review reply' : 'Failed to update the review reply'
		);
	}
	// Remove review reply
	public function delete_review_reply()
	{
		//* Expected params - $review_id,$reply_text
		$user_id = $this->current_user_id;
		$reply_id = $this->input->post_get('reply_id');

		$is_success = $this->yo_review->delete_review_reply($user_id,$reply_id);
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully deleted review reply' : 'Failed to delete review reply'
		);
	}
	#endregion

	#region Categories

	// Get category groups
	public function get_category_groups()
	{
		//* Expected params - $filters (array)
		$filters = $this->input->get('filters') ?? NULL;

		$category_groups = $this->utility->get_category_groups($filters);
		$is_success = !empty($category_groups);

		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully retrieved category groups' : 'Failed to retrieve category groups',
			$category_groups
		);
	}

	// Get categories
	public function get_categories()
	{
		//* Expected params - $filters (array)
		$filters = $this->input->get('filters') ?? NULL;

		$categories = $this->utility->get_categories($filters);
		$is_success = !empty($categories);

		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully retrieved categories' : 'Failed to retrieve categories',
			$categories
		);
	}

	// Get categories
	public function get_subcategories()
	{
		//* Expected params - $level (string|int)
		$level = $this->input->get('level') ?? NULL;

		$filters = $filters ?? [];
		if(!empty($level))
		{
			$filters['level'] = $level;
		}

		$subcategories = $this->utility->get_subcategories($filters);
		$is_success = !empty($categories);

		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully retrieved sub-categories' : 'Failed to retrieve payment info',
			$subcategories
		);
	}
	#endregion Categories
	
	public function get_current_user()
	{
		$data = $this->current_user ?? FALSE;
		return $this->api_lib->print_response(
			($data === FALSE)? FALSE : TRUE,
			($data === FALSE)? 'The user has not signed in' : 'Successfully retrieved user info',
			$data
		);
	}
	
	public function user_logged_in()
	{
		$data = $this->yo_user->user_logged_in();
		return $this->api_lib->print_response(
			TRUE,
			($data === FALSE)? 'The user has not signed in' : 'User is signed in',
			$data
		);
	}

	public function verify_recaptcha()
	{
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$data = $this->input->post('_data');
		if($data);
			$data = json_decode($data);
		// $response = $this->input->post('_data');
		// $secret = $this->input->post('secret');
		// return;
		if(!isset($data))
		{
			return $this->api_lib->print_response(
				FALSE,
				'Token or secret key is invalid',
				[FALSE]
			);
		} else 
		{
			$response = $this->api_lib->post_request($url, $data);

			var_dump($response);
		}
	}

	// Error page
	public function error()
	{
		return $this->api_lib->print_response(FALSE,'An error occured while trying to make the Api request. Sorry');
	}

	public function get_single_product($product_id)
	{
		$product = $this->utility->get_single_product($product_id);
		$cartdata = $this->yo_cart->get_cart();		
		$qty=1;
		if( !empty($cartdata)){
			foreach($cartdata as $itom){
				if($itom->product_id == $product_id)
					$qty=$itom->quantity;
			}
			
		}
		$product->alreadySelectedQty=$qty;
		$result = empty($product) ? FALSE : TRUE;
		return $this->api_lib->print_response(
			$result,
			($result === FALSE)? 'Product was not found' : 'Product is found',
			$product
		);
	}
	#region payment
	
	public function get_payment_info()
	{
		//* Expected params - $level (string|int)
		$id = $this->input->get('id') ?? NULL;
		$type = $this->input->get('type') ?? NULL;
		$this->load->library('payment');
		$info = $this->payment->get_payment_info($this->current_user_id, array(
			'id'=> $id,
		));
		$is_success = !empty($info);
		$info = (!empty($type)) ? $info[$type] : $info;
		
		return $this->api_lib->print_response(
			$is_success,
			$is_success ? 'Successfully retrieved payment info' : 'Failed to retrieve payment',
			$info
		);
	}

	public function test()
	{
		$this->load->library('mpesa_lib');
		return $this->api_lib->print_response(
			true,
			'Message',
			json_decode($this->mpesa_lib->get_request(
				'https://jsonplaceholder.typicode.com/posts/1',
				array(
					'title'=> 'bruh',
					'body'=> 'bar',
					'userId'=> 1
				)
			))
		);
	}

	// Mpesa test
	public function lipa_na_mpesa()
	{
		//TODO: Get information on the product being paid for

		$phone = $this->input->post('phone_number');
		if(!isset($phone)) {
			return;

		}

		$this->load->library('mpesa_lib');
		$response = $this->mpesa_lib->lipa_na_mpesa($phone,1200);
		
		// return;
		return  $this->api_lib->print_response(
			(@$response->ResponseCode == $result_code['success']),
			$response->CustomerMessage ?? 'Error while trying to pay via mpesa',
			$response
		);
	}

	public function complete_mpesa()
	{
		echo 'vvvv';
		redirect(base_url('u/checkout'), "refresh", 301);
	}

	// Paypal authorize payment. This is the first step
	public function authorize_paypal_payment()
	{	
		//TODO: Get information on the product being paid for
		$this->load->library(['omnipay_gateway', 'payment']);
		$user = $this->yo_user->get_current_buyer()['buyer'];
		$amount = $this->yo_cart->get_cart_amount();
		
		$response = $this->omnipay_gateway->authorize_paypal_payment(
			[
				'description' => $user->username.'\'s checkout',
				'currency' => $currency ?? 'USD',
				'amount' => $amount['total_amount'],
			]);

		// var_dump($response);
		if( $response['ok'] === TRUE)
		{
			$this->data['transaction'] = json_encode($response['data']);
			$this->session->set_flashdata('transaction', $response['data']);
			$response['message'] = "PayPal authorized";
		} else {
			$response['ok'] = FALSE;
		}
		$response['data'] = $response['data'] ?? [];
		return  $this->api_lib->print_response(
			$response['ok'],
			$response['message'] ?? 'Error while trying to pay via Paypal',
			$response['data']
		);
	}

	// Paypal complete the purchase. This is the final step
	public function complete_paypal_payment()
	{
		$this->load->library('omnipay_gateway');
		
		$transaction = $this->session->flashdata('transaction');
		
		$response = $this->omnipay_gateway->complete_paypal_payment(
			[
				'paymentID' => $this->input->post('paymentID'),
				'payerID' => $this->input->post('payerID'),
			]
		);
		if($response['ok'] === TRUE)
		{
			$response_message = $response['message'] ?? "Successfully paid";
		} else {
			$response_message = $response['message'];
		}

		return  $this->api_lib->print_response(
			$response['ok'],
			$response_message ?? 'Error while trying to complete purchase via Paypal',
			$response
		);
	}
	#endregion payment

	#region notifications
	public function get_new_user_notifications()
    {
		$this->load->library('notification_lib');
		
		$response['data'] = $this->notification_lib->get_new_notifications($this->current_user_id);

		return $this->api_lib->print_response(
			TRUE,
			$response['message'] ?? $response['data'] == null ? 'no new notifications' : 'You have new notifications',
			$response['data']
		);
    }
	
	public function mark_notification_read()
	{
		$this->load->library('notification_lib');

		$notification_id = $this->input->post('id');

		if(!$notification_id) {
			return $this->api_lib->print_response(
				FALSE,
				'Internal error \n Please provide a notification id.',
				[]
			);
		}

		$response = $this->notification_lib->mark_notification_read($notification_id);

		return $this->api_lib->print_response(
			$response,
			$response ? 'Success' : 'Sorry we could not mark notification as read',
			[]
		);
	}
	#endregion notifications
	
	#region Admin accessible
	/* 
		ADMIN ACCESSIBLE - APIs that can only be called by an admin
	*/
	// Helper, creates an account of the specified type
	private function _create_account($group,$api_alternative_condition)
	{ /* Expects $identity, $password, $email, $data as POST variables */
		// If admin is not logged in and we try to access this endpoint 
		if(!$this->yo_user->admin_logged_in() || $api_alternative_condition)
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}

		$identity = $this->input->post('identity');
		$password = $this->input->post('password');
		$email = $this->input->post('email');
		$data = $this->input->post('data');
		
		$created_user_id = $this->ion_auth->create_user($identity,$password,$email,$data,array($group));# '1'1 is the user group for Admin
		$is_ok = $created_user_id !== FALSE;

		$response_message = $is_ok ? 'Successfully created admin': 'Failed to create admin';
		$response_data = $is_ok ? array('id'=>$created_user_id) : NULL;
		
		return $this->api_lib->print_response($is_ok,$response_message,$response_data);
	}

	// [POST] Create admin account
	public function create_admin_account()
	{ /* Expects $identity, $password, $email, $data as POST variables */
		return $this->_create_account('admin');
	}

	// [POST] Create logistics account
	public function create_logistics_account()
	{ /* Expects $identity, $password, $email, $data as POST variables */
		return $this->_create_account(
			'logistics',
			!$this->yo_user->logistics_logged_in()
		);
	}

	// [POST] Update admin or logistics account
	public function update_account($user_id)
	{ /* Expects $user_id as url parameter, $data as POST data */
		// If admin is not logged in and we try to access this endpoint 
		if(!$this->yo_user->admin_logged_in())
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}

		// Init $is_ok to allow for function scoping
		$is_ok;

		// Check if the id provided belongs to an admin or logistics account
		if($this->ion_auth->in_group(['admin','logistics'],$user_id))
		{
			$data = $this->input->post('data');
			$is_ok = $this->ion_auth->update($user_id,$data);
		}
		else // If not, $is_ok is FALSE and no need to run UPDATE query
		{
			$is_ok = FALSE;
		}
		
		$response_message = $is_ok ? 'Successfully updated admin': 'Failed to update admin';
		return $this->api_lib->print_response($is_ok,$response_message);
	}

	// [POST] Set the active status of an account. Used by admin to activate/deactivate accounts
	public function update_account_active($user_id)
	{ /* Expects $user_id as url parameter, $is_active as POST data */
		// If admin is not logged in and we try to access this endpoint 
		if(!$this->yo_user->admin_logged_in())
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}

		$is_active = (bool) $this->input->post('is_active');
		$update_data = array(
			'active'=>$is_active
		);
		
		// Init $is_ok to allow for function scoping
		$is_ok = FALSE;
		$response_message = '';

		// Only allow changing the active state if it is not the first admin account ~ which should be immutable
		$first_admin = $this->ion_auth->users(['1'])->row();
		if(!($user_id == $first_admin->user_id))
		{	
			$is_ok = $this->ion_auth->update($user_id,$update_data);
			$response_message = $is_ok ? 'Successfully updated active state' : 'Failed to update active state';
		}
		else
		{
			$is_ok = FALSE;
			$response_message = 'Failed to update account active state. Cannot change the active state of the base root account';
		}

		return $this->api_lib->print_response($is_ok,$response_message);
	}

	// [POST] Delete account
	public function delete_account($user_id)
	{
		// If admin is not logged in and we try to access this endpoint 
		if(!$this->yo_user->admin_logged_in())
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}
		// Init $is_ok to allow for function scoping
		$is_ok;

		$response_message = 'Failed to delete admin';
		$first_admin = $this->ion_auth->users(array('1'))->row();

		// Check if the id provided belongs to the first admin
		if(!($user_id == $first_admin->id))
		{
			$is_ok = $this->ion_auth->delete_user($user_id);
			$response_message = $is_ok ? 'Successfully deleted admin': $response_message;
		}
		else // If not, $is_ok is FALSE and no need to run UPDATE query
		{
			$is_ok = FALSE;

			// If the user we are trying to delete is the first admin ~ set the custom error 
			if($user_id == $first_admin->id)
			{
				$response_message = 'Cannot delete root admin account. There must be at least one admin account.';
			}
		}

		return $this->api_lib->print_response($is_ok,$response_message);
	}
	
	// Get products
	public function get_products()
	{ /* Expects search filters */
		// If admin is not logged in and we try to access this endpoint 
		if(!$this->yo_user->admin_logged_in())
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}
		
		$filters = $this->input->get('filters');
		$limit = $this->input->get('limit') ?? 10;
		$in_stock = $this->input->get('in_stock') ?? TRUE;
		$in_stock = (bool)$in_stock;
		
		// Library related functionality
		$data = $this->utility->get_products($filters, $in_stock, TRUE, $limit);
		$is_ok = !empty($data);

		$this->api_lib->print_response($is_ok,'',$data);
	}

	// Admin verify product
	public function verify_product($product_id)
	{
		// If admin is not logged in and we try to access this endpoint 
		if(!$this->yo_user->admin_logged_in())
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}
		
		$this->load->library('Admin_lib');
		$verify = (bool) $this->input->post('verify') ?? FALSE;
		$product = $this->utility->get_single_product($product_id);
		
		$success_message = $product->product_name.' verified';
		$error_message = 'Error in verifying product';
		
		$response = $this->admin_lib->verify_product($product_id, $verify);

		$this->api_lib->print_response(
			$response,
			($response == TRUE) ? $success_message : $error_message
		);
	}

	#endregion Admin accessible
	
	#region Logistics accessible
	/* 
		LOGISTICS ACCESSIBLE - APIs that can only be called by a logistics account
	*/
	// [POST] request
	public function add_warehouse_item()
	{/* Expects $data*/
		if(!$this->yo_user->logistics_logged_in())
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}

		// Getting the data
		$data = $this->input->post('data');

		// Library related functionality
		$this->load->library('logistics_lib');
		$is_ok = $this->logistics_lib->add_warehouse_item($data);

		$this->api_lib->print_response($is_ok);
	}

	// [GET] request
	public function get_warehouse_items()
	{/* Expects $filters as data and $q as a url param*/
		if(!$this->yo_user->logistics_logged_in())
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}

		$filters = $this->input->get('filters');
		$limit = $this->input->get('limit') ?? 10;
		
		// Library related functionality
		$this->load->library('logistics_lib');
		$data = $this->logistics_lib->get_warehouse_items($filters, $limit, $this->current_user_id)->result_object();
		$is_ok = !empty($data);

		$this->api_lib->print_response($is_ok,'',$data);
	}

	// [POST] request
	public function update_warehouse_items()
	{/* Expects $filters,$data*/
		if(!$this->yo_user->logistics_logged_in())
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}

		// Getting the data
		$filters = $this->input->post('filters');
		$data = $this->input->post('data');

		// Library related functionality
		$this->load->library('logistics_lib');
		$is_ok = $this->logistics_lib->update_warehouse_items($filters,$data);

		$this->api_lib->print_response($is_ok);
	}

	// [POST] request
	public function delete_warehouse_item()
	{/* Expects $filters*/
		if(!$this->yo_user->logistics_logged_in())
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}

		// Getting the data
		$filters = $this->input->post('filters');

		// Library related functionality
		$this->load->library('logistics_lib');
		$is_ok = $this->logistics_lib->delete_warehouse_item($filters);

		$this->api_lib->print_response($is_ok);
	}

	// [POST] request
	public function create_shipments()
	{/* Expects $shipments*/
		if(!$this->yo_user->logistics_logged_in())
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}
		
		// Getting the data
		$shipments = $this->input->post('shipments');

		// Library related functionality
		$this->load->library('logistics_lib');
		$is_ok = $this->logistics_lib->create_shipments($shipments,$current_logistics->id);

		$this->api_lib->print_response($is_ok);
	}

	// [GET] request
	public function get_shipments()
	{/* Expects $filters as data and $q as url param */
		if(!$this->yo_user->logistics_logged_in())
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}

		// Getting the data
		$filters = $this->input->get('filters');
		
		// Library related functionality
		$this->load->library('logistics_lib');
		$data = $this->logistics_lib->get_shipments($filters)->result_object();
		$is_ok = !empty($data);

		$this->api_lib->print_response($is_ok,'',$data);
	}

	// [POST] request
	public function update_shipments()
	{/* Expects $filters,$data */
		if(!$this->yo_user->logistics_logged_in())
		{ // Show access restricted error
			return $this->api_lib->access_restricted_error();
		}

		// Getting the data
		$filters = $this->input->post('filters');
		$data = $this->input->post('data');

		// Library related functionality
		$this->load->library('logistics_lib');
		$is_ok = $this->logistics_lib->update_shipments($filters,$data);

		$this->api_lib->print_response($is_ok);
	}

	#endregion Logistics accessible

	#region merchant accessible
	
	public function change_product_quantity()
	{
		$product_id = $this->input->post('product_id');
		$quantity = $this->input->post('quantity');

		if(empty($product_id) || !isset($quantity)) {
			return $this->api_lib->print_response(
				FALSE,
				'Product id or quantity was not set',
				[]
			);
		} else {
			$this->load->library('seller_lib');
			$result = $this->seller_lib->update_product($product_id, array(
				'quantity' => $quantity
			));

			return $this->api_lib->print_response(
				$result,
				($result === FALSE)? 'Product quantity could not be updated' : 'Product quantity updated',
				[]
			);
		}
	}

	#endregion merchant accessible
}

/* End of file Api.php */
