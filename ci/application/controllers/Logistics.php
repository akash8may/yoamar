<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Logistics extends Admin_Controller {

	protected $data;
	protected $current_logistics;
	protected $autocomplete_css_link;
	protected $yo_js_link;
	protected $dashboard_count_limit = 5;
	// Constructor
	public function __construct()
	{
		// __construct(string $default_navigation,string $default_container,string $default_footer_view=''
		parent::__construct('_components/admin/side_nav','logistics/profile','logistics/notifications');

		$this->load->library(array('form_validation'));
		$this->current_logistics = $this->yo_user->get_current_logistics() ?? NULL;
		$this->redirect_to_login($this->current_logistics,'logistics');

		$this->data['brand_title'] = 'Logistics Panel';

		$this->data['nav_items'] = array(
			'dashboard'=> array(
				'link'=>site_url('logistics/dashboard'),
				'text'=>'Dashboard',
			),
			'add_items'=> array(
				'link'=>site_url('logistics/add'),
				'text'=>'Add Items',
			),
			'warehouse'=> array(
				'link'=>site_url('logistics/warehouse'),
				'text'=>'Warehouse',
			),
			'deploy'=> array(
				'link'=>site_url('logistics/deploy'),
				'text'=>'Deploy',
			),
			'shipped_items'=> array(
				'link'=>site_url('logistics/shipped'),
				'text'=>'Shipped items',
			),
			'profile'=> array(
				'link'=>site_url('logistics/profile'),
				'text'=>'Profile',
			),
			'logout'=> array(
				'link'=>site_url('auth/logout'),
				'text'=>'Logout',
			),
		);		
		$this->autocomplete_css_link = $this->get_css_string('logistics/css/easy-autocomplete.min.css');
		$this->yo_js_link = $this->get_script_string('logistics/js/yo-es.js');
		
		// Load libaries
		$this->load->library('logistics_lib');
		$this->load->helper('form');
	}
	
	// Index - Logistics Dashboard
	public function index()
	{
		$this->dashboard();
	}

	// Dashboard 
	public function dashboard()
	{
		$this->data['page_title'] = get_page_title('Dashboard');
		$this->data['nav_items']['dashboard']['active'] = TRUE;

		// Only select items that are marked as active in the warehouse (aka not 'deleted')
		$filters = array(
			TBL_WAREHOUSE.'.is_active'=>TRUE
		);

		// Ensure result is always an array so we can count the dashboard stats even when empty
		$this->data['warehouse_items'] = $this->logistics_lib->get_warehouse_items($filters,$this->dashboard_count_limit)->result_object() ?? [];

		$this->data['shipped_items'] = $this->logistics_lib->get_shipments($filters,$this->dashboard_count_limit)->result_object() ?? [];
	
		$filters[TBL_SHIPMENTS.'.is_delivered'] = TRUE;
		$this->data['delivered_items'] = $this->logistics_lib->get_shipments($filters,$this->dashboard_count_limit)->result_object() ?? [];

		$this->_render_page('logistics/dashboard/dashboard',$this->data,NULL,$this->yo_js_link);
	}

	// Add product to warehouse 
	public function add_items()
	{
		$this->data['page_title'] = get_page_title('Add items');
		$this->data['nav_items']['add_items']['active'] = TRUE;

		//TODO: Load necessary models
		$this->_render_page('logistics/warehouse/add_items',$this->data,$this->autocomplete_css_link,$this->yo_js_link);
	}

	// Add product to warehouse 
	public function warehouse()
	{
		$this->data['page_title'] = get_page_title('Warehouse');
		$this->data['nav_items']['warehouse']['active'] = TRUE;

		$filters = array(
			TBL_WAREHOUSE.'.is_active'=>TRUE
		);

		$this->data['warehouse_items'] = $this->logistics_lib->get_warehouse_items($filters)->result_object();

		$this->_render_page('logistics/warehouse/warehouse',$this->data,$this->autocomplete_css_link,$this->yo_js_link);
	}

	// Add product to warehouse 
	public function deploy()
	{
		$this->data['page_title'] = get_page_title('Deploy items');
		$this->data['nav_items']['deploy']['active'] = TRUE;

		//TODO: Load necessary models
		$this->_render_page('logistics/shipping/ship_items',$this->data,$this->autocomplete_css_link,$this->yo_js_link);
	}

	// Add product to warehouse 
	public function shipped_items()
	{
		$this->data['page_title'] = get_page_title('Shipped items');
		$this->data['nav_items']['shipped_items']['active'] = TRUE;

		// Only select items that are marked as active in the warehouse (aka not 'deleted')
		$filters = array(
			TBL_WAREHOUSE.'.is_active'=>TRUE
		);

		$this->data['shipped_items'] = $this->logistics_lib->get_shipments($filters)->result_object();
		
		$this->_render_page('logistics/shipping/shipped_items',$this->data,$this->autocomplete_css_link,$this->yo_js_link);
	}

	// Logistics profile
	public function profile()
	{
		$this->data['page_title'] = get_page_title('Profile');
		$this->data['nav_items']['profile']['active'] = TRUE;
		$this->data['user'] = $this->current_logistics;

		//TODO: Load necessary models
		$this->_render_page('logistics/profile/basic',$this->data,NULL,$this->yo_js_link);
	}
	
	// Profile edit
	public function profile_edit()
	{
		$this->data['page_title'] = get_page_title('Edit profile');
		$this->data['nav_items']['profile']['active'] = TRUE;
		$this->data['user'] = $this->current_logistics;

		if($this->input->post()) {
			$this->data['message'] = $this->yo_user->update_profile($this->current_logistics->id,'logistics/profile/edit');
		}
		$this->data['message'] = $this->session->flashdata('message');
		//TODO: Load necessary models & populate data
		$this->_render_page('logistics/profile/manage',$this->data,NULL,$this->yo_js_link);
	}

	// Current logistics notifications
	public function notifications()
	{
		$this->load->library('notification_lib');
		$this->data['page_title'] = get_page_title('Notifications');
		$this->data['new_notifications'] = $this->notification_lib->get_new_notifications($this->current_logistics->id);
		$this->data['important_notifications'] = $this->notification_lib->get_notifications($this->current_logistics->id, array(
			'is_important' => TRUE
		));
		$this->data['notifications'] = $this->notification_lib->get_notifications($this->current_logistics->id, array(
			'is_important' => FALSE
		));

		$this->_render_page('logistics/notifications/list',$this->data,NULL,$this->yo_js_link);
	}
}

/* End of file Logistics.php */
