<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Yo_Error extends CI_Controller {

	public function index()
	{
		//Default error
	}

	//404 ~ Possibly customizable to include custom messages for 404 based on a parameter
	public function error_404()
	{
		$this->load->view('errors/html/error_404');
	}
}

/* End of file Error.php */
