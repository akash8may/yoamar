<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Buyer extends Site_Controller {
	
	protected $data;
	// Currently logged in buyer
	protected $current_buyer;
	protected $current_buyer_user_id;
	
	protected $quill_css_link;
	protected $quill_js_link;
	
	// Constructor
	public function __construct()
	{
		// __construct($default_header_view,$default_footer_view)
		parent::__construct('','');
		
		$this->load->library(array('buyer_lib', 'yo_cart', 'form_validation', 'order', 'notification_lib'));
		//Check if the buyer is logged in ~ if not, redirect to login with message saying restricted access for the content they tried to access
		if(!$this->yo_user->buyer_logged_in())
		{
			//Redirect to login page 
			$this->session->set_flashdata('redirect', 'u/'.($this->uri->segment(2, 0) ?? ''));
			redirect(site_url('auth/login/buyer'), 'location', 301);
		}

		// Set the current user
		$this->current_buyer = $this->yo_user->get_current_buyer();# Get user from library
		$this->current_buyer_user_id = $this->current_buyer['buyer']->id ?? NULL;

		$this->data['user'] = $this->current_buyer['buyer'] ?? FALSE;
		$this->data['categories'] = $this->utility->get_categories();# Get data from library
		$this->data['new_notifications'] = $this->notification_lib->get_new_notifications($this->current_buyer_user_id);

		if(isset($this->current_buyer_user_id)) {
			$this->data['current_order'] = $this->order->get_orders(array(
				'buyer_id' => $this->current_buyer_user_id,
				'status !=' => 'delivered'
			))->row_object(0); 
		}

		// Setting script paths
		$this->quill_css_link = "<link href=\"//cdn.quilljs.com/1.3.6/quill.snow.css\" rel=\"stylesheet\">\n";
		$this->quill_js_link = "<script src=\"//cdn.quilljs.com/1.3.6/quill.min.js\"></script>\n";
	}
	
	// [Profile] Orders - Display current buyer orders 
	public function orders($order_id=NULL)
	{
		
		$this->load->library('payment');
		$this->data['page_title'] = get_page_title('Orders');
		$this->data['message'] = $this->session->flashdata('message');
		//TODO: Get current user payment options
		//TODO: Check for filters
		$this->data['orders'] = [];#TODO: Get from model/library

		// show all orders if order_id is not specified
		if($order_id===NULL)
		{
			$this->data['orders'] = $this->order->get_orders(array(
				'buyer_id' => $this->current_buyer_user_id,
			))->result_object();
			// var_dump($this->data['orders']);
			$this->_render_page('buyer/orders',$this->data);
		} else 
		{
			//get a specific order
			$this->data['orders'] = $this->order->get_orders(array(
				'order_id' => $order_id,
				'buyer_id' => $this->current_buyer_user_id,
			))->result_object();
			
			if(empty($this->data['orders'])) {
				$this->_render_page('buyer/single_order',$this->data);
				return;
			}

			$this->load->config('shipping');
			$shipping_method = $this->data['orders'][0]->shipping_method;
			$this->data['shipping_data']['shipping_cost'] = $this->config->item($this->data['orders'][0]->shipping_method);

			switch ($shipping_method) {
				case 'air_shipping_cost':
					$this->data['shipping_data']['method'] = 'Air';
					break;
				case 'ems_shipping_cost':
					$this->data['shipping_data']['method'] = 'EMS';
					break;
				default:
					$this->data['shipping_data']['method'] = 'Ship';
					break;
			}	
			$this->data['shipping_details'] = $this->order->get_shipping_details(array(
				'buyer_id' => $this->current_buyer_user_id,
				'order_id' => $order_id
			))[0];
			
			$this->data['payment']['data'] = $this->payment->get_payment(array(
				'order_id' => $order_id
			))[0];

			$this->data['payment']['card'] = $this->payment->get_payment_methods(
				$this->current_buyer_user_id,
				array(
					'id' => $this->data['payment']['data']->payment_info_id
			));
			
			$this->_render_page('buyer/single_order',$this->data);
		}
	}

	//Buyer return an ordered item
	public function return_order($order_id, $product_id=NULL)
	{
		if(!empty($product_id))
		{
			$this->data['orders'] = $this->order->get_orders(array(
				'order_id' => $order_id,
				'product_id' => $product_id,
				'buyer_id' => $this->current_buyer_user_id,
			))->result_object();
		} else {
			$this->data['orders'] = $this->order->get_orders(array(
				'order_id' => $order_id,
				'buyer_id' => $this->current_buyer_user_id,
			))->result_object();
		}

		$this->data['product'] = $this->utility->get_single_product($product_id);

		if(!empty($this->input->post())) 
		{
			$reversal_reason = $this->input->post('reversal_reason');
			$accept_t_and_c = $this->input->post('accept_t_and_c');
			$return_quantity = $this->input->post('return_quantity');

			if(empty($accept_t_and_c)) 
			{
				$this->data['message'] = 'You need to agree to the terms and conditions to request for the order reversal';
				$this->_render_page('buyer/return_order_item',$this->data);
				return;
			}

			$result = $this->order->create_order_reversal($order_id, array(
				'reversal_reason' => $reversal_reason,
				'quantity' => $return_quantity,
				'customer_confirmed' => TRUE
			));

			if($result == TRUE) {
				$this->session->set_flashdata('message', 'Reversal request has been sent. We will contact you within 3 business days');
				$this->session->keep_flashdata('message');
				$redirect_url = 'u/orders/return/'.$this->data['orders'][0]->transaction_id.'/success';

				redirect($redirect_url, 'refresh', 301);
				return;
			} else {
				$this->session->set_flashdata('message', 'Sorry there was an error in sumbitting your request, please try again');
				$this->keep_flashdata('message');
				
				redirect('u/orders/'.$order_id, 'refresh', 301);
				return;
			}
		}

		$this->data['reversal_data'] = $this->order->order_reversal_data($order_id, $product_id);
		
		$this->data['message'] = $this->session->flashdata('message');
		$this->_render_page('buyer/return_order_item',$this->data);
	}
	
	public function return_order_success($transaction_id)
	{
		$this->data['order'] = $this->order->get_orders(array(
			'transaction_id' => $transaction_id,
			'buyer_id' => $this->current_buyer_user_id,
		))->row_object();
		
		$this->data['reversal_order'] = $this->order->get_order_reversals(array(
			'transaction_id' => $transaction_id
		))->result_object();
		
		$this->data['message'] = $this->session->flashdata('message');
		$this->_render_page('buyer/return_order_item_success', $this->data);
	}

	// [Profile] Purchase history - Display current buyer purchase history
	public function purchase_history()
	{
		$this->data['page_title'] = get_page_title('Purchase History');

		//TODO: Get current user payment options
		$this->data['purchase_history'] = [];#TODO: Get from model/library
		
		$this->_render_page('buyer/purchase_history',$this->data);
	}
	
	// [Profile] Display Login and security settings for the current buyer
	public function security_settings()
	{
		$this->data['page_title'] = get_page_title('Security Settings');

		//TODO: Get current user payment options
		$this->data['security_settings'] = [];#TODO: Get from model/library
		
		$this->_render_page('buyer/security_settings',$this->data);
	}

	// [Profile] Display payment options for the current buyer
	public function payment_options()
	{
		$this->load->library('payment');
		$this->data['page_title'] = get_page_title('Payment Options');
		$this->data['message'] = $this->session->flashdata('message');

		// Get current user payment options
		$this->data['payment_options'] = $this->payment->get_payment_info($this->current_buyer_user_id);#TODO: Get from model/library
		
		$this->_render_page('buyer/payment_options',$this->data);
	}
	
	public function payment_edit($payment_info_id)
	{
		$this->load->library('payment');
		$message_result = $this->payment->payment_method_edit($payment_info_id, $this->current_buyer_user_id);

		$this->session->set_flashdata('message', $message_result);
		$this->session->keep_flashdata('message');
		redirect(site_url('u/payment_options'), 'location', 301);
	}

	// Check out ~ complete purchase
	public function checkout()
	{
		//TODO: Load shop library
		//$this->data['cart'] = $this->yo_cart->get_buyer_cart();
		$this->data['cart'] = $this->yo_cart->getAvailableCart();#Get from cookie/db	
		$shipping_details_id = $this->session->flashdata('shipping_details_id') ?? $this->input->post('shipping_details_id');
		
		$this->load->library('dhl_lib');	
		$selectedCart = $this->yo_cart->getAvailableCartProducts();
		$this->data['shippingDetails'] = $this->dhl_lib->GetRateRequest($shipping_details_id, $selectedCart,$this->input->post());
		 
		//if the cart is empty, redirect to cart page
		if(empty((array)$this->data['cart'])) {
			redirect(site_url('/cart'), 'location', 301);
		}

		//if the shipping id was not set, go to shipping details page
		if(! isset($shipping_details_id) || $shipping_details_id === FALSE) {
			$this->session->set_flashdata('message', 'Please enter your shipping details');

			redirect(site_url('u/shipping'), 'location', 301);
		}
		$this->load->config('shipping');
		$this->data['page_title'] = get_page_title('Checkout');
		$this->data['shipping_details_id'] = $shipping_details_id;
		

		$this->data['shipping_details'] = array(
			'air_shipping_cost' => $this->config->item('air_shipping_cost'),
			'ship_shipping_cost' => $this->config->item('ship_shipping_cost'),
		);
		$this->data['checkout_info'] = array(
			'total_amount'=>$this->yo_cart->get_cart_amount()['total_amount'],
			'sale_amount'=>$this->yo_cart->get_cart_amount()['sale_amount'],
		); #TODO: Get from cookie
		
		//proceed to payment if it was a post request
		if(!empty($this->input->post())) 
		{
			$this->load->library('payment');
			$payment_method = $this->input->post('payment_method');

			//TODO: Validate checkout information
			$payment_validation_response = $this->payment->validate_payment($this->current_buyer_user_id, TRUE);

			if(!$payment_validation_response)
			{
				// set the flash data error message if there is one
				$this->session->set_flashdata('message', validation_errors());
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			} 
			else 
			{
				//shipping config details
				$shipping_method = $this->input->post('shipping_method');

				$this->load->config('shipping');
				$shipping_cost = $this->config->item($shipping_method);
				//make order
				$order_id = $this->order->make_order($this->data['cart'], ['shipping_method' => $shipping_method], $this->data['user']->id, TRUE);
				
				if($order_id===FALSE)
				{
					$order_id = $this->order->get_orders(array(
						'buyer_id' => $this->data['user']->id,
						'paid' => FALSE
					))->result_object()[0]->order_id;
				}
				//update the shipping details order_id column
				$this->order->update_shipping_details(array(
					'id' => $shipping_details_id
				), array(
					'order_id' => $order_id
				));
				// get order details
				$order_details = $this->order->get_orders(array(
					'order_id'=>$order_id
				));

				switch($payment_method) 
				{
					case 'online':
						/**
						 * No need of adding paypal payment method since we are using paypal's api, 
						 * no paypal email input will be needed
						 * except for data's purpose...
						 */
						
						$get_payment_filter = array('paypal_id' => $this->input->post('paypal_id'));
						// $payment_response['ok'] = TRUE;
						break;
					case 'bank':
					case 'visa':
					case 'mastercard':
					case 'card':
						$get_payment_filter = array(
							'card_number' => $this->input->post('card_number'),
							'card_security_code' => $this->input->post('cvc') ?? $this->input->post('card_security_code')
						);
						break;
					case 'mobile':
						$get_payment_filter = array('mpesa_phone' => $this->input->post('mpesa_phone'));
						break;
				}

				if(isset($get_payment_filter))
				{
					//add payment info
					$payment_info_id = $this->payment->add_payment_method(
						$this->data['user']->id,
						$payment_method, NULL, TRUE
					);
					
					$payment_response = $this->payment->make_payment($payment_method, $order_id, $shipping_cost, '', $this->input->post('stripeToken'));
					// var_dump($payment_response);
					if(is_array($payment_response['data']))
					{
						$payment_added = $this->payment->add_payment(array(
							'payment_info_id' => $payment_info_id,
							'order_id' => $order_id,
							'currency_iso' => 'USD',
							'amount' => $payment_response['data']['amount'],
						));

						if($payment_response['ok'] === TRUE && $payment_added === TRUE)
						{
							$this->order->update_order($order_id, array('paid'=>TRUE));
							// Delete checkout cookie & clear cart if checkout was successful
							$this->yo_cart->delete_buyer_cart();
							redirect(site_url('u/orders/'.$order_id), 'refresh');
						} else {
							$this->data['message'] = $payment_response['data'].'\n Failed in making the order, please contact us for help';
							
						}
					}
				}
			}
			// return;
			$this->_render_page('buyer/checkout',$this->data);
		} else
		{
			$this->_render_page('buyer/checkout',$this->data);
		}
	}
	
	// Notifications - Display current buyer notifications
	public function notifications()
	{
		$this->data['page_title'] = get_page_title('Notifications');

		//TODO: Load notifications library
		$this->data['notifications'] = []; #TODO: Get notifications from library
	
		$this->_render_page('buyer/notifications',$this->data);
	}
	
	// Profile - Display buyer profile
	public function profile()
	{
		$this->data['page_title'] = get_page_title('Profile');

		//TODO: Get current user payment options
		$this->data['orders'] = $this->order->get_orders(array(
			'buyer_id' => $this->current_buyer_user_id,
		))->result_object();
		$this->data['wishlist'] = $this->yo_cart->get_buyer_wishlist();
		$this->_render_page('buyer/profile',$this->data);
	}
	
	// Profile - Display buyer profile edit page
	public function profile_edit()
	{
		$this->data['page_title'] = get_page_title('Edit Profile');
		$this->data['user'] = $this->current_buyer['buyer'];
		//TODO: Get current user payment options
		if($this->input->post()) {
			$this->data['message'] = $this->yo_user->update_profile($this->current_buyer_user_id,'u/profile/edit');
		} else {
			$this->data['message'] = $this->session->flashdata('message');
		}

		$this->_render_page('buyer/edit_profile',$this->data);
	}

	// Checkout ~ Purchase information
	public function shipping()
	{
		$this->data['page_title'] = get_page_title('Address & Shipping Information');
		// validate form input
		$this->data['cart'] = $this->yo_cart->getAvailableCart();#Get from cookie/db	
		$this->form_validation->set_rules('first_name', 'First name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last name', 'trim|required');
		$this->form_validation->set_rules('phone', 'Phone number', 'trim|required');
		$this->form_validation->set_rules('address_country', 'Country', 'trim|required');
		$this->form_validation->set_rules('address_city', 'Address city', 'trim|required');
		$this->form_validation->set_rules('address_line_1', 'Address line', 'trim|required');
		$this->form_validation->set_rules('postal_code', 'postal code', 'trim|required');
		$this->form_validation->set_rules('address_line_2', 'Address line 2', 'trim');
		$this->form_validation->set_rules('vat', 'VAT', 'trim');

		

		
		if ($this->form_validation->run() === FALSE)
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			
			//shipping prefilled form inputs
			$this->data['first_name'] = array(
				'name' => 'first_name',
				'id' => 'first_name',
				'type' => 'text',
				'value' => $this->input->post('first_name') ?? $this->data['user']->first_name,
			);
			
			$this->data['last_name'] = array(
				'name' => 'last_name',
				'id' => 'last_name',
				'type' => 'text',
				'value' => $this->input->post('last_name') ?? $this->data['user']->last_name,
			);
			$this->data['last_name']['value'] = ucfirst($this->data['last_name']['value']);
			$this->data['email'] = array(
				'name' => 'email',
				'id' => 'email',
				'type' => 'text',
				'value' => $this->data['user']->email,
			);
			$this->data['company'] = array(
				'name' => 'company',
				'id' => 'company',
				'type' => 'text',
				'value' => $this->input->post('company') ?? $this->data['user']->company,
			);
			$this->data['phone'] = array(
				'name' => 'phone',
				'id' => 'phone',
				'type' => 'text',
				'value' => $this->input->post('phone') ?? $this->data['user']->phone,
			);
			$this->data['address_city'] = array(
				'name' => 'address_city',
				'type' => 'text',
				'placeholder' => 'City',
				'value' => $this->input->post('address_city'),
			);
			$this->data['address_line_1'] = array(
				'name' => 'address_line_1',
				'type' => 'text',
				'placeholder' => 'Street address, P.O. box, company name, c/o',

				'value' => $this->input->post('address_line_1'),
			);
			$this->data['address_line_2'] = array(
				'name' => 'address_line_2',
				'type' => 'text',
				'placeholder' => 'Apartment, suit, unit, building, floor etc',
				'value' => $this->input->post('address_line_2'),
			);
			$this->data['postal_code'] = array(
				'name' => 'postal_code',
				'type' => 'text',
				'placeholder' => 'postal code',
				'value' => $this->input->post('postal_code'),
			);
			$this->data['address_country_id'] = $this->input->post('address_country');

			$this->data['countries'] = $this->utility->get_countries();

			$this->_render_page('buyer/shipping',$this->data);

		} else 
		{

			// add the form inputs to the database
			$shipping_details_id = $this->order->add_shipping_details();
			// set flashdata of the shipping detail
		    $this->session->set_flashdata('shipping_details_id', $shipping_details_id);
			// redirect them back to the checkout page
			redirect(site_url("u/checkout"), 'refresh');
		}
	}

	// Checkout ~ Purchase information
	public function review($product_id)
	{
		//first check if buyer bought the product
		$this->data['product'] = $this->utility->get_single_product($product_id);
		if(!isset($this->data['product']))
		{
			$this->session->set_flashdata('message', 'We couldn\'t find the product');
			$this->session->keep_flashdata('message');
			redirect('u/orders', 'location', 301);
			return;
		}

		$buyer_orders = $this->order->get_orders(array(
			'buyer_id' => $this->current_buyer_user_id,
			'product_id' => $product_id
		))->result_object();

		if(count($buyer_orders) == 0) 
		{
			//buyer can't review
			$this->session->set_flashdata('message', 'You need to purchase '.$this->data['product']->product_name.' to review it.');
			$this->session->keep_flashdata('message');
			redirect('u/orders', 'location', 301);
			return;
		}

		$this->data['review'] = $this->utility->get_single_review($product_id,$this->current_buyer_user_id);

		$product_title = $this->data['product']->title ?? 'Product';
		$this->data['page_title'] = get_page_title('Review '.$product_title);
		
		$extra_header_content = $this->quill_css_link;

		$extra_footer_content = '<script src="'.asset_url('site/js/ratings.js').'"></script>';
		$extra_footer_content .=  "\n\t".$this->quill_js_link;
		
		//TODO: Get current user payment options
		$this->data['user'] = [];

		$this->_render_page('buyer/review',$this->data,$extra_header_content,$extra_footer_content);
	}
}



/* End of file Buyer.php */
