<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends Site_Controller {
	
	protected $data;
	protected $user;
	// Constructor
	public function __construct()
	{
		// __construct($default_header_view,$default_footer_view)
		parent::__construct('','');
		$this->data['categories'] = $this->utility->get_categories();# Get data from model
		$this->data['user'] = FALSE;
		if($this->yo_user->user_logged_in()) {
			$this->data['user'] = $this->yo_user->get_current_user() ?? FALSE;

			switch($this->data['user']->group)
			{
				case 'admin':
					redirect('admin/dashboard');
					return;
				break;
				case 'buyer':
					$this->load->library('order');
		
					$this->data['current_order'] = $this->order->get_orders(array(
						'buyer_id' => $this->data['user']->id,
						'status !=' => 'delivered'
					))->row_object(0);
				break;
			}

		}

	}

	// Landing page
	public function index()
	{
		$this->data['page_title'] = get_page_title();
		$this->load->library('yo_cart');
		$this->data['cart'] = $this->yo_cart->getAvailableCart();#Get from cookie/db	
		// var_dump($this->yo_user->user_logged_in());
		//TODO: Check for filters
		$this->data['products'] = $this->utility->get_products(NULL,FALSE);# Get data from model
		$this->_render_page('site/home',$this->data);
	}

	// Category list - displays a list of all categories that 'currently have items'
	public function categories()
	{
		$this->data['page_title'] = get_page_title('Categories');

		//TODO: Load category model
		// var_dump($this->data['categories']);
		$this->_render_page('site/categories',$this->data);
	}

	// Category products ~ displays products belonging to the category with the id of $cat_id
	public function category_products($cat_id)
	{
		// Load utility library
		$this->data['subcategory'] = $this->utility->get_single_subcategory(array(
			TBL_SUBCATEGORIES.'.id'=>$cat_id
		)) ?? NULL;

		$this->data['product_classes'] = $this->utility->get_category_subcategories($cat_id,PRODUCT_CLASS);

		$this->data['category'] = $this->utility->get_single_category(array(
			TBL_CATEGORIES.'.id'=> @$this->data['subcategory']->category_id
		)) ?? NULL;

		$category_name = @$this->data['category']->title ?? 'Unknown Category';
		$this->data['page_title'] = get_page_title($category_name.' Products');

		$product_filters = array(
			'product_family'=>$cat_id
		);

		if($product_class_id = $this->input->get('sub'))
		{
			$product_filters['product_class'] = $product_class_id;
		}

		$this->data['products'] = $this->utility->get_products($product_filters);
		
		$this->_render_page('site/category_products',$this->data);
	}

	// Seller list - List of sellers that are currently available
	public function seller_list()
	{
		$this->data['page_title'] = get_page_title('Sellers');
		//TODO: Get sellers from ionauth
		$this->data['sellers'] = [];

		$this->_render_page('site/seller_list',$this->data);
	}

	// Seller profile - Display the profile of the seller with the id of $seller_id
	public function seller_profile($seller_id)
	{
		//TODO: Get seller info
		$this->data['page_title'] = get_page_title('Merchant - [Seller name]');#TODO: Get seller name from database
		$this->data['seller'] = [];#TODO: Get data from library

		$this->_render_page('site/seller_profile',$this->data);
	}

	// Product details for the product with the id of $product_id
	public function product($product_id)
	{
		$this->load->library('yo_review');
		$product = $this->utility->get_single_product($product_id);
		$this->load->library('yo_cart');
		$this->data['cart'] = $this->yo_cart->getAvailableCart();#Get from cookie/db	
		// If the product could not be found
		if(empty($product))
		{
			show_404();
		} else {
			$this->data['reviews'] = $this->yo_review->get_product_reviews($product->id);
			$this->data['product'] = $product;
		}

		$this->load->library('intel');
		$this->data['related_products'] = $this->intel->get_related_products($product_id);
		$this->data['page_title'] = get_page_title(@$product->product_name);#TODO: Get product name from database
		$this->data['page_description'] = @$product->description ?? '';

		$this->_render_page('site/product_details',$this->data);
	}

	// Search results ~ returns a list of products matching the search criteria
	public function search()
	{
		$search_query = $this->input->get_post('q');
		$sub_category_id = $this->input->get_post('cat') ?? FALSE;
		$this->data['page_title'] = get_page_title('Search results - '.$search_query);
		$this->data['search_query'] = $search_query;
		
		// Load search library
		$this->load->library('search');
		$this->data['products'] = $this->search->get_product_search_results();#Get from library
		// var_dump($this->data['products']);
		//if the search results were empty and the user had chosen a category id, get products in that category
		if($sub_category_id !== FALSE) {
			if(empty($this->data['products'])) {
				
				$this->data['category_products'] = $this->utility->get_subcategory_products($sub_category_id);

				// var_dump($this->data['category_products']);
				if(! empty($this->data['category_products'])) {
					$this->data['category'] = $this->utility->get_category_subcategories($this->data['category_products'][0]->product_family);
				}
			}
		}
		//last resort is to display recent products
		if($sub_category_id === FALSE && empty($this->data['products'])) {
			$this->data['recent_products'] = $this->utility->get_products();#Get from library
		
		}

		$this->_render_page('site/search_results',$this->data);
	}

	// Cart - Displays the current user's cart
	public function cart()
	{
		$this->data['page_title'] = get_page_title('Cart');

		$this->load->library('yo_cart');
		// Show user cart
		$this->data['cart'] = $this->yo_cart->get_buyer_cart();#Get from cookie/db
		$this->data['saved_items'] = $this->yo_cart->get_saved_items();#Get from cookie/db
		
		$this->_render_page('site/cart',$this->data);
	}
	// Cart - Displays the current user's cart
	public function wishlist($private=FALSE)
	{
		$this->data['page_title'] = get_page_title('Wishlist');

		$this->load->library('yo_cart');
		// Show user wishlist
		$this->data['is_private'] = (bool)$private;
		$this->data['wishlist_type'] = $private ? 'PRIVATE' : 'PUBLIC'; //TODO: Move to lang file
		$this->data['wishlist'] = $this->yo_cart->get_buyer_wishlist(NULL,$private);#Get from cookie/db
				
		$this->_render_page('site/wishlist',$this->data);
	}

	// Explore products page
	public function explore_products()
	{
		$this->data['page_title'] = get_page_title('Explore');
		
		$this->data['products'] = $this->utility->get_products(NULL,FALSE);# Get data from model
		$this->data['ad_products'] = array_slice($this->utility->get_products(array(
			'sale_price >' => '0'
		)), 0, 4);

		$this->data['adverts'] = TRUE || [];
		$this->_render_page('site/explore_products',$this->data);
	}

	// About page
	public function about()
	{
		$this->data['page_title'] = get_page_title('About');
		$this->_render_page('site/about',$this->data);
	}

	// FAQ page
	public function faq()
	{
		$this->data['page_title'] = get_page_title('Frequently Asked Questions');
		$this->_render_page('site/faq',$this->data);
	}

	public function new_account()
	{
		$seller_id = $this->session->userdata('new_seller_id');
		if(!isset($seller_id))
		{
			redirect('');
			return;
		}
		$this->data['new_user'] = $this->ion_auth->user($seller_id)->row();

		$this->_render_page('site/new_seller',$this->data);
	}
}

/* End of file Site.php */
