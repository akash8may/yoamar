<?php defined('BASEPATH') or exit('No direct script access allowed');

/* 
	SHIPPING RELATED
*/

$config['air_shipping_cost'] = 1003.33;
$config['ship_shipping_cost'] = 693.33;
$config['ems_shipping_cost'] = 193.35;