<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/* 
	CUSTOM CONSTANTS
*/
define('ASSET_DIR','assets/');
define('SITE_NAME','YoAMar');

// Id related
define('ID_INT_MIN',999);
define('ID_INT_MAX',999999);
define('ID_STRING_LENGTH',8);

// Rating related
define('MIN_RATING',1);
define('MAX_RATING',5);

// Loyalty points related
define('MAX_LOYALTY_POINTS',10000000);

// Cookie constants
define('COOKIE_PRODUCT_EXPIRE',31536000); # How long a cookie takes to expire in seconds - 1 year

// Default return type for query builder results - 'object' or 'array'
define('QUERY_RETURN_TYPE','object');

/* 
	DATABASE TABLES
*/
define('TBL_API_KEYS','api_keys');
define('TBL_API_REQUESTS','api_requests');
define('TBL_PRODUCTS','products');
define('TBL_PRODUCT_IMAGES','product_images');
define('TBL_WAREHOUSE','warehouse');
define('TBL_CATEGORY_GROUPS','category_groups');
define('TBL_CATEGORIES','categories');
define('TBL_SUBCATEGORIES','subcategories');
define('TBL_COUPONS','coupons');
define('TBL_PRODUCT_COUPONS','product_coupons');
define('TBL_CART','cart');
define('TBL_WISHLIST','wishlist');
define('TBL_ORDERS','orders');
define('TBL_ORDER_REVERSALS','order_reversals');
define('TBL_SHIPPING_CHARGES','shipping_charges');
define('TBL_SHIPMENTS','shipments');
define('TBL_SHIPPING_DETAILS','shipping_details');
define('TBL_CUSTOMS_RATES','customs_rates');
define('TBL_COMMISSION_RATES','commission_rates');
define('TBL_REVIEWS','reviews');
define('TBL_REVIEW_REPLIES','review_replies');
define('TBL_COUNTRIES','countries');
define('TBL_STATES','states');
define('TBL_CURRENCIES','currencies');
define('TBL_TAXES','taxes');
define('TBL_PAYMENTS','payments');
define('TBL_PAYMENT_INFO','payment_info');
define('TBL_NOTIFICATIONS','notifications');
define('TBL_BUYER','buyers');

define('TBL_USERS','users');
define('TBL_GROUPS','groups');
define('TBL_USER_GROUPS','users_groups');

define('TBL_SEARCH_QUERIES','search_queries');

// Product category levels
define('PRODUCT_FAMILY','1');
define('PRODUCT_CLASS','2');
define('PRODUCT_TYPE','3');
define('PRODOCUT_SUBTYPE','4');

// Product image upload
define('MAX_IMAGE_SIZE',2400); //kilobytes
define('MIN_IMAGE_HEIGHT',300);
define('MIN_IMAGE_WIDTH',300);

define('ORDER_REVERSAL_PERIOD',300);

// Placeholders
define('PRODUCT_PLACEHOLDER_IMG','assets/site/img/placeholders/placeholder-image.jpg');
define('MERCHANT_PLACEHOLDER_IMG','assets/site/img/placeholders/placeholder-image-merchant.jpg');
define('USER_PLACEHOLDER_IMG','assets/site/img/placeholders/placeholder-image-user.jpg');

// Upload paths
define('UPLOAD_BASE_PATH','uploads/');
define('UPLOAD_BASE_THUMB_PATH',UPLOAD_BASE_PATH.'thumbnails/');

define('PRODUCT_IMG_PATH',UPLOAD_BASE_PATH.'products/');
define('PRODUCT_THUMB_PATH',PRODUCT_IMG_PATH.'thumbnails/');

define('USER_IMG_PATH',UPLOAD_BASE_PATH.'users/');
define('USER_THUMB_PATH',USER_IMG_PATH.'thumbnails/');
