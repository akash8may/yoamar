<?php defined('BASEPATH') or exit('No direct script access allowed');

/* 
	PRODUCT RELATED
*/
$config['product_thumbnail_width'] = 330;

/* 
	COMPANY RELATED
*/
$config['contact_phones'] = ['+254 746 803222', '+254 775 013520'];
$config['contact_email'] = 'info@yoamar.com';
