<?php

/**
 * HybridAuth
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
 */
// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------
$config['hybridauth'] = array(
	"providers" => array(
	  "OpenID" => array(
		"enabled" => FALSE,
	  ),
	  "Yahoo" => array(
		"enabled" => FALSE,
		"keys" => array("id" => "", "secret" => ""),
	  ),
	  "Google" => array(
			"enabled" => TRUE,
			"keys" => array("id" => "292756301335-q40thg8q54ei5n4jdrq297t5gicjvajj.apps.googleusercontent.com", "secret" => "r7Wy0PWleZSVPd7l2tLqGW8m"),
		),
		"Facebook" => array(
			"enabled" => TRUE,
			"keys" => array("id" => "1953070928086924", "secret" => "3578c36e39c842325dafbc65f2687d3a"),
			"scope"   => ['email', 'user_about_me', 'user_birthday', 'user_hometown', 'public_profile'],
			"trustForwarded" => FALSE
		),
	  "Twitter" => array(
		"enabled" => FALSE,
		"keys" => array("key" => "", "secret" => ""),
		"includeEmail" => FALSE,
	  ),
	  "LinkedIn" => array(
		"enabled" => FALSE,
		"keys" => array("id" => "", "secret" => ""),
	  ),
	),
	// If you want to enable logging, set 'debug_mode' to TRUE.
	// You can also set it to
	// - "error" To log only error messages. Useful in production
	// - "info" To log info and error messages (ignore debug messages)
	"debug_mode" => ENVIRONMENT === 'development',
	// Path to file writable by the web server. Required if 'debug_mode' is not FALSE
	"debug_file" => APPPATH . 'logs/hybridauth.log',
  );
