<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

// Commission percentage taken by the facilitator
$config['commission'] = 16.0;
$config['supported_languages'] = array(
    'English',
    'Chinese'
);

/* End of file Ecommerce.php */
