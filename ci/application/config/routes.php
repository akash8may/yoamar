<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'site';
$route['404_override'] = 'yo_error/error_404';
$route['translate_uri_dashes'] = FALSE;

/* 
SITE ROUTES 
*/
$route['category/(:any)/products'] = 'site/category_products/$1';
$route['sellers'] = 'site/seller_list';
$route['seller/(:any)'] = 'site/seller_profile/$1';
$route['explore'] = 'site/explore_products';
$route['new_seller'] = 'site/new_account';

/* 
BUYER ROUTES
*/
$route['u/profile/edit'] = 'buyer/profile_edit';#TODO: Route to subdomain
$route['u/orders/return/(:any)']        = 'buyer/return_order/$1';#TODO: Route to return an order ...return/{order_id}
$route['u/orders/cancel/(:any)']        = 'buyer/return_order/$1';#TODO: Route to return an order ...return/{order_id}
$route['u/orders/return/(:any)/success'] = 'buyer/return_order_success/$1';#TODO: Route to return products ...return/{order_id}/{product_id}
$route['u/orders/cancel/(:any)/success'] = 'buyer/return_order_success/$1';#TODO: Route to return products ...return/{order_id}/{product_id}
$route['u/orders/return/(:any)/(:any)'] = 'buyer/return_order/$1/$2';#TODO: Route to return products ...return/{order_id}/{product_id}
$route['u/orders/cancel/(:any)/(:any)'] = 'buyer/return_order/$1/$2';#TODO: Route to return products ...return/{order_id}/{product_id}

$route['u/(.+)'] = 'buyer/$1';#TODO: Route to subdomain

/* 
SELLER ROUTES
*/
$route['merchant'] = 'seller/index';

$route['merchant/product/(.+)'] = 'seller/product_$1';
$route['merchant/rating/(:any)'] = 'seller/product_ratings/$1';

$route['merchant/order/(:any)'] = 'seller/order_details/$1';
$route['merchant/reports/(:any)'] = 'seller/reports_$1';
$route['merchant/profile/payment_methods'] = 'seller/profile_payment_methods';
$route['merchant/profile/edit'] = 'seller/profile_edit';

$route['merchant/(.+)'] = 'seller/$1';

/* 
ADMIN ROUTES
*/
$route['admin'] = 'admin/index';
$route['admin/accounts/(:any)'] = 'admin/$1_accounts'; # usecase : admin/accounts/admin == admin_accounts
$route['admin/accounts/(:any)/(.+)'] = 'admin/$1_$2'; # usecase : admin/accounts/admin/add == admin_add
$route['admin/profile/edit'] = 'admin/profile_edit';

$route['admin/(.+)'] = 'admin/$1';

/* 
COURIER ROUTES
*/
/* $route['courier'] = 'courier/index';

$route['courier/items/pick/(:any)'] = 'courier/items_pick_single/$1';
$route['courier/items/return/(:any)'] = 'courier/items_return_single/$1';

$route['courier/accounts/(.+)'] = 'courier/accounts_$1';

$route['courier/(.+)'] = 'courier/$1'; */

/* 
LOGISTICS ROUTES: TODO ~ route to subdomain
*/
$route['logistics'] = 'logistics/index';

$route['logistics/add'] = 'logistics/add_items';
$route['logistics/shipped'] = 'logistics/shipped_items';
$route['logistics/profile/edit'] = 'logistics/profile_edit';
$route['logistics/(.+)'] = 'logistics/$1';

$route['api/(.+)'] = 'api/$1';
$route['auth'] = 'auth';
$route['auth/(.+)'] = 'auth/$1';

//Global route ~ most general one : must be last to allow others to work
$route['(.+)'] = 'site/$1';
