-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2018 at 08:26 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yoamar`
--

-- --------------------------------------------------------

--
-- Table structure for table `buyer_accounts`
--

CREATE TABLE `buyer_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Buyer user account associated with this',
  `loyalty_points` int(10) UNSIGNED DEFAULT NULL COMMENT 'Buyer loyalty points',
  `credit_amount` int(11) UNSIGNED DEFAULT NULL COMMENT 'Amount credited to this account',
  `credit_expiry_date` date DEFAULT NULL COMMENT 'Date the credit expires'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains buyer specific extra information eg. Loyalty points';

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `buyer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the buyer the cart belongs to',
  `user_ip` varchar(39) DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the product in the cart',
  `quantity` tinyint(3) UNSIGNED DEFAULT '1' COMMENT 'The number of units of the product above in the cart',
  `is_saved_item` tinyint(1) DEFAULT '0' COMMENT 'If this is true, then the item is not added during checkout and is considered a "saved" item, otherwise it is considered a cart item waiting to be checked out',
  `is_active` tinyint(1) DEFAULT '1' COMMENT 'Whether or not the cart item is visible to the user. We may want to store this information even when a user "deletes" it for analytical purposes',
  `last_save_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The last time the cart was saved'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains item in the cart/Saved items';

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_group_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the group the category belongs to',
  `title` varchar(50) DEFAULT NULL COMMENT 'Name of the subcategory',
  `description` text COMMENT 'Description of the category',
  `img_link` varchar(256) DEFAULT NULL COMMENT 'Link to the category image'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Product categories';

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_group_id`, `title`, `description`, `img_link`) VALUES
(1, 1, 'Art', NULL, NULL),
(2, 1, 'Crafted Products', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_groups`
--

CREATE TABLE `category_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Category groups - groups of product categories';

--
-- Dumping data for table `category_groups`
--

INSERT INTO `category_groups` (`id`, `title`, `description`, `date_created`) VALUES
(1, 'Art and Crafted Products', NULL, '2018-09-05 16:22:57');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether yoamar supports this country'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='List of all the countries';

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`, `is_active`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93, 0),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355, 0),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213, 0),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684, 0),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376, 0),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244, 0),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264, 0),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268, 0),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54, 0),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374, 0),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297, 0),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61, 0),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43, 0),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994, 0),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242, 0),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973, 0),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880, 0),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246, 0),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375, 0),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32, 0),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501, 0),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229, 0),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441, 0),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975, 0),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591, 0),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387, 0),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267, 0),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55, 0),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246, 0),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673, 0),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359, 0),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226, 0),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257, 0),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855, 0),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237, 0),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1, 0),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238, 0),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345, 0),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236, 0),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235, 0),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56, 0),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86, 1),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61, 0),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672, 0),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57, 0),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269, 0),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242, 0),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242, 0),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682, 0),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506, 0),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225, 0),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385, 0),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53, 0),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357, 0),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420, 0),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45, 0),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253, 0),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767, 0),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809, 0),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593, 0),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20, 0),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503, 0),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240, 0),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291, 0),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372, 0),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251, 0),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500, 0),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298, 0),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679, 0),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358, 0),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33, 0),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594, 0),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689, 0),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241, 0),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220, 0),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995, 0),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49, 0),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233, 0),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350, 0),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30, 0),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299, 0),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473, 0),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590, 0),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671, 0),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502, 0),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224, 0),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245, 0),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592, 0),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509, 0),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39, 0),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504, 0),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852, 0),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36, 0),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354, 0),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91, 0),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62, 0),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98, 0),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964, 0),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353, 0),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972, 0),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39, 0),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876, 0),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81, 0),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962, 0),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7, 0),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254, 1),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686, 0),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850, 0),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82, 0),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965, 0),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996, 0),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856, 0),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371, 0),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961, 0),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266, 0),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231, 0),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218, 0),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423, 0),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370, 0),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352, 0),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853, 0),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389, 0),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261, 0),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265, 0),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60, 0),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960, 0),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223, 0),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356, 0),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692, 0),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596, 0),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222, 0),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230, 0),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269, 0),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52, 0),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691, 0),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373, 0),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377, 0),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976, 0),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664, 0),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212, 0),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258, 0),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95, 0),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264, 0),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674, 0),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977, 0),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31, 0),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599, 0),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687, 0),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64, 0),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505, 0),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227, 0),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234, 0),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683, 0),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672, 0),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670, 0),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47, 0),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968, 0),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92, 0),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680, 0),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970, 0),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507, 0),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675, 0),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595, 0),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51, 0),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63, 0),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48, 0),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351, 0),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787, 0),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974, 0),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262, 0),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40, 0),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70, 0),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250, 0),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290, 0),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869, 0),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758, 0),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508, 0),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784, 0),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684, 0),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378, 0),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239, 0),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966, 0),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221, 0),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381, 0),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248, 0),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232, 0),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65, 0),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421, 0),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386, 0),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677, 0),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252, 0),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27, 0),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34, 0),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94, 0),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249, 0),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597, 0),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47, 0),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268, 0),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46, 0),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41, 0),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963, 0),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886, 0),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992, 0),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255, 0),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66, 0),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670, 0),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228, 0),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690, 0),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676, 0),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868, 0),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216, 0),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90, 0),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370, 0),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649, 0),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688, 0),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256, 0),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380, 0),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971, 0),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44, 0),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1, 0),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598, 0),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998, 0),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678, 0),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58, 0),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84, 0),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284, 0),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340, 0),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681, 0),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212, 0),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967, 0),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260, 0),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263, 0),
(240, 'RS', 'SERBIA', 'Serbia', 'SRB', 688, 381, 0),
(241, 'AP', 'ASIA PACIFIC REGION', 'Asia / Pacific Region', '0', 0, 0, 0),
(242, 'ME', 'MONTENEGRO', 'Montenegro', 'MNE', 499, 382, 0),
(243, 'AX', 'ALAND ISLANDS', 'Aland Islands', 'ALA', 248, 358, 0),
(244, 'BQ', 'BONAIRE, SINT EUSTATIUS AND SABA', 'Bonaire, Sint Eustatius and Saba', 'BES', 535, 599, 0),
(245, 'CW', 'CURACAO', 'Curacao', 'CUW', 531, 599, 0),
(246, 'GG', 'GUERNSEY', 'Guernsey', 'GGY', 831, 44, 0),
(247, 'IM', 'ISLE OF MAN', 'Isle of Man', 'IMN', 833, 44, 0),
(248, 'JE', 'JERSEY', 'Jersey', 'JEY', 832, 44, 0),
(249, 'XK', 'KOSOVO', 'Kosovo', '---', 0, 381, 0),
(250, 'BL', 'SAINT BARTHELEMY', 'Saint Barthelemy', 'BLM', 652, 590, 0),
(251, 'MF', 'SAINT MARTIN', 'Saint Martin', 'MAF', 663, 590, 0),
(252, 'SX', 'SINT MAARTEN', 'Sint Maarten', 'SXM', 534, 1, 0),
(253, 'SS', 'SOUTH SUDAN', 'South Sudan', 'SSD', 728, 211, 0);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `name` varchar(20) NOT NULL COMMENT 'The name of the coupon. Cannot be more than 20 characters long. Must be unique',
  `discount_type` enum('percentage','amount') DEFAULT 'amount' COMMENT 'Percentage, amount etc.',
  `coupon_amount` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'The percentage discount this coupon will give. Can be left unset if discount_price is set. Defaults to 0',
  `min_amount` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'Minimum amount a user must spend for the coupon to work',
  `max_amount` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'Maximum amount a user must spend for the coupon to work',
  `exclude_sale_items` tinyint(1) DEFAULT NULL COMMENT 'Whether to exclude sale items or not. True by default',
  `free_shipping` tinyint(1) DEFAULT NULL COMMENT 'Whether this coupon offers free shipping. False by default',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the user (seller or admin) that created the coupon',
  `usage_limit` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'The number of times this coupon can be used',
  `usage_limit_per_user` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'The number of times this coupon can be used by a single user',
  `usage_count` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'Number of times the coupon has been used',
  `is_active` tinyint(1) DEFAULT NULL COMMENT 'Whether this coupon is active or not. By default this is true on creation. If this is false, then the coupon can still be activated later on but cannot currently be used',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date the coupon was created',
  `expiry_date` date DEFAULT NULL COMMENT 'Date the coupon will expire'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Coupons';

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'buyer', 'General User'),
(3, 'seller', 'Can sell goods'),
(4, 'logistics', 'Handles warehouses & logistics'),
(5, 'courier', 'Handles shipping and transportation'),
(6, 'superuser', 'Access to everything admin has + more');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(128) DEFAULT NULL COMMENT 'Title of the notification',
  `description` text COMMENT 'This text will be displayed as the body of the notification. Generated at application level',
  `link` varchar(256) DEFAULT NULL COMMENT 'The link the notification redirects to when clicked on.',
  `type` enum('info','warning','success') DEFAULT 'info' COMMENT 'Info, warning, success etc. This can be used to determine the icon displayed with the notification type. Default is info',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the user the notification is sent to',
  `is_read` tinyint(1) DEFAULT NULL COMMENT 'Whether the notification has been read or not',
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The date & time the notification was sent',
  `date_read` datetime DEFAULT NULL COMMENT 'The date & time the notification was read (possibly for analytical purposes)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Notifications sent to users';

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `transaction_id` int(10) UNSIGNED NOT NULL COMMENT 'Each order item will have a unique transaction id. This is the primary key',
  `order_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Multiple order items can have the same id.',
  `product_id` text COMMENT 'The id of the product that is contained in the order',
  `quantity` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Number of the product specified above to be included in the order',
  `sales_amount` float UNSIGNED DEFAULT NULL COMMENT 'Pure product Sales',
  `sales_tax1` float UNSIGNED DEFAULT NULL COMMENT 'Various Taxes as applicable',
  `sales_tax2` float UNSIGNED DEFAULT NULL COMMENT 'Various Taxes as applicable',
  `sales_tax3` float UNSIGNED DEFAULT NULL COMMENT 'Various Taxes as applicable',
  `order_total` float UNSIGNED DEFAULT NULL COMMENT 'Total paid by buyer - icluded sales, tax, shipping, etc',
  `paid` tinyint(1) DEFAULT NULL COMMENT 'Has this order been paid for',
  `coupon_name` varchar(20) DEFAULT NULL COMMENT 'Coupon applied to this order. Discount calculated at application level & not in database',
  `buyer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id of the buyer making the order',
  `status` enum('enum_value1','enum_value2') DEFAULT NULL COMMENT 'Processing, in transit, delivered',
  `shipping_method` enum('enum_value1','enum_value2') DEFAULT NULL COMMENT 'Freight, Air, EMS',
  `shipping_charges` int(10) UNSIGNED DEFAULT NULL COMMENT 'Shipping charges incurred by the order',
  `shipping_date` date DEFAULT NULL COMMENT 'The date the item is expected to arrive',
  `import_fees_deposit` int(10) UNSIGNED DEFAULT NULL COMMENT 'Import fees incurred by the order',
  `required_date` date DEFAULT NULL COMMENT 'Date the customer/buyer wants the item by. This will be useful if in future we charge differently based on how soon the buyer wants the product',
  `delivery_address` varchar(60) DEFAULT NULL COMMENT 'Will be the buyer''s specified delivery address. Automatically added to buyer addresses in buyer account',
  `return_address` varchar(60) DEFAULT NULL COMMENT 'If the product could not be delivered, return it to this address',
  `return_instruction` enum('enum_value1','enum_value2') DEFAULT NULL COMMENT 'RTS, Abandon, Destroy. Default is RTS. This should not change in the near future but is a useful property to have as far as couriers and specifically POSTA is concerned',
  `tracking_id` varchar(10) DEFAULT NULL COMMENT 'Tracking id for the order. Generated by POSTA',
  `tracking_link` varchar(128) DEFAULT NULL COMMENT 'Link to view the order being tracked',
  `date_ordered` datetime DEFAULT NULL COMMENT 'Date & time the order was made',
  `seller_id` int(10) UNSIGNED DEFAULT NULL,
  `seller_dues` float DEFAULT NULL,
  `yoamar_commision` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Orders made by buyers are stored here';

-- --------------------------------------------------------

--
-- Table structure for table `order_reversals`
--

CREATE TABLE `order_reversals` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the order that is being reversed',
  `item_condition` enum('good','bad','damaged','unchecked') NOT NULL DEFAULT 'unchecked' COMMENT 'Good, bad, etc.',
  `charges` int(10) UNSIGNED DEFAULT NULL COMMENT 'How much the buyer was charged for the reversal',
  `charges_breakdown` text COMMENT 'A breakdown of the charges. Incase the customer asks what the charges are for. This will be generated at application level',
  `customer_confirmed` tinyint(1) DEFAULT NULL COMMENT 'Customer confirms that they have received the item',
  `courier_received` tinyint(1) DEFAULT NULL COMMENT 'Courier has confirmed that they have received the item',
  `logistics_received` tinyint(1) DEFAULT NULL COMMENT 'Logistics have confirmed that they received the item',
  `reversal_reason` text COMMENT 'The reason for the reversal',
  `date_reversed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The date & time the reversal happened'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Order reversals';

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_info_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id linking to the payment information used to make this payment. Eg. Credit card, paypal as well as who made the payment',
  `order_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id of the order the payment belongs to. NOT NULL',
  `currency_iso` char(3) DEFAULT NULL COMMENT 'The currency the payment was made in. Default USD. This won''t change for now',
  `amount` int(10) UNSIGNED DEFAULT NULL COMMENT 'The amount that was made as a payment in this payment',
  `payment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The date/time the payment was made'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Payments made through the system';

-- --------------------------------------------------------

--
-- Table structure for table `payment_info`
--

CREATE TABLE `payment_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` enum('bank','card','mobile','online') NOT NULL DEFAULT 'card' COMMENT 'Mobile, credit card, paypal',
  `paypal_email` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id of the order the payment belongs to. NOT NULL',
  `card_number` varchar(15) DEFAULT NULL COMMENT 'Credit card number associated with this payment info',
  `card_expiry` date DEFAULT NULL COMMENT 'Date the card expires',
  `card_security_code` varchar(5) DEFAULT NULL COMMENT 'The extra digits on cards 3-5 chars - used for authentication',
  `mpesa_phone` varchar(13) DEFAULT NULL COMMENT 'The phone number used for mpesa by this user',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the user the payment information belongs to',
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The date/time the payment information was updated'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Stores user payment information';

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `seller_id` int(10) UNSIGNED DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `brand_name` varchar(50) DEFAULT NULL,
  `description` text,
  `unit_price` int(10) UNSIGNED DEFAULT NULL,
  `quantity` smallint(5) UNSIGNED DEFAULT NULL,
  `sale_price` float UNSIGNED DEFAULT NULL,
  `sale_price_effective_date` date DEFAULT NULL,
  `total_price` float UNSIGNED DEFAULT NULL,
  `is_available` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'I take this availability to say if item has been sold or not, correct?',
  `availability_date` date DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `product_group` int(10) UNSIGNED DEFAULT NULL COMMENT 'Top Category',
  `product_segment` int(10) UNSIGNED DEFAULT NULL COMMENT 'Category',
  `product_family` int(10) UNSIGNED DEFAULT NULL COMMENT 'Sub Category 1',
  `product_class` int(10) UNSIGNED DEFAULT NULL COMMENT 'Sub Category 2',
  `product_type` int(10) UNSIGNED DEFAULT NULL COMMENT 'Sub Category 3',
  `product_sub-type` int(10) UNSIGNED DEFAULT NULL COMMENT 'Sub Category 4',
  `product_state` enum('enum_value1','enum_value2') DEFAULT NULL COMMENT 'new, used, refurbished',
  `age_group` enum('enum_value1','enum_value2') DEFAULT NULL,
  `adult` tinyint(1) DEFAULT NULL,
  `color` varchar(25) DEFAULT NULL,
  `gender` enum('enum_value1','enum_value2') DEFAULT NULL,
  `material` varchar(50) DEFAULT NULL,
  `pattern` varchar(50) DEFAULT NULL,
  `product_size` enum('enum_value1','enum_value2') DEFAULT NULL,
  `size_type` enum('enum_value1','enum_value2') DEFAULT NULL,
  `size_system` enum('imperial','metric') DEFAULT NULL,
  `shipping_weight` smallint(5) UNSIGNED DEFAULT NULL,
  `shipping_length` smallint(5) UNSIGNED DEFAULT NULL,
  `shipping_width` smallint(5) UNSIGNED DEFAULT NULL,
  `shipping_height` smallint(5) UNSIGNED DEFAULT NULL,
  `is_bundle` tinyint(1) DEFAULT NULL,
  `energy_efficiency_class` enum('enum_value1','enum_value2') DEFAULT NULL,
  `energy_efficiency_class_min` enum('enum_value1','enum_value2') DEFAULT NULL,
  `energy_efficiency_class_max` enum('enum_value1','enum_value2') DEFAULT NULL,
  `min_handling_time` int(10) UNSIGNED DEFAULT NULL,
  `max_handling_time` int(10) UNSIGNED DEFAULT NULL,
  `tax` float UNSIGNED DEFAULT NULL COMMENT 'Tax may be associated with where item is bought (tax rate) - need a tax table',
  `tax_category` enum('enum_value1','enum_value2') DEFAULT NULL,
  `model_number` varchar(30) DEFAULT NULL,
  `style` varchar(50) DEFAULT NULL,
  `year_made` year(4) DEFAULT NULL,
  `season` enum('autumn','summer','spring','winter') DEFAULT NULL,
  `origin_country` int(10) UNSIGNED DEFAULT NULL COMMENT 'Where the product was made',
  `origin_state` int(10) UNSIGNED DEFAULT NULL,
  `origin_city` varchar(25) DEFAULT NULL,
  `views` int(10) UNSIGNED DEFAULT NULL,
  `likes` int(10) UNSIGNED DEFAULT NULL,
  `availability_country` int(10) UNSIGNED DEFAULT NULL COMMENT 'Not all goods will be available for purchase in all countries. Goods should only be visible from places where the seller can actually buy',
  `availability_state` int(10) UNSIGNED DEFAULT NULL,
  `availability_city` varchar(25) DEFAULT NULL,
  `min_quantity` int(10) UNSIGNED DEFAULT NULL,
  `max_quantity` int(10) UNSIGNED DEFAULT NULL,
  `available_quanitity` int(10) UNSIGNED DEFAULT NULL,
  `is_featured` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the product is a featured product or not'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='List of products available';

-- --------------------------------------------------------

--
-- Table structure for table `product_coupons`
--

CREATE TABLE `product_coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id of the product that the coupon  applies to. Multiple products can use the same coupon',
  `coupon_name` varchar(20) DEFAULT NULL COMMENT 'The name of the coupon that applies to the the product specified above',
  `number_of_uses` tinyint(3) UNSIGNED DEFAULT '1' COMMENT 'Number of times this product can  use the coupon. By default, this is the number will be equal to the total number of uses of the coupon',
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The date the product was added'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT=' Products paired with their coupons';

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(255) UNSIGNED NOT NULL,
  `product_id` int(255) UNSIGNED DEFAULT NULL COMMENT 'The id of the product to which the image belongs',
  `img_link` varchar(128) DEFAULT NULL COMMENT 'Link to the image (either our server or an online link)',
  `thumbnail_link` varchar(128) DEFAULT NULL COMMENT 'Link to a thumbnail version of the image. Used when showing the item in smaller display cards (speeds up the page). TODO: Consider autogenerating this',
  `alt_text` text COMMENT 'This will be an excerpt of the product description'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Images  for products';

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `rating` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'The star rating value eg. 4/5',
  `comment` text COMMENT 'Comment text',
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id of the product that is being rated',
  `buyer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id of the buyer that is rating the product',
  `comments_allowed` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether or not comments are allowed for this review ~ will be used to allow/disallow comments from being made in reviews',
  `date_created` datetime DEFAULT NULL COMMENT 'Date_Created datetime',
  `date_updated` datetime DEFAULT NULL COMMENT 'Date_Updated datetime'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Reviews/ratings on products';

-- --------------------------------------------------------

--
-- Table structure for table `review_replies`
--

CREATE TABLE `review_replies` (
  `id` int(10) UNSIGNED NOT NULL,
  `review_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the rating that the reply belongs to',
  `reply_text` text COMMENT 'Comment text',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id of the user that is being rated',
  `date_created` datetime DEFAULT NULL COMMENT 'Date_Created datetime',
  `date_updated` datetime DEFAULT NULL COMMENT 'Date_Updated datetime'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Replies to ratings - Sellers & buyers can use this';

-- --------------------------------------------------------

--
-- Table structure for table `shipments`
--

CREATE TABLE `shipments` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_transaction_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id of the order this shipment belongs to',
  `shipment_label` varchar(50) DEFAULT NULL COMMENT 'Label of the shipment as labelled by the courier',
  `warehouse_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id of the product in the order that was collected. This allows the logistics center to keep track of what goods left',
  `date_picked` datetime DEFAULT NULL COMMENT 'The date the shipment was picked from logistics',
  `condition` enum('enum_value1','enum_value2') DEFAULT NULL COMMENT 'Condition the item was in when it was delivered',
  `is_delivered` tinyint(1) DEFAULT NULL COMMENT 'If the courier has delivered the shipment. This will be true when the courier confirms that they have delivered the product',
  `customer_confirmed` tinyint(1) DEFAULT NULL COMMENT 'True when the customer confirms that they have received the shipment.',
  `tracking_id` varchar(256) DEFAULT NULL COMMENT 'To track the shipment',
  `tracking_url` varchar(256) DEFAULT NULL COMMENT 'URL where users can track shipment'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Handles shipping';

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state` varchar(22) NOT NULL,
  `state_code` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state`, `state_code`) VALUES
('Alaska', 'AK'),
('Alabama', 'AL'),
('Arkansas', 'AR'),
('Arizona', 'AZ'),
('California', 'CA'),
('Colorado', 'CO'),
('Connecticut', 'CT'),
('District of Columbia', 'DC'),
('Delaware', 'DE'),
('Florida', 'FL'),
('Georgia', 'GA'),
('Hawaii', 'HI'),
('Iowa', 'IA'),
('Idaho', 'ID'),
('Illinois', 'IL'),
('Indiana', 'IN'),
('Kansas', 'KS'),
('Kentucky', 'KY'),
('Louisiana', 'LA'),
('Massachusetts', 'MA'),
('Maryland', 'MD'),
('Maine', 'ME'),
('Michigan', 'MI'),
('Minnesota', 'MN'),
('Missouri', 'MO'),
('Mississippi', 'MS'),
('Montana', 'MT'),
('North Carolina', 'NC'),
('North Dakota', 'ND'),
('Nebraska', 'NE'),
('New Hampshire', 'NH'),
('New Jersey', 'NJ'),
('New Mexico', 'NM'),
('Nevada', 'NV'),
('New York', 'NY'),
('Ohio', 'OH'),
('Oklahoma', 'OK'),
('Oregon', 'OR'),
('Pennsylvania', 'PA'),
('Rhode Island', 'RI'),
('South Carolina', 'SC'),
('South Dakota', 'SD'),
('Tennessee', 'TN'),
('Texas', 'TX'),
('Utah', 'UT'),
('Virginia', 'VA'),
('Vermont', 'VT'),
('Washington', 'WA'),
('Wisconsin', 'WI'),
('West Virginia', 'WV'),
('Wyoming', 'WY');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(50) DEFAULT NULL COMMENT 'Name of the subcategory',
  `category_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the category the subcategory belongs to (autofilled to parent subcategory if the level >1)',
  `level` tinyint(4) DEFAULT NULL COMMENT 'Level 1 means this is a direct child of a category, level 2 means a child of a level 1 subcategory, 3 means child of level 2 etc. This recursive approach allows us to add/remove levels of subcategories without creating new tables for each',
  `description` text COMMENT 'Description of the sub-category',
  `img_link` varchar(256) DEFAULT NULL COMMENT 'Link to the subcategory''s image'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Product subcategories';

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `title`, `category_id`, `level`, `description`, `img_link`) VALUES
(1, 'Prints', 1, 1, NULL, NULL),
(2, 'Collectibles', 1, 1, NULL, NULL),
(3, 'Painting', 1, 1, NULL, NULL),
(4, 'Photography', 1, 1, NULL, NULL),
(5, 'Drawing & Illustration', 1, 1, NULL, NULL),
(6, 'Dolls & Miniatures', 1, 1, NULL, NULL),
(7, 'Sculpture', 1, 1, NULL, NULL),
(8, 'Mixed Media & Collage', 1, 1, NULL, NULL),
(9, 'Fiber Arts', 1, 1, NULL, NULL),
(10, 'Clip Art', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1534939431, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(10) UNSIGNED NOT NULL,
  `buyer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the buyer the cart belongs to',
  `user_ip` varchar(39) DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the product in the cart',
  `quantity` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'The number of units of the product above in the cart',
  `is_active` tinyint(1) DEFAULT NULL COMMENT 'Whether or not the cart item is visible to the user. We may want to store this information even when a user deletes it for analytical purposes',
  `last_save_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The last time the cart was saved'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains item in the wishlist. When a user is not logged in, the ip is used to store the item to cart instead';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buyer_accounts`
--
ALTER TABLE `buyer_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_groups`
--
ALTER TABLE `category_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `order_reversals`
--
ALTER TABLE `order_reversals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_info`
--
ALTER TABLE `payment_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_coupons`
--
ALTER TABLE `product_coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review_replies`
--
ALTER TABLE `review_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipments`
--
ALTER TABLE `shipments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_code`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `category_groups`
--
ALTER TABLE `category_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
