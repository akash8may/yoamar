-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2018 at 06:30 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yoamar`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` varchar(10) NOT NULL,
  `seller_id` int(10) UNSIGNED DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `brand_name` varchar(50) DEFAULT NULL,
  `description` text,
  `unit_price` float UNSIGNED DEFAULT NULL,
  `quantity` smallint(5) UNSIGNED DEFAULT NULL,
  `sale_price` float UNSIGNED DEFAULT NULL,
  `sale_price_effective_date` date DEFAULT NULL,
  `total_price` float UNSIGNED DEFAULT NULL,
  `is_available` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'I take this availability to say if item has been sold or not, correct?',
  `availability_date` date DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `product_group` int(10) UNSIGNED DEFAULT NULL COMMENT 'Top Category',
  `product_segment` int(10) UNSIGNED DEFAULT NULL COMMENT 'Category',
  `product_family` int(10) UNSIGNED DEFAULT NULL COMMENT 'Sub Category 1',
  `product_class` int(10) UNSIGNED DEFAULT NULL COMMENT 'Sub Category 2',
  `product_type` int(10) UNSIGNED DEFAULT NULL COMMENT 'Sub Category 3',
  `product_sub-type` int(10) UNSIGNED DEFAULT NULL COMMENT 'Sub Category 4',
  `product_state` enum('enum_value1','enum_value2') DEFAULT NULL COMMENT 'new, used, refurbished',
  `age_group` enum('enum_value1','enum_value2') DEFAULT NULL,
  `adult` tinyint(1) DEFAULT NULL,
  `color` varchar(25) DEFAULT NULL,
  `gender` enum('enum_value1','enum_value2') DEFAULT NULL,
  `material` varchar(50) DEFAULT NULL,
  `pattern` varchar(50) DEFAULT NULL,
  `product_size` enum('enum_value1','enum_value2') DEFAULT NULL,
  `size_type` enum('enum_value1','enum_value2') DEFAULT NULL,
  `size_system` enum('imperial','metric') DEFAULT NULL,
  `shipping_weight` smallint(5) UNSIGNED DEFAULT NULL,
  `shipping_length` smallint(5) UNSIGNED DEFAULT NULL,
  `shipping_width` smallint(5) UNSIGNED DEFAULT NULL,
  `shipping_height` smallint(5) UNSIGNED DEFAULT NULL,
  `is_bundle` tinyint(1) DEFAULT NULL,
  `energy_efficiency_class` enum('enum_value1','enum_value2') DEFAULT NULL,
  `energy_efficiency_class_min` enum('enum_value1','enum_value2') DEFAULT NULL,
  `energy_efficiency_class_max` enum('enum_value1','enum_value2') DEFAULT NULL,
  `min_handling_time` int(10) UNSIGNED DEFAULT NULL,
  `max_handling_time` int(10) UNSIGNED DEFAULT NULL,
  `tax` float UNSIGNED DEFAULT NULL COMMENT 'Tax may be associated with where item is bought (tax rate) - need a tax table',
  `tax_category` enum('enum_value1','enum_value2') DEFAULT NULL,
  `model_number` varchar(30) DEFAULT NULL,
  `style` varchar(50) DEFAULT NULL,
  `year_made` year(4) DEFAULT NULL,
  `season` enum('autumn','summer','spring','winter') DEFAULT NULL,
  `origin_country` int(10) UNSIGNED DEFAULT NULL COMMENT 'Where the product was made',
  `origin_state` int(10) UNSIGNED DEFAULT NULL,
  `origin_city` varchar(25) DEFAULT NULL,
  `views` int(10) UNSIGNED DEFAULT NULL,
  `likes` int(10) UNSIGNED DEFAULT NULL,
  `availability_country` int(10) UNSIGNED DEFAULT NULL COMMENT 'Not all goods will be available for purchase in all countries. Goods should only be visible from places where the seller can actually buy',
  `availability_state` int(10) UNSIGNED DEFAULT NULL,
  `availability_city` varchar(25) DEFAULT NULL,
  `min_quantity` int(10) UNSIGNED DEFAULT NULL,
  `max_quantity` int(10) UNSIGNED DEFAULT NULL,
  `available_quanitity` int(10) UNSIGNED DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the product is a featured product or not',
  `is_verified` tinyint(1) NOT NULL DEFAULT '0',
  `yoamar_shipping_weight` float NOT NULL COMMENT 'The yoamar confirmed shipping weight of the product'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='List of products available';

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `seller_id`, `product_name`, `brand_name`, `description`, `unit_price`, `quantity`, `sale_price`, `sale_price_effective_date`, `total_price`, `is_available`, `availability_date`, `expiration_date`, `product_group`, `product_segment`, `product_family`, `product_class`, `product_type`, `product_sub-type`, `product_state`, `age_group`, `adult`, `color`, `gender`, `material`, `pattern`, `product_size`, `size_type`, `size_system`, `shipping_weight`, `shipping_length`, `shipping_width`, `shipping_height`, `is_bundle`, `energy_efficiency_class`, `energy_efficiency_class_min`, `energy_efficiency_class_max`, `min_handling_time`, `max_handling_time`, `tax`, `tax_category`, `model_number`, `style`, `year_made`, `season`, `origin_country`, `origin_state`, `origin_city`, `views`, `likes`, `availability_country`, `availability_state`, `availability_city`, `min_quantity`, `max_quantity`, `available_quanitity`, `is_active`, `is_featured`, `is_verified`, `yoamar_shipping_weight`) VALUES
('0b_Oy4L0', NULL, 'Wall prints', '', '<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.</p>', 49.99, 5, 0, '0000-00-00', NULL, 1, '0000-00-00', '0000-00-00', 1, 1, 1, 24, 0, 0, '', '', NULL, '', '', '', 'N/A', '', '', '', 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 'enum_value1', NULL, '', 0000, 'autumn', 110, 110, '110', NULL, NULL, 110, 110, '110', 0, 0, NULL, 1, 0, 0, 0),
('4E-1MAfX', NULL, 'Love pendant', '', '<p>Temporibus autem <strong><em>quibusdam et aut officiis</em></strong> debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae <u>non recusandae</u>.</p><p><br></p>', 10.99, 10, 0, '0000-00-00', NULL, 1, '0000-00-00', '0000-00-00', 1, 1, 1, 21, 0, 0, '', '', NULL, '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 'enum_value1', NULL, '', 0000, 'autumn', 110, 110, '110', NULL, NULL, 110, 110, '110', 0, 0, NULL, 1, 0, 0, 0),
('5ryuKb-o', NULL, 'Blue scarf', '', '<p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.</p>', 15, 10, 0, '0000-00-00', NULL, 1, '0000-00-00', '0000-00-00', 1, 2, 1, 16, 0, 0, '', '', NULL, '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 'enum_value1', NULL, '', 0000, 'autumn', 110, 110, '110', NULL, NULL, 110, 110, '110', 0, 0, NULL, 1, 0, 0, 0),
('8hyvBHVF', NULL, 'Mahogany etched king size bed', '', '<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt</p>', 99.99, 10, 0, '0000-00-00', NULL, 1, '0000-00-00', '0000-00-00', 1, 2, 1, 17, 0, 0, '', '', NULL, '', '', '', 'N/A', '', '', '', 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 'enum_value1', NULL, '', 0000, 'autumn', 110, 110, '110', NULL, NULL, 110, 110, '110', 0, 0, NULL, 1, 0, 0, 0),
('8RP6XxnL', NULL, 'Cute framed puppy photograph', '', '<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora <strong>incidunt ut labore</strong> et dolore magnam aliquam quaerat <s>voluptatem</s>.</p>', 34.49, 20, 29.99, '2018-10-23', NULL, 1, '0000-00-00', '0000-00-00', 1, 1, 4, 34, 0, 0, '', '', NULL, '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 'enum_value1', NULL, '', 0000, 'autumn', 110, 110, '110', NULL, NULL, 110, 110, '110', 0, 0, NULL, 1, 0, 0, 0),
('9WlAbPER', NULL, 'Chibi guitar anime sculpture', '', '<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.&nbsp;</p>', 30, 5, 0, '0000-00-00', NULL, 1, '0000-00-00', '0000-00-00', 1, 1, 7, 24, 0, 0, '', '', NULL, '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 'enum_value1', NULL, '', 0000, 'autumn', 110, 110, '110', NULL, NULL, 110, 110, '110', 0, 0, NULL, 1, 0, 0, 0),
('DITwUoUx', NULL, 'Serene Lakeview painting', '', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.&nbsp;</p>', 100, 10, 0, '0000-00-00', NULL, 1, '0000-00-00', '0000-00-00', 1, 1, 3, 28, 0, 0, '', '', NULL, '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 'enum_value1', NULL, '', 0000, 'autumn', 110, 110, '110', NULL, NULL, 110, 110, '110', 0, 0, NULL, 1, 0, 0, 0),
('EYOyC3KJ', NULL, 'Ancient Timepiece', '', '<p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur <s>voluptatem</s>.</p>', 49.99, 2, 830, '2018-10-24', NULL, 1, '0000-00-00', '0000-00-00', 1, 2, 2, 24, 0, 0, '', '', NULL, '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 'enum_value1', NULL, '', 0000, 'autumn', 110, 110, '110', NULL, NULL, 110, 110, '110', 0, 0, NULL, 1, 0, 0, 0),
('iBUYr_GN', NULL, 'Black silk suit (L)', 'Armani', '<p>Temporibus autem <strong><em>quibusdam et aut officiis</em></strong> debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae <u>non recusandae</u>.</p><p><br></p>', 99.99, 10, 0, '0000-00-00', NULL, 1, '0000-00-00', '0000-00-00', 1, 2, 1, 24, 0, 0, '', '', NULL, '', '', 'Silk', 'N/A', '', '', '', 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 'enum_value1', NULL, '', 0000, 'autumn', 110, 110, '110', NULL, NULL, 110, 110, '110', 0, 0, NULL, 1, 0, 0, 0),
('OZWJb7mN', NULL, 'Checked shirt', 'Tommy Hilfiger', '<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.</p>', 39.49, 15, 0, '0000-00-00', NULL, 1, '0000-00-00', '0000-00-00', 1, 2, 1, 24, 0, 0, '', '', NULL, '', '', '', 'N/A', '', '', '', 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 'enum_value1', NULL, '', 0000, 'autumn', 110, 110, '110', NULL, NULL, 110, 110, '110', 0, 0, NULL, 1, 0, 0, 0),
('PGKEZT5k', NULL, 'Abstract Picasso-esque painting', '', '<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>', 929.5, 2, 0, '0000-00-00', NULL, 1, '0000-00-00', '0000-00-00', 1, 1, 3, 28, 0, 0, '', '', NULL, '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 'enum_value1', NULL, '', 0000, 'autumn', 110, 110, '110', NULL, NULL, 110, 110, '110', 0, 0, NULL, 1, 0, 0, 0),
('wwhpC2jT', NULL, 'Bat Hangings', '', '<p>Soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.</p>', 4.99, 5, 4.49, '2018-10-31', NULL, 1, '0000-00-00', '0000-00-00', 1, 2, 3, 24, 0, 0, '', '', NULL, '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 'enum_value1', NULL, '', 0000, 'autumn', 110, 110, '110', NULL, NULL, 110, 110, '110', 0, 0, NULL, 1, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_group` (`product_group`),
  ADD KEY `product_segment` (`product_segment`),
  ADD KEY `product_family` (`product_family`),
  ADD KEY `product_class` (`product_class`),
  ADD KEY `product_type` (`product_type`),
  ADD KEY `product_sub-type` (`product_sub-type`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_products_categories` FOREIGN KEY (`product_segment`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_products_category_groups` FOREIGN KEY (`product_group`) REFERENCES `category_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_products_subcategories` FOREIGN KEY (`product_family`) REFERENCES `subcategories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
