-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 29, 2019 at 06:41 PM
-- Server version: 5.7.21
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yoamar`
--

-- --------------------------------------------------------

--
-- Table structure for table `payment_info`
--

DROP TABLE IF EXISTS `payment_info`;
CREATE TABLE IF NOT EXISTS `payment_info` (
  `id` varchar(10) NOT NULL,
  `type` enum('bank','card','mobile','online') NOT NULL DEFAULT 'card' COMMENT 'Mobile, credit card, online',
  `paypal_id` varchar(45) DEFAULT NULL COMMENT 'payerID. id the transaction was made with in paypal api',
  `card_number` varchar(15) DEFAULT NULL COMMENT 'Credit card number associated with this payment info',
  `card_expiry` date DEFAULT NULL COMMENT 'Date the card expires',
  `card_security_code` varchar(5) DEFAULT NULL COMMENT 'The extra digits on cards 3-5 chars - used for authentication',
  `mpesa_phone` varchar(13) DEFAULT NULL COMMENT 'The phone number used for mpesa by this user',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the user the payment information belongs to',
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The date/time the payment information was updated',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Stores user payment information';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
