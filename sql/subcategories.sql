-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2019 at 03:18 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yoamar`
--

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(50) DEFAULT NULL COMMENT 'Name of the subcategory',
  `category_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'The id of the category the subcategory belongs to (autofilled to parent subcategory if the level >1)',
  `level` tinyint(4) DEFAULT NULL COMMENT 'Level 1 means this is a direct child of a category, level 2 means a child of a level 1 subcategory, 3 means child of level 2 etc. This recursive approach allows us to add/remove levels of subcategories without creating new tables for each',
  `description` text COMMENT 'Description of the sub-category',
  `img_link` varchar(256) DEFAULT NULL COMMENT 'Link to the subcategory''s image'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Product subcategories';

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `title`, `category_id`, `level`, `description`, `img_link`) VALUES
(1, 'Prints', 1, 1, NULL, 'https://images.unsplash.com/photo-1500919037572-ade5a9cd950c?auto=format&fit=crop&w=360'),
(2, 'Collectibles', 1, 1, NULL, 'https://images.unsplash.com/photo-1519635451045-a41d4361d495?auto=format&fit=crop&w=360'),
(3, 'Painting', 1, 1, NULL, 'https://images.unsplash.com/photo-1520420097861-e4959843b682?auto=format&fit=crop&w=360'),
(4, 'Photography', 1, 1, NULL, 'https://images.unsplash.com/photo-1527888477750-6fd34fbfa3f0?auto=format&fit=crop&w=360'),
(5, 'Drawing & Illustration', 1, 1, NULL, 'https://images.unsplash.com/photo-1528742945979-a6c6fe93a22e?auto=format&fit=crop&w=360'),
(6, 'Dolls & Miniatures', 1, 1, NULL, 'https://images.unsplash.com/photo-1505350480831-d146ef77b091?auto=format&fit=crop&w=360'),
(7, 'Sculpture', 1, 1, NULL, 'https://images.unsplash.com/photo-1439434768192-c60615c1b3c8?auto=format&fit=crop&w=360'),
(8, 'Mixed Media & Collage', 1, 1, NULL, 'https://images.unsplash.com/photo-1534593972954-9d78964f9663?auto=format&fit=crop&w=360'),
(9, 'Fiber Arts', 1, 1, NULL, 'https://images.unsplash.com/photo-1490030327837-eab3c250898d?auto=format&fit=crop&w=360'),
(10, 'Clip Art', 1, 1, NULL, 'https://images.unsplash.com/photo-1502951628384-d8c1d621df39?auto=format&fit=crop&w=360'),
(11, 'Fine Art Ceramics', 1, 1, NULL, 'https://images.unsplash.com/photo-1530006498959-b7884e829a04?auto=format&fit=crop&w=360'),
(12, 'Glass Art', 1, 1, NULL, 'https://images.unsplash.com/photo-1504608611486-8af05855b251?auto=format&fit=crop&w=360'),
(13, 'Glass Sculptures & Figurines', 1, 1, NULL, 'https://images.unsplash.com/photo-1466188635785-8b5f35009981?auto=format&fit=crop&w=360'),
(14, 'Artist Trading Cards', 1, 1, NULL, 'https://images.unsplash.com/photo-1532152934380-321e9a99fe20?auto=format&fit=crop&w=360'),
(15, 'Pet Portraits', 1, 1, NULL, 'https://images.unsplash.com/photo-1532386236358-a33d8a9434e3?auto=format&fit=crop&w=360'),
(16, 'Textiles Crafts', 2, 1, NULL, 'https://images.unsplash.com/photo-1524404794194-16bae22718c0?auto=format&fit=crop&w=360'),
(17, 'Woodcraft', 2, 1, NULL, 'https://images.unsplash.com/photo-1536581288774-aac05797e2f2?auto=format&fit=crop&w=333'),
(18, 'Papercraft', 2, 1, NULL, 'https://images.unsplash.com/photo-1526031013000-39374cbbf2c3?auto=format&fit=crop&w=360'),
(19, 'Pottery Crafts', 2, 1, NULL, 'https://images.unsplash.com/photo-1528789386055-75c4b717bad1?auto=format&fit=crop&w=360'),
(20, 'Pottery and Glass Crafts', 2, 1, NULL, 'https://images.pexels.com/photos/220987/pexels-photo-220987.jpeg?auto=compress&w=360'),
(21, 'Jewellery', 2, 1, NULL, 'https://images.unsplash.com/photo-1523252012848-c22188792c27?auto=format&fit=crop&w=360'),
(22, 'Woven Baskets', 2, 1, NULL, 'https://images.unsplash.com/photo-1462726337252-4773ebc370ae?auto=format&fit=crop&w=360'),
(23, 'Leather Crafted', 2, 1, NULL, 'https://images.unsplash.com/photo-1499985217418-1f2c55099b4f?w=360'),
(24, 'Digital Prints', 1, 2, NULL, NULL),
(25, 'Giclée', 1, 2, NULL, NULL),
(26, 'Lithographs', 1, 2, NULL, NULL),
(27, 'Etchings & Engravings', 1, 2, NULL, NULL),
(28, 'Music & Movie Posters', 1, 2, NULL, NULL),
(29, 'Wood & Linocut Prints', 1, 2, NULL, NULL),
(30, 'Screenprints', 1, 2, NULL, NULL),
(31, 'Letterpress Prints', 1, 2, NULL, NULL),
(32, 'Monotypes', 1, 2, NULL, NULL),
(33, 'Figurines & Knick Knacks', 2, 2, NULL, NULL),
(34, 'Memorabilia', 2, 2, NULL, NULL),
(35, 'Advertisements', 2, 2, NULL, NULL),
(36, 'Collectible Glass', 2, 2, NULL, NULL),
(37, 'Coins & Money', 2, 2, NULL, NULL),
(38, 'Collectible Plates', 2, 2, NULL, NULL),
(39, 'Tobacciana', 2, 2, NULL, NULL),
(40, 'Postage Stamps', 2, 2, NULL, NULL),
(41, 'Displays', 2, 2, NULL, NULL),
(42, 'Paperweights', 2, 2, NULL, NULL),
(43, 'Music Boxes', 2, 2, NULL, NULL),
(44, 'Acrylic', 3, 2, NULL, NULL),
(45, 'Watercolor', 3, 2, NULL, NULL),
(46, 'Oil', 3, 2, NULL, NULL),
(47, 'Mixed', 3, 2, NULL, NULL),
(48, 'Combination', 3, 2, NULL, NULL),
(49, 'Ink', 3, 2, NULL, NULL),
(50, 'Gouache', 3, 2, NULL, NULL),
(51, 'Spray Paint', 3, 2, NULL, NULL),
(52, 'Encaustics', 3, 2, NULL, NULL),
(53, 'Color', 4, 2, NULL, NULL),
(54, 'Black & White', 4, 2, NULL, NULL),
(55, 'Sepia', 4, 2, NULL, NULL),
(56, 'Digital', 5, 2, NULL, NULL),
(57, 'Pen & Ink', 5, 2, NULL, NULL),
(58, 'Pencil', 5, 2, NULL, NULL),
(59, 'Marker', 5, 2, NULL, NULL),
(60, 'Pastel', 5, 2, NULL, NULL),
(61, 'Charcoal', 5, 2, NULL, NULL),
(62, 'Miniatures', 6, 2, NULL, NULL),
(63, 'Art Dolls', 6, 2, NULL, NULL),
(64, 'Dollhouses', 6, 2, NULL, NULL),
(65, 'Dioramas', 6, 2, NULL, NULL),
(66, 'Art Objects', 7, 2, NULL, NULL),
(67, 'Figurines', 7, 2, NULL, NULL),
(68, 'Vessels', 7, 2, NULL, NULL),
(69, 'Other Assemblage', 8, 2, NULL, NULL),
(70, 'Paper', 8, 2, NULL, NULL),
(71, 'Paint & Canvas', 8, 2, NULL, NULL),
(72, 'Mosaic', 8, 2, NULL, NULL),
(73, 'Embroidery', 9, 2, NULL, NULL),
(74, 'Felting', 9, 2, NULL, NULL),
(75, 'Suncatchers', 13, 2, NULL, NULL),
(76, 'Panels & Wall Hangings', 13, 2, NULL, NULL),
(77, 'Mosaics', 13, 2, NULL, NULL),
(78, 'Appliqué', 16, 2, NULL, NULL),
(79, 'Crocheting', 16, 2, NULL, NULL),
(80, 'Embroidery', 16, 2, NULL, NULL),
(81, 'Felt making', 16, 2, NULL, NULL),
(82, 'Knitting', 16, 2, NULL, NULL),
(83, 'Lace making', 16, 2, NULL, NULL),
(84, 'Macramé', 16, 2, NULL, NULL),
(85, 'Quilting', 16, 2, NULL, NULL),
(86, 'Tapestry art', 16, 2, NULL, NULL),
(87, 'Weaving', 16, 2, NULL, NULL),
(88, 'Wood carving', 17, 2, NULL, NULL),
(89, 'Wood turning', 17, 2, NULL, NULL),
(90, 'Cabinet making', 17, 2, NULL, NULL),
(91, 'Furniture making', 17, 2, NULL, NULL),
(92, 'Lacquerware', 17, 2, NULL, NULL),
(93, 'Paper Modelling', 18, 2, NULL, NULL),
(94, 'Collage', 18, 2, NULL, NULL),
(95, 'Decoupage', 18, 2, NULL, NULL),
(96, 'Origami paper folding', 18, 2, NULL, NULL),
(97, 'Papier mâché', 18, 2, NULL, NULL),
(98, 'Ceramics', 19, 2, NULL, NULL),
(99, 'Earthenware', 98, 3, NULL, NULL),
(100, 'Stoneware', 98, 3, NULL, NULL),
(101, 'Porcelain', 98, 3, NULL, NULL),
(102, 'Mosaic Art', 19, 2, NULL, NULL),
(103, 'Glass Beadmaking', 20, 2, NULL, NULL),
(104, 'Glass Blowing', 20, 2, NULL, NULL),
(105, 'Glass Etching', 20, 2, NULL, NULL),
(106, 'Mosaic Art', 20, 2, NULL, NULL),
(107, 'Metal', 21, 2, NULL, NULL),
(108, 'Embossed', 107, 3, NULL, NULL),
(109, 'Repoussé work', 107, 3, NULL, NULL),
(110, 'Engraved', 107, 3, NULL, NULL),
(111, 'Enamelled', 107, 3, NULL, NULL),
(112, 'Granulated', 107, 3, NULL, NULL),
(113, 'Filigree decoration', 107, 3, NULL, NULL),
(114, 'Bead Jewellery', 21, 2, NULL, NULL),
(115, 'Glass Jewellery', 21, 2, NULL, NULL),
(116, 'Ceramic Jewellery', 21, NULL, NULL, NULL),
(117, 'Stone Jewellery', 21, NULL, NULL, NULL),
(118, 'Wooden Jewellery', 21, 2, NULL, NULL),
(119, 'Leather Jewellery', 21, 2, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
