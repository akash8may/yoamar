-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2018 at 06:30 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yoamar`
--

--
-- Truncate table before insert `product_images`
--

TRUNCATE TABLE `product_images`;
--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `img_link`, `thumbnail_link`, `alt_text`, `position`) VALUES
(1, 'DITwUoUx', 'https://images.pexels.com/photos/356860/pexels-photo-356860.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940', 'https://images.pexels.com/photos/356860/pexels-photo-356860.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940', 'Serene Lakeview painting', 1),
(2, '5ryuKb-o', 'https://images.pexels.com/photos/1082955/pexels-photo-1082955.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940', 'https://images.pexels.com/photos/1082955/pexels-photo-1082955.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940', NULL, 1),
(3, '8RP6XxnL', 'https://images.pexels.com/photos/940797/pexels-photo-940797.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940', 'https://images.pexels.com/photos/940797/pexels-photo-940797.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940', 'Cute framed puppy photograph', 1),
(4, '9WlAbPER', 'https://images.pexels.com/photos/164839/pexels-photo-164839.jpeg?cs=srgb&dl=amplifier-anime-blur-164839.jpg&fm=jpg', 'https://images.pexels.com/photos/164839/pexels-photo-164839.jpeg?cs=srgb&dl=amplifier-anime-blur-164839.jpg&fm=jpg', 'Chibi guitar anime sculpture', 1),
(5, 'PGKEZT5k', 'https://images.pexels.com/photos/310436/pexels-photo-310436.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940', 'https://images.pexels.com/photos/310436/pexels-photo-310436.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940', 'This painting is a picasso style painting from the mid 1940s done by none other than the great Picassoratti', 1),
(6, 'EYOyC3KJ', 'https://images.pexels.com/photos/859895/pexels-photo-859895.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940', 'https://images.pexels.com/photos/859895/pexels-photo-859895.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940', 'Copper Ancient Timepiece', 1),
(7, 'EYOyC3KJ', 'https://images.pexels.com/photos/678248/pexels-photo-678248.png?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940', 'https://images.pexels.com/photos/678248/pexels-photo-678248.png?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940', 'Ancient Timepiece Jewellery', 1),
(8, '8hyvBHVF', 'https://images.unsplash.com/photo-1523246031546-a1346b110294?auto=format&fit=crop&w=750', 'https://images.unsplash.com/photo-1523246031546-a1346b110294?auto=format&fit=crop&w=360', 'Mahogany etched king size bed', 1),
(9, 'iBUYr_GN', 'https://images.unsplash.com/photo-1523504754200-45cd634649d7?auto=format&fit=crop&w=720', 'https://images.unsplash.com/photo-1523504754200-45cd634649d7?auto=format&fit=crop&w=360', NULL, 1),
(10, 'OZWJb7mN', 'https://images.pexels.com/photos/769747/pexels-photo-769747.jpeg?auto=compress&cs=tinysrgb&h=650&w=940', 'https://images.pexels.com/photos/769747/pexels-photo-769747.jpeg?auto=compress&cs=tinysrgb&h=650&w=940', 'Checked shirt', 1),
(11, 'OZWJb7mN', 'https://images.pexels.com/photos/356252/pexels-photo-356252.jpeg?auto=compress&cs=tinysrgb&h=650&w=940', 'https://images.pexels.com/photos/356252/pexels-photo-356252.jpeg?auto=compress&cs=tinysrgb&h=650&w=940', NULL, 1),
(12, 'OZWJb7mN', 'https://images.pexels.com/photos/769729/pexels-photo-769729.jpeg?auto=compress&cs=tinysrgb&h=650&w=940', 'https://images.pexels.com/photos/769729/pexels-photo-769729.jpeg?auto=compress&cs=tinysrgb&h=650&w=940', NULL, 1),
(13, '4E-1MAfX', 'https://images.unsplash.com/photo-1486821416551-68a65ef4d618?auto=format&fit=crop&w=750', 'https://images.unsplash.com/photo-1486821416551-68a65ef4d618?auto=format&fit=crop&w=360', NULL, 1),
(14, '0b_Oy4L0', 'https://images.unsplash.com/photo-1521264811721-1e0c116e5b5a?auto=format&fit=crop&w=751', 'https://images.unsplash.com/photo-1521264811721-1e0c116e5b5a?auto=format&fit=crop&w=360', NULL, 1),
(15, 'wwhpC2jT', 'https://images.unsplash.com/photo-1528041251223-bd0f58bb7b94?&auto=format&fit=crop&w=752', 'https://images.unsplash.com/photo-1528041251223-bd0f58bb7b94?&auto=format&fit=crop&w=752', NULL, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
