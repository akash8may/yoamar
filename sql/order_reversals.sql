-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 29, 2019 at 08:54 PM
-- Server version: 5.7.21
-- PHP Version: 7.0.29

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yoamar`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_reversals`
--

DROP TABLE IF EXISTS `order_reversals`;
CREATE TABLE IF NOT EXISTS `order_reversals` (
  `id` int(10) UNSIGNED NOT NULL,
  `transaction_id` varchar(10) NOT NULL COMMENT 'The unique id of the order that is being reversed',
  `item_condition` enum('good','bad','damaged','unchecked') NOT NULL DEFAULT 'unchecked' COMMENT 'Good, bad, etc.',
  `charges` int(10) UNSIGNED DEFAULT NULL COMMENT 'How much the buyer was charged for the reversal',
  `charges_breakdown` text COMMENT 'A breakdown of the charges. Incase the customer asks what the charges are for. This will be generated at application level',
  `customer_confirmed` tinyint(1) DEFAULT NULL COMMENT 'Customer confirms that they have received the item',
  `courier_received` tinyint(1) DEFAULT NULL COMMENT 'Courier has confirmed that they have received the item',
  `logistics_received` tinyint(1) DEFAULT NULL COMMENT 'Logistics have confirmed that they received the item',
  `reversal_reason` text COMMENT 'The reason for the reversal',
  `date_reversed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The date & time the reversal happened',
  `quantity` smallint(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Order reversals';
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
