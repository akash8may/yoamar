-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 29, 2019 at 08:54 PM
-- Server version: 5.7.21
-- PHP Version: 7.0.29

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yoamar`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `transaction_id` varchar(10) NOT NULL COMMENT 'Each order item will have a unique transaction id. This is the primary key',
  `order_id` varchar(10) DEFAULT NULL COMMENT 'Multiple order items can have the same id.',
  `product_id` varchar(10) DEFAULT NULL COMMENT 'The id of the product that is contained in the order',
  `quantity` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Number of the product specified above to be included in the order',
  `sales_tax1` float UNSIGNED DEFAULT NULL COMMENT 'Various Taxes as applicable',
  `sales_tax2` float UNSIGNED DEFAULT NULL COMMENT 'Various Taxes as applicable',
  `sales_tax3` float UNSIGNED DEFAULT NULL COMMENT 'Various Taxes as applicable',
  `sales_amount` float UNSIGNED DEFAULT NULL COMMENT 'Pure product Sales',
  `order_total` float UNSIGNED DEFAULT '0' COMMENT 'Total paid by buyer - included sales, tax, shipping, etc',
  `paid` tinyint(1) DEFAULT NULL COMMENT 'Has this order been paid for',
  `coupon_name` varchar(20) DEFAULT NULL COMMENT 'Coupon applied to this order. Discount calculated at application level & not in database',
  `buyer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id of the buyer making the order',
  `status` enum('processing','in_transit','delivered') DEFAULT 'processing' COMMENT 'Processing, in transit, delivered',
  `shipping_method` enum('ship_shipping_cost','air_shipping_cost','ems_shipping_cost') DEFAULT 'ship_shipping_cost' COMMENT 'Freight, Air, EMS',
  `shipping_charges` int(10) UNSIGNED DEFAULT NULL COMMENT 'Shipping charges incurred by the order',
  `shipping_date` date DEFAULT NULL COMMENT 'The date the item is expected to arrive',
  `import_fees_deposit` int(10) UNSIGNED DEFAULT NULL COMMENT 'Import fees incurred by the order',
  `required_date` date DEFAULT NULL COMMENT 'Date the customer/buyer wants the item by. This will be useful if in future we charge differently based on how soon the buyer wants the product',
  `delivery_address` varchar(60) DEFAULT NULL COMMENT 'Will be the buyer''s specified delivery address. Automatically added to buyer addresses in buyer account',
  `return_address` varchar(60) DEFAULT NULL COMMENT 'If the product could not be delivered, return it to this address',
  `return_instruction` enum('enum_value1','enum_value2') DEFAULT NULL COMMENT 'RTS, Abandon, Destroy. Default is RTS. This should not change in the near future but is a useful property to have as far as couriers and specifically POSTA is concerned',
  `tracking_id` varchar(10) DEFAULT NULL COMMENT 'Tracking id for the order. Generated by POSTA',
  `tracking_link` varchar(128) DEFAULT NULL COMMENT 'Link to view the order being tracked',
  `date_ordered` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Date & time the order was made',
  `date_delivered` datetime DEFAULT NULL,
  `seller_id` int(10) UNSIGNED DEFAULT NULL,
  `is_cancelled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Orders made by buyers are stored here';
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
